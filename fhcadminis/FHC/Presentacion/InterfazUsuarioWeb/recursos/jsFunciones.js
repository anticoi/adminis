﻿function Valida_Rut(rut) {
    rut = rut.replace('-', '').replace('.', '');
    if (rut.length > 0) {
        var tmpstr = "";
        var intlargo = rut
        if (intlargo.length > 0) {
            crut = rut
            largo = crut.length;
            if (largo < 2) {
                return false;
            }
            for (i = 0; i < crut.length; i++)
                if (crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-') {
                    tmpstr = tmpstr + crut.charAt(i);
                }
            rut = tmpstr;
            crut = tmpstr;
            largo = crut.length;

            if (largo > 2)
                rut = crut.substring(0, largo - 1);
            else
                rut = crut.charAt(0);

            dv = crut.charAt(largo - 1);

            if (rut == null || dv == null)
                return 0;

            var dvr = '0';
            suma = 0;
            mul = 2;

            for (i = rut.length - 1; i >= 0; i--) {
                suma = suma + rut.charAt(i) * mul;
                if (mul == 7)
                    mul = 2;
                else
                    mul++;
            }

            res = suma % 11;
            if (res == 1)
                dvr = 'K';
            else if (res == 0)
                dvr = '0';
            else {
                dvi = 11 - res;
                dvr = dvi + "";
            }

            if (dvr.toLowerCase() != dv.toLowerCase()) {
                return false;
            }
            return true;
        }
    }
    return true;
}



function ChangeAllChecks(gridViewName, newState) {
    var tabla = document.getElementById(gridViewName);
    celdas = tabla.cells;
    for (i = 0; i < celdas.length - 1; i++) {
        if (celdas[i].children.length > 0)
        {
            if (celdas[i].children[0].type == "checkbox"  && celdas[i].children[0].checked != newState) {
                celdas[i].children[0].click();
            }
        }
    }
}

function Resaltar_On(GridView) {
    if (GridView != null) {
        GridView.originalBgColor = GridView.style.backgroundColor;
        GridView.style.backgroundColor = "#66A8FF";
	GridView.style.color = "#383838";
    }
}

function Resaltar_Off(GridView) {
    if (GridView != null) {
        GridView.style.backgroundColor = GridView.originalBgColor;
    }
}

function valida(obj) {
    for (i = 0; i < document.form1.elements.length; i++) {
        if (document.form1.elements[i].type == "checkbox") {
            if (obj != document.form1.elements[i].name) {
                document.form1.elements[i].checked = 0;
            }
        }
    }
}



function checkRutField(source, arguments) {
    var rut = arguments.Value.replace('.', '').replace('-', '');
    var tmpstr = "";
    for (i = 0; i < rut.length; i++)
        if (rut.charAt(i) != ' ' && rut.charAt(i) != '.' && rut.charAt(i) != '-')
        tmpstr = tmpstr + rut.charAt(i);
    rut = tmpstr;
    largo = rut.length;
    // [VARM+]
    tmpstr = "";
    for (i = 0; rut.charAt(i) == '0'; i++);
    for (; i < rut.length; i++)
        tmpstr = tmpstr + rut.charAt(i);
    rut = tmpstr;
    largo = rut.length;
    // [VARM-]
    if (largo < 2) {
        //alert("Debe ingresar el rut completo.");
        //document.frm.rut_aux.focus();
        //document.frm.rut_aux.select();
        arguments.IsValid = false;
        return;
    }
    for (i = 0; i < largo; i++) {
        if (rut.charAt(i) != "0" && rut.charAt(i) != "1" && rut.charAt(i) != "2" && rut.charAt(i) != "3" && rut.charAt(i) != "4" && rut.charAt(i) != "5" && rut.charAt(i) != "6" && rut.charAt(i) != "7" && rut.charAt(i) != "8" && rut.charAt(i) != "9" && rut.charAt(i) != "k" && rut.charAt(i) != "K") {
            //alert("El valor ingresado no corresponde a un R.U.T valido.");
            //document.frm.rut_aux.focus();
            //document.frm.rut_aux.select();
            arguments.IsValid = false;
            return;
        }
    }
    var invertido = "";
    for (i = (largo - 1), j = 0; i >= 0; i--, j++)
        invertido = invertido + rut.charAt(i);
    var drut = "";
    drut = drut + invertido.charAt(0);
    drut = drut + '-';
    cnt = 0;
    for (i = 1, j = 2; i < largo; i++, j++) {
        if (cnt == 3) {
            drut = drut + '.';
            j++;
            drut = drut + invertido.charAt(i);
            cnt = 1;
        }
        else {
            drut = drut + invertido.charAt(i);
            cnt++;
        }
    }
    invertido = "";
    for (i = (drut.length - 1), j = 0; i >= 0; i--, j++)
        invertido = invertido + drut.charAt(i);
    //document.frm.rut_aux.value = invertido;
    if (checkDV(rut)) {
        arguments.IsValid = true;
        return;
    }
    arguments.IsValid = false;
}

function checkDV(crut) {
    largo = crut.length;
    if (largo < 2) {
        //alert("Debe ingresar el rut completo.");
        //document.frm.rut_aux.focus();
        //document.frm.rut_aux.select();
        return false;
    }
    if (largo > 2)
        rut = crut.substring(0, largo - 1);
    else
        rut = crut.charAt(0);
    dv = crut.charAt(largo - 1);
    checkCDV(dv);
    if (rut == null || dv == null)
        return 0;
    var dvr = '0';
    suma = 0;
    mul = 2;
    for (i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul;
        if (mul == 7)
            mul = 2;
        else
            mul++;
    }
    res = suma % 11;
    if (res == 1)
        dvr = 'k';
    else if (res == 0)
        dvr = '0';
    else {
        dvi = 11 - res;
        dvr = dvi + "";
    }
    if (dvr != dv.toLowerCase()) {
        //alert("EL rut es incorrecto.");
        //document.frm.rut_aux.focus();
        //document.frm.rut_aux.value = "";
        return false;
    }
    return true;
}
function checkCDV(dvr) {
    dv = dvr + "";
    if (dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k' && dv != 'K') {
        //alert("Debe ingresar un digito verificador valido.");
        //document.frm.rut_aux.focus();
        //document.frm.rut_aux.select();
        return false;
    }
    return true;
}

function user_online(GridView) {
    if (GridView != null) {
        GridView.originalBgColor = GridView.style.backgroundColor;
        GridView.style.backgroundColor = "#9EFFB3";
    }
}

function user_offline(GridView) {
    if (GridView != null) {
        GridView.originalBgColor = GridView.style.backgroundColor;
        GridView.style.backgroundColor = "#FF5A3D";
    }
}

// Javascript code copyright 2009 by Fiach Reid : www.webtropy.com
// This code may be used freely, as long as this copyright notice is intact.
function CalcularDigitoImei(imei) {
    var sum = 0;
    for (i = 0; i < imei.length; i++) {
        sum += parseInt(imei.substring(i, i + 1));
    }
    var delta = new Array(0, 1, 2, 3, 4, -4, -3, -2, -1, 0);
    for (i = imei.length - 1; i >= 0; i -= 2) {
        var deltaIndex = parseInt(imei.substring(i, i + 1));
        var deltaValue = delta[deltaIndex];
        sum += deltaValue;
    }
    var mod10 = sum % 10;
    mod10 = 10 - mod10;
    if (mod10 == 10) {
        mod10 = 0;
    }
    return mod10;
}

function ValidarImei(imei) {
    var LuhnDigit = parseInt(imei.substring(imei.length - 1, imei.length));
    var LuhnLess = imei.substring(0, imei.length - 1);
    if (CalcularDigitoImei(LuhnLess) == parseInt(LuhnDigit)) {
        return true;
    }
    return false;
}