USE [BDUsuario]
GO
/****** Object:  StoredProcedure [dbo].[usp_actualizaConexionSesion]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_actualizaConexionSesion]
@usuario varchar(50),
@sessionId varchar(50)
as
	update Conexion set fechaFinSession = GETDATE() where usuario = @usuario and sessionId = @sessionId and autenticacion = 'AUTENTICADO'


GO
/****** Object:  StoredProcedure [dbo].[usp_actualizaEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[usp_actualizaEmpresa]
@rut varchar(50)
,@giro varchar(50)
,@fonoContacto varchar(50)
as
	update Empresa set giro = @giro, fono = @fonoContacto
	where rut = @rut


GO
/****** Object:  StoredProcedure [dbo].[usp_ActualizaMostrarMensaje]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE procedure [dbo].[usp_ActualizaMostrarMensaje]
 @idUsuario int,
 @mostrar bit
 as
	update MensajeUsuario set mostrar = @mostrar
	where idUsuario = @idUsuario
 

GO
/****** Object:  StoredProcedure [dbo].[usp_actualizaUsuarioConectado]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_actualizaUsuarioConectado]  
@idSistema int,  
@idUsuario int,  
@fechaConeccion datetime = null,
@conectado bit 
as  
	declare @fecha datetime
	
	select @fecha = fechaConeccion 
	from UsuarioSistema 
	where idUsuario = @idUsuario and idSistema = @idSistema
	
	if @fechaConeccion is null
	begin
		update UsuarioSistema 
		set conectado = @conectado
		where idUsuario = @idUsuario and idSistema = @idSistema
	end
	else
	begin
		update UsuarioSistema 
		Set fechaConeccion = @fechaConeccion, conectado = @conectado
		where idUsuario = @idUsuario and idSistema = @idSistema
	end

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaDatosUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[usp_buscaDatosUsuario]

@login varchar(50)    

as    

 select usuario.idUsuario, login, nombre, email, telefono, password='' , Area.AreaId, Area.AreaNombre
 from usuario  
 inner join Area on
Usuario.areaid = Area.AreaId

 where login = @login  



GO
/****** Object:  StoredProcedure [dbo].[usp_buscaDescripcionMantenedor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_buscaDescripcionMantenedor]
@descripcion varchar(50)
,@tabla varchar(50)
as
	declare @consulta varchar(2000)

	set @consulta = 'select id' + @tabla + ', nombre from ' + @tabla + ' into #tempMan where nombre like ''%' + @descripcion + '%'''
	execute (@consulta)
	execute ('select * from #tempMan')
	--print (@consulta)
	

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaEmpresaRut]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_buscaEmpresaRut]
@rutEmpresa varchar(60)
as
	select idEmpresa, rut, nombre, direccion, ciudad, codigoSucursal, rutaLogo, giro,fono 
	from Empresa
	where rut = @rutEmpresa


GO
/****** Object:  StoredProcedure [dbo].[usp_buscaImpresoraSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_buscaImpresoraSistema]    
@idSistema int,  
@nombreEquipoLocal varchar(50),
@referencia varchar(50) = null    
as    
 if @referencia = ''
	set @referencia = null
	
 select nombreEquipoLocal, nombreImpresora  , idSistema  , referencia
 from ImpresoraSistema    
 where nombreEquipoLocal = @nombreEquipoLocal and idSistema = @idSistema  
 and (referencia = @referencia or @referencia is null)
 
 

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaMarca]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_buscaMarca]
@nombre varchar(50)
as
	declare @nombreParecido varchar(1000)
	
	select idMarca, nombre into #temp from Marca where nombre like '%' + @nombre + '%'
	
	
	

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaMensajeUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  

CREATE procedure [dbo].[usp_buscaMensajeUsuario]
@idSistema int,
@idUsuario int
as
select mensaje.idMensaje, mensaje, habilitado
from mensaje inner join mensajeUsuario on mensaje.idMensaje = mensajeUsuario.idMensaje
where mensaje.habilitado = 1 and mensajeusuario.mostrar = 1
and mensajeusuario.idUsuario = @idusuario and mensajeUsuario.idSistema = @idSistema


GO
/****** Object:  StoredProcedure [dbo].[usp_buscaMenuHijo]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_buscaMenuHijo]
@idSistema int,
@idPadre int
as
	/*Si el padre es 0 esta consultando por el menu raiz*/
	if @idPadre = 0
		set @idPadre = null
		
	select idSistema, menuId, Descripcion, padreId, posicion
			, icono, habilitado, ruta, pagina, parametros
	into #temp
	from Menu
	where idSistema = @idSistema and padreId = isnull(@idPadre, menuId)
	
	
	if @idPadre > 0
		delete from #temp where padreId = MenuId
	
	select idSistema, menuId, Descripcion, padreId, posicion
			, icono, habilitado, ruta, pagina, parametros
	from #temp
	order by posicion
	


GO
/****** Object:  StoredProcedure [dbo].[usp_BuscaPermisoPorSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Proc [dbo].[usp_BuscaPermisoPorSistema]  
(  
 @nombreElemento varchar(100) = '',  
 @idSistema int = 0  
)  
As  
Begin  
  
 select   
 distinct
  Permiso.idPermiso,  
  Permiso.idSistema,  
  '' as Sistema, --Sistema.nombre as Sistema,  
  Permiso.tipoPermiso,  
  Permiso.nombreElemento  
 from Permiso --inner join Sistema on Permiso.idSistema = Sistema.idSistema  
 where   
  (Permiso.nombreElemento like '%' + @nombreElemento + '%' or @nombreElemento = '') And  
  (Permiso.idSistema = @idSistema or @idSistema = 0)  
 Order by     
  Permiso.nombreElemento  
     
End  

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaPermisosUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_buscaPermisosUsuario]    
@login varchar(50)    
,@idSistema int    
as    
 select distinct nombreElemento, tipoPermiso  
 from Usuario U inner join UsuarioRol UR on U.idUsuario = UR.idUsuario  
 inner join Rol R on UR.idRol = R.idRol   
 inner join RolPermiso RP on R.idRol = RP.idRol  
 inner join Permiso P on RP.idPermiso = P.idPermiso and P.idSistema = R.idSistema  
 where U.login = @login and R.idSistema = @idSistema  
 

GO
/****** Object:  StoredProcedure [dbo].[usp_BuscaRolPermisosPorUsuarioyPorSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[usp_BuscaRolPermisosPorUsuarioyPorSistema]
(
	@idSistema int, @idUsuario int , @idRol int , @idPermiso int 
)
As
Begin

	select  
		Usuario.login,
		Permiso.nombreElemento,
		Permiso.tipoPermiso,
		Rol.descripcion as Rol,
		RolPermiso.idPermiso,
		RolPermiso.idRol
	from 
	UsuarioRol with(nolock) 
	inner join Usuario with(nolock) on UsuarioRol.idusuario = Usuario.idUsuario
	inner join Rol with(nolock) on UsuarioRol.idRol = Rol.idRol
	inner join RolPermiso with(nolock) on Rol.idRol = RolPermiso.idRol
	inner join Permiso with(nolock) on RolPermiso.idPermiso = Permiso.idPermiso
	where 
	Permiso.idSistema = @idSistema and Rol.idSistema = @idSistema and
	(Usuario.idUsuario = @idUsuario or @idUsuario is null) and
	(Rol.idRol = @idRol or @idRol is null) and
	(Permiso.idPermiso = @idPermiso or @idPermiso is null)
	order by login

End

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaRolPorUsuarioySistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  


CREATE PROCEDURE [dbo].[usp_buscaRolPorUsuarioySistema]   
 @login VARCHAR(50),
 @IdSistema INT = -1
AS  
BEGIN  
  
 SET NOCOUNT ON;  
  
    select usuarioRol.idRol, rol.descripcion  
 from usuario inner join usuarioRol on usuario.idUsuario = usuarioRol.idUsuario  
 inner join Rol on usuarioRol.idRol = Rol.idRol  
 inner join UsuarioSistema on Usuario.idUsuario = UsuarioSistema.idUsuario
 where usuario.login =  @login  and 
 (UsuarioSistema.idSistema = @IdSistema or  @IdSistema = -1)
	
   
END  
  

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaRolUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[usp_buscaRolUsuario] 
	@login VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;

    select usuarioRol.idRol, rol.descripcion
	from usuario inner join usuarioRol on usuario.idUsuario = usuarioRol.idUsuario
	inner join Rol on usuarioRol.idRol = Rol.idRol
	where usuario.login =  @login
	
END



GO
/****** Object:  StoredProcedure [dbo].[usp_buscarUsuarioPorEmail]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[usp_buscarUsuarioPorEmail]
@email varchar(50),          
@nombreusuario varchar(50) = ''      

as
  select idUsuario, login, nombre, email, telefono          
  from usuario where email = @email    
  and [login] = @nombreusuario


GO
/****** Object:  StoredProcedure [dbo].[usp_buscarUsuarioPorEmailProvisorio]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[usp_buscarUsuarioPorEmailProvisorio]        
@email varchar(50),        
@passProvisorio varchar(50),    
@nombreusuario varchar(50) = ''    
as        
 if exists (select 1 from usuario where email = @email and ([login] = @nombreusuario or @nombreusuario = ''))        
 begin        
  update usuario set passwordProvisorio = @passProvisorio where email = @email and ([login] = @nombreusuario or @nombreusuario = '')
      
          
  select idUsuario, login, password = '', nombre, email, telefono        
  from usuario where email = @email    and ([login] = @nombreusuario  or @nombreusuario  = '')  
 end      
 else --este select debe ir aunque no encuentre registros para que el programa reconozca la esctructura de la tabla de retorno      
 select idUsuario, login, password = '', nombre, email, telefono        
 from usuario where email = @email    and  ([login] = @nombreusuario  or @nombreusuario  = '')

GO
/****** Object:  StoredProcedure [dbo].[usp_buscarUsuarioPorLoginProvisorio]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_buscarUsuarioPorLoginProvisorio]    
@Login varchar(50),    
@passProvisorio varchar(50)    
as    
 if exists (select 1 from usuario where login = @Login)    
 begin    
  update usuario set passwordProvisorio = @passProvisorio where login = @Login    
  
      
  select idUsuario, login, password = '', nombre, email, telefono    
  from usuario where login = @Login
 end  
 else --este select debe ir aunque no encuentre registros para que el programa reconozca la esctructura de la tabla de retorno  
 select idUsuario, login, password = '', nombre, email, telefono    
 from usuario where login = @Login

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE procedure [dbo].[usp_buscaSistema]  
 @idSistema int  
 as  
 select idSistema, nombre , logo 
 from Sistema  
 where idSistema = @idSistema

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 create procedure [dbo].[usp_buscaSupervisor]
 @idSistema int  
  ,@idUsuario int  
 as  
 select idSistema, idUsuarioSupervisor , idUsuario
 from UsuarioSistemaSupervisor
 where idSistema = @idSistema and  idUsuario = @idUsuario


GO
/****** Object:  StoredProcedure [dbo].[usp_buscaUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_buscaUsuario]    
@idSistema int,  
@login varchar(50)    
as    
 select usuario.idUsuario, login, nombre, email, telefono, password='' , esSupervisor
 from usuario  inner join usuarioSistema on usuario.idUsuario = usuarioSistema.idUsuario  
 where login = @login  and  usuarioSistema.idSistema = @idSistema

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaUsuarioEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_buscaUsuarioEmpresa]   
 @login VARCHAR(50)  
AS  
BEGIN  
  
 SET NOCOUNT ON;  
  
    SELECT E.idEmpresa, E.rut, E.nombre, E.direccion, E.ciudad, E.codigoSucursal  , E.rutaLogo
    FROM Empresa E join UsuarioEmpresa UE on E.idEmpresa = UE.idEmpresa  
       join Usuario U on UE.idUsuario = U.idUsuario  
    WHERE U.login = @login  
END

GO
/****** Object:  StoredProcedure [dbo].[usp_buscaUsuarioPorId]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_buscaUsuarioPorId]
@idUsuario int
as
	select idUsuario, [login], [password], nombre, email, telefono, telefono2, passwordProvisorio
	from Usuario
	where idUsuario = @idUsuario


GO
/****** Object:  StoredProcedure [dbo].[usp_buscaUsuarioPorSucursal]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_buscaUsuarioPorSucursal]
@codigoSucursal varchar(50)  
as  
 
  
	select Usuario.idUsuario, [login], [password], Usuario.nombre, email, telefono, telefono2, passwordProvisorio, activo
	from Usuario inner join UsuarioEmpresa on Usuario.idUsuario = UsuarioEmpresa.idUsuario
	inner join Empresa on UsuarioEmpresa.idEmpresa = Empresa.idEmpresa
	where codigoSucursal = @codigoSucursal


GO
/****** Object:  StoredProcedure [dbo].[usp_buscaUsuarioRolDescripcion]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE procedure [dbo].[usp_buscaUsuarioRolDescripcion]    
 @idSistema int          
 ,@nombreRol varchar(50)          
 as          
 select usuario.idUsuario, login, password, nombre, email, telefono, telefono2          
 from usuario inner join usuarioRol on usuario.idUsuario = usuarioRol.idUsuario          
 inner join rol on usuarioRol.idRol = rol.idRol          
 where Usuario.activo = 1 and rol.descripcion = @nombreRol and rol.idSistema = @idSistema          
   


GO
/****** Object:  StoredProcedure [dbo].[usp_buscaUsuarioSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_buscaUsuarioSupervisor]
		 @idSistema int,
		 @Usuario varchar(30)
		
		as
		set nocount on

		declare @idSuperVisor int


		set @idSuperVisor = (
							select  USS.idUsuarioSupervisor
							from Usuario U 
							inner join UsuarioSistema US on US.idUsuario = U.idUsuario
							inner join UsuarioSistemaSupervisor USS on USS.idSistema = USS.idSistema and USS.idUsuario = U.idUsuario
							where 
							 U.login= @Usuario
							 and US.idSistema= @idSistema
							 )
							 
		select idUsuario,login,password,nombre,email,telefono,telefono2, passwordProvisorio 
		from Usuario where idUsuario= @idSuperVisor
			 


GO
/****** Object:  StoredProcedure [dbo].[usp_cambiarPassword]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_cambiarPassword]     
@idSistema int
 ,@login varchar(50)    
 ,@password varchar(50)    
AS    
BEGIN    
    
 SET NOCOUNT ON;    
    
    UPDATE Usuario SET password = @password  , passwordProvisorio = newid()  
    FROM Usuario inner join UsuarioSistema on Usuario.idUsuario = UsuarioSistema.idUsuario
    WHERE login = @login    and UsuarioSistema.idSistema = @idSistema
END   

GO
/****** Object:  StoredProcedure [dbo].[usp_cambiarPasswordProvisorio]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_cambiarPasswordProvisorio]
@login varchar(50)
,@password varchar(50)
as
	update usuario set passwordProvisorio = @password
	where login = @login


GO
/****** Object:  StoredProcedure [dbo].[usp_comprobarPassword]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_comprobarPassword]
	@login varchar(50)
	,@password varchar(50)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @antiguoPassword varchar(50)
	
	SELECT @antiguoPassword = password
	FROM Usuario
	WHERE login = @login
    
    IF(@antiguoPassword = @password)
    BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
    
END


GO
/****** Object:  StoredProcedure [dbo].[usp_CrearPrimerSuperUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

    
CREATE Proc [dbo].[usp_CrearPrimerSuperUsuario]    
(    
 @NombreEmpresa varchar(100),    
     
 @Login varchar(50),    
     
 @NombreCompleto varchar(100),    
     
 @Email varchar(50),    
     
 @NombreSistema varchar(100),    
     
 @RutaLogoSistema varchar(100),    
     
 @DescripcionRol varchar(50),    
     
 @TipoPermiso varchar(50),    
     
 @TituloMenuPadre varchar(50),    
     
 @PaginaCrearUsuario varchar(100),    
 @TituloPaginaCrearUsuario varchar(50),    
 @RutaPaginaCrearUsuario varchar(200),    
     
 @PaginaCrearRoles  varchar(100),    
 @TituloPaginaCrearRoles varchar(50),    
 @RutaPaginaCrearRoles varchar(200),    
     
 @TituloPaginaCrearPermiso varchar(50),    
 @PaginaCrearPermiso varchar(100),    
 @RutaPaginaCrearPermiso varchar(200),    
     
 @TituloPaginaAsignarRol varchar(50),    
 @RutaPaginaAsignarRol varchar(200),    
 @PaginaAsignarRol varchar(100),    
     
 @TituloPaginaASignarRolyPermiso varchar(50),    
 @RutaPaginaAsignarRolyPermiso varchar(200),    
 @PaginaAsignarRolyPermiso varchar(100),    
     
 @PaginaPrincipal varchar(100)    
)    
As    
Begin    
    
Declare @idEmpresa as int    
Declare @password varchar(50) = '4e19c591757bce7dfd4988aecc04344a' -->csepulveda0.    
Declare @passwordProvisorio varchar(50) = 'ABAEE5FE-0BF5-46AA-8BEC-6FB074C80FD9' -->csepulveda0.    
Declare @idUsuario as int    
Declare @idSistema as int    
Declare @idRol as int    
Declare @idPermisoPaginaCrearUsuario as int    
Declare @idPermisoPaginaCrearRoles as int    
Declare @idPermisoPaginaCrearPermiso as int    
Declare @idPermisoPaginaAsignarRol as int    
Declare @idPermisoPaginaAsignarRolyPermiso as int    
Declare @idPermisoPaginaPrincipal as int    
    
    
Begin Tran    
 print '01. Buscando idEmpresa.'    
 select @idEmpresa = idEmpresa     
 from Empresa where nombre = @NombreEmpresa    
 If @@Error <> 0    
 Begin    
  Rollback    
      
  print 'Deshaciendo operación 01.'    
 End    
     
     
 print '02. IdEmpresa: ' + cast(@idEmpresa as varchar(50))    
 If @@Error <> 0    
 Begin    
  Rollback    
      
  print 'Deshaciendo operación 02.'    
 End    
    
    
 print '03. Generando usuario ' + @Login    
     
 if not exists(select 1 from Usuario where [login] = @Login or email = @Email)    
 Begin    
  insert into Usuario([login], [password], [passwordProvisorio], nombre, email)    
  select @Login, @password, @passwordProvisorio, @NombreCompleto, @Email    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 03.'    
  End    
     
  select @idUsuario = @@IDENTITY    
      
 End    
 Else    
 Begin    
  print '03. Usuario existente ' + @Login    
  select @idUsuario = idUsuario from Usuario where [login] = @Login or email = @Email    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 03.'    
  End    
      
 End    
     
     
 print '04. IdUsuario: ' + cast(@idUsuario as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 04.'    
  End     
    
    
 print '05. Generando relación usuario-empresa.'    
 if not exists(select 1 from UsuarioEmpresa where idUsuario = @idUsuario)    
 Begin    
  Insert Into UsuarioEmpresa(idUsuario, idEmpresa)    
  Select @idUsuario, @idEmpresa    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 05.'    
  End    
 End    
 Else    
 Begin    
  print '05.1. Relación usuario-empresa existente.'    
 End    
    
 print '06. Generando registro del sistema...'    
   
if not exists( select 1 from Sistema where nombre = @NombreSistema )  
Begin  
     Insert Into Sistema(nombre, logo)    
     Select @NombreSistema, @RutaLogoSistema    
     If @@Error <> 0    
     Begin    
      Rollback    
          
      print 'Deshaciendo operación 06.'    
     End    
        
        
     select @idSistema = @@IDENTITY    
  
End  
else  
Begin  
    select @idSistema =   
    (  
       select idSistema   
       from Sistema   
       where nombre = @NombreSistema   
    )  
End  
  
     print '07. IdSistema: ' + cast(@idSistema as varchar(50))    
     If @@Error <> 0    
     Begin    
      Rollback    
          
      print 'Deshaciendo operación 07.'    
     End     
  
 
     
 print '07.1 Creando relación usuario-sistema'    
 
if not exists (select 1 from UsuarioSistema where idUsuario = @idUsuario and idSistema = @idSistema )
begin 
				 insert into UsuarioSistema(idUsuario, idSistema)    
				 Select @idUsuario, @idSistema    
				 If @@Error <> 0    
				 Begin    
				  Rollback    
				      
				  print 'Deshaciendo operación 07.1'    
				 End     
End
   
  
    
 print '08. Creando Rol: ' + @DescripcionRol    
 Insert Into Rol(descripcion, idSistema)    
 Select @DescripcionRol, @idSistema    
 If @@Error <> 0    
 Begin    
  Rollback    
      
  print 'Deshaciendo operación 08.'    
 End    
    
    
 select @idRol = @@IDENTITY    
 print '09. IdRol: ' + cast(@idRol as varchar(50))    
 If @@Error <> 0    
 Begin    
  Rollback    
      
  print 'Deshaciendo operación 09.'    
 End    
    
    
 print '10. Creando relación usuario-rol'    
 Insert Into UsuarioRol(idusuario, idRol)    
 Select @idUsuario, @idRol    
 If @@Error <> 0    
 Begin    
  Rollback    
      
  print 'Deshaciendo operación 10.'    
 End    
    
    
 print '11. Creando permisos.'    
    
  print '11.1. Creando permiso: ' + @PaginaCrearUsuario    
  Insert Into Permiso(idSistema, tipoPermiso, nombreElemento)    
  Select @idSistema, @TipoPermiso, @PaginaCrearUsuario    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.1.'    
  End    
    
    
  Select @idPermisoPaginaCrearUsuario = @@IDENTITY    
  print '11.2. IdPermisoPaginaCrearUsuario: ' + cast(@idPermisoPaginaCrearUsuario as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.2.'    
  End    
    
  print '11.3.Creando relación rol-permiso.'    
  Insert Into RolPermiso(idRol, idPermiso)    
  Select @idRol, @idPermisoPaginaCrearUsuario    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.3.'    
  End    
    
    
  print '11.4. Creando permiso: ' + @PaginaCrearRoles    
  Insert Into Permiso(idSistema, tipoPermiso, nombreElemento)    
  Select @idSistema, @TipoPermiso, @PaginaCrearRoles    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.4.'    
  End    
    
    
  Select @idPermisoPaginaCrearRoles = @@IDENTITY    
  print '11.5. IdPermisoPaginaCrearRoles: ' + cast(@idPermisoPaginaCrearRoles as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.5.'    
  End    
    
    
  print '11.6. Creando relación rol-permiso.'    
  Insert Into RolPermiso(idRol, idPermiso)    
  Select @idRol, @idPermisoPaginaCrearRoles    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.6.'    
  End    
    
    
  print '11.7. Creando permiso: ' + @PaginaCrearPermiso    
  Insert Into Permiso(idSistema, tipoPermiso, nombreElemento)    
  Select @idSistema, @TipoPermiso, @PaginaCrearPermiso    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.7.'    
  End    
    
    
  Select @idPermisoPaginaCrearPermiso = @@IDENTITY    
  print '11.8. IdPermisoPaginaCrearPermiso: ' + cast(@idPermisoPaginaCrearPermiso as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.8.'    
  End    
    
    
  print '11.9. Creando relación rol-permiso.'    
  Insert Into RolPermiso(idRol, idPermiso)    
  Select @idRol, @idPermisoPaginaCrearPermiso    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.9.'    
  End    
        
      
  print '11.10. Creando permiso: ' + @PaginaAsignarRol    
  Insert Into Permiso(idSistema, tipoPermiso, nombreElemento)    
  Select @idSistema, @TipoPermiso, @PaginaAsignarRol    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.10.'    
  End    
    
    
  Select @idPermisoPaginaAsignarRol = @@IDENTITY    
  print '11.11. IdPermisoPaginaAsignarRol: ' + cast(@idPermisoPaginaAsignarRol as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.11.'    
  End    
    
  print '11.12. Creando relación rol-permiso.'    
  Insert Into RolPermiso(idRol, idPermiso)    
  Select @idRol, @idPermisoPaginaAsignarRol    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.12.'    
  End    
    
    
    
  print '11.13. Creando permiso: ' + @PaginaAsignarRolyPermiso    
  Insert Into Permiso(idSistema, tipoPermiso, nombreElemento)    
  Select @idSistema, @TipoPermiso, @PaginaAsignarRolyPermiso    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.13.'    
  End    
    
    
  Select @idPermisoPaginaAsignarRolyPermiso = @@IDENTITY    
  print '11.14. IdPermisoPaginaAsignarRolyPermiso: ' + cast(@idPermisoPaginaAsignarRolyPermiso as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.14.'    
  End    
    
  print '11.15. Creando relación rol-permiso.'    
  Insert Into RolPermiso(idRol, idPermiso)    
  Select @idRol, @idPermisoPaginaAsignarRolyPermiso    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.15.'    
  End    
    
     
  print '11.16. Creando permiso: ' + @PaginaPrincipal    
  Insert Into Permiso(idSistema, tipoPermiso, nombreElemento)    
  Select @idSistema, @TipoPermiso, @PaginaPrincipal    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.16.'    
  End    
    
    
  Select @idPermisoPaginaPrincipal = @@IDENTITY    
  print '11.17. IdPermisoPaginaAsignarRolyPermiso: ' + cast(@idPermisoPaginaPrincipal as varchar(50))    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.17.'    
  End    
    
  print '11.18. Creando relación rol-permiso.'    
  Insert Into RolPermiso(idRol, idPermiso)    
  Select @idRol, @idPermisoPaginaPrincipal    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 11.18.'    
  End     
    
    
    
    
  print '12. Creando Menú'    
    
        
  --menú padre    
  print '12.1. Creando menú padre: MenuId-> 1'    
  Insert Into Menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
  Select @idSistema, 1, @TituloMenuPadre, 1, 0, '', 1, '', NULL, NULL    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 12.1.'    
  End    
    
      
  --opciones hijas menú padre    
    
  --crear usuario,    
  print '12.2. Creando menú hijo: MenuId-> 2 ' + @TituloPaginaCrearUsuario    
  Insert Into Menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
  Select @idSistema, 2, @TituloPaginaCrearUsuario, 1, 1, '', 1, @RutaPaginaCrearUsuario, @PaginaCrearUsuario, NULL    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 12.2.'    
  End    
    
    
  --crear roles    
  print '12.3. Creando menú hijo: MenuId-> 3 ' + @TituloPaginaCrearRoles    
  Insert Into Menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
  Select @idSistema, 3, @TituloPaginaCrearRoles, 1, 2, '', 1, @RutaPaginaCrearRoles, @PaginaCrearRoles, NULL    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 12.3.'    
  End    
    
    
     
  -- crear permiso    
  print '12.4. Creando menú hijo: MenuId-> 4 ' + @TituloPaginaCrearPermiso    
  Insert Into Menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
  Select @idSistema, 4, @TituloPaginaCrearPermiso, 1, 3, '', 1, @RutaPaginaCrearPermiso, @PaginaCrearPermiso, NULL    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 12.4.'    
  End    
    
    
    
  -- asignar rol    
  print '12.5. Creando menú hijo: MenuId-> 5 ' + @TituloPaginaAsignarRol    
  Insert Into Menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
  Select @idSistema, 5, @TituloPaginaAsignarRol, 1, 4, '', 1, @RutaPaginaAsignarRol, @PaginaAsignarRol, NULL    
  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 12.5.'    
  End    
    
    
    
  -- asignar rol y permiso    
  print '12.6. Creando menú hijo: MenuId-> 6 '  + @TituloPaginaAsignarRolyPermiso    
  Insert Into Menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
  Select @idSistema, 6, @TituloPaginaAsignarRolyPermiso, 1, 5, '', 1, @RutaPaginaAsignarRolyPermiso, @PaginaAsignarRolyPermiso, NULL    

  If @@Error <> 0    
  Begin    
   Rollback    
       
   print 'Deshaciendo operación 12.6.'    
  End    
	Else

	begin    
		Commit    
	end 
End




GO
/****** Object:  StoredProcedure [dbo].[usp_crearUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[usp_crearUsuario]
	  @login VARCHAR(50)
	, @password VARCHAR(50)
	, @nombre VARCHAR(100)
	, @email VARCHAR(50)
	, @telefono VARCHAR(20)
	, @telefono2 VARCHAR(20) = null
	, @idRol INT
	, @idEmpresa INT
	, @idSistema INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @idUsuario INT
	DECLARE @error VARCHAR(50)

    BEGIN TRAN
		-- inserta usuario
		insert into Usuario (login, password, nombre, email, telefono, telefono2) values (@login, @password, @nombre, @email, @telefono, @telefono2)
		Set @idUsuario = IDENT_CURRENT('dbo.Usuario')
		-- inserta usuarioRol
		insert into UsuarioRol (idUsuario, idRol) values (@idUsuario, @idRol)
		-- inserta el usuario con la empresa
		insert into UsuarioEmpresa (idUsuario, idEmpresa) values (@idUsuario, @idEmpresa)
		-- inserta el usuarioempresa
		insert into UsuarioSistema (idUsuario, idSistema, conectado) values (@idUsuario, @idSistema, 0)
    COMMIT
    
    IF @@ERROR != 0 
		BEGIN
			ROLLBACK TRANSACTION
			SET @error = 'Error: '+ CAST(@@ERROR AS VARCHAR(30)) + ' al crear el usuario.'
			RAISERROR (@error, 16, 1)
			RETURN
		END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_desactivarMensajeUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
create procedure [dbo].[usp_desactivarMensajeUsuario]
@Pagina varchar(50),
@usuario varchar(50),
@idSistema int
as
	update TTMU set habilitado = 0
	from ToolTipMensaje TTM inner join ToolTipMensajeUsuario TTMU on   
		TTM.idToolTipMensaje = TTMU.idToolTipMensaje  
		inner join Usuario U on TTMU.idUsuario = U.idUsuario  
	where TTM.habilitado = 1 and TTMU.habilitado = 1 and
		TTMU.idSistema = @idSistema and U.[login] = @Usuario and TTM.pagina = @pagina 


GO
/****** Object:  StoredProcedure [dbo].[Usp_EliminarArea]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Usp_EliminarArea]
@AreaId int = 0 , 
@AreaUsuario varchar(50) = '' 

as

	if not exists (select 1 from area where AreaId = @AreaId)
		begin
				Update Area set 
				AreaEstado = 0 , 
				AreaFechaModificacion = getdate() , 
				AreaUsuarioModificacion = @AreaUsuario  
				where AreaId = @AreaId
		end

GO
/****** Object:  StoredProcedure [dbo].[usp_eliminaRol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create Proc [dbo].[usp_eliminaRol]
(
	@idRol as int
)
as
begin
	delete from Rol where idRol = @idRol
end 

GO
/****** Object:  StoredProcedure [dbo].[usp_eliminaRolPermiso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create Proc [dbo].[usp_eliminaRolPermiso]
(
	@IdRol as int,
	@IdPermiso as int
)
As
Begin
	Delete From RolPermiso Where idPermiso = @IdPermiso And idRol = @IdRol
End

GO
/****** Object:  StoredProcedure [dbo].[usp_eliminaRolUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[usp_eliminaRolUsuario]
@usuario varchar(50) = ''
as

begin

	delete from UsuarioRol where idusuario = (select idusuario from Usuario where login = @usuario)
end 

GO
/****** Object:  StoredProcedure [dbo].[usp_eliminaUsuarioRol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

create procedure [dbo].[usp_eliminaUsuarioRol]
@idUsuario int
,@idRol int
as
	delete from UsuarioRol where idUsuario = @idUsuario and idRol = @idRol


GO
/****** Object:  StoredProcedure [dbo].[usp_EliminaUsuarioSistemaSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
 create procedure [dbo].[usp_EliminaUsuarioSistemaSupervisor]  
 @idSistema int  
 ,@idUsuarioSupervisor int  
 ,@idUsuario int  
 as  
 delete from UsuarioSistemaSupervisor 
 where idSistema = @idSistema and idUsuarioSupervisor = @idUsuarioSupervisor
	and  idUsuario = @idUsuario


GO
/****** Object:  StoredProcedure [dbo].[usp_insertaConexion]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--CREATE Proc [dbo].[usp_EntregaListadoJobs]  
--As  
-- SELECT A.name as Trabajo, case when A.enabled = 1 then 'Vigente' else 'No activo' end EstadoTrabajo,   
-- convert(varchar, convert(datetime, 
-- case when B.last_run_date = 0 then NULL else cast(B.last_run_date as varchar) end
-- ,105), 103)  + ' ' + 
-- case when B.last_run_time = 0 then NULL else dbo.RetornaFormatoHora(B.last_run_time) end as Fecha,
-- case when B.last_run_outcome = 0 then 'Error al ejecutarse' else 'Se ejecutó correctamente' end EstadoEjecucion,   
-- dbo.EntregaUltimoMensajeJob(A.job_id) as UltimoMensaje  
-- FROM msdb.dbo.sysjobs A, msdb.dbo.sysjobservers B  
-- WHERE A.job_id = B.job_id 
--GO
 
CREATE procedure [dbo].[usp_insertaConexion]  
@idSistema int  
,@ipPeticion varchar(50)  
,@ipCliente varchar(50)  
,@esProxy bit  
,@usuario varchar(50)  
,@autenticacion varchar(50)  
,@fecha datetime  
,@navegador varchar(100)  
,@host varchar(50) 
,@sessionId varchar(50) = null 
as  
 insert into Conexion(idSistema, ipPeticion, ipCliente, esProxy, usuario, autenticacion, fecha, navegador, host, sessionId)  
 values(@idSistema, @ipPeticion, @ipCliente, @esProxy, @usuario, @autenticacion, @fecha, @navegador, @host, @sessionId)  

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertaErrorSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE Proc [dbo].[usp_InsertaErrorSistema]
(
	@IdUsuario	int,
	@IdSistema	int,
	@LoginUsuario	varchar(50),
	@Url	varchar(200),
	@IPUsuario	varchar(20),
	@IPHost	varchar(100),
	@MensajeError	ntext,
	@FechaError datetime
)
as
begin
	insert into ErrorSistema
	(
		IdUsuario,
		IdSistema,
		LoginUsuario,
		Url,
		IPUsuario,
		IPHost,
		MensajeError,
		FechaError
	)
	select
		@IdUsuario,
		@IdSistema,
		@LoginUsuario,
		@Url,
		@IPUsuario,
		@IPHost,
		@MensajeError,
		@FechaError
end


GO
/****** Object:  StoredProcedure [dbo].[usp_insertaImpresoraSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[usp_insertaImpresoraSistema]          
@idSistema int,        
@nombreEquipoLocal varchar(60),          
@nombreImpresora varchar(50)          
as          
 if not exists(select 1 from ImpresoraSistema where idSistema = @idSistema and nombreEquipoLocal = @nombreEquipoLocal )          
-- if not exists(select 1 from ImpresoraSistema where idSistema = @idSistema and nombreEquipoLocal = @nombreEquipoLocal)           
  insert into ImpresoraSistema(idSistema, nombreEquipoLocal, nombreImpresora)          
  values(@idSistema, @nombreEquipoLocal, @nombreImpresora)          
 else          
  update ImpresoraSistema set nombreImpresora = @nombreImpresora          
  where idSistema = @idSistema and nombreEquipoLocal = @nombreEquipoLocal 

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaMensaje]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[usp_insertaMensaje]
	@mensaje varchar(5000)
	,@idMensaje int output
AS
BEGIN

	SET NOCOUNT ON;

    INSERT INTO Mensaje (mensaje, habilitado) VALUES (@mensaje, 1)
    
    set @idMensaje = @@identity
    return @idMensaje
END


GO
/****** Object:  StoredProcedure [dbo].[usp_insertaMensajeUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[usp_insertaMensajeUsuario]
	@idSistema int
	,@idUsuario int
	,@idMensaje int
AS
BEGIN

	SET NOCOUNT ON;

    Insert into MensajeUsuario (idSistema, idUsuario, idMensaje, mostrar) values (@idSistema, @idUsuario, @idMensaje, 1)
END


GO
/****** Object:  StoredProcedure [dbo].[usp_insertaMenu]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[usp_insertaMenu]    
@idSistema int,    
@descripcion varchar(50),    
@padreId int,    
@posicion int,    
@icono varchar(100),    
@habilitado bit,    
@ruta varchar(200),    
@pagina varchar(200),    
@parametros varchar(200)    
as    
 declare @menuId int    
 select @menuId = isnull((select max(menuId) from menu where idSistema = @idSistema),0) + 1    
     
 if @padreId = 0    
  set @padreId = @MenuId    
     
 if @pagina = ''
	set @pagina = null    
     
 insert into menu(idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros)    
 values(@idSistema, @menuId, @descripcion, @padreId, @posicion, @icono, @habilitado, @ruta, @pagina, @parametros)    
     

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaPermiso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

CREATE Proc [dbo].[usp_insertaPermiso]
(
	@tipoPermiso varchar(50),
	@nombreElemento varchar(100),
	@idSistema int
)
as
Begin
	Insert Into Permiso(tipoPermiso, nombreElemento, idSistema)
	Select @tipoPermiso, @nombreElemento, @idSistema
End

GO
/****** Object:  StoredProcedure [dbo].[Usp_InsertArea]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Usp_InsertArea]
@AreaNombre varchar(100) = '',
@AreaUsuarioIngreso varchar(50) = '' 

as

	if not exists (select 1 from area where AreaNombre = @AreaNombre)
		begin
				insert Area ( AreaNombre , AreaEstado, AreaFechaingreso, AreaUsuarioIngreso, AreaFechaModificacion, AreaUsuarioModificacion )
				select @AreaNombre ,1, getdate(), @AreaUsuarioIngreso, getdate(),@AreaUsuarioIngreso
		end

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaRol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
Create Proc [dbo].[usp_insertaRol]
(
	@DescripcionRol as varchar(50),
	@IdSistema as int
)
as
begin
	if exists(select 1 from Rol where upper(descripcion) = UPPER(@DescripcionRol) and idSistema = @IdSistema)
	begin
		Update Rol
		set descripcion = @DescripcionRol, idSistema = @IdSistema
		where upper(descripcion) = UPPER(@DescripcionRol) and idSistema = @IdSistema 
	end
	else
	begin
		Insert into Rol(descripcion, idSistema)
		select @DescripcionRol, @IdSistema
	end
end 

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaRolPermiso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

Create Proc [dbo].[usp_insertaRolPermiso]
(
	@IdRol as int,
	@IdPermiso as int
)
As
Begin
	If exists(select 1 from RolPermiso where idRol= @IdRol and idPermiso = @IdPermiso)
		Begin
			Update RolPermiso
			Set idRol = @IdRol, idPermiso = @IdPermiso
			Where idRol = @IdRol And idPermiso = @IdPermiso
		End
	Else
		Begin
			Insert Into RolPermiso(idRol, idPermiso)
			Select @IdRol, @IdPermiso
		End
End

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE  procedure [dbo].[usp_insertaUsuario]    
@login varchar(50)    ,
@AreaId int = 0 
,@password varchar(50)    
,@nombre varchar(100)    
,@email varchar(50)    
,@telefono varchar(30)    
,@passwordProvisorio varchar(50)  

as    

 declare @idUsuario int

 select @idUsuario = idUsuario  from Usuario where login = @login

 if (@idUsuario is null)
 begin
	insert into Usuario(login, AreaId, password, nombre, email, telefono, passwordProvisorio)    
	values(@login, @AreaId, @password, @nombre, @email, @telefono, @passwordProvisorio)    
	select @idUsuario = @@IDENTITY
end
else
begin
	update Usuario set nombre = @nombre, email = @email, @telefono = @telefono, AreaId = @AreaId
	where login  = @login
end
select idUsuario = @idUsuario

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaUsuarioEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_insertaUsuarioEmpresa]  
 @idUsuario int  
 ,@idEmpresa int  
 as  
 if not exists(select  1 from UsuarioEmpresa where idUsuario = @idUsuario and idEmpresa = @idEmpresa)
	insert into UsuarioEmpresa(idUsuario, idEmpresa)  
	values(@idUsuario, @idEmpresa)  

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaUsuarioRol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_insertaUsuarioRol]
@idUsuario int
,@idRol int
as
	insert into UsuarioRol(idUsuario, idRol)
	values(@idUsuario, @idRol)

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaUsuarioSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_insertaUsuarioSistema]    
@idUsuario int    
,@idSistema int    
,@esSupervisor bit = null  
as    
 if @esSupervisor is null  
 set @esSupervisor = 0;  
 if not exists(select 1 from UsuarioSistema where idUsuario = @idUsuario and idSistema = @idSistema)
	insert into UsuarioSistema(idUsuario, idSistema, esSupervisor)    
	values (@idUsuario, @idSistema, @esSupervisor)    
else
	update UsuarioSistema set esSupervisor = @esSupervisor where idSistema = @idSistema and idUsuario = @idUsuario

   

GO
/****** Object:  StoredProcedure [dbo].[usp_insertaUsuarioSistemaSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE procedure [dbo].[usp_insertaUsuarioSistemaSupervisor]
 @idSistema int
 ,@idUsuarioSupervisor int
 ,@idUsuario int
 as
    delete from UsuarioSistemaSupervisor where idSistema = @idSistema and idUsuario = @idUsuario
   
	insert into UsuarioSistemaSupervisor(idSistema, idUsuarioSupervisor, idUsuario)
	values(@idSistema, @idUsuarioSupervisor, @idUsuario)


GO
/****** Object:  StoredProcedure [dbo].[usp_ListadoContadorErrores]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create Proc [dbo].[usp_ListadoContadorErrores]
As
Begin
	select ErrorSistema.idSistema, Sistema.nombre as sistema, count(idError) as cantidad From ErrorSistema with(nolock) inner join Sistema on ErrorSistema.IdSistema = Sistema.idSistema
	group by ErrorSistema.IdSistema, Sistema.nombre
	order by Sistema.nombre asc
End

GO
/****** Object:  StoredProcedure [dbo].[usp_listaEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_listaEmpresa]  
 @idRol int = null  ,
 @idSistema int = null
as  

 if @idSistema = 0
	set @idSistema = null

 if(@idRol is null)  
 begin
	if @idSistema is null
	begin
		select idEmpresa  
		, rut  
		, nombre = case when codigoSucursal is not null then nombre + ' [' + codigoSucursal + ']'  else nombre end  
		, direccion, ciudad, codigoSucursal, rutaLogo  
		from Empresa  
		order by nombre  
	end
	else
	begin
		select E.idEmpresa
		, E.rut
		, nombre = case when E.codigoSucursal is not null then E.nombre + ' [' + E.codigoSucursal + ']'  else E.nombre end  
		, E.direccion, E.ciudad, E.codigoSucursal, E.rutaLogo
		from Empresa E inner join SistemaEmpresa SE on E.idEmpresa = SE.idEmpresa
		where SE.idSistema = @idSistema
		order by nombre
	end
 end  
 else  
 begin  
  if(@idRol = 1) --sucursal  
  begin  
   select idEmpresa, rut, nombre, direccion, ciudad, codigoSucursal, rutaLogo  
   from Empresa  
   where codigoSucursal is not null  
   order by nombre  
  end  
  else  
  begin  
   if(@idRol = 3)  
   begin  
    select idEmpresa, rut, nombre, direccion, ciudad, codigoSucursal, rutaLogo  
    from Empresa  
    
    order by nombre  
   end  
   else  
   begin  
    select idEmpresa, rut, nombre, direccion, ciudad, codigoSucursal, rutaLogo  
    from Empresa  
  
    order by nombre  
   end  
  end  
 end  

GO
/****** Object:  StoredProcedure [dbo].[usp_ListaErroresPorSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

    
CREATE Proc [dbo].[usp_ListaErroresPorSistema]    
(    
 @IdSistema int = -1,  
 @IdUsuario int = -1,  
 @FechaDesde datetime = null,  
 @FechaHasta datetime = null,  
 @QueElMensajeContiene varchar(100) = null  
)    
As    
Begin    
 Select    
  IdError,    
  IdUsuario,    
  IdSistema,    
  FechaError,
  LoginUsuario,    
  Url,    
  IPUsuario,    
  IPHost,    
  MensajeError
 From ErrorSistema with(nolock)  
 Where   
 (IdSistema =  @IdSistema or @IdSistema = -1 ) And  
 (IdUsuario =  @IdUsuario or @IdUsuario = -1 ) And  
 (( Convert(datetime, (Convert(varchar, FechaError, 103)), 103) between @FechaDesde and @FechaHasta) Or (@FechaDesde is null) Or (@FechaHasta is null)) And  
 (MensajeError like '%' + @QueElMensajeContiene + '%' or @QueElMensajeContiene is null)  
End 

GO
/****** Object:  StoredProcedure [dbo].[usp_listaImpresoraServidor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_listaImpresoraServidor]
as
	select idImpresoraServidor, nombreImpresoraServidor from impresoraServidor order by nombreImpresoraServidor

GO
/****** Object:  StoredProcedure [dbo].[usp_listaMenu]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_listaMenu]         
@idSistema int,        
@idUsuario int        
as        
        
select menu.idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros        
into #menu        
from menu        
where habilitado = 1 and menu.idSistema = @idSistema and pagina is null        
        
        
        
        
insert into #menu        
select menu.idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros        
from menu inner join permiso on menu.pagina = permiso.nombreElemento and tipoPermiso = 'PAGINA' and menu.idSistema = permiso.idSistema        
inner join RolPermiso on permiso.idPermiso = RolPermiso.idPermiso        
inner join UsuarioRol on RolPermiso.idRol = UsuarioRol.idRol        
inner join Usuario on UsuarioRol.idUsuario = Usuario.idUsuario        
where habilitado = 1 and menu.idSistema = @idSistema and Usuario.idUsuario = @idUsuario    
group by menu.idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros        
        
    
select menuId as padreid    
into #tpadres    
from #menu    
where pagina is null    
    
    
select #tpadres.padreId     
into #sinElementos    
from #tpadres left join #menu on #tpadres.padreid = #menu.padreid    
where #menu.menuid is null   
group by #tpadres.padreId     
having COUNT(1)=1     
    
insert into #sinElementos
select #tpadres.padreId     
from #tpadres left join #menu on #tpadres.padreid = #menu.padreid    
group by #tpadres.padreId     
having COUNT(1)=1     


    
/*    
select padreId       
into #sinElementos        
from #menu       
group by padreId        
having count(1)=1      
*/    
    
        
        
delete from #menu        
from #menu inner join #sinElementos on #menu.menuId = #sinElementos.padreId        
        
select idSistema, menuId, descripcion, padreId, posicion, icono, habilitado, ruta, pagina, parametros        
from #menu order by posicion

GO
/****** Object:  StoredProcedure [dbo].[usp_listaPermiso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Proc [dbo].[usp_listaPermiso]  
( @idSistema int)  
as  
Select   
 Permiso.idPermiso, Permiso.nombreElemento, Permiso.tipoPermiso  
from   
 Permiso with(nolock)   
 --left join RolPermiso with(nolock) on Permiso.idPermiso = RolPermiso.idPermiso  
 --left join Rol with(nolock) on RolPermiso.idRol = Rol.idRol  
where   
 Permiso.idSistema = @idSistema  
Order by  
 Permiso.nombreElemento 

GO
/****** Object:  StoredProcedure [dbo].[Usp_ListarArea]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Usp_ListarArea]

as

	select * from area where AreaEstado =1 
	order by AreaNombre

GO
/****** Object:  StoredProcedure [dbo].[usp_listaRol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[usp_listaRol]  
 @idSistema INT = null, @DescripcionRol varchar(50) = null  
as  
 select idRol, descripcion, idSistema  
 from Rol  
 where 
 ((@idSistema is null) or (idSistema = @idSistema))  and 
 (@DescripcionRol is null or upper(descripcion) like '%' + upper(@DescripcionRol) + '%')
 order by descripcion

GO
/****** Object:  StoredProcedure [dbo].[usp_listarRolPorUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




Create Proc [dbo].[usp_listarRolPorUsuario]
(
	@IdUsuario as int,
	@IdSistema as int
)
As
Begin

	Select Rol.* From Rol inner join UsuarioRol on UsuarioRol.idRol = Rol.idRol
	where idusuario = @IdUsuario and Rol.idSistema = @IdSistema
End

GO
/****** Object:  StoredProcedure [dbo].[usp_listaSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_listaSistema]
as
	select idSistema, nombre, logo
	from Sistema
	order by nombre


GO
/****** Object:  StoredProcedure [dbo].[usp_listaSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_listaSupervisor]
@idSistema int
as
select U.idUsuario
	,loginNombre = '(' + U.login + ')' + ' ' + U.nombre
	,nombreLogin = U.nombre + ' ' + '(' + U.login + ')'
from Usuario U inner join UsuarioSistema US on U.idUsuario = US.idUsuario
where US.idSistema = @idSistema and US.esSupervisor = 1

GO
/****** Object:  StoredProcedure [dbo].[usp_listaToolTipMensajeUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_listaToolTipMensajeUsuario]  
@Pagina varchar(50),  
@Usuario varchar(50),  
@idSistema int  
as  
 select TTM.idToolTipMensaje, TTM.mensaje, TTM.habilitado, TTM.pagina, TTM.controlId, TTM.posicion  
 from ToolTipMensaje TTM inner join ToolTipMensajeUsuario TTMU on   
  TTM.idToolTipMensaje = TTMU.idToolTipMensaje  
  inner join Usuario U on TTMU.idUsuario = U.idUsuario  
 where TTM.habilitado = 1 and TTMU.habilitado = 1 and
	TTMU.idSistema = @idSistema and U.[login] = @Usuario and TTM.pagina = @pagina   

GO
/****** Object:  StoredProcedure [dbo].[usp_listaUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[usp_listaUsuario]
(
	@idSistema int
)
as
begin
	select Distinct U.*  
	from usuario U  
	 join usuarioEmpresa UE on U.idUsuario = UE.idUsuario  
	 join Empresa E on UE.idEmpresa = E.idEmpresa  
	 join UsuarioRol UR on U.idusuario = UR.idUsuario  
	 join Rol R on UR.idRol = R.idRol  
	 join UsuarioSistema US on U.idUsuario = US.idUsuario  
	 where US.idSistema = @idSistema  
	 order by U.nombre   
end






GO
/****** Object:  StoredProcedure [dbo].[usp_listaUsuarioEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_listaUsuarioEmpresa]
	@idSistema int
AS
BEGIN

	SET NOCOUNT ON;

    select U.idUsuario, U.login, U.nombre as 'Nombre Usuario', U.email, U.telefono, E.idEmpresa, E.rut, E.nombre as 'Empresa'
	, E.direccion + ', ' + E.ciudad as 'direccion', E.codigoSucursal, R.descripcion as rol, R.idRol, US.conectado, US.fechaConeccion
	from usuario U
	join usuarioEmpresa UE on U.idUsuario = UE.idUsuario
	join Empresa E on UE.idEmpresa = E.idEmpresa
	join UsuarioRol UR on U.idusuario = UR.idUsuario
	join Rol R on UR.idRol = R.idRol
	join UsuarioSistema US on U.idUsuario = US.idUsuario
	where US.idSistema = @idSistema
	order by E.nombre	
END


GO
/****** Object:  StoredProcedure [dbo].[usp_listaUsuariosEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_listaUsuariosEmpresa]


  @idEmpresa int,
  @idSistema int 
as
select login , nombre
from  Usuario UU
		inner Join UsuarioEmpresa UE on UU.idUsuario = UE.idUsuario
		inner join UsuarioSistema US on US.idUsuario = UU.idUsuario
		where UE.idEmpresa = @idEmpresa and US.idSistema=@idSistema





GO
/****** Object:  StoredProcedure [dbo].[usp_ListaUsuariosPorSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PRoc [dbo].[usp_ListaUsuariosPorSistema]
(
	@IdSistema int = null
)
as
Begin
	  select Usuario.idUsuario, Usuario.[login]
	  from Usuario with(nolock) inner join UsuarioSistema with(nolock) 
	  on Usuario.idUsuario = UsuarioSistema.idUsuario
	  Where 
	  (UsuarioSistema.idSistema = @IdSistema or @IdSistema is null)
End	  

GO
/****** Object:  StoredProcedure [dbo].[usp_LustaUsuariosPorEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_LustaUsuariosPorEmpresa]
			@idEmpresa int
			as
			set noCount on
			select US.login 
				from  b2b_Usuario..Usuario US
				inner join  b2b_Usuario..UsuarioEmpresa UE on US.idUsuario = UE.idUsuario
				where idEmpresa= @idEmpresa
			


GO
/****** Object:  StoredProcedure [dbo].[usp_ObtieneUltimoError]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




  
  
  
CREATE Proc [dbo].[usp_ObtieneUltimoError]  
As  
Begin  
  
Declare @IdUltimoError as int  
  
select @IdUltimoError = MAX(iderror) from ErrorSistema  
  
  
Select IdError, Sistema.nombre as sistema, ErrorSistema.LoginUsuario, ErrorSistema.IPUsuario, FechaError, ErrorSistema.MensajeError   
From ErrorSistema inner join Sistema on ErrorSistema.IdSistema = Sistema.idSistema  
Where IdError = @IdUltimoError  
  
End

GO
/****** Object:  StoredProcedure [dbo].[Usp_RescataNombreMenuLaboratorioExterno]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
create procedure [dbo].[Usp_RescataNombreMenuLaboratorioExterno]  
@SistemaId int = 0 ,  
@MenuId int = 0   
  
as  
  
 select Descripcion from Menu where idSistema = @SistemaId and menuId = @MenuId  



GO
/****** Object:  StoredProcedure [dbo].[usp_spWho2EstadoInstancia]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Proc [dbo].[usp_spWho2EstadoInstancia]
As
Begin

CREATE TABLE #sp_who2
(
   SPID INT,  
   [Status] VARCHAR(1000) NULL,  
   [Login] VARCHAR(255) NULL,  
   HostName VARCHAR(255) NULL,  
   BlkBy VARCHAR(255) NULL,  
   DBName VARCHAR(255) NULL,  
   Command VARCHAR(1000) NULL,  
   CPUTime INT NULL,  
   DiskIO INT NULL,  
   LastBatch VARCHAR(1000) NULL,  
   ProgramName VARCHAR(1000) NULL,  
   SPID2 INT, 
   REQUESTID INT
) 
--GO

INSERT INTO #sp_who2
EXEC sp_who2
--GO

SELECT *
FROM #sp_who2
Order by DiskIO DESC

DROP TABLE #sp_who2


End

GO
/****** Object:  StoredProcedure [dbo].[usp_TieneRolControlador]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  
CREATE Proc [dbo].[usp_TieneRolControlador]  
(  
 @IdUsuario as int,  
 @IdSucursal as int  
)  
As  
Begin  
  
Declare @Resultado as bit  
  
If Exists(select 1 from RolSucursal   
      inner join Rol   on RolSucursal.IdRol = Rol.idRol  
      inner join UsuarioRol on Rol.idRol = UsuarioRol.idRol  
      inner join Usuario  on UsuarioRol.idusuario = Usuario.idUsuario  
   where   
   Rol.descripcion like 'CONTROLADOR%' And Usuario.idUsuario = @IdUsuario  And RolSucursal.IdSucursal = @IdSucursal )  

Begin  
 set @Resultado = 1  
End  
  
Else  
Begin  
 set @Resultado = 0  
End  
  
select @Resultado  
  
End   


GO
/****** Object:  StoredProcedure [dbo].[usp_verificaAcceso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_verificaAcceso]  
@login varchar(50)  
,@pass varchar(50)  
as  
declare @ret int

set @ret = 0
if exists(select 1 from Usuario where login = @login and [password] = @pass and activo = 1)  
 set @ret = 1  
if exists(select 1 from Usuario where login = @login and [passwordProvisorio] = @pass and activo = 1)
 set @ret = 2

select @ret

GO
/****** Object:  StoredProcedure [dbo].[usp_verificaPermisoUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_verificaPermisoUsuario]  
 @login VARCHAR(50)  
 ,@tipoPermiso VARCHAR(50)  
 ,@elemento VARCHAR(50)  
 ,@idSistema int
AS  
BEGIN  
  
 SET NOCOUNT ON;  
  
 SELECT U.[idUsuario], [login], [password], [nombre], [email], [telefono]  
 FROM usuario U join UsuarioRol UR on U.idUsuario = UR.idUsuario  
	   join UsuarioSistema US on U.idUsuario = US.idUsuario
       join Rol R on UR.idRol = R.idRol  
       join RolPermiso RP on R.idRol = RP.idRol  
       join Permiso P on RP.idPermiso = P.idPermiso  
       and P.idSistema = US.idSistema
 WHERE U.activo = 1
    AND P.tipoPermiso = @tipoPermiso  
   AND P.nombreElemento = @elemento  
   AND U.login = @login  
   AND US.idSistema = @idSistema
END  

GO
/****** Object:  StoredProcedure [dbo].[usp_verificarPassword]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_verificarPassword] 
  @login VARCHAR(50)        , 
  @pass varchar(50)    
as

declare @ret int  
set @ret = 0  

		if exists(select 1 from Usuario where login = @login and [password] = @pass and activo = 1)    
					set @ret = 1    

		if exists(select 1 from Usuario where login = @login and [passwordProvisorio] = @pass and activo = 1)  
					set @ret = 2  
		  
		select @ret
	


GO
/****** Object:  StoredProcedure [dbo].[usp_verificarPasswordUsuarioUnion]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_verificarPasswordUsuarioUnion]      
  @login VARCHAR(50)      
AS      
     
  SET NOCOUNT ON;      
    
 
   SELECT login FROM usuario WHERE login =   @login 




GO
/****** Object:  StoredProcedure [dbo].[usp_VerificoMensajeUsuarioSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[usp_VerificoMensajeUsuarioSistema]
@Usuario varchar (50) = '' , 
@idsistema int = 0 
as

set nocount on

if exists(
				select 1
				from Mensaje MSN
				inner join MensajeUsuario as MSNU on
				MSN.idmensaje = MSNU.idmensaje
				left join Usuario US on
				MSNU.idUsuario = US.idUsuario
				where US.login = @Usuario
				and idSistema = @idsistema 
				and mostrar = 1
)
begin
				select mensaje
				from Mensaje MSN
				inner join MensajeUsuario as MSNU on
				MSN.idmensaje = MSNU.idmensaje
				left join Usuario US on
				MSNU.idUsuario = US.idUsuario
				where US.login = @Usuario
				and idSistema = @idsistema 
				and mostrar = 1


				update MSNU set
				mostrar = 0
				from Mensaje MSN
				inner join MensajeUsuario as MSNU on
				MSN.idmensaje = MSNU.idmensaje
				left join Usuario US on
				MSNU.idUsuario = US.idUsuario
				where US.login = @Usuario
				and idSistema = @idsistema 
				and mostrar = 1
end
else
begin

	select ''  as MSN
end		



GO
/****** Object:  Table [dbo].[Area]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Area](
	[AreaId] [int] IDENTITY(1,1) NOT NULL,
	[AreaNombre] [varchar](100) NOT NULL,
	[AreaEstado] [bit] NOT NULL,
	[AreaFechaingreso] [smalldatetime] NOT NULL,
	[AreaUsuarioIngreso] [varchar](50) NOT NULL,
	[AreaFechaModificacion] [smalldatetime] NOT NULL,
	[AreaUsuarioModificacion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Conexion]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Conexion](
	[idConexion] [int] IDENTITY(1,1) NOT NULL,
	[idSistema] [int] NOT NULL,
	[ipPeticion] [varchar](50) NOT NULL,
	[ipCliente] [varchar](50) NOT NULL,
	[esProxy] [bit] NOT NULL,
	[usuario] [varchar](50) NOT NULL,
	[autenticacion] [varchar](50) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[navegador] [varchar](100) NOT NULL,
	[host] [varchar](50) NOT NULL,
	[sessionId] [varchar](50) NULL,
	[fechaFinSession] [datetime] NULL,
 CONSTRAINT [PK_Conexion] PRIMARY KEY CLUSTERED 
(
	[idConexion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empresa](
	[idEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[rut] [varchar](50) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[ciudad] [varchar](50) NOT NULL,
	[codigoSucursal] [varchar](10) NULL,
	[rutaLogo] [varchar](100) NULL,
	[giro] [varchar](50) NULL,
	[fono] [varchar](50) NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[idEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ErrorSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorSistema](
	[IdError] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdSistema] [int] NOT NULL,
	[LoginUsuario] [varchar](50) NOT NULL,
	[Url] [varchar](200) NOT NULL,
	[IPUsuario] [varchar](20) NOT NULL,
	[IPHost] [varchar](100) NOT NULL,
	[MensajeError] [ntext] NOT NULL,
	[FechaError] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdError] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Hoja1$]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hoja1$](
	[IdError] [float] NULL,
	[IdUsuario] [float] NULL,
	[IdSistema] [float] NULL,
	[LoginUsuario] [nvarchar](255) NULL,
	[Url] [nvarchar](255) NULL,
	[IPUsuario] [nvarchar](255) NULL,
	[IPHost] [nvarchar](255) NULL,
	[MensajeError] [nvarchar](max) NULL,
	[FechaError] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ImpresoraServidor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImpresoraServidor](
	[idImpresoraServidor] [int] IDENTITY(1,1) NOT NULL,
	[nombreImpresoraServidor] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ImpresoraServidor] PRIMARY KEY CLUSTERED 
(
	[idImpresoraServidor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ImpresoraSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImpresoraSistema](
	[idSistema] [int] NOT NULL,
	[nombreEquipoLocal] [varchar](50) NOT NULL,
	[nombreImpresora] [varchar](50) NOT NULL,
	[referencia] [varchar](50) NULL,
 CONSTRAINT [PK_ImpresoraSistema] PRIMARY KEY CLUSTERED 
(
	[idSistema] ASC,
	[nombreEquipoLocal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mensaje]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mensaje](
	[idMensaje] [int] IDENTITY(1,1) NOT NULL,
	[mensaje] [varchar](5000) NOT NULL,
	[habilitado] [bit] NOT NULL,
 CONSTRAINT [PK_Mensaje] PRIMARY KEY CLUSTERED 
(
	[idMensaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MensajeUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MensajeUsuario](
	[idMensaje] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idSistema] [int] NOT NULL,
	[mostrar] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Menu]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Menu](
	[idSistema] [int] NOT NULL,
	[menuId] [int] NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[padreId] [int] NULL,
	[posicion] [int] NULL,
	[icono] [varchar](100) NULL,
	[habilitado] [bit] NOT NULL,
	[ruta] [varchar](200) NOT NULL,
	[pagina] [varchar](200) NULL,
	[parametros] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permiso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permiso](
	[idPermiso] [int] IDENTITY(1,1) NOT NULL,
	[idSistema] [int] NOT NULL,
	[tipoPermiso] [varchar](50) NOT NULL,
	[nombreElemento] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Permiso] PRIMARY KEY CLUSTERED 
(
	[idPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[idSistema] [int] NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RolPermiso]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolPermiso](
	[idRol] [int] NOT NULL,
	[idPermiso] [int] NOT NULL,
 CONSTRAINT [PK_RolPermiso] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC,
	[idPermiso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RolSucursal]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolSucursal](
	[IdRolSucursal] [int] IDENTITY(1,1) NOT NULL,
	[IdRol] [int] NULL,
	[IdSucursal] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdRolSucursal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sistema](
	[idSistema] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[logo] [varchar](100) NULL,
 CONSTRAINT [PK_Sistema] PRIMARY KEY CLUSTERED 
(
	[idSistema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SistemaEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SistemaEmpresa](
	[idSistema] [int] NOT NULL,
	[idEmpresa] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SucursalTemp]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SucursalTemp](
	[idSucursal] [int] NOT NULL,
	[codigoSucursal] [varchar](10) NOT NULL,
	[rutSucursal] [varchar](20) NULL,
	[nombre] [varchar](60) NOT NULL,
	[direccion] [varchar](60) NOT NULL,
	[ciudad] [varchar](60) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ToolTipMensaje]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ToolTipMensaje](
	[idToolTipMensaje] [int] IDENTITY(1,1) NOT NULL,
	[mensaje] [varchar](2000) NOT NULL,
	[habilitado] [bit] NOT NULL,
	[pagina] [varchar](50) NULL,
	[controlId] [varchar](50) NULL,
	[posicion] [varchar](50) NULL,
 CONSTRAINT [PK_ToolTipMensaje] PRIMARY KEY CLUSTERED 
(
	[idToolTipMensaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ToolTipMensajeUsuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ToolTipMensajeUsuario](
	[idToolTipMensajeUsuario] [int] IDENTITY(1,1) NOT NULL,
	[idToolTipMensaje] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idSistema] [int] NOT NULL,
	[habilitado] [bit] NOT NULL,
 CONSTRAINT [PK_ToolTipMensajeUsuario] PRIMARY KEY CLUSTERED 
(
	[idToolTipMensajeUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[login] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[telefono] [varchar](20) NULL,
	[telefono2] [varchar](20) NULL,
	[passwordProvisorio] [varchar](50) NOT NULL,
	[activo] [bit] NOT NULL,
	[AreaId] [int] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuarioEmpresa]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioEmpresa](
	[idUsuario] [int] NOT NULL,
	[idEmpresa] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioEmpresa] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC,
	[idEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioRol]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioRol](
	[idusuario] [int] NOT NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioRol] PRIMARY KEY CLUSTERED 
(
	[idusuario] ASC,
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioSistema]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioSistema](
	[idUsuario] [int] NOT NULL,
	[idSistema] [int] NOT NULL,
	[fechaConeccion] [datetime] NULL,
	[conectado] [bit] NOT NULL,
	[esSupervisor] [bit] NOT NULL,
 CONSTRAINT [PK_UsuarioSistema] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC,
	[idSistema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioSistemaSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioSistemaSupervisor](
	[idSistema] [int] NOT NULL,
	[idUsuarioSupervisor] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioSistemaSupervisor] PRIMARY KEY CLUSTERED 
(
	[idSistema] ASC,
	[idUsuarioSupervisor] ASC,
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioSupervisor]    Script Date: 21/11/2013 14:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioSupervisor](
	[idUsuario] [int] NOT NULL,
	[idUsuarioSupervisor] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioSupervisor] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC,
	[idUsuarioSupervisor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Area] ON 

INSERT [dbo].[Area] ([AreaId], [AreaNombre], [AreaEstado], [AreaFechaingreso], [AreaUsuarioIngreso], [AreaFechaModificacion], [AreaUsuarioModificacion]) VALUES (1, N'NO INFORMADO', 1, CAST(0xA2750208 AS SmallDateTime), N'srivera', CAST(0xA2750208 AS SmallDateTime), N'srivera')
INSERT [dbo].[Area] ([AreaId], [AreaNombre], [AreaEstado], [AreaFechaingreso], [AreaUsuarioIngreso], [AreaFechaModificacion], [AreaUsuarioModificacion]) VALUES (2, N'GERENCIA', 1, CAST(0xA275020A AS SmallDateTime), N'srivera', CAST(0xA275020A AS SmallDateTime), N'srivera')
INSERT [dbo].[Area] ([AreaId], [AreaNombre], [AreaEstado], [AreaFechaingreso], [AreaUsuarioIngreso], [AreaFechaModificacion], [AreaUsuarioModificacion]) VALUES (3, N'ADMINISTRACION', 1, CAST(0xA275020B AS SmallDateTime), N'srivera', CAST(0xA275020B AS SmallDateTime), N'srivera')
INSERT [dbo].[Area] ([AreaId], [AreaNombre], [AreaEstado], [AreaFechaingreso], [AreaUsuarioIngreso], [AreaFechaModificacion], [AreaUsuarioModificacion]) VALUES (4, N'RRHH', 1, CAST(0xA275020B AS SmallDateTime), N'srivera', CAST(0xA275020B AS SmallDateTime), N'srivera')
INSERT [dbo].[Area] ([AreaId], [AreaNombre], [AreaEstado], [AreaFechaingreso], [AreaUsuarioIngreso], [AreaFechaModificacion], [AreaUsuarioModificacion]) VALUES (5, N'INFORMATICA', 1, CAST(0xA2750249 AS SmallDateTime), N'srivera', CAST(0xA2750249 AS SmallDateTime), N'srivera')
SET IDENTITY_INSERT [dbo].[Area] OFF
SET IDENTITY_INSERT [dbo].[Empresa] ON 

INSERT [dbo].[Empresa] ([idEmpresa], [rut], [nombre], [direccion], [ciudad], [codigoSucursal], [rutaLogo], [giro], [fono]) VALUES (1, N'', N'T-GENESIS', N'Av. ALberto Hurtado', N'Santiago', N'01', NULL, N'informatica', N'99999')
SET IDENTITY_INSERT [dbo].[Empresa] OFF
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 1, N'Cuentas, Permisos y Roles', 1, 10, N'', 1, N'', NULL, NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 2, N'Crear Usuario', 1, 1, N'user.png', 1, N'', N'wfCrearUsuario.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 3, N'Crear Rol', 1, 2, N'cost.png', 1, N'', N'wfCrearRol.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 4, N'Crear Permiso', 1, 3, N'administrative-docs.png', 1, N'', N'wfAgregarPermiso.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 5, N'Asignar Rol', 1, 4, N'config.png', 1, N'', N'wfAsignaRol.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 6, N'Asignar Rol y Permiso', 1, 5, N'my-account.png', 1, N'', N'wfRolPermisos.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 7, N'Administracion', 7, 0, N'', 1, N'', NULL, NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 8, N'Tipo de WorkFlow', 7, 0, N'tipoworkflow.png', 1, N'~/Mantenedores/', N'wfTipoWorkFlow.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 8, N'Crear WorkFlow', 7, 0, N'workflow.png', 1, N'~/CrearWorkFlow/', N'wfCrearWorkFlow.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 10, N'Tareas Pendientes', 7, 2, N'tareas.png', 1, N'~/CrearWorkFlow/', N'wfWorkFlowPorUsuario.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 11, N'Area', 1, 0, N'area.png', 1, N'~/Mantenedores/', N'wfArea.aspx', NULL)
INSERT [dbo].[Menu] ([idSistema], [menuId], [descripcion], [padreId], [posicion], [icono], [habilitado], [ruta], [pagina], [parametros]) VALUES (1, 12, N'Contrato', 7, 8, N'contrato.png', 1, N'~/Contratos/', N'wfContratoDetalle.aspx', NULL)
SET IDENTITY_INSERT [dbo].[Permiso] ON 

INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (1, 1, N'PAGINA', N'wfCrearUsuario.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (2, 1, N'PAGINA', N'wfCrearRol.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (3, 1, N'PAGINA', N'wfAgregarPermiso.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (4, 1, N'PAGINA', N'wfAsignaRol.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (5, 1, N'PAGINA', N'wfRolPermisos.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (6, 1, N'PAGINA', N'wfLogin.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (7, 1, N'PAGINA', N'wfPrincipal.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (8, 1, N'PAGINA', N'wfTipoWorkFlow.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (9, 1, N'PAGINA', N'wfCrearWorkFlow.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (10, 1, N'PAGINA', N'wfWorkFlowPorUsuario.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (11, 1, N'PAGINA', N'wfArea.aspx')
INSERT [dbo].[Permiso] ([idPermiso], [idSistema], [tipoPermiso], [nombreElemento]) VALUES (12, 1, N'PAGINA', N'wfContratoDetalle.aspx')
SET IDENTITY_INSERT [dbo].[Permiso] OFF
SET IDENTITY_INSERT [dbo].[Rol] ON 

INSERT [dbo].[Rol] ([idRol], [descripcion], [idSistema]) VALUES (1, N'Administrador', 1)
INSERT [dbo].[Rol] ([idRol], [descripcion], [idSistema]) VALUES (2, N'VENTA', 1)
SET IDENTITY_INSERT [dbo].[Rol] OFF
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 1)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 2)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 3)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 4)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 5)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 6)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 7)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 8)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 9)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 10)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 11)
INSERT [dbo].[RolPermiso] ([idRol], [idPermiso]) VALUES (1, 12)
SET IDENTITY_INSERT [dbo].[Sistema] ON 

INSERT [dbo].[Sistema] ([idSistema], [nombre], [logo]) VALUES (1, N'FHC Administracion', N'')
SET IDENTITY_INSERT [dbo].[Sistema] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([idUsuario], [login], [password], [nombre], [email], [telefono], [telefono2], [passwordProvisorio], [activo], [AreaId]) VALUES (1, N'srivera', N'3fcd5b550fb288da2d1cb55fac0daaf5', N'Sebastian Rivera Muñoz', N'srivera@t-genesis.cl', NULL, NULL, N'C1B0980A-8960-49C2-9179-233D3486E6EC', 1, 5)
INSERT [dbo].[Usuario] ([idUsuario], [login], [password], [nombre], [email], [telefono], [telefono2], [passwordProvisorio], [activo], [AreaId]) VALUES (2, N'jorge.anticoi', N'5812f90a5db717abd267543973b61e6e', N'Jorge Anticoi', N'janticoi@fhc.cl', N'', NULL, N'6f8056357f8163c37d13922f725d0b03', 1, 1)
INSERT [dbo].[Usuario] ([idUsuario], [login], [password], [nombre], [email], [telefono], [telefono2], [passwordProvisorio], [activo], [AreaId]) VALUES (3, N'gabo', N'03e560ef8f9970ec58f5eeaa4e425db7', N'Gabriel Nuñez', N'gnunez@t-genesis.cl', N'', NULL, N'1FD3F59D-FBA0-4BB9-88F4-EA5B6D261109', 1, 5)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
INSERT [dbo].[UsuarioEmpresa] ([idUsuario], [idEmpresa]) VALUES (1, 1)
INSERT [dbo].[UsuarioEmpresa] ([idUsuario], [idEmpresa]) VALUES (2, 1)
INSERT [dbo].[UsuarioEmpresa] ([idUsuario], [idEmpresa]) VALUES (3, 1)
INSERT [dbo].[UsuarioRol] ([idusuario], [idRol]) VALUES (1, 1)
INSERT [dbo].[UsuarioRol] ([idusuario], [idRol]) VALUES (1, 2)
INSERT [dbo].[UsuarioRol] ([idusuario], [idRol]) VALUES (2, 1)
INSERT [dbo].[UsuarioRol] ([idusuario], [idRol]) VALUES (3, 1)
INSERT [dbo].[UsuarioRol] ([idusuario], [idRol]) VALUES (3, 2)
INSERT [dbo].[UsuarioSistema] ([idUsuario], [idSistema], [fechaConeccion], [conectado], [esSupervisor]) VALUES (1, 1, NULL, 0, 0)
INSERT [dbo].[UsuarioSistema] ([idUsuario], [idSistema], [fechaConeccion], [conectado], [esSupervisor]) VALUES (2, 1, NULL, 0, 1)
INSERT [dbo].[UsuarioSistema] ([idUsuario], [idSistema], [fechaConeccion], [conectado], [esSupervisor]) VALUES (3, 1, NULL, 0, 1)
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_login]    Script Date: 21/11/2013 14:06:52 ******/
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [IX_login] UNIQUE NONCLUSTERED 
(
	[login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Mensaje] ADD  CONSTRAINT [DF_Mensaje_habilitado]  DEFAULT ((1)) FOR [habilitado]
GO
ALTER TABLE [dbo].[MensajeUsuario] ADD  CONSTRAINT [DF_MensajeUsuario_mostrar]  DEFAULT ((1)) FOR [mostrar]
GO
ALTER TABLE [dbo].[ToolTipMensajeUsuario] ADD  CONSTRAINT [DF_ToolTipMensajeUsuario_habilitado]  DEFAULT ((1)) FOR [habilitado]
GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_cambiarPassword]  DEFAULT ((1)) FOR [passwordProvisorio]
GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_activo]  DEFAULT ((1)) FOR [activo]
GO
ALTER TABLE [dbo].[UsuarioSistema] ADD  CONSTRAINT [DF_UsuarioSistema_conectado]  DEFAULT ((0)) FOR [conectado]
GO
ALTER TABLE [dbo].[UsuarioSistema] ADD  CONSTRAINT [DF_UsuarioSistema_esSupervisor]  DEFAULT ((0)) FOR [esSupervisor]
GO
ALTER TABLE [dbo].[Conexion]  WITH CHECK ADD  CONSTRAINT [FK_Conexion_Sistema] FOREIGN KEY([idSistema])
REFERENCES [dbo].[Sistema] ([idSistema])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Conexion] CHECK CONSTRAINT [FK_Conexion_Sistema]
GO
ALTER TABLE [dbo].[MensajeUsuario]  WITH CHECK ADD  CONSTRAINT [FK_MensajeUsuario_Mensaje] FOREIGN KEY([idMensaje])
REFERENCES [dbo].[Mensaje] ([idMensaje])
GO
ALTER TABLE [dbo].[MensajeUsuario] CHECK CONSTRAINT [FK_MensajeUsuario_Mensaje]
GO
ALTER TABLE [dbo].[MensajeUsuario]  WITH NOCHECK ADD  CONSTRAINT [FK_MensajeUsuario_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[MensajeUsuario] CHECK CONSTRAINT [FK_MensajeUsuario_Usuario]
GO
ALTER TABLE [dbo].[Menu]  WITH CHECK ADD  CONSTRAINT [FK_Menu_Sistema] FOREIGN KEY([idSistema])
REFERENCES [dbo].[Sistema] ([idSistema])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Menu] CHECK CONSTRAINT [FK_Menu_Sistema]
GO
ALTER TABLE [dbo].[Rol]  WITH NOCHECK ADD  CONSTRAINT [FK_Rol_Sistema] FOREIGN KEY([idSistema])
REFERENCES [dbo].[Sistema] ([idSistema])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Rol] CHECK CONSTRAINT [FK_Rol_Sistema]
GO
ALTER TABLE [dbo].[RolPermiso]  WITH NOCHECK ADD  CONSTRAINT [FK_RolPermiso_Permiso] FOREIGN KEY([idPermiso])
REFERENCES [dbo].[Permiso] ([idPermiso])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolPermiso] CHECK CONSTRAINT [FK_RolPermiso_Permiso]
GO
ALTER TABLE [dbo].[RolPermiso]  WITH NOCHECK ADD  CONSTRAINT [FK_RolPermiso_Rol] FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolPermiso] CHECK CONSTRAINT [FK_RolPermiso_Rol]
GO
ALTER TABLE [dbo].[RolSucursal]  WITH CHECK ADD  CONSTRAINT [Fk_Rol_RolSucursal] FOREIGN KEY([IdRol])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[RolSucursal] CHECK CONSTRAINT [Fk_Rol_RolSucursal]
GO
ALTER TABLE [dbo].[UsuarioEmpresa]  WITH NOCHECK ADD  CONSTRAINT [FK_UsuarioEmpresa_Empresa] FOREIGN KEY([idEmpresa])
REFERENCES [dbo].[Empresa] ([idEmpresa])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioEmpresa] CHECK CONSTRAINT [FK_UsuarioEmpresa_Empresa]
GO
ALTER TABLE [dbo].[UsuarioEmpresa]  WITH NOCHECK ADD  CONSTRAINT [FK_UsuarioEmpresa_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioEmpresa] CHECK CONSTRAINT [FK_UsuarioEmpresa_Usuario]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH NOCHECK ADD  CONSTRAINT [FK_UsuarioRol_Rol] FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_UsuarioRol_Rol]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH NOCHECK ADD  CONSTRAINT [FK_UsuarioRol_Usuario] FOREIGN KEY([idusuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_UsuarioRol_Usuario]
GO
ALTER TABLE [dbo].[UsuarioSistema]  WITH NOCHECK ADD  CONSTRAINT [FK_UsuarioSistema_Sistema] FOREIGN KEY([idSistema])
REFERENCES [dbo].[Sistema] ([idSistema])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioSistema] CHECK CONSTRAINT [FK_UsuarioSistema_Sistema]
GO
ALTER TABLE [dbo].[UsuarioSistema]  WITH NOCHECK ADD  CONSTRAINT [FK_UsuarioSistema_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioSistema] CHECK CONSTRAINT [FK_UsuarioSistema_Usuario]
GO
ALTER TABLE [dbo].[UsuarioSistemaSupervisor]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioSistemaSupervisor_UsuarioSistema] FOREIGN KEY([idUsuario], [idSistema])
REFERENCES [dbo].[UsuarioSistema] ([idUsuario], [idSistema])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UsuarioSistemaSupervisor] CHECK CONSTRAINT [FK_UsuarioSistemaSupervisor_UsuarioSistema]
GO
ALTER TABLE [dbo].[UsuarioSupervisor]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioSupervisor_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[UsuarioSupervisor] CHECK CONSTRAINT [FK_UsuarioSupervisor_Usuario]
GO
ALTER TABLE [dbo].[UsuarioSupervisor]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioSupervisor_Usuario1] FOREIGN KEY([idUsuarioSupervisor])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[UsuarioSupervisor] CHECK CONSTRAINT [FK_UsuarioSupervisor_Usuario1]
GO
