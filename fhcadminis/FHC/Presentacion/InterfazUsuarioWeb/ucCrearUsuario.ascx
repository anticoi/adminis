﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCrearUsuario.ascx.cs" Inherits="ControlUsuario.Controles.ucCrearUsuario" %>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
    <table style="text-align:left;">
    <tr>
        <td style= "padding-right:70px">
            <asp:Label Text="Usuario" ID="lblUsuario" runat="server" AssociatedControlID="txtUsuario" /></td>
        <td><asp:TextBox runat="server" ID="txtUsuario"></asp:TextBox></td>
        <td><asp:Button runat="server" ID="btnBuscarUsuario" Text="Buscar" 
                onclick="btnBuscarUsuario_Click" /></td>
    
    </tr>
    </table>
    <asp:UpdatePanel ID="upDatosUsuario" runat="server">
    <ContentTemplate>
    <table style="text-align:left">
    <tr>
    <td><asp:CheckBox runat="server" ID="chkEmail" Text="Email" AutoPostBack="True" 
            oncheckedchanged="chkEmail_CheckedChanged" /></td>
    <td><asp:TextBox runat="server" ID="txtEmail" Width="250px"></asp:TextBox></td>
    <td><asp:LinkButton runat="server" ID="lnkVerificarEmail" Text="Verificar email" 
            onclick="lnkVerificarEmail_Click"></asp:LinkButton></td>
    </tr>
    <tr>
    <td><asp:Label Text="Supervisor" ID="lblSupervisor" runat="server" AssociatedControlID="ddlSupervisor" /></td>
    <td colspan="2"><div class="select" style="width:250px">
        <asp:DropDownList ID="ddlSupervisor" style="width:270px" runat="server" DataTextField="loginNombre" 
            DataValueField="idUsuario">
        </asp:DropDownList></div>


        <asp:LinkButton ID="lnkCambiarOrdenSupervisor0" runat="server" 
            onclick="lnkCambiarOrdenSupervisor_Click" Text="Cambiar Orden" Visible="False"></asp:LinkButton>
        </td>
        <td>&nbsp;</td>
    </tr>
    
    <tr>
                <td><asp:Label runat="server" ID="lblEmpresa" Text="Empresa" AssociatedControlID="ddlEmpresa"></asp:Label></td>
                <td colspan="3"><div class="select" style="width:250px;">
                <asp:DropDownList runat="server" ID="ddlEmpresa" style="width:270px"
                        DataTextField="nombre" DataValueField="idEmpresa"></asp:DropDownList></div>
                </td>
    </tr>
    <tr>
                <td>
                        <asp:Label runat="server" ID="lblarea" Text="Area" AssociatedControlID="ddlArea"></asp:Label>
                </td>
                <td colspan="3">
                    <div class="select" style="width:250px;">
                            <asp:DropDownList runat="server" ID="ddlArea" style="width:270px"
                        DataTextField="AreaNombre" DataValueField="AreaId"></asp:DropDownList></div>
                </td>
        
    </tr>
    <tr>
    <td>
        <asp:Label Text="Nombre Completo" ID="lblNombreCompleto" runat="server" AssociatedControlID="txtNombreCompleto" /> </td>
    <td colspan="3"><asp:TextBox runat="server" ID="txtNombreCompleto" Width="318px"></asp:TextBox></td>
    </tr>
    <tr>
    <td>
        <asp:Label Text="Fono" ID="lblFono" runat="server" AssociatedControlID="txtFono" /> </td>
    <td>
        <asp:TextBox ID="txtFono" runat="server"></asp:TextBox>
        </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>
        <asp:CheckBox ID="chkEsSupervisor" runat="server" Text="Es Supervisor" />
        </td>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    
    </tr>
    </table>
    
    </ContentTemplate>
    </asp:UpdatePanel>

   
    <table style="width:100%">
    <tr>
    <td style="text-align:center"><asp:Label runat="server" ID="lblMsg" Text="" CssClass="textoAlerta"></asp:Label></td>
    </tr>
    </table>
    <table>
    <tr>
    <td><asp:Button runat="server" ID="btnAsociar" 
            Text="Asociar este usuario al sistema actual" CssClass="boton" 
            onclick="btnAsociar_Click" Visible="False" /></td>
    <td><asp:Button runat="server" ID="btnGrabar" Text="Grabar" CssClass="boton" 
            onclick="btnGrabar_Click" /></td>
    <td>&nbsp; 
        <asp:Button runat="server" ID="btnAsignarRol" Text="Asignar Roles" 
            CssClass="boton" Enabled="False" onclick="btnAsignarRol_Click" /></td>
    </tr>
    </table>
</td>
</tr>

</table>