﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;
using TGENESIS.FHC.EntidadesNegocio;
using System.Data;
using TGENESIS.Framework.Utilidades;
using System.IO;

namespace InterfazUsuarioWeb.CrearWorkFlow
{
    public partial class wfMisSolicitudes : System.Web.UI.Page
    {



        string NombreArchivo;
        static string archivo;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");
                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 18);
                lTitulo.Text = NombreMenu.ToString();
                CargarCombos();
                ListaWorkFlowPorUsuario(Context.User.Identity.Name);

            }
        }

        protected void ListaWorkFlowPorUsuario(string Usuario)
        {

            try
            {


                grvworkflowusuario.Columns[4].Visible = true;
                grvworkflowusuario.Columns[5].Visible = true;
                grvworkflowusuario.Columns[6].Visible = true;
                grvworkflowusuario.Columns[7].Visible = true;
                

                grvworkflowusuario.DataSource = NegocioWorkFlow.Instancia.ListaWorkFlowSolicitadosPorUsuario (Usuario, int.Parse(ddlbEstadoSolicitud.SelectedValue.ToString()));
                grvworkflowusuario.DataKeyNames = new string[] { "WorkFlowId" };
                grvworkflowusuario.DataBind();



                if (int.Parse(ddlbEstadoSolicitud.SelectedValue.ToString()) == 1)
                {
                    btnTerminar.Visible = true;
                    grvworkflowusuario.Columns[4].Visible = false;
                    grvworkflowusuario.Columns[5].Visible = true;
                    grvworkflowusuario.Columns[6].Visible = true;

                    foreach (GridViewRow fila in grvworkflowusuario.Rows)
                    {
                        LinkButton LnBSerie = (LinkButton)fila.Cells[0].FindControl("LnBAccesorios");
                        LnBSerie.ForeColor = System.Drawing.Color.White;
                        fila.Cells[0].ForeColor = System.Drawing.Color.White;
                        fila.Cells[1].ForeColor = System.Drawing.Color.White;
                        fila.Cells[2].ForeColor = System.Drawing.Color.White;
                        fila.Cells[3].ForeColor = System.Drawing.Color.White;
                        fila.Cells[7].ForeColor = System.Drawing.Color.White;

                        if (int.Parse(fila.Cells[7].Text.ToString()) <= 3)
                        {
                            fila.Cells[0].BackColor = System.Drawing.Color.Green;
                            fila.Cells[1].BackColor = System.Drawing.Color.Green;
                            fila.Cells[2].BackColor = System.Drawing.Color.Green;
                            fila.Cells[3].BackColor = System.Drawing.Color.Green;
                            fila.Cells[7].BackColor = System.Drawing.Color.Green;
                        }
                        if (int.Parse(fila.Cells[7].Text.ToString()) >= 4 && int.Parse(fila.Cells[7].Text.ToString()) <= 9)
                        {
                            fila.Cells[0].BackColor = System.Drawing.Color.Orange;
                            fila.Cells[1].BackColor = System.Drawing.Color.Orange;
                            fila.Cells[2].BackColor = System.Drawing.Color.Orange;
                            fila.Cells[3].BackColor = System.Drawing.Color.Orange;
                            fila.Cells[7].BackColor = System.Drawing.Color.Orange;
                        }

                        if (int.Parse(fila.Cells[7].Text.ToString()) >= 10)
                        {
                            fila.Cells[0].BackColor = System.Drawing.Color.Red;
                            fila.Cells[1].BackColor = System.Drawing.Color.Red;
                            fila.Cells[2].BackColor = System.Drawing.Color.Red;
                            fila.Cells[3].BackColor = System.Drawing.Color.Red;
                            fila.Cells[7].BackColor = System.Drawing.Color.Red;
                        }

                    }

                }
                else
                {
                    btnTerminar.Visible = false;
                    grvworkflowusuario.Columns[4].Visible = false;
                    grvworkflowusuario.Columns[5].Visible = false;
                    grvworkflowusuario.Columns[6].Visible = false;
                    grvworkflowusuario.Columns[7].Visible = false;
                }

 
                grvworkflowusuario.Columns[5].Visible = false;
 
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }


        }

        protected void btnTerminar_Click(object sender, EventArgs e)
        {

            try
            {
                CheckBox check;
                LinkButton texto;
                int id;
                int Elimino;
                Elimino = 0;
                foreach (GridViewRow fila in grvworkflowusuario.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");
                    texto = (LinkButton)fila.Cells[1].FindControl("LnBAccesorios");

                    if (check.Checked == true)
                    {
                        id = int.Parse(grvworkflowusuario.DataKeys[fila.RowIndex]["WorkFlowId"].ToString());
                        hdffinaliza.Value = "1";
                        //CargarDetalleTarea(id);

                        if (hdffinaliza.Value == "1")
                        {
                            WorkFlow wf = new WorkFlow();
                            wf.WorkFlowId = id;
                            wf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;
                            NegocioWorkFlow.Instancia.FinalizaWorkFlow(wf);
                            DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(id);
                            if (dtWorkFlow != null)
                            {
                                string UsuarioFinaliza = Context.User.Identity.Name;
                                string UsuarioCreo = dtWorkFlow.Rows[0]["UsuarioCreador"].ToString(); ;
                                string Observacion = dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString(); ;
                                string Titulo = dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString(); ;
                                int idWorkFlow = id;

                                EnvioAvisoFinalizoTarea(UsuarioFinaliza, UsuarioCreo, Titulo, Observacion);
                            }

                        }

                        Elimino = Elimino + 1;
                    }
                }

                if (Elimino > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Finalizada", "La tarea fue finalizada con éxito.");
                    ListaWorkFlowPorUsuario(Context.User.Identity.Name);
                }
                else
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "No se actualizo la informacion.");
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }



        }

        protected void grvworkflowusuario_RowCommand(object sender, GridViewCommandEventArgs e)
        {



            #region Asignar o Reasignar Tarea
            if (e.CommandName == "veraccesorios3")
            {


                foreach (GridViewRow gvRow in grvworkflowusuario.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {

                        if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString()))
                        {
                            int id3 = int.Parse(grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString());
                            LinkButton LnBSerie2 = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios2");

                            hdfidworkflow.Value = id3.ToString();
                            lblasignara.Text = " Responder";
                            ddlbusuarioasignado.DataSource = NegocioReporteria.Instancia.ListarUsuarios();
                            ddlbusuarioasignado.DataValueField = "login";
                            ddlbusuarioasignado.DataTextField = "nombre";
                            ddlbusuarioasignado.DataBind();
                            ddlbusuarioasignado.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

                            DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(id3);

                            if (dtWorkFlow != null)
                            {
                                ddlbusuarioasignado.SelectedValue = dtWorkFlow.Rows[0]["usuariocreador"].ToString();
                                mpepanel2.Show();
                                return;
                            }
                        }
                    }
                }
            }
            #endregion Asignar o Reasignar Tarea


            #region Asignar o Reasignar Tarea
            if (e.CommandName == "veraccesorios2")
            {


                foreach (GridViewRow gvRow in grvworkflowusuario.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {

                        if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString()))
                        {
                            int id2 = int.Parse(grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString());
                            LinkButton LnBSerie2 = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios2");

                            hdfidworkflow.Value = id2.ToString();
                            lblasignara.Text = " Asignar";
                            ddlbusuarioasignado.DataSource = NegocioReporteria.Instancia.ListarUsuarios();
                            ddlbusuarioasignado.DataValueField = "login";
                            ddlbusuarioasignado.DataTextField = "nombre";
                            ddlbusuarioasignado.DataBind();
                            ddlbusuarioasignado.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

                            mpepanel2.Show();
                            return;
                        }
                    }
                }
            }
            #endregion Asignar o Reasignar Tarea


            #region Ver la Tarea
            if (e.CommandName == "veraccesorios")
            {

                foreach (GridViewRow gvRow in grvworkflowusuario.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {

                        if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString()))
                        {
                            int id = int.Parse(grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString());
                            LinkButton LnBSerie = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios");
                            hdffinaliza.Value = "0";
                            CargarDetalleTarea(id);
                        }
                    }

                }
            }
            #endregion Ver la Tarea
        }


        protected void CargarDetalleTarea(int Id)
        {

            try
            {

                lbltexttipo.Text = "";
                lbltexttitulo.Text = "";
                lbltextasignado.Text = "";
                lbltextcontrato.Text = "";
                txtobservacionfinalizar.Text = "";
                DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(Id);

                if (dtWorkFlow != null)
                {

                    hdfidworkflow.Value = Id.ToString();
                    lbltexttipo.Text = dtWorkFlow.Rows[0]["TipoWorkFDescripcion"].ToString();
                    lbltexttitulo.Text = dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString();
                    lbltextasignado.Text = dtWorkFlow.Rows[0]["nombre"].ToString();
                    lbltextcontrato.Text = dtWorkFlow.Rows[0]["WorkFlowContrato"].ToString();
                    txtobservacionfinalizar.Text = dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString();

                    WorkFlowAdj wfAdj = new WorkFlowAdj();
                    wfAdj.WorkFlowId = int.Parse(Id.ToString());
                    grvarchivos.DataSource = NegocioWorkFlowAdj.Instancia.Listar(wfAdj);
                    grvarchivos.DataBind();


                    grvhistorial.DataSource = NegocioReporteria.Instancia.VerHistorialAsignador(Id);
                    grvhistorial.DataBind();
                    trhistorialasignador.Visible = false;

                    if (grvhistorial.Rows.Count > 0)
                        trhistorialasignador.Visible = true;

 
                    if (hdffinaliza.Value == "0")
                        btnguardartarea.Visible = false; 
                    else
                        btnguardartarea.Visible = true; 

                    
                    mpepanel.Show();

                    if (hdffinaliza.Value == "0")
                        return;
                }

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {

            try
            {
                if (hdfidworkflow.Value != "")
                {
                    string UsuarioDeriva = Context.User.Identity.Name;
                    string UsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
                    string Observacion = txtobservacion.Text;
                    int idWorkFlow = int.Parse(hdfidworkflow.Value);

                    NegocioWorkFlow.Instancia.InsertarAsignaWorkFlow(idWorkFlow, UsuarioDeriva, Observacion, UsuarioAsignado);
                    DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(idWorkFlow);

                    if (dtWorkFlow != null)
                    {
                        EnvioAviso(UsuarioAsignado, UsuarioDeriva, dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString(), dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString());
                    }

                    ListaWorkFlowPorUsuario(UsuarioDeriva);

                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea " + lblasignara.Text, "La tarea fue " + lblasignara.Text + " con éxito.");

                }

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }


        }


        protected void EnvioAviso(string Usuario, string UsuarioCrea, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCrea);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCorreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Usuario);

                string[] to = new string[1];
                to[0] = oUsuarioCorreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Pendiente " + "\n" + " El usuario " + oUsuario.nombre + " acaba de " + lblasignara.Text + " la siguiente tarea : " + "\n" + Descripcion, Usuario, "", userMail, passMail);
                //Mail.EnviarMail(emailDe,to,"Adjunto","Archivo Adjunto","NN","prueba de envio",userMail, passMail,
            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }

        }


        protected void EnvioAvisoFinalizoTarea(string UsuarioFinaliza, string UsuarioCreo, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCreo);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioFinalizo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioFinaliza);

                string[] to = new string[1];
                to[0] = oUsuarioCreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Finalizada " + "\n" + " El usuario " + oUsuarioFinalizo.nombre + " acaba de finalizar la tarea : " + "\n" + Descripcion, UsuarioCreo, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }

        }


        protected void CargarCombos()
        {
            try
            {
                ddlbEstadoSolicitud.DataValueField = "EstadoWorkFlowId";
                ddlbEstadoSolicitud.DataTextField = "EstadoWorkFlowDescripcion";
                ddlbEstadoSolicitud.DataSource = NegocioEstadoWorkFlow.Instancia.Listar();
                ddlbEstadoSolicitud.DataBind();
                ddlbEstadoSolicitud.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

                ddlbEstadoSolicitud.SelectedValue = "1";
            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {

            if (int.Parse(ddlbEstadoSolicitud.SelectedValue.ToString()) > 0)
            {

                ListaWorkFlowPorUsuario(Context.User.Identity.Name);

            }
            else
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", "Debe seleccionar Pendiente o Finalizada");

        }



        protected void btnadjuntar_Click(object sender, EventArgs e)
        {
            try
            {

                if (ddlbEstadoSolicitud.SelectedValue.ToString() == "1")
                {

                    if (fuImagen.HasFile)
                    {
                        archivo = fuImagen.FileName;

                        string fileName = Server.HtmlEncode(fuImagen.FileName);
                        //string activeDir = Server.MapPath("/") + "/Adjuntos/" + hdfidworkflow.Value.ToString();
                        //string activeDir = Server.MapPath("/fhcadminis/") + "/Adjuntos/" + hdfidworkflow.Value.ToString();
                        string activeDir = @"\\172.110.10.5\adjuntos\" + hdfidworkflow.Value.ToString();


                        if (!(Directory.Exists(activeDir)))
                        {
                            Directory.CreateDirectory(activeDir);
                        }

                        NombreArchivo = activeDir + "/" + archivo;
                        fuImagen.SaveAs(NombreArchivo);

                        WorkFlowAdj wfadj = new WorkFlowAdj();

                        wfadj.WorkFlowId = int.Parse(hdfidworkflow.Value.ToString());
                        wfadj.WorkFlowAdjArchivo = archivo;
                        wfadj.WorkFlowAdjUsuarioIngreso = Context.User.Identity.Name;
                        wfadj.WorkFlowAdjUsuarioModificacion = Context.User.Identity.Name;

                        string ingresa = NegocioWorkFlowAdj.Instancia.Insertar(wfadj);
                        hdffinaliza.Value = "0";
                        CargarDetalleTarea(int.Parse(hdfidworkflow.Value.ToString()));
                    }

                }
                else
                {

                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Finalizada", "La tarea se encuentra finalizada, no se pueden realizar modificaciones.");
                }

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }

        }

        protected void grvarchivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {

                #region Ver Adjunto

                if (e.CommandName == "verarchivo")
                {

                    foreach (GridViewRow gvRow in grvarchivos.Rows)
                    {
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (e.CommandArgument.ToString() == gvRow.Cells[1].Text.ToString())
                            {
                                string Directorio = "";

                                Directorio = "../adjuntos/" + hdfidworkflow.Value.ToString() + "/" + gvRow.Cells[1].Text;

                                hdfimprimir.Value = Directorio;
                                string strClientscript_New = "window.open('" + hdfimprimir.Value.ToString() + "','informe','scrollbars =yes,status=no,toolbar=no,width=700,height=600');";
                                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "openPage", strClientscript_New, true);
                            }
                        }
                    }
                #endregion Ver Adjunto
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }

        }

        protected void btnguardartarea_Click(object sender, EventArgs e)
        {
            // aca finalizar

            try
            {

                CargarDetalleTarea(int.Parse(hdfidworkflow.Value.ToString()));

                WorkFlow wf = new WorkFlow();
                wf.WorkFlowId = int.Parse(hdfidworkflow.Value.ToString());
                wf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;
                NegocioWorkFlow.Instancia.FinalizaWorkFlow(wf);
                DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(int.Parse(hdfidworkflow.Value.ToString()));

                if (dtWorkFlow != null)
                {
                    string UsuarioFinaliza = Context.User.Identity.Name;
                    string UsuarioCreo = dtWorkFlow.Rows[0]["UsuarioCreador"].ToString(); ;
                    string Observacion = dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString(); ;
                    string Titulo = dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString(); ;
                    int idWorkFlow = int.Parse(hdfidworkflow.Value.ToString());

                    EnvioAvisoFinalizoTarea(UsuarioFinaliza, UsuarioCreo, Titulo, Observacion);
                }


                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Finalizada", "La tarea fue finalizada con éxito.");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

        }


        protected void EnvioAvisoModificacion(string UsuarioFinaliza, string UsuarioCreo, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCreo);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioFinalizo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioFinaliza);

                string[] to = new string[1];
                to[0] = oUsuarioCreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Modificada " + "\n" + " El usuario " + oUsuarioFinalizo.nombre + " acaba de modificar la tarea : " + "\n" + Descripcion, UsuarioCreo, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }


        }

        protected void grvarchivos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void hdfidworkflow_ValueChanged(object sender, EventArgs e)
        {

        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            mpepanel.Hide();
        }

    }

}