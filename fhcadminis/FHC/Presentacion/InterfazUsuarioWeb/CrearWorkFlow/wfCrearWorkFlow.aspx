﻿<%@ Page Title="FHC" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfCrearWorkFlow.aspx.cs" Inherits="InterfazUsuarioWeb.CrearWorkFlow.wfCrearWorkFlow" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
                function solonumeros(e) 
                {
                    var key;
                    if (window.event) // IE
                    {
                        key = e.keyCode;
                    }
                    else if (e.which) // Netscape/Firefox/Opera
                    {
                        key = e.which;
                    }

                    if (key < 48 || key > 57) {
                        return false;
                    }
                    return true;
                }

                $(function () {

                    $("#<%= txtFecha.ClientID %>").datepicker({
                        showSecond: true,
                        timeFormat: 'hh:mm:ss',
                        dateFormat: 'dd-mm-yy',
                        stepHour: 1,
                        stepMinute: 5,
                        minDate: new Date(2011, 08, 01, 8, 30),
                        changeMonth: true,
                        changeYear: true,
                        firstDay: 1,
                        stepSecond: 5,
                        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                        daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
                    });

                });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



<table width="800px" style="height:300px;">
    <tr>
    <td align="center">
            <table width="550px" >
                <tr>
                    <td align="left" valign="top" class="style1">
                            <asp:Label ID="lblcontrato" runat="server" Text="Contrato"></asp:Label>
                    </td>
                    <td class="style2">
                             
                            <asp:TextBox ID="txtcontrato" runat="server"  Text="" MaxLength="10" 
                                Width="100px"  AutoPostBack="true"
                                    onkeypress="javascript:return solonumeros(event)" 
                                    onunload="txtcontrato_Unload" ontextchanged="txtcontrato_TextChanged" ></asp:TextBox>
                             
                            <asp:Label ID= "LblVendedor" runat="server" Text=""></asp:Label>
                    </td>                
                </tr>

                <tr>
                    <td>
                            <asp:Label ID="Label1" runat="server" Text="Departamento"></asp:Label>
                    </td>
                    <td class="style3">

                                        <div class="select"   style="width:380px;"> 
                                                <asp:DropDownList ID="ddlbarea" runat="server" Width="400px" 
                                                    onselectedindexchanged="ddlbarea_SelectedIndexChanged" 
                                                    AutoPostBack="True" ></asp:DropDownList>
                                </div>
                    </td>
                </tr>

                <tr>
                    <td>
                            <asp:Label ID="lblasignar" runat="server" Text="Asignar a"></asp:Label>
                    </td>
                    <td class="style3">

                                        <div class="select"   style="width:380px;"> 
                                                <asp:DropDownList ID="ddlbusuarioasignado" runat="server" Width="400px" 
                                                    onselectedindexchanged="ddlbusuarioasignado_SelectedIndexChanged" ></asp:DropDownList>
                                </div>
                    </td>
                </tr>

                <tr>
                    <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lbltipoWorkFlow" runat="server" Text="Tipo tarea"></asp:Label>
                    </td>
                    <td class="style3">
                            <div class="select"   style="width:380px;"> 
                                    <asp:DropDownList ID="ddlbtipoworkflow" runat="server" Width="400px" 
                                        onselectedindexchanged="ddlbtipoworkflow_SelectedIndexChanged" ></asp:DropDownList>
                            </div>
                    </td>                
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:150px;">
                            <asp:CheckBox ID="chkFecha" runat="server" Text="¿Modifica Fecha?" 
                                oncheckedchanged="chkFecha_CheckedChanged" AutoPostBack="True" />
                    </td>
                    <td class="style3">
                    <asp:TextBox ID="txtFecha" runat="server"  Text="" MaxLength="10" 
                                Width="100px"  AutoPostBack="true" data-watermark="•"></asp:TextBox>
                    </td>                
                </tr>

                <tr>
                    <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lbltitulo" runat="server" Text="Titulo"></asp:Label>
                    </td>
                    <td align="left" class="style3">
                            <asp:TextBox ID="txttitulo" runat="server" Text="" MaxLength="20" Width="180px" ></asp:TextBox>
                    </td>                
                </tr>

                <tr>
                    <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblobservacion" runat="server" Text="Observación"></asp:Label>
                    </td>
                    <td class="style3">
                            <asp:TextBox ID="txtobservacion" runat="server" Text="" TextMode="MultiLine" Height="80px" Width="400px"></asp:TextBox>
                    </td>                
                </tr>



                <tr>
                    <td valign="top">
                            
                    </td>
                    <td align="right" class="style3">
                             <asp:Button ID="btnAgregar" runat="server" Text="Enviar" Width="100px"  
                                 Height="25px" class="button small blue" onclick="btnAgregar_Click" />
                    </td>                
                </tr>
            </table>
        </td>
    </tr>


    <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>



</asp:Content>
