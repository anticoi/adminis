﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.Framework.Utilidades;

namespace InterfazUsuarioWeb.CrearWorkFlow
{
    public partial class wfCrearWorkFlow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");
                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 9);
                lTitulo.Text = NombreMenu.ToString();
                //ListarTipoWorkFlow();

                CargarCombos("AREA");
                CargarCombos("USUARIO");
                CargarCombos("TAREA");
            }
        }

        #region Carga de Combos
        //protected void ListarTipoWorkFlow()
        //{

        //    try
        //    {

        //        List<TipoWorkFlow> twf = NegocioTipoWorkFlow.Instancia.Listar();

        //        ddlbtipoworkflow.DataSource = twf;
        //        ddlbtipoworkflow.DataValueField = "TipoWorkFlowId";
        //        ddlbtipoworkflow.DataTextField = "TipoWorkFDescripcion";
        //        ddlbtipoworkflow.DataBind();
        //        ddlbtipoworkflow.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));


        //        ddlbusuarioasignado.DataSource = NegocioReporteria.Instancia.ListarUsuarios();
        //        ddlbusuarioasignado.DataValueField = "login";
        //        ddlbusuarioasignado.DataTextField = "nombre";
        //        ddlbusuarioasignado.DataBind();
        //        ddlbusuarioasignado.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

        //        //Listar Area
        //        ddlbarea.DataSource = NegocioReporteria.Instancia.ListarArea();
        //        ddlbarea.DataValueField = "Areaid";
        //        ddlbarea.DataTextField = "AreaNombre";
        //        ddlbarea.DataBind();
        //        ddlbarea.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

        //    }
        //    catch (Exception ex)
        //    {
        //        ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
        //    }

        //}

        protected void CargarCombos(string Tipo)
        {
            if (Tipo == "AREA")
            {
                ddlbarea.DataTextField = "AreaNombre";
                ddlbarea.DataValueField = "AreaId";
                ddlbarea.DataSource = NegocioReporteria.Instancia.ListarArea();
                ddlbarea.DataBind();
                ddlbarea.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
            }

            if (Tipo == "USUARIO")
            {
                ddlbusuarioasignado.DataTextField = "nombre";
                ddlbusuarioasignado.DataValueField = "login";

                if (ddlbarea.SelectedValue.ToString() != "")
                    ddlbusuarioasignado.DataSource = NegocioReporteria.Instancia.ListarUsuariosArea(int.Parse(ddlbarea.SelectedValue.ToString()));
                else
                    ddlbusuarioasignado.DataSource = NegocioReporteria.Instancia.ListarUsuarios();

                ddlbusuarioasignado.DataBind();
                ddlbusuarioasignado.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
            }

            if (Tipo == "TAREA")
            {
                ddlbtipoworkflow.DataTextField = "TipoWorkFDescripcion";
                ddlbtipoworkflow.DataValueField = "TipoWorkFlowId";

                if (ddlbarea.SelectedValue.ToString() != "")
                    ddlbtipoworkflow.DataSource = NegocioReporteria.Instancia.ListarWorkflowArea(int.Parse(ddlbarea.SelectedValue.ToString()));
                else
                    ddlbtipoworkflow.DataSource = NegocioReporteria.Instancia.ListarWorkflowArea(0);

                ddlbtipoworkflow.DataBind();
                ddlbtipoworkflow.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
            }
        }
        #endregion

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (chkFecha.Checked == false)
            {
                #region Try Guardar Tarea Normal
                try
                {
                    int Validar = ValidarDatos();
                    DateTime Fecha = DateTime.Today;

                    if (Validar == 0)
                        return;

                    WorkFlow cwf = new WorkFlow();

                    int contrato = 0;
                    if (txtcontrato.Text != "")
                        contrato = int.Parse(txtcontrato.Text);

                    cwf.WorkFlowContrato = contrato;
                    cwf.WorkFlowTitulo = txttitulo.Text;
                    cwf.WorkFlowObservacion = txtobservacion.Text;
                    cwf.WorkFlowUsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
                    cwf.TipoWorkFlowId = int.Parse(ddlbtipoworkflow.SelectedValue.ToString());
                    cwf.EstadoWorkFlowId = 1;
                    cwf.WorkFlowUsuarioIngreso = Context.User.Identity.Name;
                    cwf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;
                    cwf.WorkFlowArea = int.Parse(ddlbarea.SelectedValue.ToString());
                    cwf.WorkFlowFechaIngreso = Fecha;
                    cwf.WorkFlowFechaModificacion = Fecha;

                    NegocioWorkFlow.Instancia.Insertar(cwf);

                    EnvioAviso(ddlbusuarioasignado.SelectedValue.ToString(), Context.User.Identity.Name, txttitulo.Text, txtobservacion.Text);
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "La tarea se creo con éxito.");

                    limpiarpantalla();

                }
                catch (Exception ex)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                }
                #endregion
            }
            else
            {
                #region Try Guardar Tarea con Fecha Modificada
                try
                {
                    int Validar = ValidarDatos();

                    if (Validar == 0)
                        return;

                    txtFecha.Enabled = true;

                    WorkFlow cwf = new WorkFlow();

                    int contrato = 0;
                    if (txtcontrato.Text != "")
                        contrato = int.Parse(txtcontrato.Text);

                    cwf.WorkFlowContrato = contrato;
                    cwf.WorkFlowTitulo = txttitulo.Text;
                    cwf.WorkFlowObservacion = txtobservacion.Text;
                    cwf.WorkFlowUsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
                    cwf.TipoWorkFlowId = int.Parse(ddlbtipoworkflow.SelectedValue.ToString());
                    cwf.EstadoWorkFlowId = 1;
                    cwf.WorkFlowUsuarioIngreso = Context.User.Identity.Name;
                    cwf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;
                    cwf.WorkFlowArea = int.Parse(ddlbarea.SelectedValue.ToString());
                    cwf.WorkFlowFechaIngreso = Convert.ToDateTime(txtFecha.Text);
                    cwf.WorkFlowFechaModificacion = Convert.ToDateTime(txtFecha.Text);


                    NegocioWorkFlow.Instancia.Insertar(cwf);

                    EnvioAviso(ddlbusuarioasignado.SelectedValue.ToString(), Context.User.Identity.Name, txttitulo.Text, txtobservacion.Text);
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "La tarea se creo con éxito.");

                    limpiarpantalla();
                    txtFecha.Text = "";

                }
                catch (Exception ex)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                }
                #endregion
            }
        }

        protected void limpiarpantalla()
        {

            ddlbtipoworkflow.SelectedValue = "-1";
            ddlbusuarioasignado.SelectedValue = "-1";
            txttitulo.Text = ""; 
            txtcontrato.Text = "";
            txtobservacion.Text = ""; 

        }


        protected void EnvioAviso(string Usuario, string UsuarioCrea,  string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCrea);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCorreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Usuario);

                string[] to = new string[1];
                to[0] = oUsuarioCorreo.email;  

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Pendiente " + "\n" + " El usuario " + oUsuario.nombre + " acaba de asignar la siguiente tarea a tus pendientes: " + "\n" + Descripcion, Usuario, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);

            }

        }


        protected int  ValidarDatos()
        {
            int valor = 0;
            if (ddlbtipoworkflow.SelectedValue.ToString() == "-1")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Informacion!!!", "Debe seleccionar el tipo de tarea a crear.");
                return valor;
            }

            if (ddlbusuarioasignado.SelectedValue.ToString() == "-1")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Informacion!!!", "Debe seleccionar el usuario a quien asignara la tarea.");
                return valor;
            }

            if (txtobservacion.Text == "")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Informacion!!!", "Debe detallar la tarea que asignara en texto de observación.");
                txtobservacion.Focus(); 
                return valor;
            }

            if (txttitulo.Text == "")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Informacion!!!", "Debe ingresar un titulo a la tarea.");
                return valor;
            }

            valor = 1; 
            return valor; 
        }

        protected void txtcontrato_Unload(object sender, EventArgs e)
        {
            //if (txtcontrato.Text != "")
            //{
            //    int NumeroContrato = 0;
            //    NumeroContrato = int.Parse(txtcontrato.Text.ToString());

            //    string nombre = NegocioReporteria.Instancia.BuscarNombreAsesor(NumeroContrato);

            //    ddlbusuarioasignado.SelectedValue= nombre;
            //}      
        }

        protected void ddlbusuarioasignado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlbarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarCombos("USUARIO");
            CargarCombos("TAREA");
        }

        protected void ddlbtipoworkflow_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtcontrato_TextChanged(object sender, EventArgs e)
        {

        }

        protected void chkFecha_CheckedChanged(object sender, EventArgs e)
        {
            //txtFecha.Enabled = true;
        }

  

    }
}