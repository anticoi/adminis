﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.FHC.EntidadesNegocio;
using System.Data;
using System.IO;
using TGENESIS.Framework.Utilidades;

namespace InterfazUsuarioWeb.CrearWorkFlow
{
    public partial class wfWorkFlowPorArea : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");
                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 13);
                lTitulo.Text = NombreMenu.ToString();

                //ListaWorkFlowPorArea(Context.User.Identity.Name);
                CargarCombos(Context.User.Identity.Name);
            }
        }


        protected void ListaWorkFlowPorArea(string Usuario)
        {

            try
            {
                int AreaId = 0;
                int Pertenece = 0;

                if (int.Parse(ddlbarea.SelectedValue.ToString()) > 0)
                    AreaId = int.Parse(ddlbarea.SelectedValue.ToString());

                if (ddlbPertenece.SelectedValue != "")
                    Pertenece = int.Parse(ddlbPertenece.SelectedValue.ToString());

                grvworkflowusuario.Columns[7].Visible = true;
                grvworkflowusuario.Columns[0].Visible = true; 

                grvworkflowusuario.DataSource = NegocioWorkFlow.Instancia.ListaWorkFlowPorArea(Usuario, int.Parse(ddlbEstadoTarea.SelectedValue.ToString()), AreaId, Pertenece);
                
                grvworkflowusuario.DataKeyNames = new string[] { "WorkFlowId" };
                grvworkflowusuario.DataBind();



                foreach (GridViewRow fila in grvworkflowusuario.Rows)
                {
                    LinkButton LnBSerie = (LinkButton)fila.Cells[4].FindControl("LnBAccesorios");
                    LnBSerie.ForeColor = System.Drawing.Color.White;
                    fila.Cells[0].ForeColor = System.Drawing.Color.White;
                    fila.Cells[1].ForeColor = System.Drawing.Color.White;
                    fila.Cells[2].ForeColor = System.Drawing.Color.White;
                    fila.Cells[3].ForeColor = System.Drawing.Color.White;
                    fila.Cells[4].ForeColor = System.Drawing.Color.White;
                    fila.Cells[5].ForeColor = System.Drawing.Color.White;

                    if (int.Parse(fila.Cells[7].Text.ToString()) <= 1)
                    {
                        fila.Cells[0].BackColor = System.Drawing.Color.Green;
                        fila.Cells[1].BackColor = System.Drawing.Color.Green;
                        fila.Cells[2].BackColor = System.Drawing.Color.Green;
                        fila.Cells[3].BackColor = System.Drawing.Color.Green;
                        fila.Cells[4].BackColor = System.Drawing.Color.Green;
                        fila.Cells[5].BackColor = System.Drawing.Color.Green;
                    }
                    if (int.Parse(fila.Cells[7].Text.ToString()) > 1 && int.Parse(fila.Cells[7].Text.ToString()) <= 3)
                    {
                        fila.Cells[0].BackColor = System.Drawing.Color.Orange;
                        fila.Cells[1].BackColor = System.Drawing.Color.Orange;
                        fila.Cells[2].BackColor = System.Drawing.Color.Orange;
                        fila.Cells[3].BackColor = System.Drawing.Color.Orange;
                        fila.Cells[4].BackColor = System.Drawing.Color.Orange;
                        fila.Cells[5].BackColor = System.Drawing.Color.Orange;

                    }

                    if (int.Parse(fila.Cells[7].Text.ToString()) > 3)
                    {
                        fila.Cells[0].BackColor = System.Drawing.Color.Red;
                        fila.Cells[1].BackColor = System.Drawing.Color.Red;
                        fila.Cells[2].BackColor = System.Drawing.Color.Red;
                        fila.Cells[3].BackColor = System.Drawing.Color.Red;
                        fila.Cells[4].BackColor = System.Drawing.Color.Red;
                        fila.Cells[5].BackColor = System.Drawing.Color.Red;

                    }
                }


                grvworkflowusuario.Columns[7].Visible = false;

                if (ddlbarea.SelectedValue.Count() == 1)
                    grvworkflowusuario.Columns[0].Visible = false;

                #region Esto estaba comentado

                //DataTable DtTareas = NegocioWorkFlow.Instancia.ListaWorkFlowPorArea(Usuario, int.Parse(ddlbEstadoTarea.SelectedValue.ToString()));

                //int indicePadre;
                //TreeNode nodePadre;
                //TreeNode nodoSupervisor = new TreeNode();
                //TreeNode nodoJefe = new TreeNode();
                //TreeNode nodoEmpleadoUno = new TreeNode();
                //TreeNode nodoEmpleadoDos = new TreeNode();
                //TreeNode nodoEmpleadoTarea = new TreeNode();


                //int Supervisor = 0, Jefe = 0, EmpleaUno = 0, EmpleaDos = 0;
                //string Tarea = "";

                //for (int i = 0; i < DtTareas.Rows.Count; i++)
                //{
                //    DataRow filaM = DtTareas.Rows[i];

                //    if (Supervisor != int.Parse(filaM[1].ToString()))
                //    {
                //        // preguntar por supervisor

                //        int Verifico = NegocioReporteria.Instancia.VerificaSupervisor(int.Parse(filaM[1].ToString()));

                //        if (Supervisor != Verifico)
                //        {
                //            Supervisor = int.Parse(filaM[1].ToString());
                //            nodoSupervisor = new TreeNode(filaM[2].ToString(), filaM[1].ToString());
                //            tvwTarea.Nodes.Add(nodoSupervisor);
                //        }
                //        else
                //        {

                //            Jefe = int.Parse(filaM[1].ToString());
                //            if (Jefe != Supervisor)
                //            {
                //                EmpleaUno = int.Parse(filaM[3].ToString());
                //                nodoEmpleadoUno = new TreeNode(filaM[5].ToString(), filaM[4].ToString());
                //                nodoJefe.ChildNodes.Add(nodoEmpleadoUno);
                //            }

                //        }
                //    }
                //    else
                //    {

                //        Jefe = int.Parse(filaM[3].ToString());
                //        if (Jefe != Supervisor)
                //        {
                //            Supervisor = int.Parse(filaM[1].ToString());
                //            nodoJefe = new TreeNode(filaM[5].ToString(), filaM[4].ToString());
                //            nodoSupervisor.ChildNodes.Add(nodoJefe);
                //        }
                //    }

                //}
                //DtTareas.Dispose();
                //DtTareas = null;
                #endregion

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void btnTerminar_Click(object sender, EventArgs e)
        {

            try
            {
                CheckBox check;
                int id;
                int Elimino;
                Elimino = 0;
                foreach (GridViewRow fila in grvworkflowusuario.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");

                    if (check.Checked == true)
                    {
                        id = int.Parse(grvworkflowusuario.DataKeys[fila.RowIndex]["WorkFlowId"].ToString());

                        WorkFlow wf = new WorkFlow();
                        wf.WorkFlowId = id;
                        wf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;

                        NegocioWorkFlow.Instancia.FinalizaWorkFlow(wf);

                        Elimino = Elimino + 1;
                    }
                }

                if (Elimino > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Finalizada", "La tarea fue finalizada con éxito.");
                    ListaWorkFlowPorArea(Context.User.Identity.Name);
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }


        }
 
        protected void grvworkflowusuario_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            try
            {
                                if (e.CommandName == "veraccesorios2")
                                {


                                    foreach (GridViewRow gvRow in grvworkflowusuario.Rows)
                                    {
                                        if (gvRow.RowType == DataControlRowType.DataRow)
                                        {

                                            if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString()))
                                            {
                                                int id2 = int.Parse(grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString());
                                                LinkButton LnBSerie2 = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios2");

                                                hdfidworkflow.Value = id2.ToString();

                                                ddlbusuarioasignado.DataSource = NegocioReporteria.Instancia.ListarUsuarios();
                                                ddlbusuarioasignado.DataValueField = "login";
                                                ddlbusuarioasignado.DataTextField = "nombre";
                                                ddlbusuarioasignado.DataBind();
                                                ddlbusuarioasignado.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
                                                txtobservacion.Text = "";
                                                mpepanel2.Show();
                                                return;
                                            }
                                        }
                                    }
                                }

                                if (e.CommandName == "veraccesorios")
                                {

                                    foreach (GridViewRow gvRow in grvworkflowusuario.Rows)
                                    {
                                        if (gvRow.RowType == DataControlRowType.DataRow)
                                        {

                                            if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString()))
                                            {
                                                int id = int.Parse(grvworkflowusuario.DataKeys[gvRow.RowIndex]["WorkFlowId"].ToString());
                                                LinkButton LnBSerie = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios");

                                                lbltexttipo.Text = "";
                                                lbltexttitulo.Text = "";
                                                lbltextasignado.Text = "";
                                                lbltextcontrato.Text = "";
                                                txtobservacionfinalizar.Text = "";

                                                hdfidworkflow.Value = id.ToString();

                                                DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(id);

                                                if (dtWorkFlow != null)
                                                {
                                                    lbltexttipo.Text = dtWorkFlow.Rows[0]["TipoWorkFDescripcion"].ToString();
                                                    lbltexttitulo.Text = dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString();
                                                    lbltextasignado.Text = dtWorkFlow.Rows[0]["nombre"].ToString();
                                                    lbltextcontrato.Text = dtWorkFlow.Rows[0]["WorkFlowContrato"].ToString();
                                                    txtobservacionfinalizar.Text = dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString();


                                                    grvhistorial.DataSource = NegocioReporteria.Instancia.VerHistorialAsignador(id);
                                                    grvhistorial.DataBind();
                                                    trhistorialasignador.Visible = false;

                                                    if (grvhistorial.Rows.Count > 0)
                                                        trhistorialasignador.Visible = true;

                                                    if (int.Parse(ddlbEstadoTarea.SelectedValue.ToString()) == 2)
                                                    {
                                                        btnguardartarea.Visible = false;
                                                        btnfinaliza.Visible = false;
                                                    }
                                                    else
                                                    {
                                                        btnguardartarea.Visible = true;
                                                        btnfinaliza.Visible = true;
                                                    }

                                                    mpepanel.Show();
                                                    return;
                                                }

                                            }
                                        }

                                    }
                                }

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {


            try
            {

                        if (hdfidworkflow.Value != "")
                        {
                            string UsuarioDeriva = Context.User.Identity.Name;
                            string UsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
                            string Observacion = txtobservacion.Text;
                            int idWorkFlow = int.Parse(hdfidworkflow.Value);

                            NegocioWorkFlow.Instancia.InsertarAsignaWorkFlow(idWorkFlow, UsuarioDeriva, Observacion, UsuarioAsignado);


                            DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(idWorkFlow);

                            if (dtWorkFlow != null)
                            {
                                EnvioAviso(UsuarioAsignado, UsuarioDeriva, dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString(), dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString());
                            }

                            ListaWorkFlowPorArea(UsuarioDeriva);
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Asignada", "La tarea fue asignada con éxito.");

                        }

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

        }


        protected void EnvioAviso(string Usuario, string UsuarioCrea, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCrea);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCorreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Usuario);

                string[] to = new string[1];
                to[0] = oUsuarioCorreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Pendiente " + "\n" + " El usuario " + oUsuario.nombre + " acaba de asignar la siguiente tarea a tus pendientes: " + "\n" + Descripcion, Usuario, "", userMail, passMail);
                //Mail.EnviarMail(emailDe,to,"Adjunto","Archivo Adjunto","NN","prueba de envio",userMail, passMail,
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

        }


        protected void CargarCombos( string Supervisor)
        {
            try
            {
                ddlbEstadoTarea.DataValueField = "EstadoWorkFlowId";
                ddlbEstadoTarea.DataTextField = "EstadoWorkFlowDescripcion";
                ddlbEstadoTarea.DataSource = NegocioEstadoWorkFlow.Instancia.Listar();
                ddlbEstadoTarea.DataBind();
                ddlbEstadoTarea.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

                ddlbarea.DataValueField = "AreaId";
                ddlbarea.DataTextField = "AreaNombre";
                ddlbarea.DataSource = NegocioWorkFlow.Instancia.ListaAreaPorSupervisor(Supervisor); 
                ddlbarea.DataBind();


                ddlbarea.DataTextField = "AreaNombre";
                ddlbarea.DataValueField = "AreaId";
                ddlbarea.DataSource = NegocioReporteria.Instancia.ListarArea();
                ddlbarea.DataBind();
                ddlbarea.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

                //if (ddlbarea.SelectedValue.Count() == 1)
                //{
                //    ddlbEstadoTarea.SelectedValue = "1";

                //    ddlbarea.SelectedValue = "1";
                //    ListaWorkFlowPorArea(Context.User.Identity.Name);

                    if (ddlbarea.SelectedValue != "")
                    {
                        ddlbPertenece.DataValueField = "idUsuario";
                        ddlbPertenece.DataTextField = "login";

                        ddlbPertenece.DataSource = NegocioWorkFlow.Instancia.ListaPorAreaSupervisorPertenece(Context.User.Identity.Name, int.Parse(ddlbEstadoTarea.SelectedValue.ToString()), int.Parse(ddlbarea.SelectedValue.ToString()));
                        ddlbPertenece.DataBind();

                        ddlbPertenece.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));

                    }
                //}
                //else
                //{
                //    ddlbarea.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
                //}

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {

            if (int.Parse(ddlbEstadoTarea.SelectedValue.ToString()) > 0)
            {
                ListaWorkFlowPorArea(Context.User.Identity.Name);
            }
            else
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", "Debe seleccionar Pendiente o Finalizada");
            }
            
        }

        protected void ddlbarea_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlbarea.SelectedValue != "")
            {
                ddlbPertenece.DataValueField = "idUsuario";
                ddlbPertenece.DataTextField = "login";

                ddlbPertenece.DataSource = NegocioWorkFlow.Instancia.ListaPorAreaSupervisorPertenece(Context.User.Identity.Name, int.Parse( ddlbEstadoTarea.SelectedValue.ToString()), int.Parse( ddlbarea.SelectedValue.ToString()));
                ddlbPertenece.DataBind();

                ddlbPertenece.Items.Insert(0, new ListItem("-- Seleccione --", "-1")); 

            }

        }

        protected void btnguardartarea_Click(object sender, EventArgs e)
        {


            if (ddlbEstadoTarea.SelectedValue.ToString() == "1")
            {
                        WorkFlow wkf = new WorkFlow();
                        wkf.WorkFlowObservacion = txtobservacionfinalizar.Text;
                        wkf.WorkFlowId = int.Parse(hdfidworkflow.Value.ToString());
                        wkf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;

                        NegocioWorkFlow.Instancia.WorkFlowModifica(wkf);
                        mpepanel.Hide();


                                DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(int.Parse(hdfidworkflow.Value.ToString()));
                                if (dtWorkFlow != null)
                                {
                                    string UsuarioFinaliza = Context.User.Identity.Name;
                                    string UsuarioCreo = dtWorkFlow.Rows[0]["usuarioasignado"].ToString(); ;
                                    string Observacion = dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString(); ;
                                    string Titulo = dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString(); ;
                                    int idWorkFlow = int.Parse(hdfidworkflow.Value);

                                    EnvioAvisoModificacion(UsuarioFinaliza, UsuarioCreo, Titulo, Observacion);
                                }

            }
            else
            {
                        ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Finalizada", "La tarea se encuentra finalizada, no se pueden realizar modificaciones.");
            }

        }


        protected void EnvioAvisoModificacion(string UsuarioFinaliza, string UsuarioCreo, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCreo);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioFinalizo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioFinaliza);

                string[] to = new string[1];
                to[0] = oUsuarioCreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Modificada " + "\n" + " El usuario " + oUsuarioFinalizo.nombre + " acaba de modificar la tarea : " + "\n" + Descripcion, UsuarioCreo, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }


        }

        protected void btnfinaliza_Click(object sender, EventArgs e)
        {


            // aca finalizar

            try
            {

 
                WorkFlow wf = new WorkFlow();
                wf.WorkFlowId = int.Parse(hdfidworkflow.Value.ToString());
                wf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;
                NegocioWorkFlow.Instancia.FinalizaWorkFlow(wf);
                DataTable dtWorkFlow = NegocioWorkFlow.Instancia.ListaWorkFlowUnico(int.Parse(hdfidworkflow.Value.ToString()));

                if (dtWorkFlow != null)
                {
                    string UsuarioFinaliza = Context.User.Identity.Name;
                    string UsuarioCreo = dtWorkFlow.Rows[0]["UsuarioCreador"].ToString(); ;
                    string Observacion = dtWorkFlow.Rows[0]["WorkFlowObservacion"].ToString(); ;
                    string Titulo = dtWorkFlow.Rows[0]["WorkFlowTitulo"].ToString(); ;
                    int idWorkFlow = int.Parse(hdfidworkflow.Value.ToString());

                    EnvioAvisoFinalizoTarea(UsuarioFinaliza, UsuarioCreo, Titulo, Observacion);
                }


                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Tarea Finalizada", "La tarea fue finalizada con éxito.");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }        
        


        }

        protected void EnvioAvisoFinalizoTarea(string UsuarioFinaliza, string UsuarioCreo, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCreo);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioFinalizo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioFinaliza);

                string[] to = new string[1];
                to[0] = oUsuarioCreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Finalizada " + "\n" + " El usuario " + oUsuarioFinalizo.nombre + " acaba de finalizar la tarea : " + "\n" + Descripcion, UsuarioCreo, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }

        }

        protected void ddlbEstadoTarea_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlbarea.SelectedValue  != "")
            {
                 
                ListaWorkFlowPorArea(Context.User.Identity.Name);

                ddlbPertenece.DataValueField = "idUsuario";
                ddlbPertenece.DataTextField = "login";

                ddlbPertenece.DataSource = NegocioWorkFlow.Instancia.ListaPorAreaSupervisorPertenece(Context.User.Identity.Name, int.Parse(ddlbEstadoTarea.SelectedValue.ToString()), int.Parse(ddlbarea.SelectedValue.ToString()));
                ddlbPertenece.DataBind();

                ddlbPertenece.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
            }


        }

        protected void grvworkflowusuario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}