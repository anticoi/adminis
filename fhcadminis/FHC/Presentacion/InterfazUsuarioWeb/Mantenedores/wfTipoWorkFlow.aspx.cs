﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.EntidadesNegocio;
using System.Configuration;
using TGENESIS.FHC.ComponentesNegocio;
using iTextSharp.text;
using TGENESIS.Framework.Utilidades;

namespace TGENESIS.FHC.InterfazUsuarioWeb.Mantenedores
{
    public partial class wfTipoWorkFlow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 8);

                lTitulo.Text = NombreMenu.ToString();

                ListarTipoWorkFlow();
                LlenarArea();

            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtTipoWorkFlow.Text != "")
            {
                guardarinformacion(); 
            }
        }

        private void guardarinformacion()
        {

            try
            {
                TipoWorkFlow twf = new TipoWorkFlow();

                twf.TipoWorkFDescripcion = txtTipoWorkFlow.Text;
                twf.TipoWorkFlowEstado = true;
                twf.TipoWorkFlowUsuarioIngreso = Context.User.Identity.Name;
                twf.TipoWorkFlowUsuarioModificacion = Context.User.Identity.Name;
                twf.TipoWorkFlowArea = int.Parse(ddlbarea.SelectedValue.ToString());

                NegocioTipoWorkFlow.Instancia.Insertar(twf);


                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "El registro se ingreso con éxito.");

                ListarTipoWorkFlow();
                txtTipoWorkFlow.Text = ""; 
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }


        }

        protected void LlenarArea()
        {
            List<TipoWorkFlow> twf = NegocioTipoWorkFlow.Instancia.Listar();

            //Listar Area
            ddlbarea.DataSource = NegocioReporteria.Instancia.ListarArea();
            ddlbarea.DataValueField = "Areaid";
            ddlbarea.DataTextField = "AreaNombre";
            ddlbarea.DataBind();
            //ddlbarea.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
        }

        protected void ListarTipoWorkFlow()
        {

        
            List< TipoWorkFlow> twf = NegocioTipoWorkFlow.Instancia.Listar() ;

            grvtipoworkflow.DataSource = twf;
            grvtipoworkflow.DataKeyNames = new string[] { "TipoWorkFlowId" };

            grvtipoworkflow.DataBind();



        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarTipoWorkFlow(); 
        }


        protected void EliminarTipoWorkFlow()
        {
            try
            {
                CheckBox check;
                int id;
                int Elimino;
                Elimino = 0; 
                foreach (GridViewRow fila in grvtipoworkflow.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");

                    if (check.Checked == true)
                    {
                        id = int.Parse(grvtipoworkflow.DataKeys[fila.RowIndex]["TipoWorkFlowId"].ToString());

                        TipoWorkFlow twf = new TipoWorkFlow();
                        twf.TipoWorkFlowId = id;
                        twf.TipoWorkFlowEstado = false;
                        twf.TipoWorkFlowUsuarioModificacion = Context.User.Identity.Name;

                        NegocioTipoWorkFlow.Instancia.Eliminar(twf);

                        Elimino = Elimino + 1; 
                    }
                }

                if (Elimino > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Eliminación", "El registro se elimino con éxito.");
                    ListarTipoWorkFlow(); 
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }

        
        }

        protected void ddlbarea_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtTipoWorkFlow_TextChanged(object sender, EventArgs e)
        {

        }
    }
}