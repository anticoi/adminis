﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;

namespace InterfazUsuarioWeb.Mantenedores
{
    public partial class wfArea : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 11);

                lTitulo.Text = NombreMenu.ToString();

                ListarArea();

            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtArea.Text != "")
            {
                guardarinformacion();
            }
        }

        private void guardarinformacion()
        {

            try
            {
            
                NegocioReporteria.Instancia.InsertarArea(txtArea.Text, Context.User.Identity.Name, int.Parse(hdfareaid.Value) );
                 
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "El registro se ingreso con éxito.");
                txtArea.Text = ""; 
                ListarArea();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }


        }

        protected void ListarArea()
        {

        
            grvarea.DataSource = NegocioReporteria.Instancia.ListarArea() ;
            grvarea.DataKeyNames = new string[] { "AreaId" };

            grvarea.DataBind();
            hdfareaid.Value = "0";
            btnAgregar.Text = "Agregar"; 
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarArea();
        }


        protected void EliminarArea()
        {
            try
            {
                CheckBox check;
                //int id;
                int Elimino;
                Elimino = 0;
                foreach (GridViewRow fila in grvarea.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");

                    if (check.Checked == true)
                    {

                        NegocioReporteria.Instancia.EliminarArea(int.Parse(grvarea.DataKeys[fila.RowIndex]["AreaId"].ToString()), Context.User.Identity.Name);

                        Elimino = Elimino + 1;
                    }
                }

                if (Elimino > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Eliminación", "El registro se elimino con éxito.");
                    ListarArea() ;
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }


        }



        protected void grvarea_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            #region Ver Parametros
            if (e.CommandName == "veraccesorios")
            {

                foreach (GridViewRow gvRow in grvarea.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {

                        if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvarea.DataKeys[gvRow.RowIndex]["AreaId"].ToString()))
                        {
                            int id = int.Parse(grvarea.DataKeys[gvRow.RowIndex]["AreaId"].ToString());
                            LinkButton LnBSerie = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios");
                            hdfareaid.Value = id.ToString(); 
                           txtArea.Text = LnBSerie.Text;
                           btnAgregar.Text = "Modificar"; 
                            return;
                        }

                    }
                }

            }
            #endregion Ver Parametros
        }    
    
    }
}