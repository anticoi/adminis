﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;

namespace InterfazUsuarioWeb.Mantenedores
{
    public partial class wfAreaSuperior : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 16);

                lTitulo.Text = NombreMenu.ToString();

                ListarAreaSuperior();

            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtArea.Text != "")
            {
                guardarinformacion();
            }
        }

        private void guardarinformacion()
        {

            try
            {

                NegocioReporteria.Instancia.InsertarAreaSuperior(txtArea.Text, Context.User.Identity.Name, int.Parse( hdfareasuperiorid.Value ));

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "El registro se ingreso con éxito.");
                txtArea.Text = "";
                ListarAreaSuperior();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }


        }

        protected void ListarAreaSuperior()
        {


            grvarea.DataSource = NegocioReporteria.Instancia.ListarAreaSuperior();
            grvarea.DataKeyNames = new string[] { "AreaSuperiorId" };

            grvarea.DataBind();
            hdfareasuperiorid.Value = "0";
            btnAgregar.Text = "Agregar";
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarAreaSuperior();
        }


        protected void EliminarAreaSuperior()
        {
            try
            {
                CheckBox check;
                //int id;
                int Elimino;
                Elimino = 0;
                foreach (GridViewRow fila in grvarea.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");

                    if (check.Checked == true)
                    {

                        NegocioReporteria.Instancia.EliminarAreaSuperior(int.Parse(grvarea.DataKeys[fila.RowIndex]["AreaSuperiorId"].ToString()), Context.User.Identity.Name);

                        Elimino = Elimino + 1;
                    }
                }

                if (Elimino > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Eliminación", "El registro se elimino con éxito.");
                    ListarAreaSuperior();
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }


        }



        protected void grvarea_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            #region Ver Parametros
            if (e.CommandName == "veraccesorios")
            {

                foreach (GridViewRow gvRow in grvarea.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {

                        if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvarea.DataKeys[gvRow.RowIndex]["AreaSuperiorId"].ToString()))
                        {
                            int id = int.Parse(grvarea.DataKeys[gvRow.RowIndex]["AreaSuperiorId"].ToString());
                            LinkButton LnBSerie = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios");
                            hdfareasuperiorid.Value = id.ToString();
                            txtArea.Text = LnBSerie.Text;
                            btnAgregar.Text = "Modificar";
                            return;
                        }

                    }
                }

            }
            #endregion Ver Parametros
        }

    }
}