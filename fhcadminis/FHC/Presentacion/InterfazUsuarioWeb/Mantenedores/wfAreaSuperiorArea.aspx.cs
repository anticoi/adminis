﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;
using System.Data;

namespace InterfazUsuarioWeb.Mantenedores
{
    public partial class wfAreaSuperiorArea : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label lTitulo = (Label)Master.FindControl("lblTitulo");
                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 17);
                lTitulo.Text = NombreMenu.ToString();
                CargarCombo();
                CargarGrilla(); 
            }
        }


        protected void CargarCombo()
        {
                ddlbAreaSuperior.DataTextField = "AreaSuperiorNombre";
                ddlbAreaSuperior.DataValueField = "AreaSuperiorId";
                ddlbAreaSuperior.DataSource = NegocioReporteria.Instancia.ListarAreaSuperior();
                ddlbAreaSuperior.DataBind();
                ddlbAreaSuperior.Items.Insert(0, new ListItem("", ""));
        }


        protected void CargarGrilla()
       {

           grvarea.DataSource = NegocioReporteria.Instancia.ListarArea();
           grvarea.DataKeyNames = new string[] { "AreaId" };
           grvarea.DataBind();


           if (ddlbAreaSuperior.SelectedValue.ToString() == "")
               return; 

           if ( int.Parse( ddlbAreaSuperior.SelectedValue.ToString()) > 0 )
           { 
                DataTable DtAreaSuperiorArea = NegocioReporteria.Instancia.ListarAreaDeAreaSuperior( int.Parse( ddlbAreaSuperior.SelectedValue.ToString())) ;

                if (DtAreaSuperiorArea.Rows.Count > 0)
                {
                    CheckBox check;

                    foreach (GridViewRow fila in grvarea.Rows)
                    {
                        check = (CheckBox)fila.Cells[0].FindControl("chk");
                        check.Checked = false;
                        foreach (DataRow filatabla in DtAreaSuperiorArea.Rows)
                        {
                            if (int.Parse(grvarea.DataKeys[fila.RowIndex]["AreaId"].ToString()) == int.Parse(filatabla["AreaId"].ToString()))
                            {
                                check.Checked = true;
                            }
                        }
                    }
                }


           }

       }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                if (ddlbAreaSuperior.SelectedValue.ToString() == "")
                    return; 

                CheckBox check;

                int insert;
                insert = 0;
                int IdAreaSuperior = int.Parse(ddlbAreaSuperior.SelectedValue.ToString());
                int IdArea = 0;

                foreach (GridViewRow fila in grvarea.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");
                    IdArea = int.Parse(grvarea.DataKeys[fila.RowIndex].Value.ToString());

                    NegocioReporteria.Instancia.EliminarAreaDeAreaSuperior(IdAreaSuperior, IdArea);

                    if (check.Checked == true)
                    {
                        NegocioReporteria.Instancia.InsertAreaDeAreaSuperior(IdAreaSuperior, IdArea);
                        insert = insert + 1;
                    }
                }

                if (insert > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "   Actualización.", "La información se actualizo con éxito");

                }
                else
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "   Actualización Sin Éxito.", "Debe seleccionar uno o mas Departamentos.");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }



        }

        protected void ddlbAreaSuperior_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarGrilla(); 
        }
    }
}