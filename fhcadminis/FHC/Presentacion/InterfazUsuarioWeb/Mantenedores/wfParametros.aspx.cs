﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;
using TGENESIS.FHC.EntidadesNegocio;

namespace InterfazUsuarioWeb.Mantenedores
{
    public partial class wfParametros : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 15);

                lTitulo.Text = NombreMenu.ToString();

                ListarParametros();

            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != "" && txtValor.Text !="" )
            {
                guardarinformacion();
            }
        }

        private void guardarinformacion()
        {

            try
            {

                Parametros Prt = new Parametros();

                Prt.ParametrosDescripcion = txtDescripcion.Text;
                Prt.ParametrosValor = txtValor.Text;
                Prt.ParametrosUsuarioIngreso = Context.User.Identity.Name;
                Prt.ParametrosUsuarioModificacion = Context.User.Identity.Name;

                if (hdfparametrosId.Value != "")
                {
                    Prt.ParametrosEstado = true; 
                    Prt.ParametrosId = int.Parse(hdfparametrosId.Value.ToString());
                }
                NegocioParametros.Instancia.Insertar(Prt); 

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "El registro se ingreso con éxito.");
                txtDescripcion.Text = "" ;
                txtValor.Text = "";
                ListarParametros();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        protected void ListarParametros()
        {

            hdfparametrosId.Value = "";
            grvParametros.DataSource = NegocioParametros.Instancia.Listar(); 
            grvParametros.DataKeyNames = new string[] { "ParametrosId" };

            grvParametros.DataBind();

        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarParametros();
        }


        protected void EliminarParametros()
        {
            try
            {
                CheckBox check;
                //int id;
                int Elimino;
                Elimino = 0;
                foreach (GridViewRow fila in grvParametros.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");

                    if (check.Checked == true)
                    {
                        hdfparametrosId.Value = "";
                        Parametros Prt = new Parametros();
                        Prt.ParametrosId = int.Parse(grvParametros.DataKeys[fila.RowIndex]["ParametrosId"].ToString());
                        Prt.ParametrosUsuarioModificacion = Context.User.Identity.Name;
                        NegocioParametros.Instancia.Eliminar(Prt); 
                        Elimino = Elimino + 1;
                    }
                }

                if (Elimino > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Eliminación", "El registro se elimino con éxito.");
                    ListarParametros();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }





        protected void grvParametros_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            #region Ver Parametros
            if (e.CommandName == "veraccesorios")
            {

                foreach (GridViewRow gvRow in grvParametros.Rows)
                {
                    if (gvRow.RowType == DataControlRowType.DataRow)
                    {

                        if (int.Parse(e.CommandArgument.ToString()) == int.Parse(this.grvParametros.DataKeys[gvRow.RowIndex]["ParametrosId"].ToString()))
                        {
                                int id = int.Parse(grvParametros.DataKeys[gvRow.RowIndex]["ParametrosId"].ToString());
                                LinkButton LnBSerie = (LinkButton)gvRow.Cells[0].FindControl("LnBAccesorios");

                                hdfparametrosId.Value = "";
                                txtDescripcion.Text = "";
                                txtValor.Text = "" ;

                                hdfparametrosId.Value = id.ToString() ;
                                txtDescripcion.Text = LnBSerie.Text ;
                                txtValor.Text = gvRow.Cells[2].Text; 
                                return;
                            }

                        }
                    }

                }
            #endregion Ver Parametros
            }
            
    
    }
}