﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfParametros.aspx.cs" Inherits="InterfazUsuarioWeb.Mantenedores.wfParametros" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table width="600px" style="height:300px;">
        <tr>
            <td valign="top"  align="center">
                <table width="80%">
                    <tr>
                            <td align="center">
                                <asp:Label  ID="lblParametroDescripcion" runat="server" Text="Descripción"></asp:Label>
                            </td>

                            <td colspan="2">
                                    <asp:TextBox id ="txtDescripcion" runat="server"  MaxLength="100" Width="300px"></asp:TextBox>
                                    <asp:HiddenField ID="hdfparametrosId" runat="server" />
                            </td>

                    </tr>

                    <tr>
                        <td align="center">
                                    <asp:Label  ID="lblValor" runat="server" Text="Valor"></asp:Label>
                        </td>
                        <td>
                                    <asp:TextBox id ="txtValor" runat="server"  MaxLength="100" Width="300px"></asp:TextBox>
                        </td>

                            <td>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar"  class="button small blue" Width="100px" Height="25px" onclick="btnAgregar_Click" />
                            </td>

                    </tr>

                    <tr>
                        <td align="center" colspan="6">
                                <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                                        <asp:GridView ID="grvParametros" runat="server" CssClass="mGrid" AutoGenerateColumns="False" 
                                         onrowcommand="grvParametros_RowCommand"  ShowHeaderWhenEmpty="True" GridLines="None" >
                                        
                                            <Columns>
                                                    <asp:TemplateField>
                                                                <ItemTemplate>
                                                                        <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("ParametrosId") %>' />
                                                                </ItemTemplate>
                                                            <HeaderStyle Width="50px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Descripcion">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LnBAccesorios" runat="server" CommandArgument='<%# Eval("ParametrosId") %>' Text='<%# Eval("ParametrosDescripcion") %>' CommandName="veraccesorios"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        
                                                        <HeaderStyle Width="300px" />
                                                        <ItemStyle Width="300px" />
                                                        
                                                    </asp:TemplateField>     


                                                    <asp:BoundField DataField="ParametrosValor"  HeaderText="Valor" >
                                                            <HeaderStyle Width="220px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                            

                                                    <asp:BoundField DataField="ParametrosEstado"  HeaderText="Estado" >
                                                            <HeaderStyle Width="50px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                            

                                                    <asp:BoundField DataField="ParametrosFechaIngreso"  HeaderText="Fec. Creación" >
                                                            <HeaderStyle Width="120px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                    

                                                    <asp:BoundField DataField="ParametrosUsuarioIngreso"  HeaderText="Usu. Creación" >
                                                            <HeaderStyle Width="120px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                                        

                                            </Columns>
                                        
                                        </asp:GridView>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="100px" Height="25px" class="button small blue" onclick="btnEliminar_Click" />                            
                        </td>
                    </tr>
                </table>
            </td>

        </tr>

        <tr>
            <td>
                        <uc1:ucmensaje ID="ucMensaje1" runat="server" />
            </td>
        </tr>

</table>
</asp:Content>
