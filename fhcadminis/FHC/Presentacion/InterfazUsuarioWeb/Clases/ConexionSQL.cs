﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InterfazUsuarioWeb.Clases
{
    public class ConexionSQL
    {

        public SqlConnection AbrirConexionIntranet()
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["BDAdminis"].ToString();
            cn.Open();
            return cn;
        }

        public SqlConnection AbrirConexionIntranet2()
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["b2b_Usuarios"].ToString();
            cn.Open();
            return cn;
        }
    }
}