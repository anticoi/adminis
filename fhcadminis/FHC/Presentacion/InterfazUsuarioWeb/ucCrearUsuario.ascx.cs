﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using TGENESIS.Framework.Acceso;
using ControlUsuario.ComponentesNegocio;
using ControlUsuario.EntidadesNegocio;
using System.Transactions;
using TGENESIS.Framework.Utilidades;
using TGENESIS.Framework.Enumeraciones;
using System.Data;

namespace ControlUsuario.Controles
{
    public partial class ucCrearUsuario : System.Web.UI.UserControl
    {

        System.Configuration.AppSettingsReader read = new System.Configuration.AppSettingsReader();

        public string UsuarioCreado
        {
            get { return txtUsuario.Text; }
        }


        private int idSistema;
        private string usuario;
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private bool enviarMailActivo = false;
        public bool EnviarMailActivo
        {
            set { enviarMailActivo = value; }
            get { return enviarMailActivo; }
        }

        public int IdSistema
        {
            get { return idSistema; }
            set { idSistema = value; }
        }

        private string urlAccesoSistema;
        public string UrlAccesoSistema
        {
            get { return urlAccesoSistema; }
            set { urlAccesoSistema = value; }
        }

        public event EventHandler asignarRol;
        public void OnAsignarRol(EventArgs e)
        {
            if (asignarRol != null)
            {
                asignarRol(this, e);
            }
            RaiseBubbleEvent(this, e);
        }

        public event EventHandler creaUsuario;
        public void OnCreaUsuario(EventArgs e)
        {
            if (creaUsuario != null)
            {
                creaUsuario(this, e);
            }
            RaiseBubbleEvent(this, e);
        }

        private string emailFrom;

        public string EmailFrom
        {
            get { return emailFrom; }
            set { emailFrom = value; }
        }

        private string usuarioMail;

        public string UsuarioMail
        {
            get { return usuarioMail; }
            set { usuarioMail = value; }
        }

        private string passUsuarioMail;

        public string PassUsuarioMail
        {
            get { return passUsuarioMail; }
            set { passUsuarioMail = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    LlenarCombos();

                    if (idSistema ==1)
                    {
                        ddlEmpresa.Visible = true;
                        LlenarComboEmpresaHub();
                        
                    }
                    else
                    {
                        ddlEmpresa.Items.Clear();
                        //Empresa empresa = NegocioEmpresa.Instancia.BuscaEmpresa("000000000");
                        //ddlEmpresa.Items.Add(new ListItem("T-GENESIS LTDA", empresa.idEmpresa.ToString()));
                        LlenarComboEmpresa();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }


        private string obtenerUsuario()
        {
            return usuario;
        }

        private void LlenarCombos()
        {
            ddlSupervisor.DataSource = NegocioReporteria.Instancia.listaSupervisor(idSistema);
            ddlSupervisor.DataBind();
            ddlSupervisor.Items.Insert(0, new ListItem("<NO APLICA>", ""));


            ddlArea.DataSource = NegocioReporteria.Instancia.ListaArea(); 
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("", ""));
                
        }

        private void LlenarComboEmpresaHub()
        {
            ddlEmpresa.DataSource = NegocioEmpresa.Instancia.Listar();
            ddlEmpresa.DataBind();
        }

        private void LlenarComboEmpresa()
        {
            ddlEmpresa.DataSource = NegocioEmpresa.Instancia.Listar(idSistema);
            ddlEmpresa.DataBind();
        }

        protected void btnGrabar_Click(object sender, EventArgs e)
        {
            try
            {
                OnCreaUsuario(e);
                if (ValidaDatos())
                {
                    string passProvisoria = Guid.NewGuid().ToString().Substring(0, 7);
                    Usuario usuario = new Usuario();

                    using (TransactionScope tran = new TransactionScope())
                    {
                        int idEmpresa = 0;
                        lblMsg.Text = "";


                        usuario.login = txtUsuario.Text;
                        usuario.nombre = txtNombreCompleto.Text;
                        usuario.PasswordProvisorio = Cadena.GetMD5(passProvisoria);
                        usuario.password = Cadena.GetMD5(Guid.NewGuid().ToString()); //no importa esta ya que tiene que comparar con la provisoria para que le pida cambio
                        usuario.telefono = txtFono.Text;
                        usuario.email = txtEmail.Text;

                        int idUsuario = NegocioUsuario.Instancia.Insertar(usuario);

                        int.TryParse(ddlEmpresa.SelectedValue, out idEmpresa);
                        UsuarioEmpresa usuarioEmpresa = new UsuarioEmpresa();
                        usuarioEmpresa.idEmpresa = idEmpresa;
                        usuarioEmpresa.idUsuario = idUsuario;
                        NegocioUsuarioEmpresa.Instancia.Insertar(usuarioEmpresa);

                        UsuarioSistema usuarioSistema = new UsuarioSistema();
                        usuarioSistema.idSistema = idSistema;
                        usuarioSistema.idUsuario = idUsuario;
                        usuarioSistema.esSupervisor = chkEsSupervisor.Checked;
                        NegocioUsuarioSistema.Instancia.Insertar(usuarioSistema);

                        if (!chkEmail.Checked && ddlSupervisor.SelectedValue != "")
                        {
                            int idUsuarioSupervisor = 0;
                            int.TryParse(ddlSupervisor.SelectedValue, out idUsuarioSupervisor);
                            UsuarioSistemaSupervisor uss = new UsuarioSistemaSupervisor();
                            uss.idSistema = idSistema;
                            uss.idUsuario = idUsuario;
                            uss.idUsuarioSupervisor = idUsuarioSupervisor;
                            NegocioUsuarioSistemaSupervisor.Instancia.Insertar(uss);
                        }
                        else
                        {
                            int idUsuarioSupervisor = 0;
                            int.TryParse(ddlSupervisor.SelectedValue, out idUsuarioSupervisor);
                            UsuarioSistemaSupervisor uss = new UsuarioSistemaSupervisor();
                            uss.idSistema = idSistema;
                            uss.idUsuario = idUsuario;
                            uss.idUsuarioSupervisor = idUsuarioSupervisor;
                            NegocioUsuarioSistemaSupervisor.Instancia.Eliminar(uss);
                        }
                        
                        tran.Complete();
                    }

                    lblMsg.Text = "Usuario Grabado exitosamente";
                    btnAsignarRol.Enabled = true;
                    Sistema sistema = NegocioSistema.Instancia.BuscaSistema(idSistema);
                    if (chkEmail.Checked)
                    {
                        EnviarMail(passProvisoria, usuario.login, usuario.email, sistema.nombre);
                    }
                    else
                    {
                        int idUsuario =0;
                        int.TryParse(ddlSupervisor.SelectedValue, out idUsuario);
                        Usuario usuarioSupervisor = NegocioUsuario.Instancia.BuscaDatosUsuarioPorId(idUsuario);
                        EnviarMail(passProvisoria, usuario.login, usuarioSupervisor.email, sistema.nombre);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void EnviarMail(string passProvisoria, string login, string mail, string nombreSistema)
        {
            if (enviarMailActivo)
            {
                string de = emailFrom;
                string[] para = mail.Split('~');
              
                string mensaje = "Se he creado una nueva cuenta para acceder al sistema de T-GENESIS LTDA " + nombreSistema + ". Los datos son los siguientes: \n";
                mensaje += "Usuario: " + login + "\n";
                mensaje += "Password: " + (passProvisoria == "" ? " (Utilice la misma password que utiliza habitualmente, si no la recuerda haga click en 'recordar contraseña' en el inicio de session " : passProvisoria) + "\n";
                if (urlAccesoSistema != "")
                    mensaje += "Url Sistema: " + urlAccesoSistema + "\n\n\n";
                mensaje += "Una vez que ingrese al sistema, se le pedirá que cambie su password\n\n\n";
                mensaje += "No responda este mail ya que ha sido generado de manera automática.\n\n";
                mensaje += "T-GENESIS LTDA";


                Mail.EnviarMail(de, para, "Acceso a Sistema T-GENESIS LTDA - " + nombreSistema, mensaje, "", "", usuarioMail, passUsuarioMail);
            }
        }


        private bool ValidaDatos()
        {
            lblMsg.Text = "";
            

            if (txtNombreCompleto.Text == "")
                lblMsg.Text += "Nombre completo es obligatorio. <br/>";
            if (chkEmail.Checked && txtEmail.Text == "")
            {
                lblMsg.Text += "email es obligatorio <br/>";
            }
            if (!chkEmail.Checked && ddlSupervisor.SelectedValue == "")
                lblMsg.Text += "Debe ingresar el supervisor <br/>";

            if (chkEmail.Checked && txtEmail.Text != "")
            {
                if ((!txtEmail.Text.Contains("@")) || (!txtEmail.Text.Contains(".")))
                    lblMsg.Text += "email no válido <br/>";
            }
            if (txtUsuario.Text == "")
                lblMsg.Text += "usuario es obligatorio <br/>";


            if (lblMsg.Text.Trim() == "")
                return true;
            else
                return false;

        }

        protected void btnAsignarRol_Click(object sender, EventArgs e)
        {
            OnAsignarRol(e);
        }

        protected void chkEmail_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEmail.Checked)
            {
                ddlSupervisor.Items.Insert(0, new ListItem("<NO APLICA>", ""));
                ddlSupervisor.SelectedValue = "";
                ddlSupervisor.Enabled = false;
                chkEsSupervisor.Enabled = true;

            }
            else
            {
                ddlSupervisor.Items.RemoveAt(0);
                ddlSupervisor.Enabled = true;
                chkEsSupervisor.Checked = false;
                chkEsSupervisor.Enabled = false;
                txtEmail.Text = "";
                
            }
        }

        protected void btnBuscarUsuario_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text != "")
            {
                Usuario usuario = NegocioUsuario.Instancia.BuscaDatosUsuario(txtUsuario.Text);
                if (usuario != null)
                {
                    
                    LlenarDatosUsuario(usuario);
                }
            }
        }

        private void LlenarDatosUsuario(Usuario usuario)
        {
            txtEmail.Text = usuario.email;
            txtNombreCompleto.Text = usuario.nombre;
            txtFono.Text = usuario.telefono;
            if (txtEmail.Text != "")
                chkEmail.Checked = true;
            else
                chkEmail.Checked = false;


            if (idSistema == (int)EnumSistemas.Sistemas.HUBLOGISTICO)
            {
                Empresa empresa = NegocioEmpresa.Instancia.BuscarPorUsuario(usuario.login)[0];

                ddlEmpresa.SelectedValue = empresa.idEmpresa.ToString();
            }
            else
                ddlEmpresa.SelectedIndex = 0;

            Usuario usuarioSistema = NegocioUsuario.Instancia.ObtenerUsuario(txtUsuario.Text, idSistema);
            if (usuarioSistema == null)
            {
                chkEsSupervisor.Checked = false;
                btnGrabar.Visible = false;
                btnAsociar.Visible = true;
                lblMsg.Text = "Este usuario existe, pero no esta asociado al  sistema actual";
            }
            else
            {
                btnGrabar.Visible = true;
                btnAsociar.Visible = false;
                chkEsSupervisor.Checked = usuarioSistema.esSupervisor;
                btnAsignarRol.Enabled = true;
                UsuarioSistemaSupervisor uss = NegocioUsuarioSistemaSupervisor.Instancia.BuscaSupervisor(idSistema, usuarioSistema.idUsuario);
                if (uss != null)
                    ddlSupervisor.SelectedValue = uss.idUsuarioSupervisor.ToString();

            }

            if (txtEmail.Text !="")
            {
                chkEmail.Checked = true;
                ddlSupervisor.SelectedValue = "";
                ddlSupervisor.Enabled = false;
                chkEsSupervisor.Enabled = true;

            }
            else
            {
                chkEmail.Checked = false;
                ddlSupervisor.Enabled = true;
                chkEsSupervisor.Checked = false;
                chkEsSupervisor.Enabled = false;


            }


        }

        protected void lnkCambiarOrdenSupervisor_Click(object sender, EventArgs e)
        {
            DataTable tblSupervisores = NegocioReporteria.Instancia.listaSupervisor(idSistema);

            if (ddlSupervisor.DataTextField == "loginNombre")
            {
                tblSupervisores.DefaultView.Sort = "login";
                ddlSupervisor.DataTextField = "nombreLogin";
            }
            else
            {
                tblSupervisores.DefaultView.Sort = "nombre";
                ddlSupervisor.DataTextField = "loginNombre";
            }


            ddlSupervisor.DataSource = tblSupervisores.DefaultView;
            ddlSupervisor.DataBind();



        }

        protected void lnkVerificarEmail_Click(object sender, EventArgs e)
        {
            Usuario usuarioMail = NegocioUsuario.Instancia.BuscaUsuarioPorEmail(txtEmail.Text);
            if (usuarioMail != null)
            {
                lblMsg.Text = "Ya existe este mail registrado en otro usuario";
            }
        }

        protected void btnAsociar_Click(object sender, EventArgs e)
        {
            int idEmpresa = 0;

            using (TransactionScope tran = new TransactionScope())
            {
                Usuario usuario = new Usuario();
                usuario.login = txtUsuario.Text;
                usuario.nombre = txtNombreCompleto.Text;
                usuario.PasswordProvisorio = "";
                usuario.password = "";
                usuario.telefono = txtFono.Text;
                usuario.email = txtEmail.Text;

                int idUsuario = NegocioUsuario.Instancia.Insertar(usuario);

                UsuarioSistema usuarioSistema = new UsuarioSistema();
                usuarioSistema.idSistema = idSistema;
                usuarioSistema.idUsuario = idUsuario;
                usuarioSistema.esSupervisor = chkEsSupervisor.Checked;
                NegocioUsuarioSistema.Instancia.Insertar(usuarioSistema);

                int.TryParse(ddlEmpresa.SelectedValue, out idEmpresa);
                UsuarioEmpresa usuarioEmpresa = new UsuarioEmpresa();
                usuarioEmpresa.idEmpresa = idEmpresa;
                usuarioEmpresa.idUsuario = idUsuario;
                NegocioUsuarioEmpresa.Instancia.Insertar(usuarioEmpresa);

                tran.Complete();

                lblMsg.Text = "Usuario Asociado exitosamente";

                btnAsignarRol.Enabled = true;
                
            }
            Sistema sistema = NegocioSistema.Instancia.BuscaSistema(idSistema);
            if (chkEmail.Checked)
            {
                EnviarMail("", txtUsuario.Text, txtEmail.Text, sistema.nombre);
            }
            else
            {
                int _idUsuario = 0;
                int.TryParse(ddlSupervisor.SelectedValue, out _idUsuario);
                Usuario usuarioSupervisor = NegocioUsuario.Instancia.BuscaDatosUsuarioPorId(_idUsuario);
                EnviarMail("", txtUsuario.Text, usuarioSupervisor.email, sistema.nombre);
            }

        }
        

       
    }
}