﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    /// <summary>
    /// Descripción breve de LogoutHandler
    /// </summary>
    public class LogoutHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            context.Session.Abandon();

            FormsAuthentication.SignOut();

            FormsAuthentication.RedirectToLoginPage();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}