﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.Framework.Utilidades;
using System.Reflection;
using System.Configuration;
using ControlUsuario.ComponentesNegocio;
using ControlUsuario.EntidadesNegocio;
using TGENESIS.FHC.EntidadesNegocio;
using System.Web.Security;
using System.Text;
using TGENESIS.Framework.Acceso;
using System.Web.UI.HtmlControls;
using TGENESIS.FHC.ComponentesNegocio;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    public partial class MasterTgenesisFHC : System.Web.UI.MasterPage
    {
        private Autenticacion datosUsuario;

        public Autenticacion DatosUsuario
        {
            get { return datosUsuario; }
            set { datosUsuario = value; }
        }

        System.Configuration.AppSettingsReader read = new System.Configuration.AppSettingsReader();
        Cadena cadena = new Cadena();
        protected void Page_Load(object sender, EventArgs e)
        {


            VerificarSession();

            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            System.IO.FileInfo assemblyFileInfo = new System.IO.FileInfo(Assembly.GetExecutingAssembly().Location);
            string name = ((AssemblyTitleAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyTitleAttribute))).Title;
            string buildDate = assemblyFileInfo.LastWriteTime.ToString("dd MMM yyyy HH:mm:ss");
            lblVersion.Text = string.Concat("Aplicación: ", name, " ", "Versión: ", version);
            lblVersionAcercaDe.Text = lblVersion.Text;
            lblFechaCompilacion.Text = "Fecha Compilación: " + buildDate;
            string[] user = Context.User.Identity.Name.Split('\\');
            lblUsuario.Text = "usuario: " + user[user.Length - 1];



            int idSistema = int.Parse(ConfigurationManager.AppSettings["IdSistema"].ToString());
            string usuario = ObtenerUsuario();
            //string usuario = "srivera";
            Usuario _usuario = NegocioUsuario.Instancia.ObtenerUsuario(usuario, idSistema);

            if (_usuario != null)
            //if (usuario != null)
            {
                LlenarMenuSistema(_usuario, idSistema);
            }

        }

        private void VerificarSession()
        {
            if (Session.SessionID == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "redirige", "window.location='" + Page.ResolveUrl("~/LogoutHandler.ashx") + "'", true);
            }
            else
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    //    /*Obtiene los datos de usuario desde el ticket de autenticacion*/
                    //    FormsIdentity ident = (FormsIdentity)Page.User.Identity;
                    //    FormsAuthenticationTicket ticket = FormsAuthentication.RenewTicketIfOld(ident.Ticket);
                    //    Autenticacion aut = Serializador.Deserializar<Autenticacion>(ticket.UserData);

                    //Manejando error frecuente de sesión: portiz - 11/12/2012
                    if (Sesiones.Glb_autenticacion == null)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "redirige", "window.location='" + Page.ResolveUrl("~/LogoutHandler.ashx") + "'", true);
                    }

                    else
                    {

                        Autenticacion aut = Serializador.Deserializar<Autenticacion>(Sesiones.Glb_autenticacion);
                        DatosUsuario = aut;

                        if (!TieneAcceso(aut))
                        {
                            FormsAuthentication.RedirectToLoginPage("Acceso Denegado");
                            return;
                        }


                        if (aut.SesionActiva == "S")
                        {
                            int tiempo = (int)(0.9 * (Session.Timeout * 10 * 60 * 1000));//min *seg* miliseg
                            //int tiempo = 3000;
                            StringBuilder script = new StringBuilder();
                            script.Append("setInterval('MantenSesion()',");
                            script.Append(tiempo.ToString());
                            script.Append(");");
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "sessionactiva", script.ToString(), true);
                        }
                    }
                }
            }

        }


        private bool TieneAcceso(Autenticacion auth)
        {
            if (Acceso.verificarPermiso(auth, "PAGINA", System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]), int.Parse(ConfigurationManager.AppSettings["IdSistema"].ToString())))
                return true;
            else
                //if (Request.Params["ub"].ToString() != null )
                //{
                //    return true;
                //}
                //else
                //{
                    return false;
                //}
        }

        private void JavaScriptCerrarPagina()
        {
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language=JavaScript>");
            strScript.Append("var g_isPostBack = false;");
            strScript.Append("window.onbeforeunload = function (){if (g_isPostBack==false){alert('Para salir de esta página es necesario Cerrar la sesión'); return false;};};</script>");

            btnCerrarSession.Attributes.Add("onclick", "g_isPostBack = true;");


            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "pop", strScript.ToString());
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "OnSubmitScript", "g_isPostBack = true;");
            //Explicación: Se crea una variable g_isPostBack que se establece a true cada vez que se hace postback
            // es decir que con la instruccion: RegisterOnSubmitStatement se establece a true la variable para cada postback que 
            // se haga e la pagina.
            // se agrego tambien la asignacion de la variable en crtl.attrikbutes.add porque este boton
            // hace un abandono de la pagina a la de login por lo tanto la variable seria false.
            // en resumen la variable solo será false cuando se cierre la pagina en la cruz del explorador y por lo tanto hay que 
            // liberar la OT.
        }


        protected void btnprincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Principal/wfPrincipal.aspx", true);
        }

        protected void btnCerrarSession_Click(object sender, EventArgs e)
        {

            try
            {
                ActualizaUsuarioConectado();
                FormsAuthentication.SignOut();
                foreach (var cookie in Request.Cookies.AllKeys)
                {
                    Request.Cookies.Remove(cookie);
                }
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
                lblUsuario.Text = "";
            }
            catch (Exception ex)
            { 
            }

        }

        private void ActualizaUsuarioConectado()
        {
            int idSistema = (int)read.GetValue("Sistema", typeof(int));
            UsuarioSistema dtoUsuarioSistema = new UsuarioSistema();
            dtoUsuarioSistema.idSistema = (int)read.GetValue("Sistema", typeof(int));
            dtoUsuarioSistema.idUsuario = NegocioUsuario.Instancia.ObtenerUsuario(Context.User.Identity.Name, idSistema).idUsuario;
            dtoUsuarioSistema.conectado = false;
            NegocioUsuarioSistema.Instancia.ActualizarEstado(dtoUsuarioSistema);
        }

        private string ObtenerUsuario()
        {
            if (Context.User.Identity.IsAuthenticated)
                return Context.User.Identity.Name;
            else
                return "";
        }

        private void LlenarMenuSistema(Usuario _usuario, int idSistema)
        {
            HtmlGenericControl ul_main = new HtmlGenericControl("ul");
            HtmlGenericControl li_titles = new HtmlGenericControl("li");
            HtmlGenericControl a_link = new HtmlGenericControl("a");



            if (_usuario != null)
            {
                //List<MenuSistema> listaMenu = NegocioMenu.Instancia.Listar(idSistema, _usuario.idUsuario);

                List<MenuSistema> listaMenu = NegocioMenu.Instancia.Listar(idSistema, _usuario.idUsuario);
                foreach (MenuSistema menu in listaMenu)
                {
                    if (menu.menuId.Equals(menu.padreId)) //es elemento padre
                    {
                        li_titles = new HtmlGenericControl("li");
                        li_titles.Attributes.Add("idMenu", menu.menuId.ToString());
                        a_link = new HtmlGenericControl("a");
                        string pagina = (string.Concat(ResolveClientUrl(menu.Ruta), menu.Pagina) == "" ? "#" : string.Concat(ResolveClientUrl(menu.Ruta), menu.Pagina));
                        a_link.Attributes.Add("href", pagina);
                        a_link.InnerHtml = menu.descripcion;
                        li_titles.Controls.Add(a_link);
                        AgregarHijosMenu(li_titles, listaMenu);
                        li_titles.Attributes.Add("class", "current");
                        ul_main.Controls.Add(li_titles);
                    }
                }

                ul_main.Attributes.Add("class", "sf-menu");
                ul_main.Controls.Add(li_titles);
                AgregarAcercaDe(ul_main);

                pnlMenu.Controls.Add(ul_main);
            }
        }

        public void AgregarAcercaDe(HtmlGenericControl ul)
        {
            lblVersionAcercaDe.Text = lblVersion.Text;
            HtmlGenericControl li = new HtmlGenericControl("li");
            HtmlGenericControl a = new HtmlGenericControl("a");
            a.Attributes.Add("href", "#");
            a.InnerHtml = "Acerca de";
            a.Attributes.Add("onclick", "document.getElementById('" + btnPopupAcercaDe.ClientID + "').click();");
            li.Controls.Add(a);
            li.Attributes.Add("class", "current");
            ul.Controls.Add(li);
        }


        private void AgregarHijosMenu(HtmlGenericControl ctrl, List<MenuSistema> listaMenu)
        {
            //Cadena funcionCadena = new Cadena();
            HtmlGenericControl ul_nav = new HtmlGenericControl("ul");
            HtmlGenericControl li_nav = new HtmlGenericControl("li");
            HtmlGenericControl a_link = new HtmlGenericControl("a");
            HtmlGenericControl p = new HtmlGenericControl("p");
            //ul_nav.Attributes.Add("class", "subnav");

            List<MenuSistema> hijos = listaMenu.FindAll(delegate(MenuSistema mnu)
            {
                return mnu.menuId != mnu.padreId && mnu.padreId == int.Parse(ctrl.Attributes["idMenu"].ToString());
            });

            if (hijos.Count > 0)
            {
                foreach (MenuSistema menu in hijos)
                {
                    li_nav = new HtmlGenericControl("li");
                    a_link = new HtmlGenericControl("a");
                    p = new HtmlGenericControl("p");
                    string pagina = (string.Concat(menu.Ruta, menu.Pagina) == "" ? "#" : string.Concat((menu.Ruta == "" ? ResolveClientUrl("~") : ResolveClientUrl(menu.Ruta)), menu.Pagina));
                    a_link.Attributes.Add("href", pagina);
                    a_link.InnerHtml = menu.descripcion;
                    p.Controls.Add(a_link);
                    p.Attributes.Add("style", "margin-left:20px");
                    li_nav.Controls.Add(p);
                    li_nav.Attributes.Add("idMenu", menu.menuId.ToString());
                    AgregarHijosMenu(li_nav, listaMenu);
                    string url = menu.icono;
                    string ruta = ResolveClientUrl("~");
                    li_nav.Attributes.Add("style", "background-image:url(" + ruta + "images/" + url + ");background-repeat:no-repeat;background-position:left center;");
                    ul_nav.Controls.Add(li_nav);
                }
                ctrl.Controls.Add(ul_nav);
            }
        }

        protected void btnCambiarPassword_Click(object sender, EventArgs e)
        {
            string url = ResolveClientUrl("~/wfCambiarPassword.aspx");
            Response.Redirect(url);
        }
    }
}