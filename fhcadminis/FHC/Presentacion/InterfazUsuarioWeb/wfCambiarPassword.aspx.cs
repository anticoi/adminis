﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.Framework.Utilidades;
using System.Configuration;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    public partial class wfCambiarPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ucNewCambiarPassword.Login = SessionActual.ObtenerUsuario();

            if (ucNewCambiarPassword.Login == "")
                ucNewCambiarPassword.Login = Session["usuario"].ToString();



            ucNewCambiarPassword.IdSistema = int.Parse(ConfigurationManager.AppSettings["IdSistema"].ToString());
        }

        protected void OnCambiarClave(object obj, EventArgs e)
        {

        }
    }
}