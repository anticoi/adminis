﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using TGENESIS.FHC.EntidadesNegocio;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;



namespace InterfazUsuarioWeb.Envio_Factura
{
    public partial class wfDevolucionCorreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 12);

                lTitulo.Text = "Devolución de Correo";

                LlenarGrilla();
                
            }
        }

        protected void btnBuscarC_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_BuscarDevolucionCorreoxContrato", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Contrato", txtContrato.Text.ToString());

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    txtFechaEnv.Text = Convert.ToDateTime(reader["ef_fechaenvio"]).ToString("dd-MM-yyyy");
                    txtCliente.Text = reader["ef_cliente"].ToString();
                    txtDireccion.Text = reader["ef_direccion"].ToString();
                }
                else
                {
                    lblMensaje.Text = "El Contrato buscado no existe";

                    txtFechaEnv.Text = "";
                    txtCliente.Text = "";
                    txtDireccion.Text = "";
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ActualizaEnvioDocs", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Correlativo", txtContrato.Text);
                cmd.Parameters.AddWithValue("@FechaDevolucion", txtFechaDev.Text);
                cmd.Parameters.AddWithValue("@RecibidoPor", txtRecibidoPor.Text);
                cmd.Parameters.AddWithValue("@Motivo", txtMotivo.Text);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                LlenarGrilla();

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "Devolución Agregada");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void LlenarGrilla()
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarDevolucionDocs", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                grvDevolucion.DataSource = dt;
                grvDevolucion.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error", ex.Message);
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /*Verifies that the control is rendered */
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {

            ExportGridToPDF();

        }

        private void ExportGridToPDF()  
          {  
  
            Response.ContentType = "application/pdf";  
            Response.AddHeader("content-disposition", "attachment;filename=Devolucion_Doc.pdf");  
            Response.Cache.SetCacheability(HttpCacheability.NoCache);  
            StringWriter sw = new StringWriter();  
            HtmlTextWriter hw = new HtmlTextWriter(sw);  
            
            grvDevolucion.RenderControl(hw);  

            StringReader sr = new StringReader(sw.ToString());  
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);  
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);  
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);  
            pdfDoc.Open();  
            htmlparser.Parse(sr);  
            pdfDoc.Close();  
            Response.Write(pdfDoc);  
            Response.End();
            grvDevolucion.AllowPaging = true;
            grvDevolucion.DataBind();  
          }
    }
}