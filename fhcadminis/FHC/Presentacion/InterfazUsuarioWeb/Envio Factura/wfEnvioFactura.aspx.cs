﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using TGENESIS.FHC.EntidadesNegocio;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser; 

namespace InterfazUsuarioWeb.Envio_Factura
{
    public partial class wfEnvioFactura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 12);

                lTitulo.Text = "Envio de Documentación";

                LlenarGrilla();
            }
        }

        protected void btnBuscarC_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL ccb = new ConexionSQL();
                SqlConnection cnb = ccb.AbrirConexionIntranet();

                SqlCommand cmdb = new SqlCommand("Usp_BuscarDatosContratante", cnb);
                cmdb.CommandType = CommandType.StoredProcedure;
                cmdb.Parameters.AddWithValue("@CONTRATO", txtContrato.Text.ToString());

                SqlDataReader reader = cmdb.ExecuteReader();


                if (reader.Read())
                {
                    txtCliente.Text = reader["NOMBRE"].ToString();
                    txtDireccion.Text = reader["DIRECCION"].ToString();
                    txtComuna.Text = reader["COMUNA"].ToString();
                }
                else
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "El Contrato Nº " + txtContrato.Text + " no existe");

                    txtCliente.Text = "";
                    txtDireccion.Text = "";
                    txtComuna.Text = "";
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_InsertaEnvioDocs", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Contrato", txtContrato.Text);
                cmd.Parameters.AddWithValue("@Cliente", txtCliente.Text);
                cmd.Parameters.AddWithValue("@Direccion", txtDireccion.Text);
                cmd.Parameters.AddWithValue("@Comuna", txtComuna.Text);
                cmd.Parameters.AddWithValue("@Correo", txtCorreo.Text);
                cmd.Parameters.AddWithValue("@FechaEnvio", txtFecha.Text);
                cmd.Parameters.AddWithValue("@Monto", txtMonto.Text);
                cmd.Parameters.AddWithValue("@Factura", txtFactura.Text);
                cmd.Parameters.AddWithValue("@Observacion", txtObservacion.Text);
                cmd.Parameters.AddWithValue("@Estado", 1);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                LlenarGrilla();

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion,"Información", "Cliente Agregado a la Lista");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void LlenarGrilla()
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarEnvioDocs", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                grvEnvioFact.DataSource = dt;
                grvEnvioFact.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error", ex.Message);
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /*Verifies that the control is rendered */
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            ExportGridToPDF(); 
        }

        private void ExportGridToPDF()
        {

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Devolucion_Doc.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grvEnvioFact.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
            grvEnvioFact.AllowPaging = true;
            grvEnvioFact.DataBind();
        }
    }
}