﻿<%@ Page Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfDevolucionCorreo.aspx.cs" Inherits="InterfazUsuarioWeb.Envio_Factura.wfDevolucionCorreo" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function solonumeros(e) {
        var key;
        if (window.event) // IE
        {
            key = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = e.which;
        }

        if (key < 48 || key > 57) {
            return false;
        }
        return true;
    }

    $(function () {

        $("#<%= txtFechaEnv.ClientID %>").datepicker({
            showSecond: true,
            timeFormat: 'hh:mm:ss',
            dateFormat: 'dd-mm-yy',
            stepHour: 1,
            stepMinute: 5,
            minDate: new Date(2011, 08, 01, 8, 30),
            changeMonth: true,
            changeYear: true,
            firstDay: 1,
            stepSecond: 5,
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
        });

        $("#<%= txtFechaDev.ClientID %>").datepicker({
            showSecond: true,
            timeFormat: 'hh:mm:ss',
            dateFormat: 'dd-mm-yy',
            stepHour: 1,
            stepMinute: 5,
            minDate: new Date(2011, 08, 01, 8, 30),
            changeMonth: true,
            changeYear: true,
            firstDay: 1,
            stepSecond: 5,
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
        });

    });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="800px" style="height:300px;">
        <tr>
            <td align="center">
                <table width="550px" >
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Label ID="lblCorrela" runat="server" Text="Contrato"></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:TextBox id ="txtContrato" runat="server"  MaxLength="100" Width="100px"></asp:TextBox>
                            
                            
                            <asp:Button ID="btnBuscarC" runat="server" Text="Buscar" Width="100px"  
                                 Height="25px" class="button small blue" onclick="btnBuscarC_Click" /> 
                        </td>           
                    </tr>

                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFechaEnv" runat="server" Text="Fecha Envio"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtFechaEnv" runat="server"  Text="" MaxLength="10" 
                                Width="100px"  AutoPostBack="true" data-watermark="•"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblCliente" runat="server" Text="Cliente"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtCliente" runat="server" Width="400px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblDireccion" runat="server" Text="Dirección"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtDireccion" runat="server" Width="400px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblFecha" runat="server" Text="Fecha Devolución"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtFechaDev" runat="server"  Text="" MaxLength="10" 
                                Width="100px"  AutoPostBack="true" data-watermark="•"></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblRecibido" runat="server" Text="Recibido Por"></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:TextBox ID="txtRecibidoPor" runat="server" Text="" MaxLength="20" Width="180px" ></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblMotivo" runat="server" Text="Motivo"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtMotivo" runat="server" Text="" TextMode="MultiLine" Height="80px" Width="230px"></asp:TextBox>
                            <asp:Label ID="label1" runat="server"></asp:Label>
                            <asp:Button ID="btnAgregar" runat="server" Text="Agregar" Width="100px"  
                                 Height="25px" class="button small blue" onclick="btnAgregar_Click" />
                        </td>
           
                    </tr>
                    
                </table>
            </td>
        </tr>


        <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
    </table>
    <table width="800px" style="height:300px;">
        <tr>
            <td align="center" >
                <div id="divdetallecontrato" runat="server" style="width:900px; overflow: scroll; height:250px;">
                    <asp:GridView ID="grvDevolucion" runat="server" CssClass="mGrid" Width="900px" 
                                AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None"
                                AlternatingRowStyle-BackColor = "#C2D69B">
                        <Columns>
                            <asp:BoundField DataField="ef_correlativo"  HeaderText="Correlativo" >
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ef_fechaenvio"  HeaderText="Fecha Envio" >
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ef_cliente"  HeaderText="Cliente" >
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Dirección">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# String.Format("{0}-{1}", Eval("ef_direccion"),Eval("ef_comuna"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="ef_motivodevo"  HeaderText="Motivo Devolución" >
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ef_recibidopor"  HeaderText="Recibido Por" >
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                            </asp:BoundField>
                                                                                          
                            <asp:BoundField DataField="ef_fechadevolucion"  HeaderText="Fecha Devolución" >
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </td>               
        </tr>

        <td class="style2" align="right">
            <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Width="100px"  
            Height="25px" class="button small blue" onclick="btnImprimir_Click" /> 
        </td> 
    </table>
</asp:Content>
