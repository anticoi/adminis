﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ControlUsuario.Controles;
using System.Configuration;
using TGENESIS.FHC.ComponentesNegocio;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    public partial class wfAsignaRol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            ucAsignaRol.IdSistema = int.Parse(ConfigurationManager.AppSettings["IdSistema"].ToString());

            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 5);

                lTitulo.Text = NombreMenu.ToString();
            }
        }
    }
}