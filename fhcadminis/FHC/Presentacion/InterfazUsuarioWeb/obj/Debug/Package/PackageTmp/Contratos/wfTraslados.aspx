﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfTraslados.aspx.cs" Inherits="InterfazUsuarioWeb.Contratos.wfTraslados" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function ValidarDato(source, arguments) {
            if ($.trim(arguments.Value) == "") {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FF5959";
                arguments.IsValid = false;
            }
            else {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FFFFFF";
                arguments.IsValid = true;
            }
        }


        $(function () {

            $("#<%= txtFechaDesde.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

            $("#<%= txtFechaHasta.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

        });

        
      </script>
    <style type="text/css">
        .style1
        {
            height: 45px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="900px" style="height:300px;">
        <%--Diseño Filtros--%>
        <tr>
            <td align="center"> 
                <table width="600px" style="height:100px;">
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td align="center">
                                        <asp:Label  ID="lblFechaDesde" runat="server" Text="Desde"></asp:Label>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtFechaDesde" runat="server" data-watermark="•" 
                                            ontextchanged="txtFechaDesde_TextChanged"></asp:TextBox>
                                    </td>

                                    <td align="center">
                                        <asp:Label  ID="lblFechaHasta" runat="server" Text="Hasta"></asp:Label>
                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtFechaHasta" runat="server" data-watermark="•" 
                                            ontextchanged="txtFechaHasta_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="style1" colspan="2" align="right">
                                        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" 
                                            class="button small blue" Width="80px" Height="25px" 
                                            onclick="btnFiltrar_Click" />   
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <%--Diseño Cabecera Grilla--%>
        <%--Diseño Grilla--%>
        <tr>
            <td align="center">
                <table width="550px" >
                    <tr>
                        <td>
                            <div id="divubicaciones" runat="server" 
                                style="width:909px; overflow: auto; height:250px;">
                                <asp:GridView ID="grvTRAS" runat="server" CssClass="mGrid" 
                                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                                    onselectedindexchanged="grvTRAS_SelectedIndexChanged" PageSize="8" >
                                    <Columns>
                                        <asp:BoundField DataField="OPER_NUM"  HeaderText="Contrato" >
                                            <HeaderStyle Width="80px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>                                            
                                                        
                                        <asp:BoundField DataField="Asesor"  HeaderText="Asesor Integral" >
                                            <HeaderStyle Width="200px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>                                            

                                        <asp:BoundField DataField="SUCU_DESC"  HeaderText="Sucursal" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>                                    

                                        <asp:BoundField DataField="PLAN_DESC"  HeaderText="Plan" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>
                                                    
                                        <asp:BoundField DataField="COMU_DEF"  HeaderText="Comuna Defunción" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>

                                        <asp:BoundField DataField="COMU_INS"  HeaderText="Comuna Instalación" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>

                                        <asp:BoundField DataField="COMU_FUN"  HeaderText="Comuna Funeral" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>

                                        <asp:BoundField DataField="CARGO"  HeaderText="Cargo" DataFormatString="{0:N0}" 
                                            HtmlEncode="False" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>

                                        <asp:BoundField DataField="DESCUENTO"  HeaderText="Descuento" 
                                            DataFormatString="{0:N0}" HtmlEncode="False" >
                                            <HeaderStyle Width="120px" />
                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                        </asp:BoundField>
                                    </Columns>

                                    <%--Referencia cuando no hay datos que mostrar--%>
                                    <EmptyDataTemplate>
                                        <i>{No hay datos que mostrar}</i>
                                    </EmptyDataTemplate>
                                    <PagerSettings Mode="NextPrevious" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    
        <tr>
            <td>
                <uc1:ucmensaje ID="ucMensaje1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
