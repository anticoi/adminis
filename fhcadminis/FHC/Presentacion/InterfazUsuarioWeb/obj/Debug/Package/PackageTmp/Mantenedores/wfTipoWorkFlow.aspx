﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfTipoWorkFlow.aspx.cs" Inherits="TGENESIS.FHC.InterfazUsuarioWeb.Mantenedores.wfTipoWorkFlow" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




<table width="600px" style="height:300px;">
        <tr>
            <td valign="top"  align="center">
                <table width="80%">
                    <tr>
                            <td align="center">
            
                                <asp:Label  ID="lblTipoWorkFlow" runat="server" Text="Tipo Tarea"></asp:Label>
                            </td>

                            <td>
                                    <asp:TextBox id ="txtTipoWorkFlow" runat="server"  MaxLength="100" 
                                        Width="300px" ontextchanged="txtTipoWorkFlow_TextChanged"></asp:TextBox>
                            </td>

                            <td>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar"  class="button small blue" onclick="btnAgregar_Click" Width="100px" Height="25px"/>
                            </td>
                    </tr>
                    <tr>
                    <td>
                            <asp:Label ID="Label1" runat="server" Text="Departamento"></asp:Label>
                    </td>
                    <td class="style3">

                                        <div class="select"   style="width:380px;"> 
                                                <asp:DropDownList ID="ddlbarea" runat="server" Width="400px" 
                                                    onselectedindexchanged="ddlbarea_SelectedIndexChanged" ></asp:DropDownList>
                                </div>
                    </td>
                </tr>
                    <tr>
                        <td align="center" colspan="6">
                                <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                                        <asp:GridView ID="grvtipoworkflow" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                        
                                            <Columns>
                                                    <asp:TemplateField>
                                                                <ItemTemplate>
                                                                        <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("TipoWorkFlowId") %>' />
                                                                </ItemTemplate>
                                                            <HeaderStyle Width="50px" />
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="TipoWorkFDescripcion"  HeaderText="Descripción" >
                                                            <HeaderStyle Width="220px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                            

                                                    <asp:BoundField DataField="TipoWorkFlowEstado"  HeaderText="Estado" >
                                                            <HeaderStyle Width="50px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                            

                                                    <asp:BoundField DataField="TipoWorkFlowFechaIngreso"  HeaderText="Fec. Creación" >
                                                            <HeaderStyle Width="120px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                    

                                                    <asp:BoundField DataField="TipoWorkFlowUsuarioIngreso"  HeaderText="Usu. Creación" >
                                                            <HeaderStyle Width="120px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                                        

                                            </Columns>
                                        
                                        </asp:GridView>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar"  Width="100px" Height="25px" class="button small blue" onclick="btnEliminar_Click" />                            
                        </td>
                    </tr>
                </table>
            </td>

        </tr>

        <tr>
            <td>
                        <uc1:ucmensaje ID="ucMensaje1" runat="server" />
            </td>
        </tr>

</table>


</asp:Content>
