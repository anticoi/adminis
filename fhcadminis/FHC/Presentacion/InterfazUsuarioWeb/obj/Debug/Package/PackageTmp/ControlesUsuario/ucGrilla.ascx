﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGrilla.ascx.cs" Inherits="ControlUsuario.Controles.ucGrilla" %>
<%@ Register src="ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>


<table>
    <tr>
        <td>
            <asp:Panel ID = "pnlSearch" runat= "server">
                        <table width="800px">
                            <tr>
                                    <td align="center">
                                        <table width="600px">
                                            <tr>
                                                    <td>
                                                            Serie
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                                                            <ContentTemplate>                                                        
                                                                <asp:TextBox ID="txtserie" runat="server" MaxLength="15"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td>
                                                                <asp:Button runat="server" ID="btnBuscarSerie" Text="Consultar" class="button small blue" Width="100px" onclick="btnBuscarSerie_Click" />
                                                    </td>                                            
                                            </tr>
                                        </table>
                                    </td>
                            </tr>

                            <tr>
                                    <td class="th">
                                            <table width="100%">
                                                                                                    <tr>
                                                                                                            <td align="center" style="width:50%;">
                                                                                                                    <asp:Label id="lblcargados" runat="server" Text="Cargados: " ForeColor="White"></asp:Label>
                                                                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                                    <asp:LinkButton ID="lbltotal" runat="server" Text="0" ForeColor="White" 
                                                                                                                        onclick="lbltotal_Click"></asp:LinkButton>
                                                                                                            </td>
                                                                                                            <td align="center" style="width:50%;">
                                                                                                                <asp:UpdatePanel ID="updcontador" runat="server">
                                                                                                                    <ContentTemplate>
                                                                                                                                        <asp:Label id="lblseleccionados" runat="server" Text="Seleccionados: " ForeColor="White"></asp:Label>
                                                                                                                                        &nbsp;&nbsp;&nbsp;
                                                                                                                                        <asp:Label ID="lblcontador" runat="server"  Text="0"   ForeColor="White" ></asp:Label>
                                                                                                                        </ContentTemplate>
                                                                                                                    </asp:UpdatePanel>    
                                                                                                            </td>                                    
                                                                                                    </tr>
                                                </table>                            
                                    </td>
                            </tr>

                            <tr>
                                <td colspan="1">
                                        <asp:Panel ID="pnl" runat="server" Width="100%" Height="300px" ScrollBars="Auto"  >
                                            <asp:UpdatePanel ID="updgrilla" runat="server" >
                                                <ContentTemplate>
                                                                <asp:GridView ID="gvseriescargadas" runat="server" CssClass="mGrid" 
                                                                    AutoGenerateColumns="False" Width="98%" Height="270px" 
                                                                    AllowPaging="True" AllowSorting="True"  PageSize="7"
                                                                    ondatabound="gvseriescargadas_DataBound" 
                                                                    onrowdatabound="gvseriescargadas_RowDataBound" >
                                                                    <Columns>
                                                                            <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                                <asp:CheckBox ID="chk"  runat="server"  AutoPostBack="True" 
                                                                                                    OnCheckedChanged="chkOnCheckedChanged" 
                                                                                                    Checked='<%# Eval("chk") %>'/>
                                                                                        </ItemTemplate>
                                                                                    <HeaderStyle Width="50px" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="Serie"  DataField="imei" >
                                                                                    <HeaderStyle Width="120px" />
                                                                                        <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Modelo"  DataField="Modelo" >
                                                                                    <HeaderStyle Width="150px" />
                                                                                        <ItemStyle Wrap="False" Width="150px"></ItemStyle> 
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderText="Asignable">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="LnBMmotivos" runat="server" CommandArgument='<%# Eval("id") %>' Text='<%# Eval("asignable") %>' 
                                                                                    CommandName="vermotivos"></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="40px" />
                                                                                <ItemStyle Width="40px" />
                                                                            </asp:TemplateField>     
                                                                            <asp:BoundField HeaderText="F. Ult. Mov."  ItemStyle-CssClass="ItemGrilla" DataField="fecha" >
                                                                                    <HeaderStyle Width="100px" />
                                                                                        <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Usuario"  ItemStyle-CssClass="ItemGrilla" DataField="Usuario" >
                                                                                    <HeaderStyle Width="100px" />
                                                                                        <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                                            </asp:BoundField>
                                                                    </Columns>                                                                        
 
                                                                    <RowStyle BackColor="#EFF3FB" Wrap="true"  />
                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                    <PagerStyle ForeColor="White" HorizontalAlign="Left" Height="32px" />
                                                                    <HeaderStyle   Font-Bold="True" ForeColor="White" Height="26px" />
                    
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <PagerSettings  Mode="NextPreviousFirstLast"
                                                                    FirstPageImageUrl="~/imagenes/primera.png" 
                                                                    FirstPageText="Inicial"
                                                                    PreviousPageImageUrl="~/imagenes/ultima.png" 
                                                                    PreviousPageText="Anterior"
                                                                    LastPageImageUrl="~/imagenes/atras.png" 
                                                                    LastPageText="&#218;ltima" 
                                                                    NextPageImageUrl="~/imagenes/siguiente.png" 
                                                                    NextPageText="Siguiente"/> 

                                                                    <PagerTemplate>
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="right" class="th"  style="width:550px;">
                                                                                   <table border="0" cellpadding="0" cellspacing="0" >
                                                                                            <tr>
                                                                                                <td style="height:24px; width:26px; background-image:url(../Images/primera.png);">
                                                                                                    <asp:Button ID="btnFirst" runat="server" CausesValidation="False" OnClick="btnFirst_Click"
                                                                                                    BorderStyle="none"
                                                                                                    Style="background-color:Transparent; border-top-style: none; cursor:hand;"
                                                                                                    Height="24px" 
                                                                                                    Width="26px" 
                                                                                                    ToolTip="Inicial" />
                                                                                                </td>
                                                                                                <td style="height:24px; width:26px; background-image:url(../Images/atras.png);">
                                                                                                    <asp:Button ID="btnPrev" runat="server" CausesValidation="False" OnClick="btnPrev_Click"
                                                                                                    BorderStyle="none"
                                                                                                    Style="background-color:Transparent; border-top-style: none; cursor:hand;"
                                                                                                    Height="24px" 
                                                                                                    Width="26px" 
                                                                                                    ToolTip="Anterior" />
                                                                                                </td>
                                                                                                <td style="height:24px; width:26px; background-image:url(../Images/siguiente.png);">
                                                                                                    <asp:Button ID="btnNext" runat="server" CausesValidation="False" OnClick="btnNext_Click"
                                                                                                    BorderStyle="none"
                                                                                                    Style="background-color:Transparent; border-top-style: none; cursor:hand;"
                                                                                                    Height="24px" 
                                                                                                    Width="26px" 
                                                                                                    ToolTip="Siguiente" />
                                                                                                </td>
                                                                                                <td style="height:24px; width:26px; background-image:url(../Images/ultima.png);">
                                                                                                    <asp:Button ID="btnLast" runat="server" CausesValidation="False" OnClick="btnLast_Click"
                                                                                                    BorderStyle="none"
                                                                                                    Style="background-color:Transparent; border-top-style: none; cursor:hand;"
                                                                                                    Height="24px" 
                                                                                                    Width="26px" 
                                                                                                    ToolTip="&Uacute;ltima" />
                                                                                                </td>                                            
                                                                                            </tr>
                                                                                        </table>                                                                      


                                                                                </td>
                                                                                <td align="right" class="th">
                                                                                <asp:Label ID="lbltitPagina" runat="server"  ForeColor="White"  Text="  P&aacute;gina "  >
                                                                                </asp:Label>
                                                                                &nbsp;<asp:DropDownList ID="ddlPages" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPages_SelectedIndexChanged" CssClass="textoStandar">
                                                                                </asp:DropDownList>
                                                                                <asp:Label ID="lblde" runat="server" Text="  de " ForeColor="White"></asp:Label>
                                                                                <asp:Label ID="lblPageCount" runat="server"  ForeColor="White"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>

                                                                            </table>
                                                                    </PagerTemplate>    
                                                                </asp:GridView>
                                                </ContentTemplate>

                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnBuscarSerie" EventName="Click" />
                                                 </Triggers>
                                            </asp:UpdatePanel>

                                        </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                    <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width:100%;" align="right" class="th">
                                                                    <asp:Button runat="server" ID="btnguardar" Text="Recepcionar" class="button small blue" Width="100px" onclick="btnguardar_Click" />
                                                            </td>                                                                                                                                                                 
                                                        </tr>
                                                    </table>              
                                    </td>
                            </tr>

                        </table>

            
            </asp:Panel>

        </td>
    </tr>

    <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>

</table>


