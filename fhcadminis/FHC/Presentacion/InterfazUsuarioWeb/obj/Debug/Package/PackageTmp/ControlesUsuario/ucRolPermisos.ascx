﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucRolPermisos.ascx.cs" Inherits="ControlUsuario.Controles.ucRolPermisos" %>

<table style="margin-right: 0px" width="90%">
    <tr>
        <td colspan="2">
                        </td>
    </tr>
    <tr>
        <td colspan="2">
                        </td>
    </tr>
    <tr align="left">
        <td>
            <asp:Label ID="lblUsuario" runat="server" Text="Usuario: "></asp:Label>
        </td>
        <td>
            <div class="select" style="width:250px">
                        <asp:DropDownList ID="ddlUsuarios" runat="server" 
                            AutoPostBack="True" Width="300px"
                            onselectedindexchanged="ddlUsuarios_SelectedIndexChanged"  />
            </div>
        </td>
    </tr>
    <tr align="left">
        <td>
            <asp:Label ID="lblRol" runat="server" Text="Roles: "></asp:Label>
        </td>
        <td>
            <asp:UpdatePanel ID="upRoles" runat="server">
                <ContentTemplate>
                    <div class="select" style="width:250px">
                        <asp:DropDownList ID="ddlRoles" runat="server" AutoPostBack="True" Width="300px"
                            onselectedindexchanged="ddlRoles_SelectedIndexChanged" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlUsuarios" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr align="left">
        <td>
            <asp:Label ID="lblPermisos" runat="server" Text="Permisos: "></asp:Label>
        </td>
        <td>
            <div class="select" style="width:250px">
                <asp:DropDownList ID="ddlPermisos" runat="server" AutoPostBack="True" Width="300px"
                    onselectedindexchanged="ddlPermisos_SelectedIndexChanged" />
            </div>
        </td>
    </tr>
    <tr align="left">
        <td colspan="2">
            <asp:Button ID="btnAgregar" runat="server" CssClass="boton" 
                onclick="btnAgregar_Click" Text="Agregar Rol y Permiso" Width="150px" />
        &nbsp;&nbsp;
            <asp:Button ID="btnBuscar" runat="server" CssClass="boton" 
                onclick="btnBuscar_Click" Text="Buscar" Width="150px" />
            </td>
    </tr>
    <tr align="left">
        <td colspan="2">
            <asp:UpdatePanel ID="upMensaje" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAgregar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlPermisos" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlRoles" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlUsuarios" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="gvRolPermisos" EventName="RowCommand" />
                </Triggers>
            </asp:UpdatePanel>
            </td>
    </tr>
</table>
<table width="90%">
    <tr>
        <td>
        <asp:UpdatePanel ID="upRolPermisos" runat="server">
            <ContentTemplate>
            <asp:GridView ID="gvRolPermisos" runat="server" AutoGenerateColumns="False"  
                CssClass="mGrid" onrowcommand="gvRolPermisos_RowCommand" >
                <Columns>
                    <asp:BoundField DataField="login" HeaderText="Usuario" />
                    <asp:BoundField DataField="nombreElemento" HeaderText="Permiso" />
                    <asp:BoundField DataField="tipoPermiso" HeaderText="Tipo Permiso" />
                    <asp:BoundField DataField="Rol" HeaderText="Rol" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEliminar" runat="server" 
                                ImageUrl="~/imagenes/deleteGrid.png" 
                                CommandArgument='<%# Eval("IdRol") + ";" + Eval("IdPermiso") + ";" + Eval("nombreElemento") + ";" + Eval("rol") %>' 
                                CommandName="eliminar" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAgregar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ddlPermisos" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlRoles" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlUsuarios" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        </td>
    </tr>
</table>