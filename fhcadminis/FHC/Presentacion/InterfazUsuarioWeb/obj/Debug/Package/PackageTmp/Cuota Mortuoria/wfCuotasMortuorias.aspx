﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfCuotasMortuorias.aspx.cs" Inherits="InterfazUsuarioWeb.Contratos.wfCuotasMortuorias" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function ValidarDato(source, arguments) {
            if ($.trim(arguments.Value) == "") {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FF5959";
                arguments.IsValid = false;
            }
            else {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FFFFFF";
                arguments.IsValid = true;
            }
        }


        $(function () {

            $("#<%= txtFechaInicio.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

            $("#<%= txtFechaFinal.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

        });

        
      </script>


    <style type="text/css">
        .style1
        {
            height: 45px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="600px" style="height:300px;">
    <tr>
        <td valign="top" align="center">
            <table width="80%">
                <tr>
                    <td align="center">
                        <asp:Label  ID="lblFechaInicio" runat="server" Text="Inicio"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox id ="txtFechaInicio" runat="server"></asp:TextBox>
                    </td>
                    <td align="center">
                        <asp:Label  ID="lblFechaTermino" runat="server" Text="Termino"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox id ="txtFechaFinal" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar"  class="button small blue" onclick="btnFiltrar_Click" />
                    </td>
                </tr>
            </table>                  
        </td>
    </tr>

    <tr>
        <td align="center" >
            <div id="divdetallecontrato" runat="server" style="width:900px; overflow: scroll; height:250px;">
                <asp:GridView ID="grvCMORT" runat="server" CssClass="mGrid" Width="900px" 
                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                    onrowcommand="grvCMORT_RowCommand" 
                    onrowdatabound="grvCMORT_RowDataBound" 
                    onselectedindexchanged="grvCMORT_SelectedIndexChanged" >
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnmail" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/atras.png" Width="20px" Height="20px" CommandArgument='<%# Eval("icono") %>' Text='<%# Eval("Icono") %>' CommandName="actualizarcm" /> 
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>  

                        <asp:BoundField DataField="OPER_NUM"  HeaderText="Contrato" >
                            <HeaderStyle Width="260px" />
                            <ItemStyle Width="260px" HorizontalAlign="Left" />
                        </asp:BoundField>
                                                                                          
                        <asp:BoundField DataField="OPER_FECH"  HeaderText="Fecha Contrato" >
                            <HeaderStyle Width="300px" />
                            <ItemStyle Width="300px" HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField DataField="Asesor"  HeaderText="Asesor" >
                            <HeaderStyle Width="300px" />
                            <ItemStyle Width="300px" HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField DataField="SUCU_DESC"  HeaderText="Sucursal" >
                            <HeaderStyle Width="300px" />
                            <ItemStyle Width="300px" HorizontalAlign="Left" />
                        </asp:BoundField>

                        <asp:BoundField DataField="FOPA_PAGO_COD"  HeaderText="Forma de Pago" >
                            <HeaderStyle Width="200px" />
                            <ItemStyle Width="200px" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <asp:Button ID="btnpopupgrilla" runat="server"  Text="pepepepe" style="display:none" />
                <cc1:ModalPopupExtender ID="mpepanel" runat="server"  PopupControlID="pnlenviarcorreo" TargetControlID="btnpopupgrilla"  BackgroundCssClass="fondoAplicacion"  />        
                    <asp:Panel ID="pnlenviarcorreo" runat="server"  DefaultButton="btnActualizar"  >
                        <table border="1" >
                            <tr>
                                <td class="style1">
                                    <div  style=" height:206px; width:328px">
                                        <table border="0" style="background-color: #FFFFFF" >
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hdfruta" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdfarchivo" runat="server" Value=""/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width:700px; height:20px;">
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../imagenes/delete.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td  style="width:80px;">
                                                                <asp:Label ID="lblContrato" runat="server" Text="Contrato"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtContratoCM" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar"  
                                                                    class="button small blue" onclick="btnActualizar_Click" />                                                                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  style="width:80px;">
                                                                <asp:Label ID="lblCheque" runat="server" Text="¿Cheque en Custodia?"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:CheckBox ID="chkCheque" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblEstadoFact" runat="server" Text="Estado Factura"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:DropDownList ID="ddlEstaFact" runat="server">
                                                                    <asp:ListItem Text="Sin Factura" Value="Sin Factura" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Emitida" Value="Emitida"></asp:ListItem>
                                                                    <asp:ListItem Text="Presentada" Value="Presentada"></asp:ListItem>
                                                                    <asp:ListItem Text="Pagada" Value="Pagada"></asp:ListItem>
                                                                    <asp:ListItem Text="Rechazada" Value="Rechazada"></asp:ListItem>
                                                                </asp:DropDownList>
                                                             </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>  
        </td>
    </tr>

    <tr>
        <td>
            <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>


<asp:HiddenField ID="hdfimprimir" runat="server" Value="" />
</asp:Content>
