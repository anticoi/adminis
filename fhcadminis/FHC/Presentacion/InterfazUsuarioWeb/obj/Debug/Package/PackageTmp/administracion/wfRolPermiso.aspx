﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfRolPermiso.aspx.cs" Inherits="InterfazUsuarioWeb.administracion.wfRolPermiso" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table border="0" width="900px" style="height:400px;" >

    <tr>
        <td style="height:5px;">&nbsp;</td>
    </tr>


    <tr>
        <td align="center" valign="top">
                <table width="400px">
                    
                    <tr>
                        <td colspan="2" style="height:5px;">
                            <asp:Label ID="lblFiltrosOpcionales" runat="server" Font-Bold="true" Text="Filtros Opcionales"></asp:Label>
                        </td>
                    </tr>
                                        
                    <tr>
                        <td style="width:100px">
                            <asp:Label ID="lblArea" runat="server" Text = "Departamento"></asp:Label>
                        </td>
                        <td>
                            <div class="select" style="width:200px;" >
                                        <asp:DropDownList ID="ddlbArea" runat="server" Width="220px"  
                                            AutoPostBack="true" onselectedindexchanged="ddlbArea_SelectedIndexChanged" ></asp:DropDownList>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:100px">
                            <asp:Label ID="lblUsuario" runat="server" Text = "Usuario"></asp:Label>
                        </td>
                        <td>
                            <div class="select" style="width:300px;" >
                                        <asp:DropDownList ID="ddlbUsuario" runat="server" Width="320px" 
                                            onselectedindexchanged="ddlbUsuario_SelectedIndexChanged" ></asp:DropDownList>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="2" style="height:5px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="width:100px">
                            <asp:Label ID="lblRol" runat="server" Text = "Roles"></asp:Label>
                        </td>
                        <td>
                            <div class="select" style="width:300px;" >
                                        <asp:DropDownList ID="ddlbRol" runat="server" Width="320px" AutoPostBack="true" 
                                            onselectedindexchanged="ddlbRol_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblPermiso" runat="server" Text = "Permisos" Font-Bold="true"></asp:Label>
                        </td>
 
                    </tr>
                    <tr>
                        <td colspan="2">
                                    <div id="divpermiso" runat="server" style="width:600px; overflow: scroll; height:200px;">
                                            <asp:GridView ID="grvpermiso" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                                <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("idpermiso") %>' />
                                                                        </ItemTemplate>
                                                                    <HeaderStyle Width="50px" />
                                                            </asp:TemplateField>
                                                                  

                                                            <asp:BoundField DataField="Nombre"  HeaderText="Nombre" >
                                                                    <HeaderStyle Width="320px" />
                                                                        <ItemStyle CssClass="ItemGrilla" Width="320px"></ItemStyle> 
                                                            </asp:BoundField>                                                                   
                                                                  
                                                                  
                                                                                                            
                                                        </Columns>
                                            </asp:GridView>
                                    </div>
                        </td>
                    </tr>
                        <tr>
                            <td colspan="2" align="right">
                              <asp:Button runat="server" ID="btnAgregar" Text="Agregar" class="button small blue"  onclick="btnAgregar_Click"  />
                            </td>
                        </tr>
                </table>
        </td>
    </tr>

    <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>     
</table>



</asp:Content>
