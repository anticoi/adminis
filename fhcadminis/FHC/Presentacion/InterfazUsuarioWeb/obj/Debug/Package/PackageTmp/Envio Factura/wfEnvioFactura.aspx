﻿<%@ Page Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfEnvioFactura.aspx.cs" Inherits="InterfazUsuarioWeb.Envio_Factura.wfEnvioFactura" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function solonumeros(e) {
        var key;
        if (window.event) // IE
        {
            key = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = e.which;
        }

        if (key < 48 || key > 57) {
            return false;
        }
        return true;
    }

    $(function () {

        $("#<%= txtFecha.ClientID %>").datepicker({
            showSecond: true,
            timeFormat: 'hh:mm:ss',
            dateFormat: 'dd-mm-yy',
            stepHour: 1,
            stepMinute: 5,
            minDate: new Date(2011, 08, 01, 8, 30),
            changeMonth: true,
            changeYear: true,
            firstDay: 1,
            stepSecond: 5,
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
        });

    });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="800px" style="height:300px;">
        <tr>
            <td align="center">
                <table width="550px" >
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Label ID="lblcontrato" runat="server" Text="Contrato"></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:TextBox id ="txtContrato" runat="server"  MaxLength="100" Width="100px"></asp:TextBox>
                             
                            <asp:Label ID="LblVendedor" runat="server" Text="" Visible="false"></asp:Label>

                            <asp:Button ID="btnBuscarC" runat="server" Text="Buscar" Width="100px"  
                                 Height="25px" class="button small blue" onclick="btnBuscarC_Click" /> 
                        </td>                
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblCliente" runat="server" Text="Cliente"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtCliente" runat="server" Width="400px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblDireccion" runat="server" Text="Dirección"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtDireccion" runat="server" Width="400px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblComuna" runat="server" Text="Comuna"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtComuna" runat="server" Width="180px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblCorreo" runat="server" Text="Correo"></asp:Label>
                        </td>
                        <td class="style3">
                                <asp:TextBox ID="txtCorreo" runat="server" Width="400px"></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblFecha" runat="server" Text="Fecha Pago"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtFecha" runat="server"  Text="" MaxLength="10" 
                                Width="100px"  AutoPostBack="true" data-watermark="•"></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblMonto" runat="server" Text="Monto"></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:TextBox ID="txtMonto" runat="server" Text="" MaxLength="20" Width="180px" ></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblFactura" runat="server" Text="Factura"></asp:Label>
                        </td>
                        <td align="left" class="style3">
                            <asp:TextBox ID="txtFactura" runat="server" Text="" MaxLength="20" Width="180px" ></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="left" valign="top" style="width:150px;">
                            <asp:Label ID="lblObservacion" runat="server" Text="Observación"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtObservacion" runat="server" Text="" TextMode="MultiLine" Height="80px" Width="230px"></asp:TextBox>
                            <asp:Label ID="label1" runat="server"></asp:Label>
                            <asp:Button ID="btnAgregar" runat="server" Text="Agregar" Width="100px"  
                                 Height="25px" class="button small blue" onclick="btnAgregar_Click" />
                        </td>
           
                    </tr>
                    
                </table>
            </td>
        </tr>


        <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
    </table>
    <table width="800px" style="height:300px;">
        <tr>
            <td align="center" >
                <div id="divdetallecontrato" runat="server" style="width:900px; overflow: scroll; height:250px;">
                    <asp:GridView ID="grvEnvioFact" runat="server" CssClass="mGrid" Width="900px" 
                                AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None">
                        <Columns>
                            <asp:BoundField DataField="ef_correlativo"  HeaderText="Correlativo" >
                                <HeaderStyle Width="50px" />
                                <ItemStyle Width="50px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ef_contrato"  HeaderText="Contrato" >
                                <HeaderStyle Width="300px" />
                                <ItemStyle Width="300px" HorizontalAlign="Left" />
                            </asp:BoundField>
                                                                                          
                            <asp:BoundField DataField="ef_cliente"  HeaderText="Cliente" >
                                <HeaderStyle Width="300px" />
                                <ItemStyle Width="300px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ef_direccion"  HeaderText="Dirección" >
                                <HeaderStyle Width="400px" />
                                <ItemStyle Width="400px" HorizontalAlign="Left" />
                            </asp:BoundField>

                            <asp:BoundField DataField="ef_comuna"  HeaderText="Comuna" >
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </td>               
        </tr>

        <td class="style2" align="right">
            <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Width="100px"  
            Height="25px" class="button small blue" onclick="btnImprimir_Click" /> 
        </td> 
    </table>
</asp:Content>

