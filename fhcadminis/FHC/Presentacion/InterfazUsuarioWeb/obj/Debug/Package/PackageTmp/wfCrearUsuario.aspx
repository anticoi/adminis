﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfCrearUsuario.aspx.cs" Inherits="TGENESIS.FHC.InterfazUsuarioWeb.wfCrearUsuario" %>
<%@ Register Src="~/ControlesUsuario/ucCrearUsuario.ascx" TagPrefix="uc" TagName="ucCrearUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
        
        
        <table width="100%">
            <tr>
                <th>
                    <b>
                        INGRESO DE USUARIO
                    </b>
                </th>
            </tr>
            
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>

            <tr>
                <td align="center" valign="top">
                            <uc:ucCrearUsuario runat="server" ID="ucCrearUsuario" OnasignarRol="OnAsignarRoles" />
                </td>
            </tr>
        
        </table>    

</asp:Content>

