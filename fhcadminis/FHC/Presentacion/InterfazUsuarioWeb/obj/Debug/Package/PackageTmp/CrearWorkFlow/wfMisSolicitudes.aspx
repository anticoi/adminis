﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfMisSolicitudes.aspx.cs" Inherits="InterfazUsuarioWeb.CrearWorkFlow.wfMisSolicitudes" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




<table width="900px" style="height:300px;">
    
    <tr id="trfiltros" runat="server" >
    
        <td align="center"> 
            <table width="500px" style="height:100px;">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblEstado" runat="server" Text="Estado Soliciud"></asp:Label>
                    </td>
                    <td>
                        <div class="select"  style="width:230px;">
                            <asp:DropDownList ID="ddlbEstadoSolicitud" runat="server" Width="250px"></asp:DropDownList>
                        </div>
                    </td>
                    <td>
                            <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" class="button small blue" Width="80px" Height="25px" onclick="btnFiltrar_Click"  />   
                    </td>
                </tr>


                <tr>
                    <td colspan="3">

                    </td>
                </tr>
            </table>
        </td>
    
    </tr>
    
    
    <tr>
        <td align="center">

                        <table width="550px" >
                            <tr>

                                <td>
                                            <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                                                    <asp:GridView ID="grvworkflowusuario" runat="server" CssClass="mGrid" 
                                                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                                                        onrowcommand="grvworkflowusuario_RowCommand"  >
                                                        <Columns>

                                                                <asp:BoundField DataField="TipoWorkFDescripcion"  HeaderText="Tipo" >
                                                                        <HeaderStyle Width="220px" />
                                                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                                </asp:BoundField>                                            
                                                     
                                                            <asp:TemplateField HeaderText="Titulo">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LnBAccesorios" runat="server" CommandArgument='<%# Eval("WorkFlowId") %>' Text='<%# Eval("WorkFlowTitulo") %>' 
                                                                    CommandName="veraccesorios"></asp:LinkButton>
                                                                </ItemTemplate>
                                                        
                                                                <HeaderStyle Width="300px" />
                                                                <ItemStyle Width="300px" />
                                                        
                                                            </asp:TemplateField>     



                                                                <asp:BoundField DataField="WorkFlowFechaIngreso"  HeaderText="Fecha" >
                                                                        <HeaderStyle Width="120px" />
                                                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                                </asp:BoundField>                                    

                                                                <asp:BoundField DataField="nombre"  HeaderText="Asignado a" >
                                                                        <HeaderStyle Width="220px" />
                                                                            <ItemStyle CssClass="ItemGrilla" Width="200px"></ItemStyle> 
                                                                </asp:BoundField>                                                        

                                                            <asp:TemplateField HeaderText="Asignar">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton id="img" runat="server" CommandArgument='<%# Eval("WorkFlowId") %>'   CommandName="veraccesorios2" Width="20px" Height="20px" ImageUrl="~/imagenes/avatar.jpg" />
                                                                </ItemTemplate>

                                                            </asp:TemplateField>     


                                                            <asp:TemplateField HeaderText="Responder">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton id="imgresponder" runat="server" CommandArgument='<%# Eval("WorkFlowId") %>'   CommandName="veraccesorios3" Width="20px" Height="20px" ImageUrl="~/imagenes/avatar.jpg" />
                                                                </ItemTemplate>
                                                        
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                            </asp:TemplateField>     



                                                                <asp:TemplateField HeaderText="Termino">
                                                                            <ItemTemplate>
                                                                                    <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("WorkFlowId") %>' />
                                                                            </ItemTemplate>
                                                                        <HeaderStyle Width="150px" />
                                                                </asp:TemplateField>


                                                                <asp:BoundField DataField="Color"  HeaderText="Dias" >
                                                                        <HeaderStyle Width="10px" />
                                                                            <ItemStyle CssClass="ItemGrilla" Width="10px"></ItemStyle> 
                                                                </asp:BoundField>

                                                        </Columns>
                                        
                                        
                                        
                                                    </asp:GridView>
                                           </div>
                                                   
                                </td>
                
                            </tr>
                            <tr>
                                <td align="right">
                                            <asp:Button ID="btnTerminar" runat="server" Text="Finalizar" class="button small blue" Width="80px" Height="25px" onclick="btnTerminar_Click" />                            
                                </td>
                            </tr>
                        </table>
        
          
        </td>
    </tr>

    <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>

        <tr>
            <td>
                           <div class="curved-box">
                           <asp:Button ID="btnpopupgrilla" runat="server"  Text="" style="display:none" />
        
                           <cc1:ModalPopupExtender ID="mpepanel" runat="server"  PopupControlID="pnlaccesorios" TargetControlID="btnpopupgrilla"  BackgroundCssClass="fondoAplicacion"  />        

                           <asp:Panel ID="pnlaccesorios" runat="server" >

                                                         <table border="0" style="background-color: #FFFFFF; width:700px; height:400px;" >
                                                                <tr>
                                                                        <td>
                                                
                                                                                    <table border="0" style="background-color: #FFFFFF" >
                                                                                        <tr>
                                                                                            <td align="right" style="width:700px; height:20px;">
                                                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../imagenes/delete.png" />
                                                                                            </td>
                                                                                        </tr> 
                                                                                      </table>                                               
                                                
                                                                        </td>
                                                                </tr>

                                                                <tr>
                                                                    <td valign="top" align="left">

                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                        <ContentTemplate>
                                                                                <table border="0" width="100%">
                                                
                                                                                    <tr>
                                                                                        <td align="left" colspan="2">

                                                                                            <table width="98%">
                                                                                                <tr>

                                                                                                    <td style="width:120px;">
                                                                                                                <asp:Label ID="lbltitulotipo" runat="server" Text="Tipo : "></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltexttipo" runat ="server" BorderStyle="Inset" Width="400px"></asp:Label>
                                                                                                                <asp:HiddenField ID="hdffinaliza" runat="server" Value="0" />
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr>
                                                                
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltitulotitulo" runat="server" Text="Titulo : "></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltexttitulo" runat ="server" Text="" BorderStyle="Inset" Width="400px"></asp:Label>
                                                                                                    </td>

                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltituloasignado" runat="server" Text="Asignado A :"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltextasignado" runat ="server" Text="" BorderStyle="Inset" Width="400px"></asp:Label>
                                                                                                    </td>
                                                                
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltitulocontrato" runat="server" Text="Contrato :"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                                <asp:Label ID="lbltextcontrato" runat ="server" Text="" BorderStyle="Inset" Width="400px"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr>
                                                                                                   <td valign="top"  >
                                                                                                                <asp:Label ID="lbltituloobservacion" runat="server" Text="Observación :"></asp:Label>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                                <asp:TextBox ID="txtobservacionfinalizar" runat="server" Width="530px" Height="80px" TextMode="MultiLine"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>

                                                                                                <tr id="trhistorialasignador" runat="server">
                                                                                                    <td colspan="2">
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td align="center">
                                                                                                                        Historial de Asignaciones
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <div ID="div1" runat="server" 
                                                                                                                        style="width:700px; overflow: scroll; height:100px;">

                                                                                                                                <asp:GridView ID="grvhistorial" runat="server" AutoGenerateColumns="False" 
                                                                                                                                    CssClass="mGrid" GridLines="None"  
                                                                                                                                    ShowHeaderWhenEmpty="True" Width="750px">
                                                                                                                                    <Columns>
                                                                                                                                        <asp:BoundField DataField="usuarioderiva" HeaderText="Usuario Deriva">
                                                                                                                                        <HeaderStyle Width="160px" />
                                                                                                                                        <ItemStyle HorizontalAlign="Left" Width="160px" />
                                                                                                                                        </asp:BoundField>

                                                                                                                                        <asp:BoundField DataField="usuarioasignado" HeaderText="Usuario Asignado">
                                                                                                                                        <HeaderStyle Width="160px" />
                                                                                                                                        <ItemStyle HorizontalAlign="Left" Width="160px" />
                                                                                                                                        </asp:BoundField>

                                                                                                                                        <asp:TemplateField HeaderText="Observacion">
                                                                                                                                                    <ItemTemplate >
                                                                                                                                                            <asp:Label ID="Observacion" runat="server" ToolTip='<%# Eval("asignaobservacion") %>' Text ='<%# Eval("asignaobservacion") %>' ></asp:Label>
                                                                                                                                                    </ItemTemplate>
                                                                                                                                                     <ItemStyle HorizontalAlign="Left" Width="250px"/>
                                                                                                                                                <HeaderStyle Width="250px" />
                                                                                                                                        </asp:TemplateField>

                                                                                                                                        <asp:BoundField DataField="asignaworkflowfecha" HeaderText="Fecha">
                                                                                                                                        <HeaderStyle Width="120px" />
                                                                                                                                        <ItemStyle HorizontalAlign="Left" Width="120px"/>
                                                                                                                                        </asp:BoundField>


                                                                                                                                    </Columns>
                                                                                                                                </asp:GridView>


                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>


                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="tradjunto"  runat="server" visible="false">
                                                                                        <td>
                                                                                            <asp:Label ID="lbladjunto" runat="server" Text="Adj. Archivo"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:FileUpload ID="fuImagen" runat="server" />
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:Button ID="btnadjuntar" runat="server" class="button small blue" 
                                                                                                            Height="25px" onclick="btnadjuntar_Click" Text="Adjuntar" Width="80px" />
                                                                                                    </td>

                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <div ID="divadjuntos" runat="server" 
                                                                                                style="width:600px; overflow: scroll; height:150px;">
                                                                                                <asp:GridView ID="grvarchivos" runat="server" AutoGenerateColumns="False" 
                                                                                                    CssClass="mGrid" GridLines="None" onrowcommand="grvarchivos_RowCommand" 
                                                                                                    ShowHeaderWhenEmpty="True" Width="350px" 
                                                                                                    onselectedindexchanged="grvarchivos_SelectedIndexChanged">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField HeaderText="">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:ImageButton ID="btnruta" runat="server" 
                                                                                                                    CommandArgument='<%# Eval("workflowadjarchivo") %>' CommandName="verarchivo" 
                                                                                                                    Height="20px" ImageAlign="Top" ImageUrl="~/imagenes/guardar.jpg" Width="20px" />
                                                                                                            </ItemTemplate>
                                                                                                            <HeaderStyle Width="60px" />
                                                                                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:BoundField DataField="workflowadjarchivo" HeaderText="Nombre  Archivo">
                                                                                                        <HeaderStyle Width="260px" />
                                                                                                        <ItemStyle HorizontalAlign="Left" Width="260px" />
                                                                                                        </asp:BoundField>
                                                                                                    </Columns>
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                        <tr>
                                                                                    <td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td align="right" colspan="2">
                                                                                                        <asp:Button ID="btnguardartarea" runat="server" class="button small blue" 
                                                                                                            Height="25px" onclick="btnguardartarea_Click" Text="Finalizar" Width="80px" />
                                                                                                    </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    
                                                                                                    <td align="right" colspan="2">
                                                                                                        <asp:Button ID="btnVolver" runat="server" class="button small blue" 
                                                                                                            Height="25px" onclick="btnVolver_Click" Text="Volver" Width="80px" />
                                                                                                    </td>
                                                                                                                                                                                        
                                                                                    </tr>
                                                                                    </td>
                                                                                    
                                                                                                    </tr>
                                                                                    
                                                                                                                                                                                        
                                                                                </table>

                                                                                </table>
                                                                                                    
                                                                                    </tr>
                                                                                </table>


                                                                                </td>
                                                                                </tr>
                                                                                </table>


                                                                        </ContentTemplate>

                                                                        <Triggers>
                                                                           <asp:PostBackTrigger ControlID="btnadjuntar" />
                                                                         </Triggers>

                                                                    </asp:UpdatePanel>    
                                                                    </td>
                                        
                                                                </tr>
                                                         </table>
                            

                            </asp:Panel>

                            </div>
            </td>
        </tr>

        <tr>
            <td>
            
                           <asp:Button ID="btnpopupgrilla2" runat="server"  Text="" style="display:none" />
        
                           <cc2:ModalPopupExtender ID="mpepanel2" runat="server"  PopupControlID="pnlaccesorios2" TargetControlID="btnpopupgrilla2"  BackgroundCssClass="fondoAplicacion"  />        

                           <asp:Panel ID="pnlaccesorios2" runat="server" >
                                <table border="0" width="300px" style=" height:200px ;background-color: #FFFFFF"  >
                                        <tr>
                                                <td>
                                                
                                                            <table border="0" style="background-color: #FFFFFF" >
                                                                <tr>
                                                                    <td align="right" style="width:500px; height:20px;">
                                                                        <asp:ImageButton ID="ImageButton2" runat="server" 
                                                                            ImageUrl="../imagenes/delete.png" onclick="ImageButton2_Click" />
                                                                    </td>
                                                                </tr>
                                                                 
                                                              </table>                                               
                                                
                                                </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <table width="400px">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                                <asp:HiddenField ID="hdfidworkflow" runat="server" Value="" 
                                                                    onvaluechanged="hdfidworkflow_ValueChanged" />
                                                                <asp:Label ID="lblasignara" runat="server" Text="Asignar A"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="ddlbusuarioasignado" runat="server" Width="400px" ></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                    <asp:TextBox ID="txtobservacion" runat="server" TextMode="MultiLine" Width="400px" Height="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                             <asp:Button ID="btnAgregar" runat="server" Text="Aceptar"  onclick="btnAgregar_Click" />                                            
                                            </td>
                                        </tr>

                                 </table>
                            </asp:Panel>
            
            </td>
        </tr>

</table>
    <asp:HiddenField ID="hdfimprimir" runat="server" Value="" />
</asp:Content>
