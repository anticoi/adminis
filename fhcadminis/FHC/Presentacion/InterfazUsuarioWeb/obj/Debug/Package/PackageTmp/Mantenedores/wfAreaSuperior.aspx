﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfAreaSuperior.aspx.cs" Inherits="InterfazUsuarioWeb.Mantenedores.wfAreaSuperior" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table width="600px" style="height:300px;">
        <tr>
            <td valign="top"  align="center">
                <table width="80%">
                    <tr>
                            <td align="center">
            
                                <asp:Label  ID="lblArea" runat="server" Text="Area"></asp:Label>
                            </td>

                            <td>
                                    <asp:TextBox id ="txtArea" runat="server"  MaxLength="100" Width="300px"></asp:TextBox>
                                    <asp:HiddenField ID="hdfareasuperiorid" runat="server" Value="0" />
                            </td>

                            <td>
                                <asp:Button ID="btnAgregar" runat="server" Text="Agregar"  class="button small blue" Width="100px" Height="25px" onclick="btnAgregar_Click" />
                            </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                                <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                                        <asp:GridView ID="grvarea" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                                        onrowcommand="grvarea_RowCommand"  >
                                        
                                            <Columns>
                                                    <asp:TemplateField>
                                                                <ItemTemplate>
                                                                        <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("AreaSuperiorId") %>' />
                                                                </ItemTemplate>
                                                            <HeaderStyle Width="50px" />
                                                    </asp:TemplateField>
 
                                                    <asp:TemplateField HeaderText="Descripcion">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LnBAccesorios" runat="server" CommandArgument='<%# Eval("AreaSuperiorId") %>' Text='<%# Eval("AreaSuperiorNombre") %>' CommandName="veraccesorios"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        
                                                        <HeaderStyle Width="300px" />
                                                        <ItemStyle Width="300px" />
                                                        
                                                    </asp:TemplateField>     


                                                    <asp:BoundField DataField="AreaSuperiorEstado"  HeaderText="Estado" >
                                                            <HeaderStyle Width="50px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                            

                                                    <asp:BoundField DataField="AreaSuperiorFechaIngreso"  HeaderText="Fec. Creación" >
                                                            <HeaderStyle Width="120px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                    

                                                    <asp:BoundField DataField="AreaSuperiorUsuarioIngreso"  HeaderText="Usu. Creación" >
                                                            <HeaderStyle Width="120px" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                                    </asp:BoundField>                                                        

                                            </Columns>
                                        
                                        </asp:GridView>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="100px" Height="25px" class="button small blue" onclick="btnEliminar_Click" />                            
                        </td>
                    </tr>
                </table>
            </td>

        </tr>

        <tr>
            <td>
                        <uc1:ucmensaje ID="ucMensaje1" runat="server" />
            </td>
        </tr>

</table>
</asp:Content>
