﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="wfLogin.aspx.cs" Inherits="TGENESIS.FHC.InterfazUsuarioWeb.wfLogin" %>
<%@ Register Src="~/ControlesUsuario/ucNewLogin.ascx" TagPrefix="uc" TagName="ucNewLogin" %>


<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
       <title>Login FHC</title>
    <link href="estilos/login.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/ico" href="images/favicon.ico" /> 
    <link rel="shortcut icon" href="images/favicon.ico"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <uc:ucNewLogin runat="server" ID="ucucNewLogin" OnAceptar="OnAceptar"/>
    </div>
    </form>
</body>
</html>
