﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewCambiarPassword.ascx.cs" Inherits="ControlUsuario.Controles.ucNewCambiarPassword" %>
<style type="text/css">
    .validation { background-color:#FFB2B2; background:#FFB2B2; } 
</style>

<script type="text/javascript">
    function ValidarTexto(source, args) {
        var controlName = source.controltovalidate;
        var control = $('#' + controlName);

        if (control.is('input:text')) {
            if (control.val() == '') {
                control.addClass("validation");
                args.IsValid = false;
            }
            else {
                control.removeClass("validation");
                args.IsValid = true;
            }
        }

        if (control.is('input:password')) {
            if (control.val() == '') {
                control.addClass("validation");
                args.IsValid = false;
            }
            else {
                control.removeClass("validation");
                args.IsValid = true;
            }
        }
    }
</script>

<div>
<table>
<tr>
<td style="text-align:left"><asp:label runat="server" id="lblClaveActual" Text="Password Actual" AssociatedControlID="txtClaveActual"></asp:label></td>
<td style="text-align:left"><asp:TextBox runat="server" id="txtClaveActual" 
        CssClass="required" TextMode="Password"></asp:TextBox>
    <asp:CustomValidator ID="cvClaveActual" runat="server" style="display:none"
        ErrorMessage="Clave Actual" ClientValidationFunction="ValidarTexto" 
        ControlToValidate="txtClaveActual" ValidateEmptyText="True" 
        ValidationGroup="Cambiar"></asp:CustomValidator>
    </td>
</tr>
<tr>
<td style="text-align:left"><asp:Label runat="server" ID="lblNuevaClave" Text="Nueva Password" AssociatedControlID="txtNuevaClave"></asp:Label></td>
<td style="text-align:left"><asp:TextBox runat="server" ID="txtNuevaClave" 
        MaxLength="16" TextMode="Password"></asp:TextBox>
    <asp:CustomValidator ID="cvNuevaPassword" runat="server" style="display:none"
        ClientValidationFunction="ValidarTexto" ControlToValidate="txtNuevaClave" 
        ErrorMessage="Nueva Password" ValidateEmptyText="True" 
        ValidationGroup="Cambiar"></asp:CustomValidator>
    </td>
</tr>
<tr>
<td style="text-align:left"><asp:Label runat="server" ID="lblReingresar" Text="Reingrese Password" AssociatedControlID="txtReingresar"></asp:Label></td>
<td style="text-align:left"><asp:TextBox runat="server" ID="txtReingresar" 
        MaxLength="16" TextMode="Password"></asp:TextBox>
    <asp:CustomValidator ID="cvReingresar" runat="server" style="display:none"
        ClientValidationFunction="ValidarTexto" ControlToValidate="txtReingresar" 
        ErrorMessage="Reingresar Password" ValidateEmptyText="True" 
        ValidationGroup="Cambiar"></asp:CustomValidator>
    </td>
</tr>
<tr>
<td colspan="2" style="text-align:center"><asp:Label runat="server" ID="lblMensaje" 
        CssClass="mensaje"></asp:Label>   </td>
</tr>
<tr>
<td colspan="2" style="text-align:center"><asp:Button runat="server" 
        ID="btnCambiar" Text="Cambiar Password" style="width:auto" 
        onclick="btnCambiar_Click" ValidationGroup="Cambiar"  /></td>
</tr>
</table>
</div>