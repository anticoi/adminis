﻿<%@ Page Title="FHC" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfContratoDetalle.aspx.cs" Inherits="InterfazUsuarioWeb.Contratos.wfContratoDetalle" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 
 <script type="text/javascript" language="javascript">

     function Imprimir() {

         var sUrl = document.getElementById("<%=hdfimprimir.ClientID%>").value;
         window.open(sUrl, '', 'toolbar=no, scrollbars=yes, resizable=yes, top=50, left=50, width=700, height=400');
         
     }

    </script>

    <style type="text/css">
        .style1
        {
            width: 700px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="600px" style="height:300px;">
    <tr>
        <td valign="top" align="center">
            <table width="80%">
                <tr>
                    <td align="center">
                        <asp:Label  ID="lblContrato" runat="server" Text="Contrato"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox id ="txtContrato" runat="server"  MaxLength="100" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar"  class="button small blue" onclick="btnBuscar_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label  ID="lblAnoCaratula" runat="server" Text="Seleccione Año (solo para busqueda de caratulas)"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAnoCaratula" runat="server">
                            <asp:ListItem Text="1995" Value="1995"></asp:ListItem>
                            <asp:ListItem Text="1996" Value="1996"></asp:ListItem>
                            <asp:ListItem Text="1997" Value="1997"></asp:ListItem>
                            <asp:ListItem Text="1998" Value="1998"></asp:ListItem>
                            <asp:ListItem Text="1999" Value="1999"></asp:ListItem>
                            <asp:ListItem Text="2000" Value="2000"></asp:ListItem>
                            <asp:ListItem Text="2001" Value="2001"></asp:ListItem>
                            <asp:ListItem Text="2002" Value="2002"></asp:ListItem>
                            <asp:ListItem Text="2003" Value="2003"></asp:ListItem>
                            <asp:ListItem Text="2004" Value="2004"></asp:ListItem>
                            <asp:ListItem Text="2005" Value="2005"></asp:ListItem>
                            <asp:ListItem Text="2006" Value="2006"></asp:ListItem>
                            <asp:ListItem Text="2007" Value="2007"></asp:ListItem>
                            <asp:ListItem Text="2008" Value="2008"></asp:ListItem>
                            <asp:ListItem Text="2009" Value="2009"></asp:ListItem>
                            <asp:ListItem Text="2010" Value="2010"></asp:ListItem>
                            <asp:ListItem Text="2011" Value="2011"></asp:ListItem>
                            <asp:ListItem Text="2012" Value="2012"></asp:ListItem>
                            <asp:ListItem Text="2013" Value="2013"></asp:ListItem>
                            <asp:ListItem Text="2014" Value="2014"></asp:ListItem>
                            <asp:ListItem Text="2015" Value="2015" Selected></asp:ListItem>
                            <asp:ListItem Text="2016" Value="2016"></asp:ListItem>
                            <asp:ListItem Text="2017" Value="2017"></asp:ListItem>
                            <asp:ListItem Text="2018" Value="2018"></asp:ListItem>
                            <asp:ListItem Text="2019" Value="2019"></asp:ListItem>
                            <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>                  
        </td>
    </tr>

    <tr>
        <td align="center" >
            <div id="divdetallecontrato" runat="server" style="width:900px; overflow: scroll; height:250px;">
                <asp:GridView ID="grvarchivos" runat="server" CssClass="mGrid" Width="350px" 
                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                    onrowcommand="grvarchivos_RowCommand" 
                    onrowdatabound="grvarchivos_RowDataBound" 
                    onselectedindexchanged="grvarchivos_SelectedIndexChanged" >
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnmail" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/email.png" Width="20px" Height="20px" CommandArgument='<%# Eval("Ruta") %>' CommandName="enviararchivo" /> 
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                                
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton id= "btnruta" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/dokument.png" Width="20px" Height="20px" CommandArgument='<%#Eval("Ruta") %>' CommandName="verarchivo" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>     

                        <asp:BoundField DataField="Nombre"  HeaderText="Nombre Documento" >
                            <HeaderStyle Width="260px" />
                            <ItemStyle Width="260px" HorizontalAlign="Left" />
                        </asp:BoundField>
                                                                                          
                        <asp:BoundField DataField="Ruta"  HeaderText="Tipo Documento" >
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="grvarchivos2" runat="server" CssClass="mGrid" Width="350px" 
                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                    onrowcommand="grvarchivos2_RowCommand" 
                    onrowdatabound="grvarchivos2_RowDataBound" 
                    onselectedindexchanged="grvarchivos2_SelectedIndexChanged" 
                    ShowHeader="False" >
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnmail" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/email.png" Width="20px" Height="20px" CommandArgument='<%# Eval("icono") %>' CommandName="enviararchivo2" /> 
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                                
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton id= "btnruta" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/dokument.png" Width="20px" Height="20px" CommandArgument='<%#Eval("icono") %>' CommandName="verarchivo2" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>     

                        <asp:BoundField DataField="Nombre"  HeaderText="Nombre Documento" >
                            <HeaderStyle Width="260px" />
                            <ItemStyle Width="260px" HorizontalAlign="Left" />
                        </asp:BoundField>
                                                                                          
                        <asp:BoundField DataField="Ruta"  HeaderText="Tipo Documento" >
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="grvarchivos3" runat="server" CssClass="mGrid" Width="350px" 
                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                    onrowcommand="grvarchivos3_RowCommand" 
                    onrowdatabound="grvarchivos3_RowDataBound" 
                    onselectedindexchanged="grvarchivos3_SelectedIndexChanged" 
                    ShowHeader="False" >
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnmail" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/email.png" Width="20px" Height="20px" CommandArgument='<%# Eval("icono") %>' CommandName="enviararchivo3" /> 
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                                
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton id= "btnruta" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/dokument.png" Width="20px" Height="20px" CommandArgument='<%#Eval("icono") %>' CommandName="verarchivo3" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>     

                        <asp:BoundField DataField="Nombre"  HeaderText="Nombre Documento" >
                            <HeaderStyle Width="260px" />
                            <ItemStyle Width="260px" HorizontalAlign="Left" />
                        </asp:BoundField>
                                                                                          
                        <asp:BoundField DataField="Ruta"  HeaderText="Tipo Documento" >
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="grvarchivos4" runat="server" CssClass="mGrid" Width="350px" 
                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                    onrowcommand="grvarchivos4_RowCommand" 
                    onrowdatabound="grvarchivos4_RowDataBound" 
                    onselectedindexchanged="grvarchivos4_SelectedIndexChanged" 
                    ShowHeader="False" >
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnmail" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/email.png" Width="20px" Height="20px" CommandArgument='<%# Eval("icono") %>' CommandName="enviararchivo4" /> 
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                                
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:ImageButton id= "btnruta" runat="server" ImageAlign="Top" ImageUrl="~/imagenes/dokument.png" Width="20px" Height="20px" CommandArgument='<%#Eval("icono") %>' CommandName="verarchivo4" />
                            </ItemTemplate>
                            <HeaderStyle Width="60px" />
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:TemplateField>     

                        <asp:BoundField DataField="Nombre"  HeaderText="Nombre Documento" >
                            <HeaderStyle Width="260px" />
                            <ItemStyle Width="260px" HorizontalAlign="Left" />
                        </asp:BoundField>
                                                                                          
                        <asp:BoundField DataField="Ruta"  HeaderText="Tipo Documento" >
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <asp:Button ID="btnpopupgrilla" runat="server"  Text="pepepepe" style="display:none" />
                <cc1:ModalPopupExtender ID="mpepanel" runat="server"  PopupControlID="pnlenviarcorreo" TargetControlID="btnpopupgrilla"  BackgroundCssClass="fondoAplicacion"  />        
                    <asp:Panel ID="pnlenviarcorreo" runat="server"  DefaultButton="btnenviarcorreo"  >
                        <table border="1" >
                            <tr>
                                <td class="style1">
                                    <div  style=" height:300px; width:700px">
                                        <table border="0" style="background-color: #FFFFFF" >
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hdfruta" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdfarchivo" runat="server" Value=""/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width:700px; height:20px;">
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../imagenes/delete.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td  style="width:80px;">
                                                                <asp:Label ID="lblpara" runat="server" Text="Para"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="ddlCorreo" runat="server" Width="400px"></asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnenviarcorreo" runat="server" Text="Enviar"  
                                                                    class="button small blue" onclick="btnenviarcorre_Click" />                                                                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  style="width:80px;">
                                                                <asp:Label ID="lblasunto" runat="server" Text="Asunto"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:TextBox ID="txtasunto" runat="server" MaxLength="100" Width="300px" ></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbladjunto" runat="server" Text="Adjunto"></asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lblrutaadjunto" runat="server" Visible="False"></asp:Label>
                                                                <asp:Label ID="lblrutaadjunto2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="3">
                                                                <asp:TextBox ID="txtbody" runat="server" TextMode="MultiLine" Width="600px" Height="200px" ></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>  
        </td>
    </tr>

    <tr>
        <td>
            <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>


<asp:HiddenField ID="hdfimprimir" runat="server" Value="" />

</asp:Content>
