﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewLogin.ascx.cs" Inherits="ControlUsuario.Controles.ucNewLogin" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .validation { background-color:#FFB2B2; background:#FFB2B2; } 
</style>

<script type="text/javascript">
    function ValidarTexto(source, args) {
        var controlName = source.controltovalidate;
        var control = $('#' + controlName);

        if (control.is('input:text')) {
            if (control.val() == '') {
                control.addClass("validation");
                args.IsValid = false;
            }
            else {
                control.removeClass("validation");
                args.IsValid = true;
            }
        }

        if (control.is('input:password')) {
            if (control.val() == '') {
                control.addClass("validation");
                args.IsValid = false;
            }
            else {
                control.removeClass("validation");
                args.IsValid = true;
            }
        }
    }
</script>

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div class="divLogin" >
 
    <label for="lblUsuario">Usuario:</label>
    <asp:TextBox runat="server" ID="txtUsuario"></asp:TextBox>
    <asp:CustomValidator ID="cvUsuario" runat="server" style="display:none" 
        ClientValidationFunction="ValidarTexto" ControlToValidate="txtUsuario" 
        ErrorMessage="Usuario" ValidateEmptyText="True" ValidationGroup="Login"></asp:CustomValidator>
    <label for="lblPassword">Password:</label>
    
    <asp:TextBox runat="server" ID="txtPassword" TextMode="Password"></asp:TextBox>
    <label for="lblPassword">
    <asp:CustomValidator ID="cvPassword" runat="server" 
        ClientValidationFunction="ValidarTexto" ControlToValidate="txtPassword" style="display:none"
        ErrorMessage="Password" ValidationGroup="Login" ValidateEmptyText="True"></asp:CustomValidator>
    </label>
    <p><asp:LinkButton runat="server" ID="lnkPassword">Olvidaste tu password?</asp:LinkButton> </p>
    <div id="lower">
        <asp:CheckBox runat="server" ID="chkSession" 
            Text="Mantener la session iniciada" Checked="True" />
	    <asp:Button runat="server" ID="btnIniciarSession" Text="Login" 
            onclick="btnIniciarSession_Click" ValidationGroup="Login" />				
    </div><!--/ lower-->
    <div style="text-align:center">
    <asp:Label runat="server" ID="lblMsg" style="text-align:center" CssClass="mensaje"></asp:Label>
    </div>
</div>


<asp:ModalPopupExtender ID="mpeRecuperar" runat="server" PopupControlID="pnlRecuperarPassword" 
    BackgroundCssClass="fondoAplicacion" CancelControlID="btnCerrar" TargetControlID="lnkPassword" >
</asp:ModalPopupExtender>


<asp:Panel runat="server" ID="pnlRecuperarPassword" CssClass="popup_block">
<table>
<tr>
<td><label>Ingrese su email:</label></td>
    <td>
        <asp:TextBox runat="server" ID="txtEmail" Width="300px"></asp:TextBox>
        <asp:CustomValidator ID="cvEmail" runat="server" style="display:none"
            ClientValidationFunction="ValidarTexto" ControlToValidate="txtEmail" 
            ErrorMessage="Email" ValidateEmptyText="True" ValidationGroup="Recuperar"></asp:CustomValidator>
    </td>
</tr>
<tr>
<td>
    <label>Ingrese su nombre de usuario:</label>
</td>
<td>
    <asp:TextBox runat="server" ID="txtNombreUsuario" Width="300px"></asp:TextBox>
        <asp:CustomValidator ID="cvNombreUsuario" runat="server" style="display:none"
            ClientValidationFunction="ValidarTexto" ControlToValidate="txtNombreUsuario" 
            ErrorMessage="NombreUsuario" ValidateEmptyText="True" ValidationGroup="Recuperar"></asp:CustomValidator>
</td>
<tr>
<td colspan="2" style="text-align:center"><asp:Label runat="server" ID="lblMsgRecuperacion" CssClass="mensaje"></asp:Label></td>
</tr>
<tr>
<td><asp:Button runat="server" ID="btnRecuperar" Text="Recuperar" style="width:auto"
        onclick="btnRecuperar_Click" ValidationGroup="Recuperar" /></td>
<td><asp:Button runat="server" ID="btnCerrar" Text="Cerrar" /></td>
</tr>
</table>
</asp:Panel>

