﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfContratoDescuento.aspx.cs" Inherits="InterfazUsuarioWeb.Contratos.wfContratoDescuento" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function ValidarDato(source, arguments) {
            if ($.trim(arguments.Value) == "") {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FF5959";
                arguments.IsValid = false;
            }
            else {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FFFFFF";
                arguments.IsValid = true;
            }
        }
       
      </script>


    <style type="text/css">
        .style1
        {
            height: 45px;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="600px" style="height:300px;">
    <tr>
        <td valign="top"  align="center">
            <table width="80%">
                <tr>
        <td align="center"> 
            <table width="600px" style="height:100px;">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label  ID="lblMes" runat="server" Text="Mes"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlMes" runat="server">
                                        <asp:ListItem Text="Enero" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="Febrero" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="Marzo" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="Abril" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="Mayo" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="Junio" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="Julio" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="Agosto" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="Septiembre" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="Octubre" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Noviembre" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Diciembre" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                 <td align="center">
                                    <asp:Label  ID="lblAno" runat="server" Text="Año"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAno" runat="server" Width="60px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                        
                        <tr>
                            <td class="style1" colspan="2" align="right">
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" 
                                        class="button small blue" Width="80px" Height="25px" 
                                        onclick="btnFiltrar_Click" />   
                            </td>
                        </tr>
                        
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

                <tr>
                    <td align="center" colspan="6">
                        <div id="div1" runat="server">
                            <%--<asp:GridView ID="GridView1" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                <Columns>
                                    <asp:BoundField HeaderText="Contrato" >
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Plan" >
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                  

                                    <asp:BoundField HeaderText="Cargo" >
                                        <HeaderStyle Width="200px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>        
                                                    
                                    <asp:BoundField HeaderText="Descuento" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>     
                                </Columns>
                            </asp:GridView>--%>
                        </div>
                        <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                            <asp:GridView ID="grvCD" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                <Columns>
                                    <asp:BoundField DataField="OPER_NUM" HeaderText="Contrato">
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>

                                    <asp:BoundField DataField="PLAN_DESC" HeaderText="Plan">
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                  

                                    <asp:BoundField DataField="CARGO" HeaderText="Cargo" DataFormatString="{0:N0}" 
                                        HtmlEncode="False">
                                        <HeaderStyle Width="200px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>        
                                                    
                                    <asp:BoundField DataField="DESCUENTO" HeaderText="Descuento" 
                                        DataFormatString="{0:N0}" HtmlEncode="False">
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>     
                                </Columns>

                                <EmptyDataTemplate>
                                    <i>{No hay datos que mostrar}</i>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>
</asp:Content>
