﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfAreaSuperiorArea.aspx.cs" Inherits="InterfazUsuarioWeb.Mantenedores.wfAreaSuperiorArea" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="600px"  style=" height:200px;">
    <tr>
        <td valign="top">
            <table width="100%">
                    <tr>
                        <td style="width:100px">
                            <asp:Label ID="lblArea" runat="server" Text = "Area"></asp:Label>
                        </td>
                        <td>
                            <div class="select" style="width:300px;" >
                                        <asp:DropDownList ID="ddlbAreaSuperior" runat="server" Width="320px" 
                                            AutoPostBack="true" 
                                            onselectedindexchanged="ddlbAreaSuperior_SelectedIndexChanged" ></asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                                    <div id="divareas" runat="server" style="width:600px; overflow: scroll; height:200px;">
                                            <asp:GridView ID="grvarea" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                                <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("AreaId") %>' />
                                                                        </ItemTemplate>
                                                                    <HeaderStyle Width="50px" />
                                                            </asp:TemplateField>
                                                                  

                                                            <asp:BoundField DataField="AreaNombre"  HeaderText="Descripcion" >
                                                                    <HeaderStyle Width="320px" />
                                                                        <ItemStyle CssClass="ItemGrilla" Width="320px"></ItemStyle> 
                                                            </asp:BoundField>                                                                   
                                                        </Columns>
                                            </asp:GridView>
                                    </div>                        
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" align="right">
                          <asp:Button runat="server" ID="btnAgregar" Text="Agregar" 
                                class="button small blue" onclick="btnAgregar_Click" />
                        </td>
                    </tr>                                
            </table>
        </td>
    </tr>

    <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>     
</table>


</asp:Content>
