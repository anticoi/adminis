﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAsignaRol.ascx.cs"
    Inherits="ControlUsuario.Controles.ucAsignaRol" %>

    
<%@ Register src="ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tabla">
    <tr>
        <td align="center">
            <table width="400px">
                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblUsuario" Text="Usuario" runat="server" AssociatedControlID="txtUsuario" />
                                    </td>
                                    <td align="left">
                                       
                                            <asp:UpdatePanel ID="pnltextto" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox runat="server" ID="txtUsuario" MaxLength="50"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnBuscar" Text="Buscar" CssClass="boton" OnClick="btnBuscar_Click" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnLimpiar" Text="Limpiar" CssClass="boton" OnClick="btnLimpiar_Click" />
                                    </td>
                </tr>
            </table>
            <br />
            <asp:HiddenField ID="hfIdUsuario" runat="server" />
            <table>
                <tr>
                    <td id="tdcomborol" runat="server" visible="false"><div class="select" style="width:300px">
                        <asp:DropDownList ID="ddlRoles" style="width:320px" runat="server" DataTextField="descripcion" DataValueField="idRol">
                        </asp:DropDownList>
                        </div>
                    </td>
 
                </tr>

                <tr>
                
                    <td align="left">
                                <div id="divroles" runat="server" style="width:400px; overflow: scroll; height:250px;">
                                        <asp:GridView ID="grvroles" runat="server" CssClass="mGrid"
                                            AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                            <Columns>
                                                    <asp:TemplateField>
                                                                <ItemTemplate>
                                                                        <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("idrol") %>' />
                                                                </ItemTemplate>
                                                            <HeaderStyle Width="50px" />
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="descripcion"  HeaderText="Descripción" >
                                                            <HeaderStyle Width="220px" ForeColor="White" />
                                                                <ItemStyle CssClass="ItemGrilla" Width="220px"></ItemStyle> 
                                                    </asp:BoundField>                                            

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                                                 
                    </td>
                </tr>
                <tr>
                
                    <td  align="right">
                        <asp:Button runat="server" ID="btnAgregar" Text="Agregar" CssClass="boton" OnClick="btnAgregar_Click" />
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="text-align:center; width:100%">
                        <asp:Label ID="lblMsg" runat="server" CssClass="textoAlerta"></asp:Label>
                        <br />
                    </td>
                </tr>
            </table>
            <table id="tablarol" runat="server" visible="false">
                <tr>
                    <th colspan="2">
                        Roles Asignados
                    </th>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView CssClass="mGrid" runat="server" ID="gvRoles" 
                            AutoGenerateColumns="False" onrowcreated="gvRoles_RowCreated">
                            <Columns>
                                <asp:BoundField DataField="idRol" HeaderText="idRol" />
                                <asp:BoundField DataField="descripcion" HeaderText="Descripción Rol" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEliminar" runat="server" OnCommand="OnEliminar" CommandArgument='<%# Eval("idRol") %>'
                                            ImageUrl="~/imagenes/deleteGrid.png" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
    </tr>


    <tr>
        <td>
                    <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>

</table>
