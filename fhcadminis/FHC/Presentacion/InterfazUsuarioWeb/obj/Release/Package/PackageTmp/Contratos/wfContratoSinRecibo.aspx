﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfContratoSinRecibo.aspx.cs" Inherits="InterfazUsuarioWeb.Contratos.wfContratoSinRecibo" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function ValidarDato(source, arguments) {
            if ($.trim(arguments.Value) == "") {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FF5959";
                arguments.IsValid = false;
            }
            else {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FFFFFF";
                arguments.IsValid = true;
            }
        }


        $(function () {

            $("#<%= txtFechaDesde.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

            $("#<%= txtFechaHasta.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

        });

        
      </script>

    <style type="text/css">
        .style1
        {
            height: 45px;
        }
        .style2
        {
            width: 401px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="900px" style="height:300px;">
    <tr>
        <td align="center"> 
            <table width="600px" style="height:100px;">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label  ID="lblfechainicio" runat="server" Text="Inicio"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaDesde" runat="server" data-watermark="•" 
                                        ontextchanged="txtFechaDesde_TextChanged"></asp:TextBox>
                                </td>
                                 <td align="center">
                                    <asp:Label  ID="lblfechatermino" runat="server" Text="Termino"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaHasta" runat="server" data-watermark="•"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                        
                        <tr>
                            <td class="style1" colspan="2" align="right">
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" 
                                        class="button small blue" Width="80px" Height="25px" 
                                        onclick="btnFiltrar_Click" />   
                            </td>
                        </tr>
                        
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center">
            <table width="550px" >
                <tr>
                    <td>
                        <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                            <asp:GridView ID="grvCSREC" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                <Columns>
<%--                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk"  runat="server" CommandArgument='<%# Eval("OPER_NUM") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle Width="150px" />
                                    </asp:TemplateField>--%>

                                    <asp:BoundField DataField="OPER_NUM"  HeaderText="Contrato" >
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                            
                                                        
                                    <asp:BoundField DataField="OPER_NECE_TIPO"  HeaderText="Tipo" >
                                        <HeaderStyle Width="40px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                            

                                    <asp:BoundField DataField="OPER_FECH"  HeaderText="Fecha" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                    

                                    <asp:BoundField DataField="OPER_ESTA_COD"  HeaderText="Estado" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>
                                                    
                                    <asp:BoundField DataField="SUCU_DESC"  HeaderText="Sucursal" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ASESOR"  HeaderText="Asesor" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>

                                    <asp:BoundField DataField="FOPA_MONTO"  HeaderText="Monto" 
                                        DataFormatString="{0:N0}" HtmlEncode="False" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>
                                </Columns>

                                <EmptyDataTemplate>
                                    <i>{No hay datos que mostrar}</i>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td  align="right">
                        <table width="80%">
                            <tr>
                                <td align="center">
                                    <table width="550px" >
                                        <tr>
                                            <td align="left" valign="top" class="style1">
                                                <asp:Label ID="lblcontrato" runat="server" Text="Contrato"></asp:Label>
                                            </td>
                                            <td class="style2">
                                                <asp:TextBox ID="txtContrato" runat="server"  Text="" MaxLength="10" 
                                                    Width="100px"  AutoPostBack="true"
                                                    onkeypress="javascript:return solonumeros(event)" 
                                                    ontextchanged="txtContrato_TextChanged" ></asp:TextBox>
                                                <asp:Label ID= "LblVendedor" runat="server" Text=""></asp:Label>
                                            </td>                
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="lblasignar" runat="server" Text="Asignar a"></asp:Label>
                                            </td>
                                            <td class="style2">
                                                <div class="select"   style="width:380px;"> 
                                                    <asp:DropDownList ID="ddlbusuarioasignado" runat="server" Width="400px" 
                                                        onselectedindexchanged="ddlbusuarioasignado_SelectedIndexChanged" ></asp:DropDownList>
<%--                                                        <asp:TextBox ID="txtUsuarioAsignado" runat="server" Width="400px"
                                                        ontextchanged="txtUsuarioAsignado_TextChanged"></asp:TextBox>--%>
                                                </div>  
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" valign="top" style="width:150px;">
                                                <asp:Label ID="lbltipoWorkFlow" runat="server" Text="Tipo tarea"></asp:Label>
                                            </td>
                                            <td class="style2">
                                                <div class="select"   style="width:380px;"> 
                                                    <asp:DropDownList ID="ddlbtipoworkflow" runat="server" Width="400px" ></asp:DropDownList>
                                                </div>
                                            </td>                
                                        </tr>

                                        <tr>
                                            <td align="left" valign="top" style="width:150px;">
                                                <asp:Label ID="lbltitulo" runat="server" Text="Titulo"></asp:Label>
                                            </td>
                                            <td align="left" class="style2">
                                                <asp:TextBox ID="txttitulo" runat="server" Text="Revisar contrato sin recibo" 
                                                    MaxLength="20" Width="180px" ></asp:TextBox>
                                            </td>                
                                        </tr>

                                        <tr>
                                            <td align="left" valign="top" style="width:150px;">
                                                <asp:Label ID="lblobservacion" runat="server" Text="Observación"></asp:Label>
                                            </td>
                                            <td class="style2">
                                                <asp:TextBox ID="txtobservacion" runat="server" Text="" TextMode="MultiLine" Height="80px" Width="400px"></asp:TextBox>
                                            </td>                
                                        </tr>

                                        <tr>
                                            <td valign="top">
                                            </td>
                                            <td align="right" class="style2">
                                                <asp:Button ID="btnAgregar" runat="server" Text="Enviar" Width="100px"  
                                                    Height="25px" class="button small blue" onclick="btnAgregar_Click" />
                                            </td>                
                                        </tr>
                                    </table>                            
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>
</asp:Content>
