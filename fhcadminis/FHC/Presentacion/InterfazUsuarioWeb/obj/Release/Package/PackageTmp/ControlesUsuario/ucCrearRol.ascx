﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCrearRol.ascx.cs" Inherits="ControlUsuario.Controles.ucCrearRol" %>
<table>
    <tr align="left">
        <td colspan="3"><asp:Label ID="lblSistema" runat="server" Text="Sistema: "></asp:Label></td>
        
    </tr>
    <tr>
        <td><asp:Label ID="lblDescripcion" runat="server" Text="Rol: "></asp:Label></td>
        <td>
            <asp:UpdatePanel ID="upRol" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txtDescripcion" runat="server" Width="250px" ></asp:TextBox>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnCrearRol" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>    
                </td>
        <td><asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="100px" 
                onclick="btnBuscar_Click" /> &nbsp;<asp:Button ID="btnCrearRol" 
                runat="server" Text="Crear Rol" Width="100px" onclick="btnCrearRol_Click" /></td>
    </tr>
    <tr align="left">
        <td colspan="3">
            <asp:UpdatePanel ID="upMensaje" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblMensaje" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="gvRoles" EventName="RowCommand" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<br />
<table>
    <tr>
        <td>
            <asp:UpdatePanel ID="upRoles" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gvRoles" runat="server" AutoGenerateColumns="False" 
                        CssClass="mGrid" onrowcommand="gvRoles_RowCommand" >
                        <Columns>
                            <asp:BoundField DataField="descripcion" HeaderText="Rol" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEliminar" runat="server" 
                                        CommandArgument='<%# Eval("idRol") %>' CommandName="eliminar" ImageUrl="~/imagenes/delete.png" />
                                </ItemTemplate>
                    
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>    
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCrearRol" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </td>
        
    </tr>
</table>