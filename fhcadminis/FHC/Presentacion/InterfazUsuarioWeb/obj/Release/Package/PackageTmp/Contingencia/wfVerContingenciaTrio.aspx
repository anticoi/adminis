﻿<%@ Page Title="Ver Contratos Contingencia" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfVerContingenciaTrio.aspx.cs" Inherits="InterfazUsuarioWeb.Contingencia.wfVerContingenciaTrio" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">



        function solonumeros(e) {
            var key;
            if (window.event) // IE
            {
                key = e.keyCode;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                key = e.which;
            }

            if (key < 48 || key > 57) {
                return false;
            }
            return true;
        }

        $(function () {

            $("#<%= txtFechaDesde.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

            $("#<%= txtFechaHasta.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="600px" style="height:300px;">
    <tr>
        <td valign="top"  align="center">
            <table width="80%">
                <tr>
        <td align="center"> 
            <table width="600px" style="height:50px;">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label  ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstado" runat="server" 
                                        onselectedindexchanged="ddlEstado_SelectedIndexChanged">
                                        <asp:ListItem Text="Todos" Value="Todos"></asp:ListItem>
                                        <asp:ListItem Text="Pendiente" Value="Pendiente"></asp:ListItem>
                                        <asp:ListItem Text="Finalizado" Value="Finalizado"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="style1" colspan="2" align="right">
                                    <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar x Estado" 
                                        class="button small blue" Width="100px" Height="25px" 
                                        onclick="btnFiltrar_Click" />   
                            </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="600px" style="height:50px;">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label  ID="lblfechainicio" runat="server" Text="Inicio"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaDesde" runat="server" data-watermark="•" 
                                        ontextchanged="txtFechaDesde_TextChanged"></asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:Label  ID="lblfechatermino" runat="server" Text="Termino"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaHasta" runat="server" data-watermark="•" 
                                        ontextchanged="txtFechaHasta_TextChanged"></asp:TextBox>
                                </td>

                                <td>
                                    <table>
                                        <tr>
                                            <td class="style1" colspan="2" align="right">
                                                <asp:Button ID="btnFiltrar2" runat="server" Text="Filtrar x Periodo" 
                                                    class="button small blue" Width="100px" Height="25px" 
                                                    onclick="btnFiltrar2_Click" />   
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

                <tr>
                    <td align="center" colspan="6">
                        <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                            <asp:GridView ID="grvVerContingencia" runat="server" CssClass="mGrid" 
                                AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" 
                                onselectedindexchanged="grvVerContingencia_SelectedIndexChanged">
                                <%--onrowcommand="grvVerContingencia_RowCommand"--%>
                                <Columns>
                                    <%--<asp:CommandField ShowSelectButton="True" SelectText="Enviar Aviso" />--%>

                                    <asp:TemplateField HeaderText="Aviso">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAviso" runat="server" CommandArgument='<%# Eval("Cont_Folio") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle Width="50px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Finalizar">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkFinaliza" runat="server" CommandArgument='<%# Eval("Cont_Folio") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle Width="50px" />
                                    </asp:TemplateField>


                                    <asp:BoundField DataField="Cont_Fecha" HeaderText="Fecha">
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Cont_Folio" HeaderText="Folio">
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                  

                                    <asp:BoundField DataField="Cont_Contrato" HeaderText="Contrato" DataFormatString="{0:N0}" 
                                        HtmlEncode="False">
                                        <HeaderStyle Width="200px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>        
                                                    
                                    <asp:BoundField DataField="Cont_Serv_Plan" HeaderText="Plan">
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="Cont_Vend_Nombre" HeaderText="Asesor Integral">
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>      

                                    <asp:BoundField DataField="Cont_Estado" HeaderText="Estado">
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>  

                                    <asp:BoundField DataField="WorkFlowId" HeaderText="Tarea Nº">
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>
                                </Columns>

                                <EmptyDataTemplate>
                                    <i>{No hay datos que mostrar}</i>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="style1" colspan="2" align="left">
                                                <asp:Button ID="btnEnvioAviso" runat="server" Text="Enviar Aviso" 
                                                    class="button small blue" Width="100px" Height="25px" 
                                                    onclick="EnviarAviso_Click" />   
                                            </td>
                                            <td class="style1" colspan="2" align="left">
                                                <asp:label ID="lbl" runat="server" Visible="false" Width="100px" />   
                                            </td>
                                            <td class="style1" colspan="2" align="right">
                                                <asp:Button ID="btnFinalizar" runat="server" Text="Finalizar" 
                                                    class="button small blue" Width="100px" Height="25px" 
                                                    onclick="Finalizar_Click" />   
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr>
        <td>
            <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>
</asp:Content>
