﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TGENESIS.Framework.Enumeraciones;
using TGENESIS.Framework.Utilidades;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.ComponentesNegocio;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{

    public partial class wfLogin : System.Web.UI.Page
    {
        System.Configuration.AppSettingsReader read = new System.Configuration.AppSettingsReader();
        protected void Page_Load(object sender, EventArgs e)
        {

            ucucNewLogin.IdSistema = int.Parse( ConfigurationManager.AppSettings["IdSistema"].ToString());
            ucucNewLogin.EmailFrom = read.GetValue("emailErrorFrom", typeof(string)).ToString();
            ucucNewLogin.UsuarioMail = read.GetValue("userMail", typeof(string)).ToString();
            ucucNewLogin.PassUsuarioMail = read.GetValue("passMail", typeof(string)).ToString();
            ucucNewLogin.EnviarMailActivo = true;
        }

        protected void OnAceptar(object sender, EventArgs e)
        {
            ucucNewLogin.IdSistema = int.Parse(ConfigurationManager.AppSettings["IdSistema"].ToString());
            Enumeraciones.Autenticacion Autentica = ucucNewLogin.ObtenerAutenticacion();
            InsertarConexion(Autentica);

            Session["usuario"] = ucucNewLogin.Usuario.ToString();

            if (Autentica == Enumeraciones.Autenticacion.AUTENTICADO)
            {


                Sesiones.Glb_autenticacion = Serializador.Serializar<Autenticacion>(ucucNewLogin.autenticacion);
                //Response.Redirect("ControlCalidad/wfRecepcionEquipos.aspx", true);
                Response.Redirect("Principal/wfPrincipal.aspx", true);
            }

            if (Autentica == Enumeraciones.Autenticacion.PROVISORIO)
            {
                Response.Redirect("wfCambiarPassword.aspx", true);
            }
        }

        private void InsertarConexion(Enumeraciones.Autenticacion autenticacion)
        {
            try
            {
                string peticion = Request.ServerVariables["REMOTE_ADDR"];
                string navegador = Request.ServerVariables["HTTP_USER_AGENT"];
                string host = Request.ServerVariables["HTTP_HOST"];
                bool proxy = false;
                string clienteIp = Request.ServerVariables["REMOTE_HOST"];
                Conexion conexion = new Conexion();
                conexion.fecha = DateTime.Now;
                conexion.autenticacion = autenticacion.ToString("F");
                conexion.esProxy = proxy;
                conexion.idSistema = ucucNewLogin.IdSistema;
                conexion.ipCliente = clienteIp;
                conexion.ipPeticion = peticion;
                conexion.usuario = ucucNewLogin.Usuario;
                conexion.Navegador = navegador;
                conexion.Host = host;
                NegocioConexion.Instancia.Insertar(conexion);
                //HTTP_REFERER

            }
            catch (Exception ex)
            {

            }
        }
    }

}