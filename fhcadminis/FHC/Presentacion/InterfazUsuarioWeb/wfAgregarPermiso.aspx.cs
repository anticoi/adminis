﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using TGENESIS.FHC.ComponentesNegocio;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    public partial class wfAgregarPermiso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 4);

                lTitulo.Text = NombreMenu.ToString();
            }
        }
    }
}