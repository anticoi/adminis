﻿<%@ Page Title="Contingencia Trio" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfContingenciaTrio.aspx.cs" Inherits="InterfazUsuarioWeb.Contingencia.wfContingenciaTrio" %>

<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<%@ Register assembly="Tde.Controles.RutTextBox" namespace="Tde.Controles" tagprefix="tde" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function ValidarDato(source, arguments) {
            if ($.trim(arguments.Value) == "") {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FF5959";
                arguments.IsValid = false;
            }
            else {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FFFFFF";
                arguments.IsValid = true;
            }
        }


        $(function () {

            $("#<%= txtFecha.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

        });


        function solonumeros(e) {
            var key;
            if (window.event) // IE
            {
                key = e.keyCode;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                key = e.which;
            }

            if (key < 48 || key > 57) {
                return false;
            }
            return true;
        }
    </script>
  
    <style type="text/css">
        .style1
        {
            width: 150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="800px" style="height:300px;">
        <tr>
            <td valign="top" align="center">
                <table width="80%">
                    <tr>
                        <td align="center">
                            <asp:Label  ID="lblContrato" runat="server" Text="Contrato"></asp:Label>
                        </td>

                        <td>
                            <asp:TextBox id ="txtContrato" runat="server"  MaxLength="100" Width="300px"
                            AutoPostBack="true" TabIndex="1"></asp:TextBox>
                        </td>

                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar Contrato"  
                                class="button small blue" onclick="btnBuscar_Click" TabIndex="2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True"></asp:Label>
                        </td>

                        <td>
                            <asp:Label ID="lblExisteContrato" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>                  
            </td>
        </tr>

        <tr>
            <td align="center">
                <table width="600px" >
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Label ID="lblFolio" runat="server" Text="Folio Contingencia"></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="txtFolio" runat="server"  Text="" MaxLength="50" Width="300px" 
                                TabIndex="3"></asp:TextBox>
                        </td>                

                        <td>
                            <asp:Button ID="btnBfolio" runat="server" Text="Buscar Folio" 
                                Class="button small blue" OnClick="btnBFolio_Click" TabIndex="4" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblExisteFolio" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Label ID="lblFecha" runat="server" Text="Fecha"></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="txtFecha" runat="server"  Text="" MaxLength="50" Width="100px"  
                                AutoPostBack="true" TabIndex="5"></asp:TextBox>
                        </td>                
                    </tr>

                    <tr>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="Label3" Text="" runat="server" 
                                Font-Bold="True" ForeColor="White"></asp:Label>
                        </td>

                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="lblDatosFall" Text="Datos del Fallecido" runat="server" 
                                Font-Bold="True" ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblNombreFall" runat="server" Text="Nombre Completo"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtNombreFall" runat="server" Width="300px" TabIndex="6" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblRutFall" runat="server" Text="Rut"></asp:Label>
                        </td>
                        <td class="style3">
<%--                            <tde:RutTextBox ID="RuttxtFall" runat="server" TabIndex="7"></tde:RutTextBox>
--%>                            <asp:TextBox ID="txtRutFallecido" runat="server" TabIndex="7"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="Label4" Text="" runat="server" 
                                Font-Bold="True" ForeColor="White"></asp:Label>
                        </td>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="lblDatosCont" Text="Datos Contratante" runat="server" 
                                Font-Bold="True" ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblNombreCont" runat="server" Text="Nombre Completo"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtNombreCont" runat="server" Width="300px" TabIndex="8" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblRutCont" runat="server" Text="Rut"></asp:Label>
                        </td>
                        <td class="style3">
                            <%--<tde:RutTextBox ID="RuttxtCont" runat="server" TabIndex="9"></tde:RutTextBox>--%>
                            <asp:TextBox ID="txtRutCont" runat="server" TabIndex="9"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="Label5" Text="" runat="server" Font-Bold="True" 
                                ForeColor="White"></asp:Label>
                        </td>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="lblVendedor" Text="Vendedor" runat="server" Font-Bold="True" 
                                ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblNombreVend" runat="server" Text="Nombre Vendedor"></asp:Label>
                        </td>
                        <%--<td class="style3">
                            <asp:TextBox ID="txtNombreVend" runat="server" Width="300px" TabIndex="10" ></asp:TextBox>
                        </td>--%>
                        <td class="style3">
                            <asp:DropDownList ID="ddlVendedor" runat="server" Width="300px" TabIndex="10" 
                                onselectedindexchanged="ddlVendedor_SelectedIndexChanged"></asp:DropDownList>
                                <asp:Label ID="lblVendedor1" runat="server" Width="300px"></asp:Label>
                                <asp:Label ID="lblMensajeVend" runat="server" Width="300px" 
                                Text="Seleccione al vendedor en la lista por favor" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblFono" runat="server" Text="Teléfono"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtFonoVend" runat="server" Width="100px" MaxLength="10" 
                                TabIndex="11"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="Label6" Text="" runat="server" Font-Bold="True" 
                                ForeColor="White"></asp:Label>
                        </td>

                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="lblTramites" Text="Tramites" runat="server" Font-Bold="True" 
                                ForeColor="White"></asp:Label>
                        </td>
                    </tr>
<%--                    <tr>
                        <td align="center" class="style1">
                            <asp:Label ID="lblSubTramite" Text="TRAMITES" runat="server"></asp:Label>
                        </td>
                        <td align="center" class="style1">
                            <asp:Label ID="lblLaFuneraria" Text="LA FUNERARIA" runat="server"></asp:Label>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblLugar" runat="server" Text="Lugar del Deceso"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtLugarDeseso" runat="server" Width="300px" TabIndex="12" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblEstado" runat="server" Text="Estado Civil"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtEstadoCiv" runat="server" Width="200px" MaxLength="10" 
                                TabIndex="13"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblEstudios" runat="server" Text="Nivel de Estudios"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtEstudios" runat="server" Width="200px" TabIndex="14" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblProfesion" runat="server" Text="Profesión u Oficio"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtProfesion" runat="server" Width="200px" MaxLength="10" 
                                TabIndex="15"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblCementerio" runat="server" Text="Cementerio"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtCementerio" runat="server" Width="300px" TabIndex="16" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblObservacion" runat="server" Text="Observación"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtObservacion" runat="server" Width="300px" Height="50px" 
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="Label7" Text="" runat="server" Font-Bold="True" 
                                ForeColor="White"></asp:Label>
                        </td>
                        <td align="center" class="style1" bgcolor="#339933">
                            <asp:Label ID="lblServicio" Text="Servicio" runat="server" Font-Bold="True" 
                                ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblPlan" runat="server" Text="Plan"></asp:Label>
                        </td>
                        <td class="style3">
                            <div class="select"   style="width:380px;"> 
                                <asp:TextBox ID="txtPlan" runat="server" Width="400px" TabIndex="17"></asp:TextBox>
                            </div>
                        </td> 
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblIncluye" runat="server" Text="Incluye"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtIncluye" runat="server" Width="400px" TabIndex="18"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblTraslado" runat="server" Text="Traslado"></asp:Label>
                        </td>
                        <td class="style3">
                            <asp:TextBox ID="txtTraslado" runat="server" Width="400px" TabIndex="19"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td valign="top" class="style1">  
                        </td>
                        <td align="right" class="style3">
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="100px"  
                                 Height="25px" class="button small blue" onclick="btnGuardar_Click" 
                                TabIndex="20" />
                        </td>                
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <uc1:ucmensaje ID="ucMensaje1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
