﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using TGENESIS.FHC.EntidadesNegocio;
using System.Configuration;

namespace InterfazUsuarioWeb.Contingencia
{
    public partial class wfContingenciaTrio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 12);

                lTitulo.Text = NombreMenu.ToString();
                CargarComboVendedor();
            }

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            if (txtContrato.Text == "")
            {
                txtContrato.Text = "-";
            }

                if (txtFecha.Text == "")
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "El campo FECHA no puede estar vacio");
                }
                else
                {

                    
                    if (txtFolio.Text == "")
                    {
                        #region Consulta Codigo
                        try
                        {

                            int cant = 0;

                            ConexionSQL cc = new ConexionSQL();
                            SqlConnection cn = cc.AbrirConexionIntranet();

                            SqlCommand cmd = new SqlCommand("Usp_ConsultaCodContingencia", cn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cant = Convert.ToInt32(cmd.ExecuteScalar());

                            string CantFinal = Convert.ToString(cant + 1);

                            txtFolio.Text = CantFinal;
                        }
                        catch (Exception ex)
                        {
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
                        }
                        #endregion

                        #region Creación Tarea
                        try
                        {
                            DateTime Fecha = DateTime.Today;
                            WorkFlow cwf = new WorkFlow();

                            cwf.WorkFlowContrato = Convert.ToInt32(txtFolio.Text);
                            cwf.WorkFlowTitulo = "Contingencia TRIO";
                            cwf.WorkFlowObservacion = "Favor regularizar la contingencia TRIO, folio: " + txtFolio.Text;
                            cwf.WorkFlowUsuarioAsignado = ddlVendedor.SelectedValue.ToString();
                            cwf.TipoWorkFlowId = 15;
                            cwf.EstadoWorkFlowId = 1;
                            cwf.WorkFlowUsuarioIngreso = "maria.arancibia";
                            cwf.WorkFlowUsuarioModificacion = "maria.arancibia";
                            cwf.WorkFlowArea = 1;
                            cwf.WorkFlowFechaIngreso = Fecha;
                            cwf.WorkFlowFechaModificacion = Fecha;

                            NegocioWorkFlow.Instancia.Insertar(cwf);

                            EnvioAviso(ddlVendedor.SelectedValue.ToString(), "maria.arancibia", "Contingencia TRIO Folio Nº: " + txtFolio.Text, "Se genera contingencia TRIO con folio Nº: " + txtFolio.Text);
                        }
                        catch (Exception ex)
                        {
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                        }
                        #endregion

                        #region Consulta ID WorkFlow Tarea Creada y Guarda Tarea
                        try
                        {

                            int IDWorkFlow = 0;

                            ConexionSQL cc1 = new ConexionSQL();
                            SqlConnection cn1 = cc1.AbrirConexionIntranet();

                            SqlCommand cmd1 = new SqlCommand("Usp_ConsultaIDWorkFlow", cn1);
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.AddWithValue("@Folio", Convert.ToInt32(txtFolio.Text));

                            IDWorkFlow = Convert.ToInt32(cmd1.ExecuteScalar());

                            #region Guarda Contingencia
                        try
                        {
                            ConexionSQL cc = new ConexionSQL();
                            SqlConnection cn = cc.AbrirConexionIntranet();

                            SqlCommand cmd = new SqlCommand("Usp_InsertaContingencia", cn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@Contrato", txtContrato.Text);
                            cmd.Parameters.AddWithValue("@Folio", txtFolio.Text);
                            cmd.Parameters.AddWithValue("@Fecha", txtFecha.Text);
                            cmd.Parameters.AddWithValue("@NombreFallecido", txtNombreFall.Text);
                            cmd.Parameters.AddWithValue("@RutFallecido", txtRutFallecido.Text);
                            cmd.Parameters.AddWithValue("@NombreContratante", txtNombreCont.Text);
                            cmd.Parameters.AddWithValue("@RutContratante", txtRutCont.Text);
                            cmd.Parameters.AddWithValue("@NombreVendedor", ddlVendedor.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@FonoVendedor", txtFonoVend.Text);
                            cmd.Parameters.AddWithValue("@LugarTramite", txtLugarDeseso.Text);
                            cmd.Parameters.AddWithValue("@EstadoCivil", txtEstadoCiv.Text);
                            cmd.Parameters.AddWithValue("@Estudios", txtEstudios.Text);
                            cmd.Parameters.AddWithValue("@Profesion", txtProfesion.Text);
                            cmd.Parameters.AddWithValue("@CementerioTramite", txtCementerio.Text);
                            cmd.Parameters.AddWithValue("@ObservacionesTramite", txtObservacion.Text);
                            cmd.Parameters.AddWithValue("@PlanServicio", txtPlan.Text);
                            cmd.Parameters.AddWithValue("@IncluyeServicio", txtIncluye.Text);
                            cmd.Parameters.AddWithValue("@TrasladoServicio", txtTraslado.Text);
                            cmd.Parameters.AddWithValue("@Estado", "Pendiente");
                            cmd.Parameters.AddWithValue("@IDWorkFlow", Convert.ToInt32(IDWorkFlow));

                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);

                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "La contingencia con folio: " + txtFolio.Text.ToString() + " se guardo con éxito.");


                            //LimpiarPantalla();
                        }
                        catch (Exception ex)
                        {
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
                        }
                        #endregion

                        }
                        catch (Exception ex)
                        {
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
                        }
                        #endregion

                        
                    }
                    else
                    {
                        #region Actualiza Contingencia
                        try
                        {
                            ConexionSQL cc = new ConexionSQL();
                            SqlConnection cn = cc.AbrirConexionIntranet();

                            SqlCommand cmd = new SqlCommand("Usp_ActualizaContingencia", cn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@Contrato", txtContrato.Text);
                            cmd.Parameters.AddWithValue("@Folio", txtFolio.Text);
                            cmd.Parameters.AddWithValue("@Fecha", txtFecha.Text);
                            cmd.Parameters.AddWithValue("@NombreFallecido", txtNombreFall.Text);
                            cmd.Parameters.AddWithValue("@RutFallecido", txtRutFallecido.Text);
                            cmd.Parameters.AddWithValue("@NombreContratante", txtNombreCont.Text);
                            cmd.Parameters.AddWithValue("@RutContratante", txtRutCont.Text);
                            cmd.Parameters.AddWithValue("@NombreVendedor", ddlVendedor.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@FonoVendedor", txtFonoVend.Text);
                            cmd.Parameters.AddWithValue("@LugarTramite", txtLugarDeseso.Text);
                            cmd.Parameters.AddWithValue("@EstadoCivil", txtEstadoCiv.Text);
                            cmd.Parameters.AddWithValue("@Estudios", txtEstudios.Text);
                            cmd.Parameters.AddWithValue("@Profesion", txtProfesion.Text);
                            cmd.Parameters.AddWithValue("@CementerioTramite", txtCementerio.Text);
                            cmd.Parameters.AddWithValue("@ObservacionesTramite", txtObservacion.Text);
                            cmd.Parameters.AddWithValue("@PlanServicio", txtPlan.Text);
                            cmd.Parameters.AddWithValue("@IncluyeServicio", txtIncluye.Text);
                            cmd.Parameters.AddWithValue("@TrasladoServicio", txtTraslado.Text);

                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);

                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "La contingencia con folio: " + txtFolio.Text.ToString() + " se actualizo con éxito.");

                            LimpiarPantalla();
                        }
                        catch (Exception ex)
                        {
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
                        }
                        #endregion

                        #region Creación Tarea (Comentada)
                        //try
                        //{
                        //    DateTime Fecha = DateTime.Today;
                        //    WorkFlow cwf = new WorkFlow();

                        //    cwf.WorkFlowContrato = 0;
                        //    cwf.WorkFlowTitulo = "Contingencia TRIO";
                        //    cwf.WorkFlowObservacion = "Favor regularizar la contingencia TRIO, folio: " + txtFolio.Text;
                        //    cwf.WorkFlowUsuarioAsignado = ddlVendedor.SelectedIndex.ToString();
                        //    cwf.TipoWorkFlowId = 15;
                        //    cwf.EstadoWorkFlowId = 1;
                        //    cwf.WorkFlowUsuarioIngreso = "maria.arancibia";
                        //    cwf.WorkFlowUsuarioModificacion = "maria.arancibia";
                        //    cwf.WorkFlowArea = 1;
                        //    cwf.WorkFlowFechaIngreso = Fecha;
                        //    cwf.WorkFlowFechaModificacion = Fecha;

                        //    NegocioWorkFlow.Instancia.Insertar(cwf);

                        //    EnvioAviso(Context.User.Identity.Name, "maria.arancibia", "Contingencia TRIO Folio Nº: " + txtFolio.Text, "Se genera contingencia TRIO con folio Nº: " + txtFolio.Text);
                        //}
                        //catch (Exception ex)
                        //{
                        //    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                        //}
                        #endregion
                    }

                }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {

                string Contrato = txtContrato.Text;

                #region "Busca Datos Fallecido y Verifica Existencia de Contrato"
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_BuscarDatosFallecidoxContrato", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@Folio", Contrato);

                SqlDataReader readerFallecido = cmd.ExecuteReader();

                if (readerFallecido.Read())
                {
                    txtFecha.Text = Convert.ToDateTime(readerFallecido["OPER_FECH"]).ToString("dd-MM-yyyy");
                    txtNombreFall.Text = readerFallecido["FALLECIDO"].ToString();
                    txtRutFallecido.Text = readerFallecido["PERS_ID"].ToString();
                    //RuttxtFall.Text = readerFallecido["PERS_ID"].ToString();
                    //ddlVendedor.SelectedValue = Convert.ToString(readerFallecido["login"]);
                    txtPlan.Text = readerFallecido["PLAN_DESC"].ToString();
                    txtTraslado.Text = readerFallecido["TRASLADO"].ToString();
                }
                else
                {
                    lblExisteContrato.Text = "NO EXISTE EL CONTRATO";
                }
                #endregion

                #region "Busca Datos Contratante"
                ConexionSQL cc1 = new ConexionSQL();
                SqlConnection cn1 = cc1.AbrirConexionIntranet();

                SqlCommand cmd1 = new SqlCommand("Usp_BuscarDatosContratantexContrato", cn1);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.CommandTimeout = 0;
                cmd1.Parameters.AddWithValue("@Folio", txtContrato.Text);

                SqlDataReader readerContratante = cmd1.ExecuteReader();

                if (readerContratante.Read())
                {
                    txtNombreCont.Text = readerContratante["CONTRATANTE"].ToString();
                    txtRutCont.Text = readerContratante["PERS_ID"].ToString();
                    //RuttxtCont.Text = readerContratante["PERS_ID"].ToString();
                }
                #endregion

                #region "Busca Datos Vendedor"
                ConexionSQL cc2 = new ConexionSQL();
                SqlConnection cn2 = cc2.AbrirConexionIntranet();

                SqlCommand cmd2 = new SqlCommand("Usp_BuscarDatosVendedorxContrato", cn2);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.CommandTimeout = 0;
                cmd2.Parameters.AddWithValue("@Folio", txtContrato.Text);

                SqlDataReader readerVendedor = cmd2.ExecuteReader();

                if (readerVendedor.Read())
                {
                    //txtNombreVend.Text = readerVendedor["VENDEDOR"].ToString();
                    lblVendedor1.Visible = true;

                    lblVendedor1.Text = readerVendedor["VENDEDOR"].ToString();

                    lblMensajeVend.Visible = true;
                }
                #endregion

                #region "Busca Datos Tramites"
                ConexionSQL cc3 = new ConexionSQL();
                SqlConnection cn3 = cc3.AbrirConexionIntranet();

                SqlCommand cmd3 = new SqlCommand("Usp_BuscarDatosTramitexContrato", cn3);
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.CommandTimeout = 0;
                cmd3.Parameters.AddWithValue("@Folio", txtContrato.Text);

                SqlDataReader readerTramites = cmd3.ExecuteReader();

                if (readerTramites.Read())
                {
                    txtLugarDeseso.Text = readerTramites["LUGAR_DECESO"].ToString();
                    txtEstadoCiv.Text = readerTramites["FALL_ESTA_CIVIL"].ToString();
                    txtEstudios.Text = readerTramites["FALL_NIVE_ESTU"].ToString();
                    txtProfesion.Text = readerTramites["FALL_OCUP_PROF"].ToString();
                    txtCementerio.Text = readerTramites["LUGA_DESC"].ToString();
                }
                #endregion

                #region "Busca Datos Servicios"

                #endregion
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }

        }

        protected void LimpiarPantalla()
        {
            txtContrato.Text = "";
            txtFolio.Text="";
            txtFecha.Text="";
            txtNombreFall.Text="";
            txtRutFallecido.Text = "";
            txtNombreCont.Text = "";
            txtRutCont.Text = "";
            txtFonoVend.Text="";
            txtLugarDeseso.Text="";
            txtEstadoCiv.Text="";
            txtEstudios.Text="";
            txtProfesion.Text="";
            txtCementerio.Text="";
            txtObservacion.Text="";
            txtPlan.Text="";
            txtIncluye.Text="";
            txtTraslado.Text="";
        }

        protected void LimpiarPantallaSF()
        {
            txtContrato.Text = "";
            txtFecha.Text = "";
            txtNombreFall.Text = "";
            txtRutFallecido.Text = "";
            txtNombreCont.Text = "";
            txtRutCont.Text = "";
            //txtNombreVend.Text = "";
            txtFonoVend.Text = "";
            txtLugarDeseso.Text = "";
            txtEstadoCiv.Text = "";
            txtEstudios.Text = "";
            txtProfesion.Text = "";
            txtCementerio.Text = "";
            txtObservacion.Text = "";
            txtPlan.Text = "";
            txtIncluye.Text = "";
            txtTraslado.Text = "";
        }

        protected void btnBFolio_Click(object sender, EventArgs e)
        {
            LimpiarPantallaSF();

            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_BuscarContingenciaxFolio", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Folio", txtFolio.Text);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    txtContrato.Text = reader["Cont_Contrato"].ToString();
                    txtFecha.Text = Convert.ToDateTime(reader["Cont_Fecha"]).ToString("dd-MM-yyyy");
                    txtNombreFall.Text = reader["Cont_Fall_Nombre"].ToString();
                    txtRutFallecido.Text = reader["Cont_Fall_Rut"].ToString();
                    //RuttxtFall.Text = reader["Cont_Fall_Rut"].ToString();
                    txtNombreCont.Text = reader["Cont_Cont_Nombre"].ToString();
                    txtRutCont.Text = reader["Cont_Cont_Rut"].ToString();
                    //RuttxtCont.Text = reader["Cont_Cont_Rut"].ToString();
                    //txtNombreVend.Text = reader["Cont_Vend_Nombre"].ToString();
                    ddlVendedor.SelectedValue = reader["Cont_Vend_Nombre"].ToString();
                    txtFonoVend.Text = reader["Cont_Vend_Fono"].ToString();
                    txtLugarDeseso.Text = reader["Cont_Tram_Lugar"].ToString();
                    txtEstadoCiv.Text = reader["Cont_Tram_EstCivil"].ToString();
                    txtEstudios.Text = reader["Cont_Tram_Estudios"].ToString();
                    txtProfesion.Text = reader["Cont_Tram_Profesion"].ToString();
                    txtCementerio.Text = reader["Cont_Tram_Cementerio"].ToString();
                    txtObservacion.Text = reader["Cont_Tram_Observaciones"].ToString();
                    txtPlan.Text = reader["Cont_Serv_Plan"].ToString();
                    txtIncluye.Text = reader["Cont_Serv_Incluye"].ToString();
                    txtTraslado.Text = reader["Cont_Serv_Traslado"].ToString();
                }
                else
                {
                    lblExisteFolio.Text = "NO EXISTE EL FOLIO";
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void CrearTarea(object sender, EventArgs e)
        {
            try
            {
                int Validar = ValidarDatos();

                if (Validar == 0)
                    return;

                WorkFlow cwf = new WorkFlow();

                int contrato = 0;
                if (txtFolio.Text != "")
                    contrato = int.Parse(txtFolio.Text);

                cwf.WorkFlowContrato = contrato;
                cwf.WorkFlowTitulo = "Contingencia Trio Folio Nº: " + txtFolio.Text;
                cwf.WorkFlowObservacion = "Se genera contingencia TRIO con folio Nº: " + txtFolio.Text;
                //cwf.WorkFlowUsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
                cwf.WorkFlowUsuarioAsignado = Context.User.Identity.Name;
                cwf.TipoWorkFlowId = 15;
                cwf.EstadoWorkFlowId = 1;
                cwf.WorkFlowUsuarioIngreso = "maria.arancibia";
                cwf.WorkFlowUsuarioModificacion = "maria.arancibia";
                cwf.WorkFlowArea = 1;


                NegocioWorkFlow.Instancia.Insertar(cwf);

                EnvioAviso(Context.User.Identity.Name, Context.User.Identity.Name, "Contingencia Trio Folio Nº: " + txtFolio.Text, "Se genera contingencia TRIO con folio Nº: " + txtFolio.Text);
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "La tarea se creo con éxito.");

                //limpiarpantalla();

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);

            }
        }

        protected int ValidarDatos()
        {
            int valor = 0;
            
            if (txtFolio.Text == "")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Informacion!!!", "Debe ingresar un folio para la tarea.");
                return valor;
            }

            valor = 1;
            return valor;
        }

        protected void EnvioAviso(string Usuario, string UsuarioCrea, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCrea);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCorreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Usuario);

                string[] to = new string[1];
                to[0] = oUsuarioCorreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Pendiente " + "\n" + " El usuario " + oUsuario.nombre + " acaba de asignar la siguiente tarea a tus pendientes: " + "\n" + Descripcion, Usuario, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);

            }

        }

        public void ConsultaCod()
        {
            try
            {

                int cant = 0;

                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ConsultaCodContingencia", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cant = Convert.ToInt32(cmd.ExecuteScalar());

                string CantFinal = Convert.ToString(cant + 1);

                txtFolio.Text = CantFinal;
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void CargarComboVendedor()
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet2();

                SqlCommand cmd = new SqlCommand("Usp_ListarVendedor", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                this.ddlVendedor.DataSource = ds;
                this.ddlVendedor.DataValueField = "login";
                this.ddlVendedor.DataTextField = "nombre";
                this.ddlVendedor.DataBind();
                //this.ddlVendedor.Items.Insert(0, new ListItem("Seleccione un Vendedor...", "0"));
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }

        }

    }
}