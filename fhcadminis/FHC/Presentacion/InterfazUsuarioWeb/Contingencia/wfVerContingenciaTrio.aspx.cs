﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;
using TGENESIS.FHC.EntidadesNegocio;

namespace InterfazUsuarioWeb.Contingencia
{
    public partial class wfVerContingenciaTrio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 19);

                lTitulo.Text = "Ver Contingencia TRIO";

                CargarGrilla();
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                

                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarContingenciaxEstado", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Estado", ddlEstado.SelectedValue.ToString());

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                dt.Columns.Add("Ruta");

                grvVerContingencia.DataSource = dt;
                grvVerContingencia.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

        }

        protected void btnFiltrar2_Click(object sender, EventArgs e)
        {
            try
            {
                

                ConexionSQL cc1 = new ConexionSQL();
                SqlConnection cn1 = cc1.AbrirConexionIntranet();

                SqlCommand cmd1 = new SqlCommand("Usp_ListarContingenciaxFecha", cn1);
                cmd1.CommandType = CommandType.StoredProcedure;

                cmd1.Parameters.AddWithValue("@FechaInicio", txtFechaDesde.Text);
                cmd1.Parameters.AddWithValue("@FechaFinal", txtFechaHasta.Text);

                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);

                dt1.Columns.Add("Ruta");

                grvVerContingencia.DataSource = dt1;
                grvVerContingencia.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void CargarGrilla()
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarContingenciaxEstado", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Estado", "Todos");

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                dt.Columns.Add("Ruta");
                dt.Columns.Add("Icono");

                grvVerContingencia.DataSource = dt;
                grvVerContingencia.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void grvVerContingencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = grvVerContingencia.SelectedRow;
            string Asesor = row.Cells[5].Text;
            string Folio = row.Cells[2].Text;
            string Login="";

            try
            {
                ConexionSQL cc1 = new ConexionSQL();
                SqlConnection cn1 = cc1.AbrirConexionIntranet2();
                SqlCommand cmd1 = new SqlCommand("usp_buscaUsuarioLogin", cn1);
                cmd1.CommandType = CommandType.StoredProcedure;

                cmd1.Parameters.AddWithValue("@Nombre", Asesor);

                Login = Convert.ToString(cmd1.ExecuteScalar());
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

            #region Envio Aviso Mediante Tarea
            //try
            //{

            //    DateTime Fecha = DateTime.Today;
            //    WorkFlow cwf = new WorkFlow();

            //    cwf.WorkFlowContrato = 0;
            //    cwf.WorkFlowTitulo = "Contingencia Trio";
            //    cwf.WorkFlowObservacion = "Favor regularizar la contingencia TRIO, folio: " + Folio;
            //    //cwf.WorkFlowUsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
            //    cwf.WorkFlowUsuarioAsignado = Login;
            //    cwf.TipoWorkFlowId = 15;
            //    cwf.EstadoWorkFlowId = 1;
            //    cwf.WorkFlowUsuarioIngreso = "maria.arancibia";
            //    cwf.WorkFlowUsuarioModificacion = "maria.arancibia";
            //    cwf.WorkFlowArea = 1;
            //    cwf.WorkFlowFechaIngreso = Fecha;
            //    cwf.WorkFlowFechaModificacion = Fecha;


            //    NegocioWorkFlow.Instancia.Insertar(cwf);

            //    EnvioAviso(Context.User.Identity.Name, Context.User.Identity.Name, "Contingencia Trio Folio Nº: " + Folio, "Se genera contingencia TRIO con folio Nº: " + Folio);
            //    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "El aviso se envio de forma exitosa");

            //    //limpiarpantalla();

            //}
            //catch (Exception ex)
            //{
            //    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);

            //}
            #endregion
        }

        protected void txtFechaDesde_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtFechaHasta_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void EnvioAviso(string Usuario, string UsuarioCrea, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCrea);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCorreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Usuario);

                string[] to = new string[1];
                to[0] = oUsuarioCorreo.email;

                Mail.EnviarMail(emailDe, to, "Tarea :" + Titulo, " - Tarea Pendiente " + "\n" + " El usuario " + oUsuario.nombre + " acaba de asignar la siguiente tarea a tus pendientes: " + "\n" + Descripcion, Usuario, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);

            }

        }

        protected void EnviarAviso_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox check;
                //int id;
                int elimino;
                elimino = 0;

                foreach (GridViewRow fila in grvVerContingencia.Rows)
                {
                    
                    #region Busca Login Asesor (Comentado)
                    //string Asesor = fila.Cells[6].Text;
                    //string LoginAsesor = "";

                    //try
                    //{
                    //    ConexionSQL cc1 = new ConexionSQL();
                    //    SqlConnection cn1 = cc1.AbrirConexionIntranet2();
                    //    SqlCommand cmd1 = new SqlCommand("usp_buscaUsuarioLogin", cn1);
                    //    cmd1.CommandType = CommandType.StoredProcedure;

                    //    cmd1.Parameters.AddWithValue("@Nombre", Asesor);

                    //    LoginAsesor = Convert.ToString(cmd1.ExecuteScalar());
                    //}
                    //catch (Exception ex)
                    //{
                    //    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                    //}
                    #endregion

                    string Folio = fila.Cells[3].Text;
                    int IdWorkFlow = Convert.ToInt32(fila.Cells[8].Text);

                    check = (CheckBox)fila.Cells[0].FindControl("chkAviso");

                    if (check.Checked == true)
                    {
                        //Verificar si existe el WorkFlowID
                        ConexionSQL cc2 = new ConexionSQL();
                        SqlConnection cn2 = cc2.AbrirConexionIntranet();
                        SqlCommand cmd2 = new SqlCommand("Usp_ListarExisteID", cn2);
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@IDWorkFlow", Convert.ToInt32(fila.Cells[8].Text));

                        int Resultado = Convert.ToInt32(cmd2.ExecuteScalar());
                        
                        //Se verifica si Resultado=1 para UPDATE Observaciones o Resultado=0 para INSERT
                        //Aca se ejecuta el código de envio de mensaje
                        if (Resultado == 1)
                        {
                            #region Actualización Tarea
                            try
                            {
                                DateTime Fecha = DateTime.Today;

                                ConexionSQL cc3 = new ConexionSQL();
                                SqlConnection cn3 = cc3.AbrirConexionIntranet();
                                SqlCommand cmd3 = new SqlCommand("Usp_ActualizaWorkFlowContingencia", cn3);
                                cmd3.CommandType = CommandType.StoredProcedure;
                                cmd3.Parameters.AddWithValue("@IDWorkFlow", Convert.ToInt32(fila.Cells[8].Text));
                                cmd3.Parameters.AddWithValue("@WFObservacion", "Favor regularizar la contingencia TRIO, folio: " + Folio);
                                cmd3.Parameters.AddWithValue("@WFFechaModificacion", Fecha);

                                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                                DataTable dt3 = new DataTable();
                                da3.Fill(dt3);

                                EnvioAviso(fila.Cells[6].Text, "maria.arancibia", "Contingencia TRIO Folio Nº: " + Folio, "Actualizar contingencia TRIO Nº: " + Folio);
                            }
                            catch (Exception ex)
                            {
                                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                            }
                            #endregion
                        }
                    }
                }

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "Los avisos fueron enviados de forma exitosa");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }

        }

        protected void Finalizar_Click(object sender, EventArgs e)
        {
            try
            {
                CheckBox check;
                //int id;
                int elimino;
                elimino = 0;

                foreach (GridViewRow fila in grvVerContingencia.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chkFinaliza");

                    if (check.Checked == true)
                    {
                        
                        string Contrato = fila.Cells[4].Text;
                        var strings = new List<string>() { "", "&nbsp;", "&nbsp", "-" };
                        if (strings.Contains(Contrato))
                        {
                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error", "No se puede finalizar una contingencia sin nº de contrato");
                        }
                        else
                        {
                            string Folio = fila.Cells[3].Text;

                            //Verificar si existe el WorkFlowID
                            ConexionSQL cc2 = new ConexionSQL();
                            SqlConnection cn2 = cc2.AbrirConexionIntranet();
                            SqlCommand cmd2 = new SqlCommand("Usp_ListarExisteID", cn2);
                            cmd2.CommandType = CommandType.StoredProcedure;
                            cmd2.Parameters.AddWithValue("@IDWorkFlow", Convert.ToInt32(fila.Cells[8].Text));

                            int Resultado = Convert.ToInt32(cmd2.ExecuteScalar());

                            //Se verifica si Resultado=1 para UPDATE Observaciones o Resultado=0 para INSERT

                            //Aca código de actualización de estado
                            ConexionSQL cc1 = new ConexionSQL();
                            SqlConnection cn1 = cc1.AbrirConexionIntranet();
                            SqlCommand cmd1 = new SqlCommand("Usp_FinalizaContingencia", cn1);
                            cmd1.CommandType = CommandType.StoredProcedure;

                            cmd1.Parameters.AddWithValue("@Folio", Folio);

                            SqlDataAdapter da = new SqlDataAdapter(cmd1);
                            DataTable dt = new DataTable();
                            da.Fill(dt);

                            //Aca se finaliza la tarea
                            if (Resultado == 1)
                            {
                                #region Finalización Tarea
                                try
                                {
                                    DateTime Fecha = DateTime.Today;

                                    ConexionSQL cc3 = new ConexionSQL();
                                    SqlConnection cn3 = cc3.AbrirConexionIntranet();
                                    SqlCommand cmd3 = new SqlCommand("Usp_FinalizaWorkFlowEstadoContingencia", cn3);
                                    cmd3.CommandType = CommandType.StoredProcedure;
                                    cmd3.Parameters.AddWithValue("@IDWorkFlow", Convert.ToInt32(fila.Cells[8].Text));

                                    SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                                    DataTable dt3 = new DataTable();
                                    da3.Fill(dt3);

                                    EnvioAviso(fila.Cells[6].Text, "maria.arancibia", "Contingencia TRIO Folio Nº: " + Folio, "Contingencia TRIO Nº: " + Folio + "Finalizada");
                                }
                                catch (Exception ex)
                                {
                                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
                                }
                                #endregion
                            }

                            ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "La contingencia con folio: " + Folio + " ha sido finalizada con éxito.");

                            CargarGrilla();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }
    }
}