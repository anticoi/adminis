﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;

namespace InterfazUsuarioWeb.Contratos
{
    public partial class wfCuotasMortuorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                //string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 20);

                string NombreMenu = "Cuotas Mortuorias";

                lTitulo.Text = NombreMenu.ToString();

            }

        }

        protected void txtFechaHasta_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtFechaDesde_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                //DataTable dt = new DataTable();
                //dt.Columns.Add("Nombre");
                //dt.Columns.Add("Icono");
                //dt.Columns.Add("Ruta");

                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarCuotaMortuoria", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FechaInicio", txtFechaInicio.Text);
                cmd.Parameters.AddWithValue("@FechaTermino", txtFechaFinal.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                dt.Columns.Add("Icono");
                da.Fill(dt);

                grvCMORT.DataSource = dt;
                grvCMORT.DataBind();
            }
            catch (Exception)
            {
            }
        }

        protected void btnactualizacm_Click(object sender, EventArgs e)
        {

        }

        protected void grvCMORT_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grvCMORT_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "actualizarcm")
                {
                    mpepanel.Show();
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }  

        }

        protected void grvCMORT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_InsertaCuotaMortuoria", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Contrato", txtContratoCM.Text);
                cmd.Parameters.AddWithValue("@Estado_Factura", ddlEstaFact.Text);
                cmd.Parameters.AddWithValue("@Cheque_Custodia", chkCheque.Checked);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "Contrato Actualizado con Exito.");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }

        }
    }
}