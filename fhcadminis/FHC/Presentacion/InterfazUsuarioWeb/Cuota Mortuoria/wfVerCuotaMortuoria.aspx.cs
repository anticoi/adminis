﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;

namespace InterfazUsuarioWeb.Cuota_Mortuoria
{
    public partial class wfVerCuotaMortuoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                //string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 20);

                string NombreMenu = "Visualizar Cuotas Mortuorias Procesadas";

                lTitulo.Text = NombreMenu.ToString();

            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarCuotaMortuoriaCM", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FechaInicio", txtFechaDesde.Text);
                cmd.Parameters.AddWithValue("@FechaTermino", txtFechaHasta.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                da.Fill(dt);

                grvCMORT_CM.DataSource = dt;
                grvCMORT_CM.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }

        }

        protected void txtFechaDesde_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtFechaHasta_TextChanged(object sender, EventArgs e)
        {

        }
    }
}