﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ControlUsuario.ComponentesNegocio;
using System.Configuration;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    public partial class wfCrearUsuario : System.Web.UI.Page
    {
        System.Configuration.AppSettingsReader read = new System.Configuration.AppSettingsReader();
        protected void Page_Load(object sender, EventArgs e)
        {
            ucCrearUsuario.IdSistema = int.Parse(ConfigurationManager.AppSettings["IdSistema"].ToString());
            ucCrearUsuario.EnviarMailActivo = true;

            ucCrearUsuario.UsuarioMail = read.GetValue("userMail", typeof(string)).ToString();
            ucCrearUsuario.PassUsuarioMail = read.GetValue("passMail", typeof(string)).ToString();

            ucCrearUsuario.EmailFrom = read.GetValue("emailErrorFrom", typeof(string)).ToString();
            ucCrearUsuario.UrlAccesoSistema = read.GetValue("UrlSistema", typeof(string)).ToString();

            if (!IsPostBack)
            {

            }
        }

        protected void OnAsignarRoles(object sender, EventArgs e)
        {
            Response.Redirect("wfAsignaRol.aspx?usuario=" + ucCrearUsuario.UsuarioCreado);
        }
    }
}   