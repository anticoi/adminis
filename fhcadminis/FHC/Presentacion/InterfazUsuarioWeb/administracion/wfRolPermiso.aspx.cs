﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;
using System.Data;

namespace InterfazUsuarioWeb.administracion
{
    public partial class wfRolPermiso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");
                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 6);
                lTitulo.Text = NombreMenu.ToString();

                CargarCombos("AREA");
                CargarCombos("USUARIO");
                CargarCombos("ROLES");
                CargarCombos("PERMISOS");
            }

        }


        protected void CargarCombos(string Tipo)
        {

            if (Tipo =="AREA")
               {
                        ddlbArea.DataTextField = "AreaNombre";
                        ddlbArea.DataValueField = "AreaId"; 
                        ddlbArea.DataSource = NegocioReporteria.Instancia.ListarArea();
                        ddlbArea.DataBind();
                        ddlbArea.Items.Insert(0, new ListItem("", ""));
                }

            if (Tipo == "USUARIO")
                {
                        ddlbUsuario.DataValueField = "idUsuario";
                        ddlbUsuario.DataTextField = "Nombre";
                        

                        if ( ddlbArea.SelectedValue.ToString() !="")
                            ddlbUsuario.DataSource = NegocioReporteria.Instancia.ListarUsuariosArea(int.Parse(ddlbArea.SelectedValue.ToString()));
                        else
                            ddlbUsuario.DataSource = NegocioReporteria.Instancia.ListarUsuarios();

                        ddlbUsuario.DataBind();
                        ddlbUsuario.Items.Insert(0, new ListItem("", ""));
                }


            if (Tipo == "ROLES")
                {

                        ddlbRol.DataSource = NegocioReporteria.Instancia.ListarRol(); 
                        ddlbRol.DataValueField = "idRol";
                        ddlbRol.DataTextField = "descripcion";
                        ddlbRol.DataBind();
                        ddlbRol.Items.Insert(0, new ListItem("", ""));
                }


            if (Tipo == "PERMISOS")
            {

                grvpermiso.DataSource = NegocioReporteria.Instancia.ListarPermisos();
                grvpermiso.DataKeyNames = new string[] { "idPermiso" };
                grvpermiso.DataBind(); 
            }


        }

        protected void ddlbArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                CargarCombos("USUARIO");
        }


        protected void MarcarPermisos()
        {


            try
            {
                if (ddlbRol.SelectedValue.ToString() == "")
                    return; 

                CargarCombos("PERMISOS");

                DataTable ListaRoles = NegocioReporteria.Instancia.ListarPermisosRol (int.Parse(ddlbRol.SelectedValue.ToString())) ; 

                if (ListaRoles.Rows.Count > 0)
                {
                    CheckBox check;

                    foreach (GridViewRow fila in grvpermiso.Rows)
                    {
                        check = (CheckBox)fila.Cells[0].FindControl("chk");
                        check.Checked = false;
                        foreach (DataRow filatabla in ListaRoles.Rows)
                        {
                            if (int.Parse(grvpermiso.DataKeys[fila.RowIndex]["idPermiso"].ToString()) == int.Parse(filatabla["idPermiso"].ToString()))
                            {
                                check.Checked = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }        


        
        }

        protected void ddlbRol_SelectedIndexChanged(object sender, EventArgs e)
        {
            MarcarPermisos(); 
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {


            try
            {

                if (ddlbRol.SelectedValue.ToString() == "")
                    return; 

                CheckBox check;
 
                int insert;
                insert = 0;
                int idRol = int.Parse(ddlbRol.SelectedValue.ToString() ) ;
                int IdPermiso = 0 ;

                foreach (GridViewRow fila in grvpermiso.Rows)
                {
                    check = (CheckBox)fila.Cells[0].FindControl("chk");
                    IdPermiso = int.Parse(grvpermiso.DataKeys[fila.RowIndex].Value.ToString());

                    NegocioReporteria.Instancia.EliminaRolPermiso(idRol, @IdPermiso); 

                    if (check.Checked == true)
                    {
                        NegocioReporteria.Instancia.InsertarRolPermiso(idRol, IdPermiso); 
                        insert = insert + 1;
                    }
                }

                if (insert > 0)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "   Actualización.", "La información se actualizo con éxito");
 
                }
                else
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "   Actualización Sin Éxito.", "Debe seleccionar uno o mas permisos.");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        protected void ddlbUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


     }
     
}