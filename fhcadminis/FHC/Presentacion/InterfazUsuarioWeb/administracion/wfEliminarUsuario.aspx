﻿<%@ Page Title="FHC" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfEliminarUsuario.aspx.cs" Inherits="InterfazUsuarioWeb.administracion.wfEliminarUsuario" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table border="0" width="900px" style="height:400px;" >
    <tr>
        <td style="height:5px;">&nbsp;</td>
    </tr>

    <tr>
        <td align="center" valign="top">
            <table width="350px">
                <tr>
                    <td colspan="2" style="height:5px;">
                        <asp:Label ID="lblEliminar" runat="server" Font-Bold="true" Text="Eliminar Usuario"></asp:Label>
                    </td>                        
                </tr>

                <tr>
                    <td colspan="2" style="height:5px;">&nbsp;</td>
                </tr>

                <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblUsuario" runat="server" Text="Usuario"></asp:Label>
                    </td>

                    <td>
                        <div class="select" style="width:300px">
                            <asp:DropDownList ID="ddlUsuario" runat="server" Width="300px" 
                                AutoPostBack="true" OnSelectedIndexChanged="ddlUsuario_SelectedIndexChanged" 
                                Height="16px"></asp:DropDownList>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblLogin" runat="server" Text="Login"></asp:Label>
                    </td>

                    <td>
                        <div class="style3">
                            <asp:TextBox ID="txtLogin" runat="server" Width="200"></asp:TextBox>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    </td>

                    <td>
                        <div class="style3">
                            <asp:TextBox ID="txtEmail" runat="server" Width="300"></asp:TextBox>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblArea" runat="server" Text="Area"></asp:Label>
                    </td>

                    <td>
                        <div class="style3">
                            <asp:TextBox ID="txtArea" runat="server" Width="300"></asp:TextBox>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="height:5px;">&nbsp</td>
                </tr>

                <tr>
                    <td colspan="2" align="right">
                        <asp:Button runat="server" ID="btnEliminar" Text="Eliminar" class="button small blue" OnClick="btnEliminar_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <uc1:ucMensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>
</asp:Content>