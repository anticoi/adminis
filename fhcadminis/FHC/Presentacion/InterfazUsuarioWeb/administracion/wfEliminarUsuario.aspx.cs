﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using InterfazUsuarioWeb.Clases;

namespace InterfazUsuarioWeb.administracion
{
    public partial class wfEliminarUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label lTitulo = (Label)Master.FindControl("lblTitulo");
                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 12);

                lTitulo.Text = NombreMenu.ToString();

                CargarCombo();
            }
        }

        private void CargarCombo()
        {
            try
            {
                ddlUsuario.DataSource = NegocioReporteria.Instancia.ListarUsuarios();
                ddlUsuario.DataValueField = "idUsuario";
                ddlUsuario.DataTextField = "nombre";
                ddlUsuario.DataBind();
                ddlUsuario.Items.Insert(0, new ListItem(" -- Seleccione -- ", "-1"));
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void ddlUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarDatos(int.Parse(ddlUsuario.SelectedValue.ToString()));
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet2();

                SqlCommand cmd = new SqlCommand("usp_eliminaUsuario", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@idUsuario", int.Parse(ddlUsuario.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Login", txtLogin.Text.ToString());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToString());
                cmd.Parameters.AddWithValue("@AreaId", int.Parse(txtArea.Text.ToString()));

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "El usuario " + ddlUsuario.SelectedItem.ToString() + "fue eliminado exitosamente.");
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }

        protected void CargarDatos(int ID)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet2();

                SqlCommand cmd = new SqlCommand("usp_buscarUsuarioxID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idUsuario", ID);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    txtLogin.Text = reader["login"].ToString();
                    txtEmail.Text = reader["email"].ToString();
                    txtArea.Text = reader["AreaNombre"].ToString();
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.error, "Error!!!", ex.Message);
            }
        }
    }
}