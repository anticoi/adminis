﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMantenedorPermiso.ascx.cs" Inherits="ControlUsuario.Controles.ucPermiso" %>

        <asp:HiddenField ID="hfIdPermiso" runat="server" />
        <asp:HiddenField ID="hfIdSistema" runat="server" />


            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblNombreElemento" runat="server" AssociatedControlID="txtNombreElemento" 
                            Text="Nombre del elemento: " />
                    </td>
                    <td>
                        <asp:TextBox ID="txtNombreElemento" runat="server"></asp:TextBox>
                            <asp:Button ID="btnBuscar" runat="server" CssClass="boton" 
                            OnClick="btnBuscar_Click" Text="Buscar" />
                    </td>
                    </tr>
            </table>
                        

         <asp:UpdatePanel ID="upMensaje" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                         <asp:Label ID="lblTipoPermiso" runat="server" AssociatedControlID="txtTipoPermiso" 
                                Text="Tipo de permiso: " />
                            <asp:TextBox ID="txtTipoPermiso" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="btnAgregar" runat="server" CssClass="boton" 
                                Text="Agregar" onclick="btnAgregar_Click" />
                        &nbsp;<asp:Button ID="btnCancelar" runat="server" CssClass="boton" 
                                OnClick="btnCancelar_Click" Text="Cancelar" />
                        </td>
                    </tr>
                    <tr>
                                    <td colspan="2" style="text-align:left; ">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="textoAlerta"></asp:Label>    
                         
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAgregar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnCancelar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>


            <table>
                <tr>
                    <th colspan="2">
                        
                    </th>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="upPermisos" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvPermisos" runat="server" AutoGenerateColumns="False" 
                                    CssClass="mGrid" >
                                    <Columns>
                                        <asp:BoundField DataField="nombreElemento" HeaderText="Nombre elemento" />
                                        <asp:BoundField DataField="tipoPermiso" HeaderText="Tipo permiso" />
                                        <asp:BoundField DataField="sistema" HeaderText="Sistema" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnEliminar" runat="server" 
                                                    CommandArgument='<%# Eval("idPermiso") %>' ImageUrl="~/imagenes/deleteGrid.png" 
                                                     />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" />
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnAgregar" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnCancelar" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>


