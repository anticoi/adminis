﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNotificacion.ascx.cs" Inherits="ControlUsuario.Controles.ucNotificacion" %>

<div class="notificacion" style="display:none">
    <h3><asp:Label runat="server" ID="lblTitulo"></asp:Label></h3>
    <p><asp:Label runat="server" ID="lblMensaje"></asp:Label></p>
</div>
