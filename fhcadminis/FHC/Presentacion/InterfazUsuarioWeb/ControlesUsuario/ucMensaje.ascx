﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMensaje.ascx.cs" Inherits="ControlUsuario.Controles.ucMensaje" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script language="javascript" type="text/javascript">
    /* On page load */
    function pageLoad() {
        var modalPopup = $find("mpeMensaje");
        modalPopup.add_shown(OnPopupShow);
    }

    /* Set the focus to the correct control */
    function OnPopupShow() {
        var tb = $get('<%= btnAceptar.ClientID %>');
        tb.focus();
    } 


</script>


<asp:ModalPopupExtender BehaviorID="mpeMensaje" ID="mpeMsg" runat="server" TargetControlID="btnPopup"
                            PopupControlID="pnlMensaje" BackgroundCssClass="fondoAplicacion" CancelControlID="btnAceptar">
    </asp:ModalPopupExtender>
<asp:Panel runat="server" ID="pnlMensaje" style="display:none"   CssClass="msgbox">
<table>
    
<tr>
<th align="center">
    <div style="float:left; position:absolute; margin:0; top:0px ; left:0px; height:68px;"><asp:Image style="height:inherit" ID="imgIcon" runat="server" ImageUrl="~/imagenes/alert.png" /></div>
    <table style="width:100%;">
    <tr>
    <td style="padding-left:60px"><span><asp:Label runat="server" ID="lblTitulo"></asp:Label></span></td>
    </tr>
</table>
    
    
</th>
</tr>
<tr>
<td><p>
    <asp:Label ID="lblMensaje" runat="server"></asp:Label>
    </p>
    <asp:HiddenField ID="hfConcepto" runat="server" />
    </td>
</tr>
<tr>
<td  style="text-align:center">
    <asp:Button runat="server"  ID="btnAceptar" Text="Aceptar" CssClass="btnAceptarMsgbox" onclick="btnAceptar_Click"   />
    <asp:Button ID="btnPopup" runat="server" Height="16px" Width="16px" style="visibility:hidden" />
    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btnCancelarMsgbox" />
</td>
</tr>
</table>

</asp:Panel>