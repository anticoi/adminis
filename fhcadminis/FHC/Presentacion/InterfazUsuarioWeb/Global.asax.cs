﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Configuration; 
using TGENESIS.Framework.Utilidades;
using TGENESIS.FHC.EntidadesNegocio;

namespace TGENESIS.FHC.InterfazUsuarioWeb
{
    public class Global : System.Web.HttpApplication
    {


        private static Autenticacion datosUsuario;


        private string WS_ERRORES_SISTEMA = String.Format("{0}", ConfigurationManager.AppSettings["wsErroresSistema"]);

        public static Autenticacion DatosUsuario
        {
            get { return Serializador.Deserializar<Autenticacion>(Sesiones.Glb_autenticacion); }
            set { datosUsuario = value; }
        }

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            //Session["~Err"] = ex;
            string paginaAnterior = Request.ServerVariables["HTTP_REFERER"];
            string url = Request.ServerVariables["URL"];
            string usuario = "";
            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');
                usuario = Context.User.Identity.Name;
                
                
                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Context.User.Identity.Name);

                Mail.EnviarMail(emailDe, to, " - Error", ex.Message + "\n" + ex.InnerException + "\n\n Página Actual:" + url + "\n Página Anterior:" + paginaAnterior + "\n\n" + Request.ServerVariables["ALL_HTTP"], usuario, "", userMail, passMail);

            }
            catch (Exception errEx)
            {
                throw new Exception(errEx.Message);
            }
            finally
            {
                //LogEvents.EscribirLog(ex.Message + "\n" + ex.InnerException, usuario, HttpContext.Current.Request.Url.ToString());
                //Server.ClearError();
                //Server.Transfer("~/error.htm", false);
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}