﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;
using TGENESIS.FHC.EntidadesNegocio;

namespace InterfazUsuarioWeb.Contratos
{
    public partial class wfContratoSinRendicion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                //string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 20);

                string NombreMenu = "Contrato sin Rendición";

                lTitulo.Text = NombreMenu.ToString();

                cargarcombo();
                ListarTipoWorkFlow();

            }

        }


        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_VerContratosSinRendicion", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FechaInicio", txtFechaDesde.Text);
                cmd.Parameters.AddWithValue("@FechaTermino", txtFechaHasta.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                grvCSRE.DataSource = dt;
                grvCSRE.DataBind();
            }
            catch (Exception)
            {
            }
        }

        protected void cargarcombo()
        {

            ConexionSQL cc1 = new ConexionSQL();
            SqlConnection cn1 = cc1.AbrirConexionIntranet2();
            SqlCommand cmd1 = new SqlCommand("Usp_ListarAsesoresIntegrales", cn1);
            cmd1.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);

            ddlbusuarioasignado.DataSource = dt1;
            ddlbusuarioasignado.DataValueField = "login";
            ddlbusuarioasignado.DataTextField = "nombre";
            ddlbusuarioasignado.DataBind();
            ddlbusuarioasignado.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));


        }

        protected void ListarTipoWorkFlow()
        {
            try
            {
                List<TipoWorkFlow> twf = NegocioTipoWorkFlow.Instancia.Listar();

                ddlbtipoworkflow.DataSource = twf;
                ddlbtipoworkflow.DataValueField = "TipoWorkFlowId";
                ddlbtipoworkflow.DataTextField = "TipoWorkFDescripcion";
                ddlbtipoworkflow.DataBind();
                ddlbtipoworkflow.Items.Insert(0, new ListItem("-- Seleccione --", "-1"));
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void EnvioAviso(string Usuario, string UsuarioCrea, string Titulo, string Descripcion)
        {

            try
            {
                AppSettingsReader read = new AppSettingsReader();
                string userMail = read.GetValue("userMail", typeof(string)).ToString();
                string passMail = read.GetValue("passMail", typeof(string)).ToString();
                string emailDe = read.GetValue("emailErrorFrom", typeof(string)).ToString();
                //string[] to = read.GetValue("emailErrorTo", typeof(string)).ToString().Split(';');

                ControlUsuario.EntidadesNegocio.Usuario oUsuario = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(UsuarioCrea);
                ControlUsuario.EntidadesNegocio.Usuario oUsuarioCorreo = ControlUsuario.ComponentesNegocio.NegocioUsuario.Instancia.BuscaDatosUsuario(Usuario);

                string[] to = new string[1];
                to[0] = oUsuarioCorreo.email;

                Mail.EnviarMail(emailDe, to, "" + Titulo, " - El Contrato N° " + Descripcion + " se encuentra sin recibo \n" + " El usuario " + oUsuario.nombre, Usuario, "", userMail, passMail);
                //Mail.EnviarMail(emailDe,to,"Adjunto","Archivo Adjunto","NN","prueba de envio",userMail, passMail,
            }
            catch (Exception errEx)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", errEx.Message);
            }

        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                int Validar = ValidarDatos();

                if (Validar == 0)
                    return;

                WorkFlow cwf = new WorkFlow();

                int contrato = 0;
                if (txtContrato.Text != "")
                    contrato = int.Parse(txtContrato.Text);

                cwf.WorkFlowContrato = contrato;
                cwf.WorkFlowTitulo = txttitulo.Text;
                cwf.WorkFlowObservacion = txtobservacion.Text;
                cwf.WorkFlowUsuarioAsignado = ddlbusuarioasignado.SelectedValue.ToString();
                cwf.TipoWorkFlowId = int.Parse(ddlbtipoworkflow.SelectedValue.ToString());
                cwf.EstadoWorkFlowId = 1;
                cwf.WorkFlowUsuarioIngreso = Context.User.Identity.Name;
                cwf.WorkFlowUsuarioModificacion = Context.User.Identity.Name;

                NegocioWorkFlow.Instancia.Insertar(cwf);

                EnvioAviso(ddlbusuarioasignado.SelectedValue.ToString(), Context.User.Identity.Name, txttitulo.Text, txtobservacion.Text);
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Actualización", "La tarea se creo con éxito.");

                LimpiarPantalla();

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);

            }
        }

        protected int ValidarDatos()
        {
            int valor = 0;
            if (ddlbusuarioasignado.SelectedValue.ToString() == "-1")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información!!!", "Debe Seleccionar el Usuario a quien se le asignara la tarea.");
                return valor;
            }

            valor = 1;
            return valor;
        }

        protected void LimpiarPantalla()
        {
            ddlbtipoworkflow.SelectedValue = "-1";
            ddlbtipoworkflow.SelectedValue = "-1";
            txtobservacion.Text = "";
        }

        protected void txtContrato_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlbusuarioasignado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlbtipoworkflow_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grvCSRE_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtContrato.Text = grvCSRE.Rows[grvCSRE.SelectedIndex].Cells[1].Text;
        }
    }
}