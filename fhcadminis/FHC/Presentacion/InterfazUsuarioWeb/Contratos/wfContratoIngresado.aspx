﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTgenesisFHC.Master" AutoEventWireup="true" CodeBehind="wfContratoIngresado.aspx.cs" Inherits="InterfazUsuarioWeb.Contratos.wfContratoIngresado" %>
<%@ Register src="../ControlesUsuario/ucMensaje.ascx" tagname="ucMensaje" tagprefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function ValidarDato(source, arguments) {
            if ($.trim(arguments.Value) == "") {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FF5959";
                arguments.IsValid = false;
            }
            else {
                var txt = source.controltovalidate;
                document.getElementById(txt).style.backgroundColor = "#FFFFFF";
                arguments.IsValid = true;
            }
        }


        $(function () {

            $("#<%= txtFechaDesde.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

            $("#<%= txtFechaHasta.ClientID %>").datepicker({
                showSecond: true,
                timeFormat: 'hh:mm:ss',
                dateFormat: 'dd-mm-yy',
                stepHour: 1,
                stepMinute: 5,
                minDate: new Date(2011, 08, 01, 8, 30),
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                stepSecond: 5,
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                daythNamesShort: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
            });

        });

        
      </script>


    <style type="text/css">
        .style1
        {
            height: 45px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table width="600px" style="height:300px;">
    <tr>
        <td valign="top"  align="center">
            <table width="80%">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:Label  ID="lblfechainicio" runat="server" Text="Inicio"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaDesde" runat="server" data-watermark="•" 
                                        ontextchanged="txtFechaDesde_TextChanged"></asp:TextBox>
                                </td>
                                <td align="center">
                                    <asp:Label  ID="lblfechatermino" runat="server" Text="Termino"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaHasta" runat="server" data-watermark="•" 
                                        ontextchanged="txtFechaHasta_TextChanged"></asp:TextBox>
                                </td>

                                <td>
                                    <table>
                                        <tr>
                                            <td class="style1" colspan="2" align="right">
                                                <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar" 
                                                    class="button small blue" Width="80px" Height="25px" 
                                                    onclick="btnFiltrar_Click" />   
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" colspan="6">
                        <div id="divubicaciones" runat="server" style="width:900px; overflow: scroll; height:250px;">
                            <asp:GridView ID="grvCING" runat="server" CssClass="mGrid" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" GridLines="None" >
                                <Columns>
                                    <asp:BoundField DataField="OPER_NUM"  HeaderText="Contrato" >
                                        <HeaderStyle Width="80px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                            

                                    <asp:BoundField DataField="OPER_FECH"  HeaderText="Fecha Contrato" >
                                        <HeaderStyle Width="120px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                            

                                    <asp:BoundField DataField="Asesor"  HeaderText="Asesor Integral" >
                                        <HeaderStyle Width="200px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>                                    

                                    <asp:BoundField DataField="SUCU_DESC"  HeaderText="Sucursal" >
                                        <HeaderStyle Width="200px" />
                                            <ItemStyle CssClass="ItemGrilla" Width="100px"></ItemStyle> 
                                    </asp:BoundField>            
                                </Columns>

                                <EmptyDataTemplate>
                                    <i>{No hay datos que mostrar}</i>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <uc1:ucmensaje ID="ucMensaje1" runat="server" />
        </td>
    </tr>
</table>
</asp:Content>
