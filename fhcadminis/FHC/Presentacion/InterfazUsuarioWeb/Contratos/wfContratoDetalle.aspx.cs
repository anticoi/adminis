﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using TGENESIS.FHC.ComponentesNegocio;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Net;
using TGENESIS.Framework.Utilidades;
using System.Text.RegularExpressions;

namespace InterfazUsuarioWeb.Contratos
{
    public partial class wfContratoDetalle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

 
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 12);

                lTitulo.Text = NombreMenu.ToString();

                CargarCombo();

            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (txtContrato.Text != "")
            {
                BuscarArchivos(txtContrato.Text.ToString());
                BuscarCaratula(txtContrato.Text.ToString());
                BuscarNotasCredito(txtContrato.Text.ToString());
                BuscarNotasDebito(txtContrato.Text.ToString());
            }
            else
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "Debe ingresar un numero de contrato.");
            }
        }


        protected void BuscarArchivos(string Contrato)
        {

            try
            {

                                DataTable dt = new DataTable();
                                dt.Columns.Add("Nombre");
                                dt.Columns.Add("Icono");
                                dt.Columns.Add("Ruta");


                                //DirectoryInfo dirInfo = new DirectoryInfo(@"E:\Busqueda\Cheque_Custodia\nomina de cheques\CHEQUES C.M\");
                                DirectoryInfo dirInfo = new DirectoryInfo(@"\\172.110.10.5\Cheque_Custodia\nomina de cheques\CHEQUES C.M\");
                                System.IO.FileInfo[] fileNames = dirInfo.GetFiles(@"" + Contrato + ".*");
             
                                foreach (System.IO.FileInfo fi in fileNames)
                                {
                                            DataRow row = dt.NewRow() ;
                                            row[0] = fi.Name;
                                            row[1] = fi.Name;
                                            row[2] = "Cheque"; 
                                            dt.Rows.Add(row) ;
                                }

                                //DirectoryInfo dirInfoDos = new DirectoryInfo(@"E:\Busqueda\Factura\");
                                DirectoryInfo dirInfoDos = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                                System.IO.FileInfo[] fileNamesDos = dirInfoDos.GetFiles(@"" + Contrato + ".*");

                                foreach (System.IO.FileInfo fiDos in fileNamesDos)
                                {
                                    DataRow row = dt.NewRow();
                                    row[0] = fiDos.Name;
                                    row[1] = fiDos.Name;
                                    row[2] = "Factura";
                                    dt.Rows.Add(row);
                                }

                                #region "Comentado"
                                #region "Busqueda Factura -1"
                                //DirectoryInfo dirInfoDosUno = new DirectoryInfo(@"E:\Busqueda\Factura\");
                                DirectoryInfo dirInfoDosUno = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                                System.IO.FileInfo[] fileNameDosUno = dirInfoDosUno.GetFiles(@"" + Contrato + "-1" + ".*");

                                foreach (System.IO.FileInfo fiDosUno in fileNameDosUno)
                                {
                                    DataRow row = dt.NewRow();
                                    row[0] = fiDosUno.Name;
                                    row[1] = fiDosUno.Name;
                                    row[2] = "Factura-1";
                                    dt.Rows.Add(row);
                                }
                                #endregion

                                #region "Busqueda Factura -2"
                                //DirectoryInfo dirInfoDosDos = new DirectoryInfo(@"E:\Busqueda\Factura\");
                                DirectoryInfo dirInfoDosDos = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                                System.IO.FileInfo[] fileNameDosDos = dirInfoDosDos.GetFiles(@"" + Contrato + "-2" + ".*");

                                foreach (System.IO.FileInfo fiDosDos in fileNameDosDos)
                                {
                                    DataRow row = dt.NewRow();
                                    row[0] = fiDosDos.Name;
                                    row[1] = fiDosDos.Name;
                                    row[2] = "Factura-2";
                                    dt.Rows.Add(row);
                                }
                                //Hacer desde dos hasta 5
                                #endregion

                                #region "Busqueda Factura -3"
                                //DirectoryInfo dirInfoDosTres = new DirectoryInfo(@"E:\Busqueda\Factura\");
                                DirectoryInfo dirInfoDosTres = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                                System.IO.FileInfo[] fileNameDosTres = dirInfoDosTres.GetFiles(@"" + Contrato + "-3" + ".*");

                                foreach (System.IO.FileInfo fiDosTres in fileNameDosTres)
                                {
                                    DataRow row = dt.NewRow();
                                    row[0] = fiDosTres.Name;
                                    row[1] = fiDosTres.Name;
                                    row[2] = "Factura-3";
                                    dt.Rows.Add(row);
                                }
                                #endregion

                                #region "Busqueda Factura -4"
                                //DirectoryInfo dirInfoDosCuatro = new DirectoryInfo(@"E:\Busqueda\Factura\");
                                DirectoryInfo dirInfoDosCuatro = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                                System.IO.FileInfo[] fileNameDosCuatro = dirInfoDosCuatro.GetFiles(@"" + Contrato + "-4" + ".*");

                                foreach (System.IO.FileInfo fiDosCuatro in fileNameDosCuatro)
                                {
                                    DataRow row = dt.NewRow();
                                    row[0] = fiDosCuatro.Name;
                                    row[1] = fiDosCuatro.Name;
                                    row[2] = "Factura-4";
                                    dt.Rows.Add(row);
                                }
                                #endregion
                                #endregion


                                grvarchivos.DataSource = dt;
                                grvarchivos.DataBind();

                                //if (dt.Rows.Count < 1)
                                //{
                                    //ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "No se encontraron resultados con los criterios de busqueda ingresados.");
                                //}

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }

        }

        protected void BuscarCaratula(string Contrato)
        {
            try
            {

                                DataTable dt = new DataTable();
                                dt.Columns.Add("Nombre");
                                dt.Columns.Add("Icono");
                                dt.Columns.Add("Ruta");



                                //DirectoryInfo dirInfoTres = new DirectoryInfo(@"E:\Busqueda\caratula_contrato\" + ddlAnoCaratula.SelectedValue.ToString() + @"\" + Contrato + @"\");
                                //DirectoryInfo dirInfoTres = new DirectoryInfo(@"\\172.110.10.5\caratulas_contrato\2015\" + Contrato + @"\");
                                DirectoryInfo dirInfoTres = new DirectoryInfo(@"\\172.110.10.5\caratulas_contrato\" + ddlAnoCaratula.SelectedValue.ToString() + @"\" + Contrato + @"\");
                                //se debe especificar el año para busqueda de caratula, para  mejorar la busqueda...
                                //basta con agregar cbo con año y unirlo a la busqueda

                                if (dirInfoTres.Exists)
                                {
                                        System.IO.FileInfo[] fileNamesTres = dirInfoTres.GetFiles(@"*.pdf");

                                        foreach (System.IO.FileInfo fiTres in fileNamesTres)
                                        {
                                            DataRow row = dt.NewRow();
                                            row[0] = fiTres.Name;
                                            row[1] = fiTres.Name;
                                            row[2] = "Caratula";
                                            dt.Rows.Add(row);
                                        }
                                }

                                grvarchivos2.DataSource = dt;
                                grvarchivos2.DataBind();

                                //if (dt.Rows.Count < 1)
                                //{
                                    //ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "No se encontraron caratulas de contratos con los criterios de busqueda ingresados.");
                                //}

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }


        }

        protected void BuscarNotasCredito(string Contrato)
        {
            try
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("Nombre");
                dt.Columns.Add("Icono");
                dt.Columns.Add("Ruta");

                //DirectoryInfo dirInfoCuatro = new DirectoryInfo(@"E:\Busqueda\Factura\");
                DirectoryInfo dirInfoCuatro = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                System.IO.FileInfo[] fileNameCuatro = dirInfoCuatro.GetFiles(@"NC" + Contrato + ".*");


                    foreach (System.IO.FileInfo fiCuatro in fileNameCuatro)
                    {
                        DataRow row = dt.NewRow();
                        row[0] = fiCuatro.Name;
                        row[1] = fiCuatro.Name;
                        row[2] = "Nota de Credito";
                        dt.Rows.Add(row);
                    }

                grvarchivos3.DataSource = dt;
                grvarchivos3.DataBind();

                //if (dt.Rows.Count < 1)
                //{
                    //ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "No se encontraron Notas de Crédito con los criterios de busqueda ingresados.");
                //}

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }
        }

        protected void BuscarNotasDebito(string Contrato)
        {
            try
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("Nombre");
                dt.Columns.Add("Icono");
                dt.Columns.Add("Ruta");

                //DirectoryInfo dirInfoCinco = new DirectoryInfo(@"E:\Busqueda\Factura\");
                DirectoryInfo dirInfoCinco = new DirectoryInfo(@"\\172.110.10.5\Facturas\");
                System.IO.FileInfo[] fileNameCinco = dirInfoCinco.GetFiles(@"ND" + Contrato + ".*");


                foreach (System.IO.FileInfo fiCinco in fileNameCinco)
                {
                    DataRow row = dt.NewRow();
                    row[0] = fiCinco.Name;
                    row[1] = fiCinco.Name;
                    row[2] = "Nota de Debito";
                    dt.Rows.Add(row);
                }

                grvarchivos4.DataSource = dt;
                grvarchivos4.DataBind();

                //if (dt.Rows.Count < 1)
                //{
                    //ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", "No se encontraron Notas de Debito con los criterios de busqueda ingresados.");
                //}

            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }
        }



        protected void grvarchivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
 
                                if (e.CommandName == "verarchivo")
                                {
                                    string TipoDocumento = e.CommandArgument.ToString();

                                    foreach (GridViewRow gvRow in grvarchivos.Rows)
                                    {

                                        if (gvRow.RowType == DataControlRowType.DataRow)
                                        {

                                                if (e.CommandArgument.ToString() == TipoDocumento)
                                                {

                                                    string NomArchivo = gvRow.Cells[2].Text.ToString();
                                                    string Directorio = "";


                                                    if (TipoDocumento == "Cheque")
                                                    {
                                                        Directorio = "../cq/";
                                                        //Directorio = @"E:\Busqueda\Factura\";
                                                        hdfimprimir.Value = Directorio + NomArchivo;
                                                        ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                                        return;
                                                    }

                                                    if (TipoDocumento == "Factura")
                                                    {
                                                        Directorio = "../fc/";
                                                        //Directorio = @"E:\Busqueda\Factura\"; 
                                                        hdfimprimir.Value = Directorio + NomArchivo;
                                                        ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                                        return;
                                                    }

                                                    if (TipoDocumento == "Factura-1")
                                                    {
                                                        Directorio = "../fc/";
                                                        //Directorio = @"E:\Busqueda\Factura\";
                                                        string NomArchivoOK = "";
                                                        string ExtensionOK = "";

                                                        NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                        ExtensionOK = Path.GetExtension(NomArchivo);

                                                        hdfimprimir.Value = Directorio + NomArchivoOK + ExtensionOK;
                                                        ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                                        return;
                                                    }

                                                    if (TipoDocumento == "Factura-2")
                                                    {
                                                        Directorio = "../fc/";
                                                        //Directorio = @"E:\Busqueda\Factura\";
                                                        string NomArchivoOK = "";
                                                        string ExtensionOK = "";

                                                        NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                        ExtensionOK = Path.GetExtension(NomArchivo);

                                                        hdfimprimir.Value = Directorio + NomArchivoOK + ExtensionOK;
                                                        ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                                        return;
                                                    }

                                                    if (TipoDocumento == "Factura-3")
                                                    {
                                                        Directorio = "../fc/";
                                                        //Directorio = @"E:\Busqueda\Factura\";
                                                        string NomArchivoOK = "";
                                                        string ExtensionOK = "";

                                                        NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                        ExtensionOK = Path.GetExtension(NomArchivo);
                                                        
                                                        hdfimprimir.Value = Directorio + NomArchivoOK + ExtensionOK;
                                                        ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                                        return;
                                                    }

                                                    if (TipoDocumento == "Factura-4")
                                                    {
                                                        Directorio = "../fc/";
                                                        //Directorio = @"E:\Busqueda\Factura\";
                                                        string NomArchivoOK = "";
                                                        string ExtensionOK = "";

                                                        NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                        ExtensionOK = Path.GetExtension(NomArchivo);

                                                        hdfimprimir.Value = Directorio + NomArchivoOK + ExtensionOK;
                                                        ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                                        return;
                                                    }
                                                }
                                        }
                                    }
                                    return;  
                                }

                                if (e.CommandName == "enviararchivo")
                                {
                                    string TipoDocumento = e.CommandArgument.ToString();

                                                foreach (GridViewRow gvRow in grvarchivos.Rows)
                                                {
                                                    if (gvRow.RowType == DataControlRowType.DataRow)
                                                    {
                                                        //string NomArchivo = gvRow.Cells[2].Text.ToString();

                                                        //if (e.CommandArgument.ToString() == gvRow.Cells[2].Text.ToString())
                                                        if (e.CommandArgument.ToString() == TipoDocumento)
                                                        {
                                                            string NomArchivo = gvRow.Cells[2].Text.ToString();

                                                            string Directorio = "";
                                                            string DireServer = "";

                                                            //if (gvRow.Cells[3].Text.ToString() == "Contrato")
                                                            //    Directorio = @"E:\Busqueda\Cheque_Custodia\nomina de cheques\CHEQUES C.M\";

                                                            if (TipoDocumento == "Cheque")
                                                            {
                                                                Directorio = "../cq/";
                                                                DireServer = Server.MapPath("~/cq/");
                                                                //Directorio = @"E:\Busqueda\Cheque_Custodia\nomina de cheques\CHEQUES C.M\";

                                                                lblrutaadjunto.Text = Directorio + NomArchivo;
                                                                lblrutaadjunto2.Text = DireServer + NomArchivo;

                                                                txtasunto.Text = "";
                                                                txtbody.Text = "";
                                                                ddlCorreo.Text = "-1";
                                                                mpepanel.Show();

                                                                return;
                                                            }

                                                            //if (gvRow.Cells[3].Text.ToString() == "Factura")
                                                            //    Directorio = @"E:\Busqueda\Factura\";


                                                            if (TipoDocumento == "Factura")
                                                            {
                                                                Directorio = "../fc/";
                                                                DireServer = Server.MapPath("~/fc/");
                                                                //Directorio = @"E:\Busqueda\Factura\";

                                                                lblrutaadjunto.Text = Directorio + NomArchivo;
                                                                lblrutaadjunto2.Text = DireServer + NomArchivo;

                                                                txtasunto.Text = "";
                                                                txtbody.Text = "";
                                                                ddlCorreo.Text = "-1";
                                                                mpepanel.Show();

                                                                return;
                                                            }

                                                            if (TipoDocumento == "Factura-1")
                                                            {
                                                                Directorio = "../fc/";
                                                                DireServer = Server.MapPath("~/fc/");
                                                                //Directorio = @"E:\Busqueda\Factura\";

                                                                string NomArchivoOK = "";
                                                                string ExtensionOK = "";

                                                                NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                                ExtensionOK = Path.GetExtension(NomArchivo);

                                                                lblrutaadjunto.Text = Directorio + NomArchivoOK + ExtensionOK;
                                                                lblrutaadjunto2.Text = DireServer + NomArchivoOK + ExtensionOK;

                                                                txtasunto.Text = "";
                                                                txtbody.Text = "";
                                                                ddlCorreo.Text = "-1";
                                                                mpepanel.Show();

                                                                return;
                                                            }

                                                            if (TipoDocumento == "Factura-2")
                                                            {
                                                                Directorio = "../fc/";
                                                                DireServer = Server.MapPath("~/fc/");
                                                                //Directorio = @"E:\Busqueda\Factura\";

                                                                string NomArchivoOK = "";
                                                                string ExtensionOK = "";

                                                                NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                                ExtensionOK = Path.GetExtension(NomArchivo);

                                                                lblrutaadjunto.Text = Directorio + NomArchivoOK + ExtensionOK;
                                                                lblrutaadjunto2.Text = DireServer + NomArchivoOK + ExtensionOK;

                                                                txtasunto.Text = "";
                                                                txtbody.Text = "";
                                                                ddlCorreo.Text = "-1";
                                                                mpepanel.Show();

                                                                return;
                                                            }

                                                            if (TipoDocumento == "Factura-3")
                                                            {
                                                                Directorio = "../fc/";
                                                                DireServer = Server.MapPath("~/fc/");
                                                                //Directorio = @"E:\Busqueda\Factura\";

                                                                string NomArchivoOK = "";
                                                                string ExtensionOK = "";

                                                                NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                                ExtensionOK = Path.GetExtension(NomArchivo);

                                                                lblrutaadjunto.Text = Directorio + NomArchivoOK + ExtensionOK;
                                                                lblrutaadjunto2.Text = DireServer + NomArchivoOK + ExtensionOK;

                                                                txtasunto.Text = "";
                                                                txtbody.Text = "";
                                                                ddlCorreo.Text = "-1";
                                                                mpepanel.Show();

                                                                return;
                                                            }

                                                            if (TipoDocumento == "Factura-4")
                                                            {
                                                                Directorio = "../fc/";
                                                                DireServer = Server.MapPath("~/fc/");
                                                                //Directorio = @"E:\Busqueda\Factura\";

                                                                string NomArchivoOK = "";
                                                                string ExtensionOK = "";

                                                                NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                                                ExtensionOK = Path.GetExtension(NomArchivo);

                                                                lblrutaadjunto.Text = Directorio + NomArchivoOK + ExtensionOK;
                                                                lblrutaadjunto2.Text = DireServer + NomArchivoOK + ExtensionOK;

                                                                txtasunto.Text = "";
                                                                txtbody.Text = "";
                                                                ddlCorreo.Text = "-1";
                                                                mpepanel.Show();

                                                                return;
                                                            }

                                                            //if (gvRow.Cells[3].Text.ToString() == "Caratula")
                                                            //    Directorio = @"E:\Busqueda\caratula_contrato\2014\" + txtContrato.Text + "\"";


                                                            //if (TipoDocumento == "Caratula")
                                                            //{
                                                            //    Directorio = "../cc2014/" + txtContrato.Text + "/";
                                                            //    DireServer = Server.MapPath("~/cc2014/" + txtContrato.Text + "/");
                                                            //}


                                                            //lblrutaadjunto.Text = Directorio + gvRow.Cells[2].Text.ToString();
                                                            //lblrutaadjunto2.Text = DireServer + gvRow.Cells[2].Text.ToString();

                                                            //lblrutaadjunto.Text = Directorio + NomArchivo;
                                                            //lblrutaadjunto2.Text = DireServer + NomArchivo;
                                                             
                                                        }
                                                    }
                                                }
                                                //txtasunto.Text = "";
                                                //txtbody.Text = "";
                                                //ddlCorreo.Text = "-1";
                                                //mpepanel.Show();
                                                //return;
                                }

           
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }            
            
        }

        protected void grvarchivos2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "verarchivo2")
                {

                    foreach (GridViewRow gvRow in grvarchivos2.Rows)
                    {

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (e.CommandArgument.ToString() == gvRow.Cells[2].Text.ToString())
                            {
                                string Directorio = "";

                                    //if (gvRow.Cells[3].Text.ToString() == "Caratula")
                                    //    Directorio = @"E:\Busqueda\caratula_contrato\2014\" + txtContrato.Text + "\"";

                                    if (gvRow.Cells[3].Text.ToString() == "Caratula")
                                    {
                                        Directorio = "../cc/" + ddlAnoCaratula.SelectedValue.ToString() + "/" + txtContrato.Text + "/";
                                       //Directorio = @"E:\Busqueda\caratula_contrato\2014\" + txtContrato.Text + "\"";
                                    }

                                    hdfimprimir.Value = Directorio + gvRow.Cells[2].Text.ToString();
                                    ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                    return;
                            }
                        }
                    }
                    return;
                }

                if (e.CommandName == "enviararchivo2")
                {

                    foreach (GridViewRow gvRow in grvarchivos2.Rows)
                    {
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (e.CommandArgument.ToString() == gvRow.Cells[2].Text.ToString())
                            {

                                string NomArchivo = gvRow.Cells[2].Text.ToString();
                                string Directorio = "";
                                string DireServer = "";

                                if (gvRow.Cells[3].Text.ToString() == "Caratula")
                                {
                                    Directorio = "../cc/" + ddlAnoCaratula.SelectedValue.ToString() + "/" + txtContrato.Text + "/";
                                    DireServer = Server.MapPath("~/cc/" + ddlAnoCaratula.SelectedValue.ToString() + "/" + txtContrato.Text + "/");
                                    //Directorio = @"E:\Busqueda\caratula_contrato\2014\" + txtContrato.Text + "\"";
                                }


                                lblrutaadjunto.Text = Directorio + NomArchivo;
                                lblrutaadjunto2.Text = DireServer + NomArchivo;

                            }
                        }
                    }
                    txtasunto.Text = "";
                    txtbody.Text = "";
                    ddlCorreo.Text = "-1";
                    mpepanel.Show();
                }


            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            } 
        }

        protected void grvarchivos3_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "verarchivo3")
                {
                    //string TipoDocumento = e.CommandArgument.ToString();

                    foreach (GridViewRow gvRow in grvarchivos3.Rows)
                    {

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            //if (e.CommandArgument.ToString() == TipoDocumento)
                            if (e.CommandArgument.ToString() == gvRow.Cells[2].Text.ToString())
                            {

                                string NomArchivo = gvRow.Cells[2].Text.ToString();
                                string Directorio = "";


                                //if (TipoDocumento == "Nota de Debito")
                                if (gvRow.Cells[3].Text.ToString() == "Nota de Credito")
                                {
                                    Directorio = "../fc/";
                                    //Directorio = @"E:\Busqueda\Factura\";

                                    string NomArchivoOK = "";
                                    string ExtensionOK = "";

                                    NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                    ExtensionOK = Path.GetExtension(NomArchivo);

                                    hdfimprimir.Value = Directorio + NomArchivoOK + ExtensionOK;
                                    ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                    return;
                                }
                            }
                        }
                    }
                    return;
                }

                if (e.CommandName == "enviararchivo3")
                {
                    string TipoDocumento = e.CommandArgument.ToString();

                    foreach (GridViewRow gvRow in grvarchivos3.Rows)
                    {
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (gvRow.Cells[3].Text.ToString() == "Nota de Credito")
                            {
                                string NomArchivo = gvRow.Cells[2].Text.ToString();

                                string Directorio = "";
                                string DireServer = "";

                                //Revisar correccion
                                if (TipoDocumento == "Nota de Credito")
                                {
                                    Directorio = "../fc/";
                                    DireServer = Server.MapPath("~/fc/");
                                    //Directorio = @"E:\Busqueda\Factura\";

                                    string NomArchivoOK = "";
                                    string ExtensionOK = "";

                                    NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                    ExtensionOK = Path.GetExtension(NomArchivo);

                                    lblrutaadjunto.Text = Directorio + NomArchivoOK + ExtensionOK;
                                    lblrutaadjunto2.Text = DireServer + NomArchivoOK + ExtensionOK;

                                    txtasunto.Text = "";
                                    txtbody.Text = "";
                                    ddlCorreo.Text = "-1";
                                    mpepanel.Show();

                                    return;
                                }
                            }
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }
        }

        protected void grvarchivos4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "verarchivo4")
                {
                    //string TipoDocumento = e.CommandArgument.ToString();

                    foreach (GridViewRow gvRow in grvarchivos4.Rows)
                    {

                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            //if (e.CommandArgument.ToString() == TipoDocumento)
                            if (e.CommandArgument.ToString() == gvRow.Cells[2].Text.ToString())
                            {

                                string NomArchivo = gvRow.Cells[2].Text.ToString();
                                string Directorio = "";


                                //if (TipoDocumento == "Nota de Debito")
                                if (gvRow.Cells[3].Text.ToString() == "Nota de Debito") 
                                {
                                    Directorio = "../fc/";
                                    //Directorio = @"E:\Busqueda\Factura\";

                                    string NomArchivoOK = "";
                                    string ExtensionOK = "";

                                    NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                    ExtensionOK = Path.GetExtension(NomArchivo);

                                    hdfimprimir.Value = Directorio + NomArchivo + ExtensionOK;
                                    ClientScript.RegisterStartupScript(GetType(), "bajararchivo", "Imprimir();", true);
                                    return;
                                }
                            }
                        }
                    }
                    return;
                }

                if (e.CommandName == "enviararchivo4")
                {
                    string TipoDocumento = e.CommandArgument.ToString();

                    foreach (GridViewRow gvRow in grvarchivos4.Rows)
                    {
                        if (gvRow.RowType == DataControlRowType.DataRow)
                        {

                            if (e.CommandArgument.ToString() == TipoDocumento)
                            {
                                string NomArchivo = gvRow.Cells[2].Text.ToString();

                                string Directorio = "";
                                string DireServer = "";

                                //Revisar correccion
                                if (gvRow.Cells[3].Text.ToString() == "Nota de Debito")
                                {
                                    Directorio = "../fc/";
                                    DireServer = Server.MapPath("~/fc/");
                                    //Directorio = @"E:\Busqueda\Factura\";

                                    string NomArchivoOK = "";
                                    string ExtensionOK = "";

                                    NomArchivoOK = Path.GetFileNameWithoutExtension(NomArchivo);
                                    ExtensionOK = Path.GetExtension(NomArchivo);

                                    lblrutaadjunto.Text = Directorio + NomArchivoOK + ExtensionOK;
                                    lblrutaadjunto2.Text = DireServer + NomArchivoOK + ExtensionOK;

                                    txtasunto.Text = "";
                                    txtbody.Text = "";
                                    ddlCorreo.Text = "-1";
                                    mpepanel.Show();

                                    return;
                                }
                            }
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
            }
        }


        protected void grvarchivos_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridViewRow row = e.Row;
                ImageButton img = e.Row.FindControl("btnruta") as  ImageButton;
                string extencion = img.CommandArgument.Substring(int.Parse(img.CommandArgument.Length.ToString()) - 3, 3).ToUpper(); 
                if (row.RowIndex > -1)
                {
                    if (extencion == "LSX")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";
                    
                    if (extencion == "XLS")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "PDF")
                        img.ImageUrl = "~/imagenes/pdf.jpg";

                    if (extencion == "DOC")
                        img.ImageUrl = "~/imagenes/work.jpg";

                    if (extencion == "JPG")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                    if (extencion == "BMP")
                        img.ImageUrl = "~/imagenes/jpg.jpg"; 
                                
                }
            }
        }

        protected void grvarchivos2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridViewRow row = e.Row;
                ImageButton img = e.Row.FindControl("btnruta") as ImageButton;
                string extencion = img.CommandArgument.Substring(int.Parse(img.CommandArgument.Length.ToString()) - 3, 3).ToUpper();
                if (row.RowIndex > -1)
                {
                    if (extencion == "LSX")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "XLS")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "PDF")
                        img.ImageUrl = "~/imagenes/pdf.jpg";

                    if (extencion == "DOC")
                        img.ImageUrl = "~/imagenes/work.jpg";

                    if (extencion == "JPG")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                    if (extencion == "BMP")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                }
            }
        }

        protected void grvarchivos3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridViewRow row = e.Row;
                ImageButton img = e.Row.FindControl("btnruta") as ImageButton;
                string extencion = img.CommandArgument.Substring(int.Parse(img.CommandArgument.Length.ToString()) - 3, 3).ToUpper();
                if (row.RowIndex > -1)
                {
                    if (extencion == "LSX")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "XLS")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "PDF")
                        img.ImageUrl = "~/imagenes/pdf.jpg";

                    if (extencion == "DOC")
                        img.ImageUrl = "~/imagenes/work.jpg";

                    if (extencion == "JPG")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                    if (extencion == "BMP")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                }
            }
        }

        protected void grvarchivos4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridViewRow row = e.Row;
                ImageButton img = e.Row.FindControl("btnruta") as ImageButton;
                string extencion = img.CommandArgument.Substring(int.Parse(img.CommandArgument.Length.ToString()) - 3, 3).ToUpper();
                if (row.RowIndex > -1)
                {
                    if (extencion == "LSX")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "XLS")
                        img.ImageUrl = "~/imagenes/xlsx.jpg";

                    if (extencion == "PDF")
                        img.ImageUrl = "~/imagenes/pdf.jpg";

                    if (extencion == "DOC")
                        img.ImageUrl = "~/imagenes/work.jpg";

                    if (extencion == "JPG")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                    if (extencion == "BMP")
                        img.ImageUrl = "~/imagenes/jpg.jpg";

                }
            }
        }


        protected void btnenviarcorre_Click(object sender, EventArgs e)
        {
            enviar_correo(lblrutaadjunto2.Text.Trim());
        }

        protected void grvarchivos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grvarchivos2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grvarchivos3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grvarchivos4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected bool agregar_archivosadjuntos(System.Net.Mail.MailMessage email, string texto)
        {
            string file;
            string NombreDoc;

            NombreDoc = txtContrato.Text;

            string[] archivo = null;
            System.Net.Mail.Attachment adjunto;

            try
            {
                texto = texto.Replace(";", ",");
                archivo = texto.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < archivo.Length; i++)
                {
                    file = archivo[i].ToString().Trim();
                    adjunto = new System.Net.Mail.Attachment(file);

                    if (archivo.Length > 1)
                    {
                        adjunto.Name = NombreDoc + ".pdf";
                    }
                    else
                    {
                        adjunto.Name = NombreDoc + ".pdf";
                    }
                    email.Attachments.Add(adjunto);
                }
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);
                return false;
            }
            return true;
        }

        protected bool agregar_destinatarios(System.Net.Mail.MailMessage email, String texto)
        {

            bool retorno = false;

            string[] para = null;



            //        String _pattern = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";

            String _pattern = "^([\\w]+)(([-\\.][\\w]+)?)*@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";

            Regex _TagParser = new Regex(_pattern);



            texto = texto.Replace(";", ",");

            para = texto.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);



            for (int i = 0; i < para.Length; i++)
            {



                if (_TagParser.IsMatch(para[i].ToString().Trim()))
                {

                    email.To.Add(para[i].ToString().Trim());

                    retorno = true;

                }

                else
                {
                    //error_enviar_mail = "El correo electronico: " + para[i].ToString().Trim() + " no es valido";

                    retorno = false;

                    break;

                }

            }



            return retorno;



        }

        protected bool enviar_correo(string ArchivoAdjunto)
        {

            /// RecuperaMail devuelve el mail del usuario

            string emailUsuario = ddlCorreo.SelectedValue.ToString();



            /// Enviar El Correo



            System.Net.Mail.MailMessage email = new System.Net.Mail.MailMessage();

            System.Net.Mail.MailAddress reply = new System.Net.Mail.MailAddress(emailUsuario);



            // Establecer el Asunto

            email.Subject = txtasunto.Text;
            email.IsBodyHtml = true;
            email.ReplyTo = reply;



            // Crear el Cuerpo del Mensaje

            email.Body = txtbody.Text;


            // Agregar Destinatarios

            if (agregar_destinatarios(email, emailUsuario))
            {

                // Adjuntar Archivo

                if (ArchivoAdjunto.Trim() != string.Empty)
                {

                    if (!agregar_archivosadjuntos(email, ArchivoAdjunto))
                    {

                        return false;

                    }

                }


                // Crear el objeto Correo

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                // Enviar el Mail

                try
                {
                    smtp.Send(email);

                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Correo Enviado", "El archivo adjunto fue enviado de manera satisfactoria.");

                    return true;
                }

                catch (Exception ex)
                {
                    ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información", ex.Message);

                }
            }
            return false;
        }

        private void CargarCombo()
        {
            try
            {
                ddlCorreo.DataSource = NegocioReporteria.Instancia.ListarUsuarios();
                ddlCorreo.DataValueField = "email";
                ddlCorreo.DataTextField = "nombre";
                ddlCorreo.DataBind();
                ddlCorreo.Items.Insert(0, new ListItem(" -- Seleccione --", "-1"));
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }
    }

}