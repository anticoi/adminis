﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;

namespace InterfazUsuarioWeb.Contratos
{
    public partial class wfContratoDescuento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 19);

                lTitulo.Text = NombreMenu.ToString();

            }

        }


        protected void CargarGrilla()
        {
            try
            {
                int Validar = ValidarDatos();
                if (Validar == 0)
                    return;

                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_ListarContratoDescuento", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Mes", ddlMes.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@Ano", txtAno.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                grvCD.DataSource = dt;
                grvCD.DataBind();
            }
            catch (Exception ex)
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Error!!!", ex.Message);
            }
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            CargarGrilla();
        }


        protected int ValidarDatos()
        {
            int valor = 0;
            if (txtAno.Text == "")
            {
                ucMensaje1.Show(ControlUsuario.Controles.tipoMensaje.informacion, "Información!!!", "Debe Escribir el Año para Realizar la Busqueda.");
                return valor;
            }
            valor = 1;
            return valor;
        }

    }
}