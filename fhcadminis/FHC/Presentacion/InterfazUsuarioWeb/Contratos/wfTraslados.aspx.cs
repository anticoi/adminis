﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using TGENESIS.FHC.InterfazUsuarioWeb;
using InterfazUsuarioWeb.Clases;
using TGENESIS.FHC.ComponentesNegocio;
using TGENESIS.Framework.Utilidades;
using System.Configuration;

namespace InterfazUsuarioWeb.Contratos
{
    public partial class wfTraslados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Label lTitulo = (Label)Master.FindControl("lblTitulo");

                //string NombreMenu = NegocioReporteria.Instancia.BuscarNombreOpcion(Convert.ToInt32(ConfigurationManager.AppSettings["IdSistema"].ToString()), 20);

                string NombreMenu = "Ver Traslados";

                lTitulo.Text = NombreMenu.ToString();

            }
        }


        protected void txtFechaDesde_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtFechaHasta_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                ConexionSQL cc = new ConexionSQL();
                SqlConnection cn = cc.AbrirConexionIntranet();

                SqlCommand cmd = new SqlCommand("Usp_VerTraslados", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@FechaInicio", txtFechaDesde.Text);
                cmd.Parameters.AddWithValue("@FechaTermino", txtFechaHasta.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                grvTRAS.DataSource = dt;
                grvTRAS.DataBind();
            }
            catch (Exception)
            {
            }
        }


        protected void grvTRAS_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal d = 0;
                decimal.TryParse(e.Row.Cells[8].Text, out d);
                e.Row.Cells[8].Text = d.ToString("N0");
            }
        }

        protected void grvTRAS_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grvTRAS_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvTRAS.PageIndex = e.NewPageIndex;
        }
    }
}