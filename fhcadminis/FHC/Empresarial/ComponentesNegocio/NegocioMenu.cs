﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioMenu
    {

        #region Metodos Privados

        private static NegocioMenu instancia = null;

        #endregion

        #region constructores

        public NegocioMenu()
        {

        }

        #endregion

        #region Singleton
        public static NegocioMenu Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioMenu();
                }
                return instancia;
            }
        }
        #endregion

        #region Metodos Publicos
        public MenuSistema BuscarMenu(int idMenu)
        {

            try
            {
                MenuSistema menu = DAOFabrica.CrearDAO().CrearMenuDAO().BuscarMenu(idMenu);
                return menu;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public List<MenuSistema> Listar(int idSistema, int idUsuario)
        {
            List<MenuSistema> lista = new List<MenuSistema>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearMenuDAO().Listar(idSistema, idUsuario);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

        public string Insertar(MenuSistema dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearMenuDAO().Insertar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }

        public string CrearMenu(int SistemaId, string Nombre, int Padre, int Posicion, int Habilitado, string Ruta, string Pagina)
        {
            string CrearMenu = "";
            try
            {
                CrearMenu = DAOFabrica.CrearDAO().CrearMenuDAO().CrearMenu(SistemaId, Nombre, Padre, Posicion, Habilitado, Ruta, Pagina);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return CrearMenu;
        }

        public void Eliminar(MenuSistema dto)
        {
            try
            {
                DAOFabrica.CrearDAO().CrearMenuDAO().Eliminar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}
