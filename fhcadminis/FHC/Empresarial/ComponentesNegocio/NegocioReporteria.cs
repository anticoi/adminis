﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.SQLServer;
using System.Data;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioReporteria
    {

        #region Metodos Privados

        private static NegocioReporteria instancia = null;

        #endregion

        #region constructores


        public NegocioReporteria()
        {

        }
        #endregion

        #region Singleton
        public static NegocioReporteria Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioReporteria();
                }
                return instancia;
            }
        }
        #endregion


        public string BuscarNombreOpcion(int SistemaId, int MenuId)
        {
            try
            {
                return FabricaReporteria.GetInstancia().BuscarNombreOpcion(SistemaId, MenuId);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }


        public DataTable ListarUsuarios()
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarUsuarios(); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public string InsertarArea(string AreaNombre, string Usuario, int AreaId)
        {
            try
            {
                return FabricaReporteria.GetInstancia().InsertarArea(AreaNombre, Usuario, AreaId);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }


        public string InsertarAreaSuperior(string AreaSuperiorNombre, string Usuario, int AreaSuperiorId)
        {
            try
            {
                return FabricaReporteria.GetInstancia().InsertarAreaSuperior(AreaSuperiorNombre, Usuario, AreaSuperiorId);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }


        public DataTable ListarArea()
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarArea();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ListarAreaDeAreaSuperior(int AreaSuperiorId)
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarAreaDeAreaSuperior(AreaSuperiorId) ; 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable ListarAreaSuperior()
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarAreaSuperior();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string EliminarArea(int AreaId, string Usuario)
        {
            try
            {
                return FabricaReporteria.GetInstancia().EliminarArea(AreaId , Usuario);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }


        public string EliminarAreaSuperior(int AreaSuperiorId, string Usuario)
        {
            try
            {
                return FabricaReporteria.GetInstancia().EliminarAreaSuperior(AreaSuperiorId, Usuario);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }

        public DataTable ListarUsuariosArea(int IdArea)
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarUsuariosArea(IdArea); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ListarWorkflowArea(int IdWArea)
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarWorkflowArea(IdWArea);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ListarRol()
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarRol(); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable ListarPermisos()
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarPermisos(); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ListarPermisosRol(int IdRol)
        {
            try
            {
                return FabricaReporteria.GetInstancia().ListarPermisosRol(IdRol);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string InsertarRolPermiso(int IdRol, int IdPermiso)
        {
            string salida = "";
            try
            {
                salida = FabricaReporteria.GetInstancia().InsertarRolPermiso(IdRol, IdPermiso);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }

        public string InsertAreaDeAreaSuperior(int IdAreaSuperior, int IdArea)
        {
            string salida = "";
            try
            {
                salida = FabricaReporteria.GetInstancia().InsertAreaDeAreaSuperior(IdAreaSuperior, IdArea); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }

        public string EliminaRolPermiso(int IdRol, int @IdPermiso)
        {
            string salida = "";
            try
            {
                salida = FabricaReporteria.GetInstancia().EliminaRolPermiso(IdRol, @IdPermiso);  
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }



        public string EliminarAreaDeAreaSuperior(int IdAreaSuperior, int IdArea)
        {
            string salida = "";
            try
            {
                salida = FabricaReporteria.GetInstancia().EliminarAreaDeAreaSuperior(IdAreaSuperior, IdArea); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }



        public int VerificaSupervisor(int IdUsuario)
        {
            int salida =0 ;
            try
            {
                salida = FabricaReporteria.GetInstancia().VerificaSupervisor(IdUsuario); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }


        public DataTable VerHistorialAsignador(int Id)
        {
            try
            {
                return FabricaReporteria.GetInstancia().VerHistorialAsignador(Id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string BuscarNombreAsesor(int Contrato)
        {
            try
            {
                return FabricaReporteria.GetInstancia().BuscarNombreAsesor(Contrato); 
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }

        }
    }
}
