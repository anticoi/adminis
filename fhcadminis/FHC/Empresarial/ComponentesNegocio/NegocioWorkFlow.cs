﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;
using System.Data;

namespace TGENESIS.FHC.ComponentesNegocio

{
    public class NegocioWorkFlow
    {

        #region Metodos Privados

        private static NegocioWorkFlow instancia = null;

        #endregion

		#region constructores

        public NegocioWorkFlow()
		{
			
		}
		
		#endregion


        #region Singleton
        public static NegocioWorkFlow Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioWorkFlow();
                }
                return instancia;
            }
        }
        #endregion

        #region Metodos Publicos

        public List<WorkFlow> Listar()
        {
            List<WorkFlow> lista = new List<WorkFlow>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearWorkFlowDAO().Listar(); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

        public string Insertar(WorkFlow dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearWorkFlowDAO().Insertar(dto); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }



        public DataTable ListaWorkFlowPorUsuario(string Usuario, int EstadoWorkFlowId)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearWorkFlowDAO().ListaWorkFlowPorUsuario(Usuario, EstadoWorkFlowId); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ListaWorkFlowSolicitadosPorUsuario(string Usuario, int EstadoWorkFlowId)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearWorkFlowDAO().ListaWorkFlowSolicitadosPorUsuario(Usuario, EstadoWorkFlowId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public string FinalizaWorkFlow(WorkFlow dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearWorkFlowDAO().FinalizaWorkFlow(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }


        public DataTable ListaWorkFlowUnico(int idworkflow)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearWorkFlowDAO().ListaWorkFlowUnico(idworkflow); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public string InsertarAsignaWorkFlow(int WorkFlowId, string UsuarioDeriva, string Observacion,  string UsuarioAsignado)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearWorkFlowDAO().InsertarAsignaWorkFlow (WorkFlowId, UsuarioDeriva, Observacion , UsuarioAsignado);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }



        public DataTable ListaWorkFlowPorArea(string Supervisor, int Estado, int AreaId, int Pertenece)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearWorkFlowDAO().ListaWorkFlowPorArea(Supervisor, Estado, AreaId, Pertenece);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

 
        public DataTable ListaAreaPorSupervisor(string Supervisor)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearWorkFlowDAO().ListaAreaPorSupervisor(Supervisor);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public string WorkFlowModifica(WorkFlow dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearWorkFlowDAO().WorkFlowModifica(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }



        public DataTable ListaPorAreaSupervisorPertenece(string Supervisor, int EstadoWorkFlowId, int AreaId)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearWorkFlowDAO().ListaPorAreaSupervisorPertenece(Supervisor, EstadoWorkFlowId, AreaId) ;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion


    }
}
