﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;
 

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioConexion
    {
        #region Metodos Privados

        private static NegocioConexion instancia = null;

        #endregion

        #region constructores

        public NegocioConexion()
        {

        }

        #endregion

        #region Singleton
        public static NegocioConexion Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioConexion();
                }
                return instancia;
            }
        }
        #endregion

        #region Metodos Publicos
        public List<Conexion> Listar()
        {
            List<Conexion> lista = new List<Conexion>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearConexionDAO().Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

        public string Insertar(Conexion dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearConexionDAO().Insertar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }

        public void Eliminar(Conexion dto)
        {
            try
            {
                DAOFabrica.CrearDAO().CrearConexionDAO().Eliminar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}
