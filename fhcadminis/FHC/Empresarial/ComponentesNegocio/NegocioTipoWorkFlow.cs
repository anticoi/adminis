﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioTipoWorkFlow
    {

		#region Metodos Privados
		
		private static NegocioTipoWorkFlow instancia = null;
		
		#endregion
		
		#region constructores

        public NegocioTipoWorkFlow()
		{
			
		}
		
		#endregion
		
		#region Singleton
        public static NegocioTipoWorkFlow Instancia
		{
			get 	
			{
				if (instancia == null)
				{
                    instancia = new NegocioTipoWorkFlow();
				}
				return instancia;
			}
		}
		#endregion
		
		#region Metodos Publicos

        public List<TipoWorkFlow> Listar()
        {
            List<TipoWorkFlow> lista = new List<TipoWorkFlow>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearTipoWorkFlowDAO().Listar(); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

 
        public string Insertar(TipoWorkFlow dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearTipoWorkFlowDAO().Insertar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }


        public int  Eliminar(TipoWorkFlow dto)
        {
            int salida = 0;
            try
            {
                salida = DAOFabrica.CrearDAO().CrearTipoWorkFlowDAO().Eliminar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }
		 



		#endregion


    }
}
