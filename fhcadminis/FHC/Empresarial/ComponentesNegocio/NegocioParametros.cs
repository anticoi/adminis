﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioParametros
    {

        #region Metodos Privados

        private static NegocioParametros instancia = null;

        #endregion

		#region constructores

        public NegocioParametros()
		{
			
		}
		
		#endregion


        #region Singleton
        public static NegocioParametros Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioParametros();
                }
                return instancia;
            }
        }
        #endregion



        #region Metodos Publicos

        public List<Parametros> Listar()
        {
            List<Parametros> lista = new List<Parametros>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearParametrosDAO().Listar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

        public string Insertar(Parametros dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearParametrosDAO().Insertar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }


        public int Eliminar(Parametros dto)
        {
            int salida = 0;
            try
            {
                salida = DAOFabrica.CrearDAO().CrearParametrosDAO().Eliminar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }




        #endregion

    }
}
