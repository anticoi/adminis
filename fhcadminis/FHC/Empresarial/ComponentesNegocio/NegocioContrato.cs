﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TGENESIS.FHC.AccesoDatos;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioContrato
    {

        #region Metodos Privados

        private static NegocioContrato instancia = null;

        #endregion

        #region constructores


        public NegocioContrato()
        {

        }
        #endregion


        #region Singleton
        public static NegocioContrato Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioContrato();
                }
                return instancia;
            }
        }
        #endregion


        public DataTable contratossinrendicion(int fechainicio, int fechatermino)
        {
            try
            {
                return DAOFabrica.CrearDAO().CrearContratoDAO().contratossinrendicion(fechainicio, fechatermino); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
