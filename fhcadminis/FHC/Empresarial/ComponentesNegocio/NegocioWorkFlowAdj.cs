﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioWorkFlowAdj
    {

        #region Metodos Privados

        private static NegocioWorkFlowAdj instancia = null;

        #endregion

		#region constructores

        public NegocioWorkFlowAdj()
		{
			
		}
		
		#endregion

        #region Singleton

        public static NegocioWorkFlowAdj Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioWorkFlowAdj();
                }
                return instancia;
            }
        }
        #endregion

        #region Metodos Publicos

        public List<WorkFlowAdj> Listar(WorkFlowAdj dto)
        {
            List<WorkFlowAdj> lista = new List<WorkFlowAdj>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearWorkFlowAdjDAO().Listar( dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

        public string Insertar(WorkFlowAdj dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearWorkFlowAdjDAO().Insertar(dto);  
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }


             

        #endregion


    }
}
