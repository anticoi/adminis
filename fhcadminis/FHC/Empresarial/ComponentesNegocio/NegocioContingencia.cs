﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;
using System.Data;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioContingencia
    {

        #region Metodos Privados

        private static NegocioContingencia instancia = null;

        #endregion

        #region Constructores
        public NegocioContingencia()
        {
        }
        #endregion

        #region Singleton
        public static NegocioContingencia Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new NegocioContingencia();
                }
                return instancia;
            }
        }
        #endregion

        #region Metodos Publicos

        public string Insertar(Contingencia dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearContingenciaDAO().Insertar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }

        #endregion
    }
}
