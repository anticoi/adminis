﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TGENESIS.FHC.EntidadesNegocio;
using TGENESIS.FHC.AccesoDatos;

namespace TGENESIS.FHC.ComponentesNegocio
{
    public class NegocioEstadoWorkFlow
    {


		#region Metodos Privados
		
		private static NegocioEstadoWorkFlow instancia = null;
		
		#endregion
		
		#region constructores

        public NegocioEstadoWorkFlow()
		{
			
		}
		
		#endregion
		
		#region Singleton
        public static NegocioEstadoWorkFlow Instancia
		{
			get 	
			{
				if (instancia == null)
				{
                    instancia = new NegocioEstadoWorkFlow();
				}
				return instancia;
			}
		}
		#endregion
		
		#region Metodos Publicos

        public List<EstadoWorkFlow> Listar()
        {
            List<EstadoWorkFlow> lista = new List<EstadoWorkFlow>();
            try
            {
                lista = DAOFabrica.CrearDAO().CrearEstadoWorkFlowDAO().Listar(); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return lista;
        }

        public string Insertar(EstadoWorkFlow dto)
        {
            string salida = "";
            try
            {
                salida = DAOFabrica.CrearDAO().CrearEstadoWorkFlowDAO().Insertar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }


        public int  Eliminar(EstadoWorkFlow dto)
        {
            int salida = 0;
            try
            {
                salida = DAOFabrica.CrearDAO().CrearEstadoWorkFlowDAO().Eliminar(dto);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return salida;
        }
		 



		#endregion



    }
}
