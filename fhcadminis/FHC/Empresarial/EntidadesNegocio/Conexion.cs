﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class Conexion
    {
        #region Metodos Privados
        private int _idConexion;
        private int _idSistema;
        private string _ipPeticion;
        private string _ipCliente;
        private bool _esProxy;
        private string _usuario;
        private string _autenticacion;
        private DateTime _fecha;
        private string _navegador;
        private string _host;



        #endregion

        #region Metodos Publicos

        public int idConexion
        {
            set { _idConexion = value; }
            get { return _idConexion; }
        }

        public int idSistema
        {
            set { _idSistema = value; }
            get { return _idSistema; }
        }

        public string ipPeticion
        {
            set { _ipPeticion = value; }
            get { return _ipPeticion; }
        }

        public string ipCliente
        {
            set { _ipCliente = value; }
            get { return _ipCliente; }
        }

        public bool esProxy
        {
            set { _esProxy = value; }
            get { return _esProxy; }
        }

        public string usuario
        {
            set { _usuario = value; }
            get { return _usuario; }
        }

        public string autenticacion
        {
            set { _autenticacion = value; }
            get { return _autenticacion; }
        }

        public DateTime fecha
        {
            set { _fecha = value; }
            get { return _fecha; }
        }

        public string Navegador
        {
            get { return _navegador; }
            set { _navegador = value; }
        }

        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }
        #endregion

        #region constructor
        public Conexion()
        {

        }
        #endregion

    }
}
