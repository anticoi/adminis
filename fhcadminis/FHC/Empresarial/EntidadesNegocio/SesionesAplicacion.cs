﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public static class Sesiones
    {
        #region constantes
        const string C_tblDiagnosticoAccion = "FHC_tblDiagnosticoAccion";
        const string C_tblFotos = "FHC_tblFoto";
        const string C_autenticacion = "FHC_Autenticacion";
        const string C_tblPresupuestoTarjeta = "FHC_PresupuestoTarjeta";
        public const string C_tblInformeReclamos = "FHC_InformeReclamos";
        #endregion

 

        public static string Glb_autenticacion
        {
            get
            {
                if (HttpContext.Current.Session[C_autenticacion] == null)
                    return null;
                else
                    return (string)HttpContext.Current.Session[C_autenticacion];
            }
            set
            {
                HttpContext.Current.Session[C_autenticacion] = value;
            }
        }

         
 

 

    }
}
