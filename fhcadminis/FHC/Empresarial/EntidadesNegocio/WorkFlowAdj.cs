﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class WorkFlowAdj
    {

        #region Metodos Privados
        private int _WorkFlowAdjId;
        private int _WorkFlowId;
        private string _WorkFlowAdjArchivo;
        private bool _WorkFlowAdjEstado;
        private DateTime _WorkFlowAdjFechaIngreso;
        private string _WorkFlowAdjUsuarioIngreso;
        private DateTime _WorkFlowAdjFechaModificacion;
        private string _WorkFlowAdjUsuarioModificacion;

        #endregion

        #region Metodos Publicos


        public int WorkFlowAdjId
        {
            get { return _WorkFlowAdjId; }
            set { _WorkFlowAdjId = value; }
        }


        public int WorkFlowId
        {
            get { return _WorkFlowId; }
            set { _WorkFlowId = value; }
        }


        public string WorkFlowAdjArchivo
        {
            set { _WorkFlowAdjArchivo = value; }
            get { return _WorkFlowAdjArchivo; }
        }

        public bool WorkFlowAdjEstado
        {
            set { _WorkFlowAdjEstado = value; }
            get { return _WorkFlowAdjEstado; }
        }

        public DateTime WorkFlowAdjFechaIngreso
        {
            set { _WorkFlowAdjFechaIngreso = value; }
            get { return _WorkFlowAdjFechaIngreso; }
        }

        public string WorkFlowAdjUsuarioIngreso
        {
            set { _WorkFlowAdjUsuarioIngreso = value; }
            get { return _WorkFlowAdjUsuarioIngreso; }
        }

        public DateTime WorkFlowAdjFechaModificacion
        {
            set { _WorkFlowAdjFechaModificacion = value; }
            get { return _WorkFlowAdjFechaModificacion; }
        }

        public string WorkFlowAdjUsuarioModificacion
        {
            set { _WorkFlowAdjUsuarioModificacion = value; }
            get { return _WorkFlowAdjUsuarioModificacion; }
        }
        #endregion


		#region constructor


        public WorkFlowAdj()
		{
		
		}
		#endregion    



    }
}
