﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class MenuSistema
    {

		#region Metodos Privados
        private int _idSistema;
        private int _menuId;
		private string _descripcion;
		private int _padreId;
		private int _posicion;
		private string _icono;
		private bool _habilitado;
		private string _ruta;
        private string _pagina;
        private string _parametros;

       

		#endregion
		
		#region Metodos Publicos
        public int idSistema
        {
            get { return _idSistema; }
            set { _idSistema = value; }
        }
		
		public int menuId
		{
			set { _menuId= value;}
			get { return _menuId;}
		}

		public string descripcion
		{
			set { _descripcion= value;}
			get { return _descripcion;}
		}

		public int padreId
		{
			set { _padreId= value;}
			get { return _padreId;}
		}

		public int posicion
		{
			set { _posicion= value;}
			get { return _posicion;}
		}

		public string icono
		{
			set { _icono= value;}
			get { return _icono;}
		}

		public bool habilitado
		{
			set { _habilitado= value;}
			get { return _habilitado;}
		}

		public string Ruta
		{
			set { _ruta= value;}
			get { return _ruta;}
		}

        public string Pagina
        {
            get { return _pagina; }
            set { _pagina = value; }
        }

        public string Parametros
        {
            get { return _parametros; }
            set { _parametros = value; }
        }
		#endregion
		
		#region constructor
		public MenuSistema()
		{
		
		}
		#endregion    
    
    }
}
