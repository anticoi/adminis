﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class EstadoWorkFlow
    {


		#region Metodos Privados
                private int _EstadoWorkFlowId ;
                private string _EstadoWorkFlowDescripcion;
                private bool _EstadoWorkFlowEstado;
                private DateTime _EstadoWorkFlowFechaIngreso;
                private string _EstadoWorkFlowUsuarioIngreso ;
                private DateTime _EstadoWorkFlowFechaModificacion;
                private string _EstadoWorkFlowUsuarioModificacion;

		#endregion
		
		#region Metodos Publicos
        
        
        public int EstadoWorkFlowId
        {
            get { return _EstadoWorkFlowId; }
            set { _EstadoWorkFlowId = value; }
        }

        public string EstadoWorkFlowDescripcion
		{
            set { _EstadoWorkFlowDescripcion = value; }
            get { return _EstadoWorkFlowDescripcion; }
		}

        public bool EstadoWorkFlowEstado
		{
            set { _EstadoWorkFlowEstado = value; }
            get { return _EstadoWorkFlowEstado; }
		}

        public DateTime EstadoWorkFlowFechaIngreso
		{
            set { _EstadoWorkFlowFechaIngreso = value; }
            get { return _EstadoWorkFlowFechaIngreso; }
		}

        public string EstadoWorkFlowUsuarioIngreso
		{
            set { _EstadoWorkFlowUsuarioIngreso = value; }
            get { return _EstadoWorkFlowUsuarioIngreso; }
		}

        public DateTime EstadoWorkFlowFechaModificacion
		{
            set { _EstadoWorkFlowFechaModificacion = value; }
            get { return _EstadoWorkFlowFechaModificacion; }
		}

        public string EstadoWorkFlowUsuarioModificacion
        {
            set { _EstadoWorkFlowUsuarioModificacion = value; }
            get { return _EstadoWorkFlowUsuarioModificacion; }
        }
		#endregion
		
		#region constructor


        public EstadoWorkFlow()
		{
		
		}
		#endregion    


    }
}
