﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class TipoWorkFlow
    {

		#region Metodos Privados
                private int _TipoWorkFlowId ;
                private string _TipoWorkFDescripcion ;
                private bool _TipoWorkFlowEstado;
                private DateTime _TipoWorkFlowFechaIngreso;
                private string _TipoWorkFlowUsuarioIngreso ;
                private DateTime _TipoWorkFlowFechaModificacion;
                private string _TipoWorkFlowUsuarioModificacion;
                private int _TipoWorkFlowArea;

		#endregion
		
		#region Metodos Publicos
        
        
        public int TipoWorkFlowId
        {
            get { return _TipoWorkFlowId; }
            set { _TipoWorkFlowId = value; }
        }

        public string TipoWorkFDescripcion
		{
            set { _TipoWorkFDescripcion = value; }
            get { return _TipoWorkFDescripcion; }
		}

        public bool TipoWorkFlowEstado
		{
            set { _TipoWorkFlowEstado = value; }
            get { return _TipoWorkFlowEstado; }
		}

        public DateTime TipoWorkFlowFechaIngreso
		{
            set { _TipoWorkFlowFechaIngreso = value; }
            get { return _TipoWorkFlowFechaIngreso; }
		}

        public string TipoWorkFlowUsuarioIngreso
		{
            set { _TipoWorkFlowUsuarioIngreso = value; }
            get { return _TipoWorkFlowUsuarioIngreso; }
		}

        public DateTime TipoWorkFlowFechaModificacion
		{
            set { _TipoWorkFlowFechaModificacion = value; }
            get { return _TipoWorkFlowFechaModificacion; }
		}

        public string TipoWorkFlowUsuarioModificacion
        {
            set { _TipoWorkFlowUsuarioModificacion = value; }
            get { return _TipoWorkFlowUsuarioModificacion; }
        }

        public int TipoWorkFlowArea
        {
            set { _TipoWorkFlowArea = value; }
            get { return _TipoWorkFlowArea; }
        }
		#endregion
		
		#region constructor


        public TipoWorkFlow()
		{
		
		}
		#endregion    

    }
}
