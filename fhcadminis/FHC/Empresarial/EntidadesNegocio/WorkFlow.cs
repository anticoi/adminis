﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class WorkFlow
    {
        #region Metodos Privados
        private int _WorkFlowId;
        private int _WorkFlowContrato;
        private string _WorkFlowTitulo;
        private string _WorkFlowObservacion;
        private string _WorkFlowUsuarioAsignado;
        private int _TipoWorkFlowId;
        private int _EstadoWorkFlowId;
        private bool _WorkFlowEstado;
        private DateTime _WorkFlowFechaIngreso;
        private string _WorkFlowUsuarioIngreso;
        private DateTime _WorkFlowFechaModificacion;
        private string _WorkFlowUsuarioModificacion;
        private int _WorkFlowArea;

        #endregion

        #region Metodos Publicos

        public int WorkFlowId
        {
            get { return _WorkFlowId; }
            set { _WorkFlowId = value; }
        }

        public int WorkFlowArea
        {
            get { return _WorkFlowArea; }
            set { _WorkFlowArea = value; }
        }

        public int WorkFlowContrato
        {
            get { return _WorkFlowContrato; }
            set { _WorkFlowContrato = value; }
        }


        public string WorkFlowTitulo
        {
            set { _WorkFlowTitulo = value; }
            get { return _WorkFlowTitulo; }
        }

        public string WorkFlowObservacion
        {
            set { _WorkFlowObservacion = value; }
            get { return _WorkFlowObservacion; }
        }

        public string WorkFlowUsuarioAsignado
        {
            set { _WorkFlowUsuarioAsignado = value; }
            get { return _WorkFlowUsuarioAsignado; }
        }

        public int TipoWorkFlowId
        {
            get { return _TipoWorkFlowId; }
            set { _TipoWorkFlowId = value; }
        }

        public int EstadoWorkFlowId
        {
            get { return _EstadoWorkFlowId; }
            set { _EstadoWorkFlowId = value; }
        }

        public bool WorkFlowEstado
        {
            set { _WorkFlowEstado = value; }
            get { return _WorkFlowEstado; }
        }

        public DateTime WorkFlowFechaIngreso
        {
            set { _WorkFlowFechaIngreso = value; }
            get { return _WorkFlowFechaIngreso; }
        }

        public string WorkFlowUsuarioIngreso
        {
            set { _WorkFlowUsuarioIngreso = value; }
            get { return _WorkFlowUsuarioIngreso; }
        }

        public DateTime WorkFlowFechaModificacion
        {
            set { _WorkFlowFechaModificacion = value; }
            get { return _WorkFlowFechaModificacion; }
        }

        public string WorkFlowUsuarioModificacion
        {
            set { _WorkFlowUsuarioModificacion = value; }
            get { return _WorkFlowUsuarioModificacion; }
        }

        #endregion

		#region constructor


        public WorkFlow()
		{
		
		}
		#endregion    
    }
}
