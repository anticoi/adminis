﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class Contingencia
    {
        #region Metodos Privados

        private string _ContingenciaContrato;
        private int _ContingenciaFolio;
        private DateTime _ContingenciaFecha;
        private string _ContingenciaFallNombre;
        private int _ContingenciaFallRut;
        private string _ContingenciaFallDigito;
        private string _ContingenciaContNombre;
        private int _ContingenciaContRut;
        private string _ContingenciaContDigito;
        private string _ContingenciaVendNombre;
        private string _ContingenciaVendFono;
        private string _ContingenciaLugar;
        private string _ContingenciaEstCivil;
        private string _ContingenciaEstudios;
        private string _ContingenciaProfesion;
        private string _ContingenciaCementerio;
        private string _ContingenciaObservaciones;
        private string _ContingenciaPlan;
        private string _ContingenciaIncluye;
        private string _ContingenciaTraslado;

        #endregion

        #region Metodos Publicos

        public string ContingenciaContrato
        {
            set { _ContingenciaContrato = value; }
            get { return _ContingenciaContrato; }
        }

        public int ContingenciaFolio
        {
            set { _ContingenciaFolio = value; }
            get { return _ContingenciaFolio; }
        }

        public DateTime ContingenciaFecha
        {
            set { _ContingenciaFecha = value; }
            get { return _ContingenciaFecha; }
        }

        public string ContingenciaFallNombre
        {
            set { _ContingenciaFallNombre = value; }
            get { return _ContingenciaFallNombre; }
        }

        public int ContingenciaFallRut
        {
            set { _ContingenciaFallRut = value; }
            get { return _ContingenciaFallRut; }
        }

        public string ContingenciaFallDigito
        {
            set { _ContingenciaFallDigito = value; }
            get { return _ContingenciaFallDigito; }
        }

        public string ContingenciaContNombre
        {
            set { _ContingenciaContNombre = value; }
            get { return _ContingenciaContNombre; }
        }

        public int ContingenciaContRut
        {
            set { _ContingenciaContRut = value; }
            get { return _ContingenciaContRut; }
        }

        public string ContingenciaContDigito
        {
            set { _ContingenciaContDigito = value; }
            get { return _ContingenciaContDigito; }
        }

        public string ContingenciaVendNombre
        {
            set { _ContingenciaVendNombre = value; }
            get { return _ContingenciaVendNombre; }
        }

        public string ContingenciaVendFono
        {
            set { _ContingenciaVendFono = value; }
            get { return _ContingenciaVendFono; }
        }

        public string ContingenciaLugar
        {
            set { _ContingenciaLugar = value; }
            get { return _ContingenciaLugar; }
        }

        public string ContingenciaEstCivil
        {
            set { _ContingenciaEstCivil = value; }
            get { return _ContingenciaEstCivil; }
        }

        public string ContingenciaEstudios
        {
            set { _ContingenciaEstudios = value; }
            get { return _ContingenciaEstudios; }
        }

        public string ContingenciaProfesion
        {
            set { _ContingenciaProfesion = value; }
            get { return _ContingenciaProfesion; }
        }

        public string ContingenciaCementerio
        {
            set { _ContingenciaCementerio = value; }
            get { return _ContingenciaCementerio; }
        }

        public string ContingenciaObservaciones
        {
            set { _ContingenciaObservaciones = value; }
            get { return _ContingenciaObservaciones; }
        }

        public string ContingenciaPlan
        {
            set { _ContingenciaPlan = value; }
            get { return _ContingenciaPlan; }
        }

        public string ContingenciaIncluye
        {
            set { _ContingenciaIncluye = value; }
            get { return _ContingenciaIncluye; }
        }

        public string ContingenciaTraslado
        {
            set { _ContingenciaTraslado = value; }
            get { return _ContingenciaTraslado; }
        }

        #endregion

        #region Constructor

        public Contingencia()
        {

        }

        #endregion
    }
}
