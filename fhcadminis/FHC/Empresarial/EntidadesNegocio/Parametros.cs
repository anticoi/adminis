﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TGENESIS.FHC.EntidadesNegocio
{
    public class Parametros
    {

        #region Metodos Privados
        private int _ParametrosId;
        private string _ParametrosDescripcion;
        private string _ParametrosValor;
        private bool _ParametrosEstado;
        private DateTime _ParametrosFechaIngreso;
        private string _ParametrosUsuarioIngreso;
        private DateTime _ParametrosFechaModificacion;
        private string _ParametrosUsuarioModificacion;

        #endregion



        #region Metodos Publicos

        public int ParametrosId
        {
            get { return _ParametrosId; }
            set { _ParametrosId = value; }
        }

        public string ParametrosDescripcion
        {
            set { _ParametrosDescripcion = value; }
            get { return _ParametrosDescripcion; }
        }

        public string ParametrosValor
        {
            set { _ParametrosValor = value; }
            get { return _ParametrosValor; }
        }

        public bool ParametrosEstado
        {
            set { _ParametrosEstado = value; }
            get { return _ParametrosEstado; }
        }

        public DateTime ParametrosFechaIngreso
        {
            set { _ParametrosFechaIngreso = value; }
            get { return _ParametrosFechaIngreso; }
        }

        public string ParametrosUsuarioIngreso
        {
            set { _ParametrosUsuarioIngreso = value; }
            get { return _ParametrosUsuarioIngreso; }
        }

        public DateTime ParametrosFechaModificacion
        {
            set { _ParametrosFechaModificacion = value; }
            get { return _ParametrosFechaModificacion; }
        }

        public string ParametrosUsuarioModificacion
        {
            set { _ParametrosUsuarioModificacion = value; }
            get { return _ParametrosUsuarioModificacion; }
        }
        #endregion

		#region constructor


        public Parametros()
		{
		
		}
		#endregion    


    }
}
