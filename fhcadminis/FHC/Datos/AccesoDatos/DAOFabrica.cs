﻿using System;
using System.Collections.Generic;
using System.Text;
 
using TGENESIS.FHC.AccesoDatos;
using TGENESIS.FHC.SQLServer;
 


namespace TGENESIS.FHC.AccesoDatos
{
    public abstract class DAOFabrica
    {
        public static DAOFabrica CrearDAO()
        {
            return new SqlServerDAOFabrica();
        }

        public abstract ConexionDAO CrearConexionDAO();
        public abstract MenuDAO CrearMenuDAO();
        public abstract TipoWorkFlowDAO CrearTipoWorkFlowDAO();
        public abstract EstadoWorkFlowDAO CrearEstadoWorkFlowDAO();
        public abstract WorkFlowDAO CrearWorkFlowDAO();
        public abstract WorkFlowAdjDAO CrearWorkFlowAdjDAO();
        public abstract ParametrosDAO CrearParametrosDAO();
        public abstract ContratoDAO CrearContratoDAO();
        public abstract ContingenciaDAO CrearContingenciaDAO();
    
    
    }
}
