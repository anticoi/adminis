﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TGENESIS.FHC.EntidadesNegocio;


namespace TGENESIS.FHC.AccesoDatos
{
   public class WorkFlowDAO
    {

        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        
        
        public WorkFlowDAO()
        {
        }
        #endregion

        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private WorkFlow Crear(IDataReader dr)
        {
            WorkFlow vObj = new WorkFlow();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("WorkFlowId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowId"] is System.DBNull))
                    {
                        vObj.WorkFlowId = (int)dr["WorkFlowId"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowContrato", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowContrato"] is System.DBNull))
                    {
                        vObj.WorkFlowContrato = (int)dr["WorkFlowContrato"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowTitulo", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowTitulo"] is System.DBNull))
                    {
                        vObj.WorkFlowTitulo = (string)dr["WorkFlowTitulo"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowObservacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowObservacion"] is System.DBNull))
                    {
                        vObj.WorkFlowObservacion = (string)dr["WorkFlowObservacion"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowUsuarioAsignado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowUsuarioAsignado"] is System.DBNull))
                    {
                        vObj.WorkFlowUsuarioAsignado = (string)dr["WorkFlowUsuarioAsignado"];
                    }
                }

                if (listaEsquema.BinarySearch("TipoWorkFlowId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowId"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowId = (int)dr["TipoWorkFlowId"];
                    }
                }


                if (listaEsquema.BinarySearch("EstadoWorkFlowId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowId"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowId = (int)dr["EstadoWorkFlowId"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowEstado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowEstado"] is System.DBNull))
                    {
                        vObj.WorkFlowEstado = (bool)dr["WorkFlowEstado"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowFechaIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowFechaIngreso"] is System.DBNull))
                    {
                        vObj.WorkFlowFechaIngreso = (DateTime)dr["WorkFlowFechaIngreso"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowUsuarioIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowUsuarioIngreso"] is System.DBNull))
                    {
                        vObj.WorkFlowUsuarioIngreso = (string)dr["WorkFlowUsuarioIngreso"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowFechaModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowFechaModificacion"] is System.DBNull))
                    {
                        vObj.WorkFlowFechaModificacion = (DateTime)dr["WorkFlowFechaModificacion"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowUsuarioModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowUsuarioModificacion"] is System.DBNull))
                    {
                        vObj.WorkFlowUsuarioModificacion = (string)dr["WorkFlowUsuarioModificacion"];
                    }
                }



                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<WorkFlow> CrearDTOs(IDataReader dr)
        {
            List<WorkFlow> listaDTO = new List<WorkFlow>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos

        public List<WorkFlow> Listar()
        {
            List<WorkFlow> lista = new List<WorkFlow>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_TipoWorkFlow");
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Insertar(WorkFlow dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertaWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowContrato", DbType.Int32, dto.WorkFlowContrato);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowTitulo", DbType.AnsiString, dto.WorkFlowTitulo);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowObservacion", DbType.AnsiString, dto.WorkFlowObservacion);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowUsuarioAsignado", DbType.AnsiString, dto.WorkFlowUsuarioAsignado);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowId", DbType.AnsiString, dto.TipoWorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowId", DbType.AnsiString, dto.EstadoWorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowUsuarioIngreso", DbType.AnsiString, dto.WorkFlowUsuarioIngreso);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowUsuarioModificacion", DbType.AnsiString, dto.WorkFlowUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowArea", DbType.AnsiString, dto.WorkFlowArea);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowFechaIngreso", DbType.DateTime, dto.WorkFlowFechaIngreso);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowFechaModificacion", DbType.DateTime, dto.WorkFlowFechaModificacion);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable ListaWorkFlowPorUsuario(string Usuario, int EstadoWorkFlowId)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListaWorkFlowPorUsuario");
                objDataBase.AddInParameter(objDbCommand, "UsuarioAsignado", DbType.AnsiString, Usuario);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowId", DbType.Int32, EstadoWorkFlowId);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListaWorkFlowPorUsuario");
                    return ds.Tables["dtListaWorkFlowPorUsuario"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public DataTable ListaWorkFlowSolicitadosPorUsuario(string Usuario, int EstadoWorkFlowId)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListaWorkFlowSolicitadosPorUsuario");
                objDataBase.AddInParameter(objDbCommand, "UsuarioSolicita", DbType.AnsiString, Usuario);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowId", DbType.Int32, EstadoWorkFlowId);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListaWorkFlowPorUsuario");
                    return ds.Tables["dtListaWorkFlowPorUsuario"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string FinalizaWorkFlow(WorkFlow dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_FinalizaWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, dto.WorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowUsuarioModificacion", DbType.AnsiString, dto.WorkFlowUsuarioModificacion);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public DataTable ListaWorkFlowUnico(int idworkflow)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_ListaWorkFlowUnico");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, idworkflow);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListaWorkFlowUnico");
                    return ds.Tables["dtListaWorkFlowUnico"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }





        public string InsertarAsignaWorkFlow(int WorkFlowId, string UsuarioDeriva, string Observacion ,string UsuarioAsignado)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_AsignaWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, WorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "UsuarioDeriva", DbType.AnsiString, UsuarioDeriva);
                objDataBase.AddInParameter(objDbCommand, "AsignaObservacion", DbType.String, Observacion);
                objDataBase.AddInParameter(objDbCommand, "UsuarioAsignado", DbType.AnsiString, UsuarioAsignado); 

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public DataTable ListaWorkFlowPorArea(string Supervisor, int Estado, int AreaId, int Pertenece)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_VerTareaPorArea");
                objDataBase.AddInParameter(objDbCommand, "Supervisor", DbType.AnsiString, Supervisor);
                objDataBase.AddInParameter(objDbCommand, "EstadoTarea", DbType.Int32, Estado);
                objDataBase.AddInParameter(objDbCommand, "AreaId", DbType.Int32, AreaId);
                objDataBase.AddInParameter(objDbCommand, "Pertenece", DbType.Int32, Pertenece);

                  using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListaWorkFlowPorArea");
                    return ds.Tables["dtListaWorkFlowPorArea"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




        public DataTable ListaAreaPorSupervisor(string Supervisor)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListaPorAreaSupervisor");
                objDataBase.AddInParameter(objDbCommand, "Supervisor", DbType.AnsiString, Supervisor);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListaAreaPorSupervisor");
                    return ds.Tables["dtListaAreaPorSupervisor"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public string WorkFlowModifica(WorkFlow dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_WorkFlowModifica");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, dto.WorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowObservacion", DbType.AnsiString, dto.WorkFlowObservacion);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowUsuarioModificacion", DbType.AnsiString, dto.WorkFlowUsuarioModificacion);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public DataTable ListaPorAreaSupervisorPertenece(string Supervisor, int EstadoWorkFlowId, int AreaId)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListaPorAreaSupervisorPertenece");
                objDataBase.AddInParameter(objDbCommand, "Supervisor", DbType.AnsiString, Supervisor);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowId", DbType.Int32, EstadoWorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "AreaId", DbType.Int32, AreaId);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListaAreaPorSupervisor");
                    return ds.Tables["dtListaAreaPorSupervisor"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion

    }
}
