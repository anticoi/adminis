﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TGENESIS.FHC.EntidadesNegocio;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TGENESIS.FHC.AccesoDatos
{
    public class WorkFlowAdjDAO
    {


        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        public WorkFlowAdjDAO()
        {
        }
        #endregion


        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private WorkFlowAdj Crear(IDataReader dr)
        {
            WorkFlowAdj vObj = new WorkFlowAdj();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("WorkFlowAdjId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjId"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjId = (int)dr["WorkFlowAdjId"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowAdjArchivo", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjArchivo"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjArchivo = (string)dr["WorkFlowAdjArchivo"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowAdjEstado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjEstado"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjEstado = (bool)dr["WorkFlowAdjEstado"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowAdjFechaIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjFechaIngreso"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjFechaIngreso = (DateTime)dr["WorkFlowAdjFechaIngreso"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowAdjUsuarioIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjUsuarioIngreso"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjUsuarioIngreso = (string)dr["WorkFlowAdjUsuarioIngreso"];
                    }
                }

                if (listaEsquema.BinarySearch("WorkFlowAdjFechaModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjFechaModificacion"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjFechaModificacion = (DateTime)dr["WorkFlowAdjFechaModificacion"];
                    }
                }


                if (listaEsquema.BinarySearch("WorkFlowAdjUsuarioModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["WorkFlowAdjUsuarioModificacion"] is System.DBNull))
                    {
                        vObj.WorkFlowAdjUsuarioModificacion = (string)dr["WorkFlowAdjUsuarioModificacion"];
                    }
                }



                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<WorkFlowAdj> CrearDTOs(IDataReader dr)
        {
            List<WorkFlowAdj> listaDTO = new List<WorkFlowAdj>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos

        public List<WorkFlowAdj> Listar(WorkFlowAdj dto)
        {
            List<WorkFlowAdj> lista = new List<WorkFlowAdj>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarWorkFlowAdjPorTarea");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, dto.WorkFlowId);
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Insertar(WorkFlowAdj dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertaWorkFlowAdjPorTarea");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, dto.WorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowAdjArchivo", DbType.AnsiString, dto.WorkFlowAdjArchivo);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowAdjUsuarioIngreso", DbType.AnsiString, dto.WorkFlowAdjUsuarioIngreso);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowAdjUsuarioModificacion", DbType.AnsiString, dto.WorkFlowAdjUsuarioModificacion);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int Eliminar(WorkFlowAdj dto)
        {
            int RetornaValor;
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_EliminarEstadoWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "WorkFlowAdjUsuarioModificacion", DbType.AnsiString, dto.WorkFlowAdjUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, dto.WorkFlowId);
                objDataBase.AddInParameter(objDbCommand, "WorkFlowAdjArchivo", DbType.AnsiString, dto.WorkFlowAdjArchivo);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtEliminar");
                    if (ds.Tables[0].Rows.Count > 0)
                        RetornaValor = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                    else
                        RetornaValor = 0;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RetornaValor;
        }



        #endregion

    }
}
