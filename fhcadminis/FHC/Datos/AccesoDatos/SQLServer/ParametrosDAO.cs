﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TGENESIS.FHC.EntidadesNegocio;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TGENESIS.FHC.AccesoDatos
{
    public class ParametrosDAO
    {

        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        public ParametrosDAO()
        {
        }
        #endregion


        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private Parametros Crear(IDataReader dr)
        {
            Parametros vObj = new Parametros();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("ParametrosId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosId"] is System.DBNull))
                    {
                        vObj.ParametrosId = (int)dr["ParametrosId"];
                    }
                }

                if (listaEsquema.BinarySearch("ParametrosDescripcion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosDescripcion"] is System.DBNull))
                    {
                        vObj.ParametrosDescripcion = (string)dr["ParametrosDescripcion"];
                    }
                }

                if (listaEsquema.BinarySearch("ParametrosValor", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosValor"] is System.DBNull))
                    {
                        vObj.ParametrosValor = (string)dr["ParametrosValor"];
                    }
                }


                if (listaEsquema.BinarySearch("ParametrosEstado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosEstado"] is System.DBNull))
                    {
                        vObj.ParametrosEstado = (bool)dr["ParametrosEstado"];
                    }
                }


                if (listaEsquema.BinarySearch("ParametrosFechaIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosFechaIngreso"] is System.DBNull))
                    {
                        vObj.ParametrosFechaIngreso = (DateTime)dr["ParametrosFechaIngreso"];
                    }
                }


                if (listaEsquema.BinarySearch("ParametrosUsuarioIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosUsuarioIngreso"] is System.DBNull))
                    {
                        vObj.ParametrosUsuarioIngreso = (string)dr["ParametrosUsuarioIngreso"];
                    }
                }

                if (listaEsquema.BinarySearch("ParametrosFechaModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosFechaModificacion"] is System.DBNull))
                    {
                        vObj.ParametrosFechaModificacion = (DateTime)dr["ParametrosFechaModificacion"];
                    }
                }


                if (listaEsquema.BinarySearch("ParametrosUsuarioModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ParametrosUsuarioModificacion"] is System.DBNull))
                    {
                        vObj.ParametrosUsuarioModificacion = (string)dr["ParametrosUsuarioModificacion"];
                    }
                }



                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<Parametros> CrearDTOs(IDataReader dr)
        {
            List<Parametros> listaDTO = new List<Parametros>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos

        public List<Parametros> Listar()
        {
            List<Parametros> lista = new List<Parametros>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarParametros");
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Insertar(Parametros dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertaParametros");
                objDataBase.AddInParameter(objDbCommand, "ParametrosDescripcion", DbType.AnsiString, dto.ParametrosDescripcion);
                objDataBase.AddInParameter(objDbCommand, "ParametrosValor", DbType.AnsiString, dto.ParametrosValor);
                objDataBase.AddInParameter(objDbCommand, "ParametrosEstado", DbType.Boolean, dto.ParametrosEstado);
                objDataBase.AddInParameter(objDbCommand, "ParametrosUsuarioIngreso", DbType.AnsiString, dto.ParametrosUsuarioIngreso);
                objDataBase.AddInParameter(objDbCommand, "ParametrosUsuarioModificacion", DbType.AnsiString, dto.ParametrosUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "ParametrosId", DbType.Int32, dto.ParametrosId);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int Eliminar(Parametros dto)
        {
            int RetornaValor;
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_EliminarParametros");
                objDataBase.AddInParameter(objDbCommand, "ParametrosUsuarioModificacion", DbType.AnsiString, dto.ParametrosUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "ParametrosId", DbType.Int32, dto.ParametrosId);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return 1;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RetornaValor;
        }



        #endregion


    }
}
