﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TGENESIS.FHC.EntidadesNegocio;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TGENESIS.Framework.Utilidades;

namespace TGENESIS.FHC.AccesoDatos
{
    public class TipoWorkFlowDAO
    {

        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        public TipoWorkFlowDAO()
        {
        }
        #endregion

        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private TipoWorkFlow Crear(IDataReader dr)
        {
            TipoWorkFlow vObj = new TipoWorkFlow();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("TipoWorkFlowId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowId"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowId = (int)dr["TipoWorkFlowId"];
                    }
                }

                if (listaEsquema.BinarySearch("TipoWorkFDescripcion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFDescripcion"] is System.DBNull))
                    {
                        vObj.TipoWorkFDescripcion = (string)dr["TipoWorkFDescripcion"];
                    }
                }

                if (listaEsquema.BinarySearch("TipoWorkFlowEstado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowEstado"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowEstado = (bool)dr["TipoWorkFlowEstado"];
                    }
                }


                if (listaEsquema.BinarySearch("TipoWorkFlowFechaIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowFechaIngreso"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowFechaIngreso = (DateTime)dr["TipoWorkFlowFechaIngreso"];
                    }
                }


                if (listaEsquema.BinarySearch("TipoWorkFlowUsuarioIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowUsuarioIngreso"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowUsuarioIngreso = (string)dr["TipoWorkFlowUsuarioIngreso"];
                    }
                }

                if (listaEsquema.BinarySearch("TipoWorkFlowFechaModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowFechaModificacion"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowFechaModificacion = (DateTime)dr["TipoWorkFlowFechaModificacion"];
                    }
                }


                if (listaEsquema.BinarySearch("TipoWorkFlowUsuarioModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowUsuarioModificacion"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowUsuarioModificacion = (string)dr["TipoWorkFlowUsuarioModificacion"];
                    }
                }


                if (listaEsquema.BinarySearch("TipoWorkFlowArea", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["TipoWorkFlowArea"] is System.DBNull))
                    {
                        vObj.TipoWorkFlowArea = (int)dr["TipoWorkFlowArea"];
                    }
                }



                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<TipoWorkFlow> CrearDTOs(IDataReader dr)
        {
            List<TipoWorkFlow> listaDTO = new List<TipoWorkFlow>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos

        public List<TipoWorkFlow> Listar()
        {
            List<TipoWorkFlow> lista = new List<TipoWorkFlow>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarTipoWorkFlow");
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public string Insertar(TipoWorkFlow dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertaTipoWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFDescripcion", DbType.AnsiString, dto.TipoWorkFDescripcion);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowEstado", DbType.Boolean, dto.TipoWorkFlowEstado );
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowFechaIngreso", DbType.DateTime, DateTime.Now);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowUsuarioIngreso", DbType.AnsiString, dto.TipoWorkFlowUsuarioIngreso);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowFechaModificacion", DbType.DateTime, DateTime.Now);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowUsuarioModificacion", DbType.AnsiString, dto.TipoWorkFlowUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowArea", DbType.Int32, dto.TipoWorkFlowArea);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int Eliminar(TipoWorkFlow dto)
        {
             int RetornaValor;
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_EliminarTipoWorkFlow");
 
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowUsuarioModificacion", DbType.AnsiString, dto.TipoWorkFlowUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "TipoWorkFlowId", DbType.Int32, dto.TipoWorkFlowId);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtEliminar");
                    if (ds.Tables[0].Rows.Count > 0)
                        RetornaValor = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                    else
                        RetornaValor = 0;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RetornaValor;
        }        
        

        
        #endregion




    }
}
