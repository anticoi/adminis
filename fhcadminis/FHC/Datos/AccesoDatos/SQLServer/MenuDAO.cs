﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TGENESIS.FHC.EntidadesNegocio;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TGENESIS.FHC.AccesoDatos
{
    public class MenuDAO
    {

        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        public MenuDAO()
        {
        }
        #endregion

        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private MenuSistema Crear(IDataReader dr)
        {
            MenuSistema vObj = new MenuSistema();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("idSistema", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["idSistema"] is System.DBNull))
                    {
                        vObj.idSistema = (int)dr["idSistema"];
                    }
                }
                if (listaEsquema.BinarySearch("menuId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["menuId"] is System.DBNull))
                    {
                        vObj.menuId = (int)dr["menuId"];
                    }
                }
                if (listaEsquema.BinarySearch("descripcion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["descripcion"] is System.DBNull))
                    {
                        vObj.descripcion = (string)dr["descripcion"];
                    }
                }
                if (listaEsquema.BinarySearch("padreId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["padreId"] is System.DBNull))
                    {
                        vObj.padreId = (int)dr["padreId"];
                    }
                }
                if (listaEsquema.BinarySearch("posicion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["posicion"] is System.DBNull))
                    {
                        vObj.posicion = (int)dr["posicion"];
                    }
                }
                if (listaEsquema.BinarySearch("icono", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["icono"] is System.DBNull))
                    {
                        vObj.icono = (string)dr["icono"];
                    }
                }
                if (listaEsquema.BinarySearch("habilitado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["habilitado"] is System.DBNull))
                    {
                        vObj.habilitado = (bool)dr["habilitado"];
                    }
                }
                if (listaEsquema.BinarySearch("ruta", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ruta"] is System.DBNull))
                    {
                        vObj.Ruta = (string)dr["ruta"];
                    }
                }
                if (listaEsquema.BinarySearch("pagina", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["pagina"] is System.DBNull))
                    {
                        vObj.Pagina = (string)dr["pagina"];
                    }
                }

                if (listaEsquema.BinarySearch("parametros", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["parametros"] is System.DBNull))
                    {
                        vObj.Parametros = (string)dr["parametros"];
                    }
                }


                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<MenuSistema> CrearDTOs(IDataReader dr)
        {
            List<MenuSistema> listaDTO = new List<MenuSistema>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos
        public MenuSistema BuscarMenu(int idMenu)
        {
            List<MenuSistema> lista = new List<MenuSistema>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_buscarMenu");
                objDataBase.AddInParameter(objDbCommand, "menuId", DbType.Int32, idMenu);
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public List<MenuSistema> Listar(int idSistema, int idUsuario)
        {
            List<MenuSistema> lista = new List<MenuSistema>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaMenu");
                objDataBase.AddInParameter(objDbCommand, "idSistema", DbType.Int32, idSistema);
                objDataBase.AddInParameter(objDbCommand, "idUsuario", DbType.Int32, idUsuario);

                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string CrearMenu(int SistemaId, string Nombre, int Padre, int Posicion, int Habilitado, string Ruta, string Pagina)
        {
            string RetornaMenu;
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_CrearMenu");
                objDataBase.AddInParameter(objDbCommand, "SistemaId", DbType.Int32, SistemaId);
                objDataBase.AddInParameter(objDbCommand, "Nombre", DbType.String, Nombre);
                objDataBase.AddInParameter(objDbCommand, "padre", DbType.Int32, Padre);
                objDataBase.AddInParameter(objDbCommand, "posicion", DbType.Int32, Posicion);
                objDataBase.AddInParameter(objDbCommand, "Habilitado", DbType.Boolean, Habilitado);
                objDataBase.AddInParameter(objDbCommand, "ruta", DbType.String, Ruta);
                objDataBase.AddInParameter(objDbCommand, "pagina", DbType.String, Pagina);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtRetornaValor");
                    if (ds.Tables[0].Rows.Count > 0)
                        RetornaMenu = ds.Tables[0].Rows[0][0].ToString();
                    else
                        RetornaMenu = "";
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RetornaMenu;

        }


        public string Insertar(MenuSistema dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_insertaMenu");
                objDataBase.AddInParameter(objDbCommand, "idSistema", DbType.Int32,  (dto.idSistema));
                objDataBase.AddInParameter(objDbCommand, "menuId", DbType.Int32, (dto.menuId));
                objDataBase.AddInParameter(objDbCommand, "descripcion", DbType.String, (dto.descripcion));
                objDataBase.AddInParameter(objDbCommand, "padreId", DbType.Int32, (dto.padreId));
                objDataBase.AddInParameter(objDbCommand, "posicion", DbType.Int32, (dto.posicion));
                objDataBase.AddInParameter(objDbCommand, "icono", DbType.String, (dto.icono));
                objDataBase.AddInParameter(objDbCommand, "habilitado", DbType.Boolean, (dto.habilitado));
                objDataBase.AddInParameter(objDbCommand, "ruta", DbType.String, (dto.Ruta));
                objDataBase.AddInParameter(objDbCommand, "pagina", DbType.String, (dto.Pagina));

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(MenuSistema dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_eliminaMenu");

                objDataBase.ExecuteNonQuery(objDbCommand);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion




    }




}
