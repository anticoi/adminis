﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using TGENESIS.FHC.EntidadesNegocio;
using System.Data;


namespace TGENESIS.FHC.SQLServer
{
    public class ConexionDAO
    {
        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        public ConexionDAO()
        {
        }
        #endregion

        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private Conexion Crear(IDataReader dr)
        {
            Conexion vObj = new Conexion();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("idConexion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["idConexion"] is System.DBNull))
                    {
                        vObj.idConexion = (int)dr["idConexion"];
                    }
                }
                if (listaEsquema.BinarySearch("idSistema", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["idSistema"] is System.DBNull))
                    {
                        vObj.idSistema = (int)dr["idSistema"];
                    }
                }
                if (listaEsquema.BinarySearch("ipPeticion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ipPeticion"] is System.DBNull))
                    {
                        vObj.ipPeticion = (string)dr["ipPeticion"];
                    }
                }
                if (listaEsquema.BinarySearch("ipCliente", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ipCliente"] is System.DBNull))
                    {
                        vObj.ipCliente = (string)dr["ipCliente"];
                    }
                }
                if (listaEsquema.BinarySearch("esProxy", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["esProxy"] is System.DBNull))
                    {
                        vObj.esProxy = (bool)dr["esProxy"];
                    }
                }
                if (listaEsquema.BinarySearch("usuario", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["usuario"] is System.DBNull))
                    {
                        vObj.usuario = (string)dr["usuario"];
                    }
                }
                if (listaEsquema.BinarySearch("autenticacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["autenticacion"] is System.DBNull))
                    {
                        vObj.autenticacion = (string)dr["autenticacion"];
                    }
                }
                if (listaEsquema.BinarySearch("fecha", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["fecha"] is System.DBNull))
                    {
                        vObj.fecha = (DateTime)dr["fecha"];
                    }
                }
                if (listaEsquema.BinarySearch("navegador", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["navegador"] is System.DBNull))
                    {
                        vObj.Navegador = (String)dr["navegador"];
                    }
                }
                if (listaEsquema.BinarySearch("host", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["host"] is System.DBNull))
                    {
                        vObj.Host = (String)dr["host"];
                    }
                }

                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<Conexion> CrearDTOs(IDataReader dr)
        {
            List<Conexion> listaDTO = new List<Conexion>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos
        public List<Conexion> Listar()
        {
            List<Conexion> lista = new List<Conexion>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaConexion");
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Insertar(Conexion dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_insertaConexion");
                //objDataBase.AddInParameter(objDbCommand, "idSistema", DbType.Int32, AssertNull.getValue(dto.idSistema));
                //objDataBase.AddInParameter(objDbCommand, "ipPeticion", DbType.String, AssertNull.getValue(dto.ipPeticion));
                //objDataBase.AddInParameter(objDbCommand, "ipCliente", DbType.String, AssertNull.getValue(dto.ipCliente));
                //objDataBase.AddInParameter(objDbCommand, "esProxy", DbType.Boolean, AssertNull.getValue(dto.esProxy));
                //objDataBase.AddInParameter(objDbCommand, "usuario", DbType.String, AssertNull.getValue(dto.usuario));
                //objDataBase.AddInParameter(objDbCommand, "autenticacion", DbType.String, AssertNull.getValue(dto.autenticacion));
                //objDataBase.AddInParameter(objDbCommand, "fecha", DbType.DateTime, AssertNull.getValue(dto.fecha));
                //objDataBase.AddInParameter(objDbCommand, "navegador", DbType.String, AssertNull.getValue(dto.Navegador));
                //objDataBase.AddInParameter(objDbCommand, "host", DbType.String, AssertNull.getValue(dto.Host));
                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar(Conexion dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_eliminaConexion");

                objDataBase.ExecuteNonQuery(objDbCommand);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

    }
}
