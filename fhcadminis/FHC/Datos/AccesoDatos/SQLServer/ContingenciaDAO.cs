﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TGENESIS.FHC.EntidadesNegocio;

namespace TGENESIS.FHC.AccesoDatos
{
    public class ContingenciaDAO
    {

        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores

        public ContingenciaDAO()
        {
        }
        #endregion

        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;
        }

        private Contingencia Crear(IDataReader dr)
        {
            Contingencia vObj = new Contingencia();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("ContingenciaContrato", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaContrato"] is System.DBNull))
                    {
                        vObj.ContingenciaContrato = (string)dr["ContingenciaContrato"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaFolio", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaFolio"] is System.DBNull))
                    {
                        vObj.ContingenciaFolio = (int)dr["ContingenciaFolio"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaFecha", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaFecha"] is System.DBNull))
                    {
                        vObj.ContingenciaFecha = (DateTime)dr["ContingenciaFecha"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaFallNombre", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaFallNombre"] is System.DBNull))
                    {
                        vObj.ContingenciaFallNombre = (string)dr["ContingenciaFallNombre"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaFallRut", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaFallRut"] is System.DBNull))
                    {
                        vObj.ContingenciaFallRut = (int)dr["ContingenciaFallRut"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaFallDigito", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaFallDigito"] is System.DBNull))
                    {
                        vObj.ContingenciaFallDigito = (string)dr["ContingenciaFallDigito"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaContNombre", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaContNombre"] is System.DBNull))
                    {
                        vObj.ContingenciaContNombre = (string)dr["ContingenciaContNombre"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaContRut", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaContRut"] is System.DBNull))
                    {
                        vObj.ContingenciaContRut = (int)dr["ContingenciaContRut"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaContDigito", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaContDigito"] is System.DBNull))
                    {
                        vObj.ContingenciaContDigito = (string)dr["ContingenciaContDigito"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaVendNombre", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaVendNombre"] is System.DBNull))
                    {
                        vObj.ContingenciaVendNombre = (string)dr["ContingenciaVendNombre"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaVendFono", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaVendFono"] is System.DBNull))
                    {
                        vObj.ContingenciaVendFono = (string)dr["ContingenciaVendFono"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaLugar", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaLugar"] is System.DBNull))
                    {
                        vObj.ContingenciaLugar = (string)dr["ContingenciaLugar"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaEstCivil", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaEstCivil"] is System.DBNull))
                    {
                        vObj.ContingenciaEstCivil = (string)dr["ContingenciaEstCivil"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaEstudios", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaEstudios"] is System.DBNull))
                    {
                        vObj.ContingenciaEstudios = (string)dr["ContingenciaEstudios"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaProfesion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaProfesion"] is System.DBNull))
                    {
                        vObj.ContingenciaProfesion = (string)dr["ContingenciaProfesion"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaCementerio", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaCementerio"] is System.DBNull))
                    {
                        vObj.ContingenciaCementerio = (string)dr["ContingenciaCementerio"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaObservaciones", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaObservaciones"] is System.DBNull))
                    {
                        vObj.ContingenciaObservaciones = (string)dr["ContingenciaObservaciones"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaPlan", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaPlan"] is System.DBNull))
                    {
                        vObj.ContingenciaPlan = (string)dr["ContingenciaPlan"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaIncluye", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaIncluye"] is System.DBNull))
                    {
                        vObj.ContingenciaIncluye = (string)dr["ContingenciaIncluye"];
                    }
                }

                if (listaEsquema.BinarySearch("ContingenciaTraslado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["ContingenciaTraslado"] is System.DBNull))
                    {
                        vObj.ContingenciaTraslado = (string)dr["ContingenciaTraslado"];
                    }
                }

                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<Contingencia> CrearDTOs(IDataReader dr)
        {
            List<Contingencia> listaDTO = new List<Contingencia>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr); //Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos

        public string Insertar(Contingencia dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("Usp_InsertaContingencia");
                objDataBase.AddInParameter(objDbCommand, "ContingenciaContrato", DbType.AnsiString, dto.ContingenciaContrato);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaFolio", DbType.Int32, dto.ContingenciaFolio);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaFecha", DbType.DateTime, dto.ContingenciaFecha);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaFallNombre", DbType.AnsiString, dto.ContingenciaFallNombre);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaFallRut", DbType.Int32, dto.ContingenciaFallRut);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaFallDigito", DbType.AnsiString, dto.ContingenciaFallDigito);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaContNombre", DbType.AnsiString, dto.ContingenciaContNombre);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaContRut", DbType.Int32, dto.ContingenciaContRut);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaContDigito", DbType.AnsiString, dto.ContingenciaContDigito);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaVendNombre", DbType.AnsiString, dto.ContingenciaVendNombre);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaVendFono", DbType.AnsiString, dto.ContingenciaVendFono);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaLugar", DbType.AnsiString, dto.ContingenciaLugar);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaEstCivil", DbType.AnsiString, dto.ContingenciaEstCivil);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaEstudios", DbType.AnsiString, dto.ContingenciaEstudios);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaProfesion", DbType.AnsiString, dto.ContingenciaProfesion);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaCementerio", DbType.AnsiString, dto.ContingenciaCementerio);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaObservaciones", DbType.AnsiString, dto.ContingenciaObservaciones);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaPlan", DbType.AnsiString, dto.ContingenciaPlan);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaIncluye", DbType.AnsiString, dto.ContingenciaIncluye);
                objDataBase.AddInParameter(objDbCommand, "ContingenciaTraslado", DbType.AnsiString, dto.ContingenciaTraslado);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}