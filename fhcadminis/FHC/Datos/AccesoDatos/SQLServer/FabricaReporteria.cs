﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace TGENESIS.FHC.SQLServer
{
    public class FabricaReporteria
    {
        #region Patron Singleton

        private static FabricaReporteria instancia = null;

        public static FabricaReporteria GetInstancia()
        {
            if (instancia == null)
            {
                instancia = new FabricaReporteria();
            }
            return instancia;
        }
        #endregion

        #region Contructores
        public FabricaReporteria()
        {

        }
        #endregion

        #region Metodos publicos

        public string BuscarNombreOpcion(int SistemaId, int MenuId)
        {
            string RetornaValor;
            try
            {

                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_RescataNombreMenuLaboratorioExterno");
                objDataBase.AddInParameter(objDbCommand, "SistemaId", DbType.Int64, SistemaId);
                objDataBase.AddInParameter(objDbCommand, "MenuId", DbType.Int32, MenuId);
                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtRetornaValor");
                    if (ds.Tables[0].Rows.Count > 0)
                        RetornaValor = ds.Tables[0].Rows[0][0].ToString();
                    else
                        RetornaValor = "";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return RetornaValor;
        }


       public DataTable ListarUsuarios()
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaUsuario");
                objDataBase.AddInParameter(objDbCommand, "idSistema", DbType.Int64, 1);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtListarUsuarios");
                    return ds.Tables["dtListarUsuarios"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

         public string InsertarArea(string AreaNombre, string Usuario , int AreaId)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertArea");
               objDataBase.AddInParameter(objDbCommand, "AreaNombre", DbType.AnsiString, AreaNombre);
               objDataBase.AddInParameter(objDbCommand, "AreaUsuarioIngreso", DbType.AnsiString, Usuario);
               objDataBase.AddInParameter(objDbCommand, "AreaId", DbType.Int32, AreaId);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }


       public string InsertarAreaSuperior(string AreaSuperiorNombre, string Usuario, int AreaSuperiorId)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertAreaSuperior");
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorNombre", DbType.AnsiString, AreaSuperiorNombre);
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorUsuarioIngreso", DbType.AnsiString, Usuario);
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorId", DbType.Int32, AreaSuperiorId);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }


       public DataTable ListarArea()
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarArea");

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarArea");
                   return ds.Tables["dtListarArea"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarAreaDeAreaSuperior(int AreaSuperiorId)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarAreaDeAreaSuperior");
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorId", DbType.Int32, AreaSuperiorId);

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarAreaDeAreaSuperior");
                   return ds.Tables["dtListarAreaDeAreaSuperior"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarAreaSuperior()
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarAreaSuperior");

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarAreaSuperior");
                   return ds.Tables["dtListarAreaSuperior"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public string EliminarArea(int AreaId, string Usuario)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_EliminaArea");
               objDataBase.AddInParameter(objDbCommand, "AreaId", DbType.AnsiString, AreaId);
               objDataBase.AddInParameter(objDbCommand, "usuario", DbType.AnsiString, Usuario);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public string EliminarAreaSuperior(int AreaIdSuperior, string Usuario)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_EliminaAreaSuperior");
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorId", DbType.AnsiString, AreaIdSuperior);
               objDataBase.AddInParameter(objDbCommand, "usuario", DbType.AnsiString, Usuario);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarUsuariosArea(int IdArea)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaUsuarioArea");
               objDataBase.AddInParameter(objDbCommand, "idarea", DbType.Int64, IdArea);

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarUsuariosArea");
                   return ds.Tables["dtListarUsuariosArea"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarWorkflowArea(int IdWArea)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaWorkFlowArea");
               objDataBase.AddInParameter(objDbCommand, "idArea", DbType.Int64, IdWArea);

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarWorkflowArea");
                   return ds.Tables["dtListarWorkflowArea"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarRol()
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaRol");
               objDataBase.AddInParameter(objDbCommand, "idSistema", DbType.Int32, 1);

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarRol");
                   return ds.Tables["dtListarRol"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarPermisos()
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaPermisoMenu");
 

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarPermisos");
                   return ds.Tables["dtListarPermisos"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public DataTable ListarPermisosRol(int IdROl)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_listaPermisoMenuRol");
               objDataBase.AddInParameter(objDbCommand, "idRol", DbType.Int32, IdROl);


               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtListarPermisos");
                   return ds.Tables["dtListarPermisos"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }


       public string InsertarRolPermiso(int IdRol, int IdPermiso)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_insertaRolPermiso");
               objDataBase.AddInParameter(objDbCommand, "IdRol", DbType.Int32, IdRol);
               objDataBase.AddInParameter(objDbCommand, "IdPermiso", DbType.Int32, IdPermiso); 

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public string InsertAreaDeAreaSuperior(int IdAreaSuperior, int IdArea)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_InsertAreaDeAreaSuperior");
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorId", DbType.Int32, IdAreaSuperior);
               objDataBase.AddInParameter(objDbCommand, "AreaId", DbType.Int32, IdArea);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public string EliminaRolPermiso(int IdRol, int IdPermiso)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_EliminaRolPermiso");
               objDataBase.AddInParameter(objDbCommand, "IdRol", DbType.Int32, IdRol);
               objDataBase.AddInParameter(objDbCommand, "IdPermiso", DbType.Int32, IdPermiso);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public string EliminarAreaDeAreaSuperior(int IdAreaSuperior, int IdArea)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.usp_EliminarAreaDeAreaSuperior");
               objDataBase.AddInParameter(objDbCommand, "AreaSuperiorId", DbType.Int32, IdAreaSuperior);
               objDataBase.AddInParameter(objDbCommand, "AreaId", DbType.Int32, IdArea);

               objDataBase.ExecuteNonQuery(objDbCommand);
               return "";
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public int VerificaSupervisor(int IdUsuario)
       {

           int RetornaValor;
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_VerificaSupervisor");
               objDataBase.AddInParameter(objDbCommand, "idusuario", DbType.Int32, IdUsuario);

               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtRetornaValor");
                   if (ds.Tables[0].Rows.Count > 0)
                       RetornaValor = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                   else
                       RetornaValor = 0;
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
           return RetornaValor;
       }

       public DataTable VerHistorialAsignador(int Id)
       {
           try
           {
               Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_VerHistorialAsignador");
               objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, Id);


               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtVerHistorialAsignador");
                   return ds.Tables["dtVerHistorialAsignador"];
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
       }

       public string BuscarNombreAsesor(int Contrato)
       {
           string RetornaValor;
           try
           {

               Database objDataBase = DatabaseFactory.CreateDatabase("b2b_Usuarios");
               DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_BuscaAsesor");
               objDataBase.AddInParameter(objDbCommand, "contrato", DbType.Int64, Contrato);
               using (DataSet ds = new DataSet())
               {
                   objDataBase.LoadDataSet(objDbCommand, ds, "dtRetornaValor");
                   if (ds.Tables[0].Rows.Count > 0)
                       RetornaValor = ds.Tables[0].Rows[0][0].ToString();
                   else
                       RetornaValor = "";
               }
           }
           catch (Exception ex)
           {
               throw new Exception(ex.Message);
           }
           return RetornaValor;
       }
        
        #endregion
    }




}
