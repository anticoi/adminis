﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TGENESIS.FHC.SQLServer
{
    public class ContratoDAO
    {

        #region Patron Singleton

        private static ContratoDAO instancia = null;

        public static ContratoDAO GetInstancia()
        {
            if (instancia == null)
            {
                instancia = new ContratoDAO();
            }
            return instancia;
        }
        #endregion

        #region Contructores
        public ContratoDAO()
        {

        }
        #endregion

        #region Metodos publicos

        public DataTable contratossinrendicion(int fechainicio, int fechatermino)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_VerContratosSinRecibo");
                //objDataBase.AddInParameter(objDbCommand, "WorkFlowId", DbType.Int32, Id);


                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtcontratossinrendicion");
                    return ds.Tables["dtcontratossinrendicion"];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

    }
}
