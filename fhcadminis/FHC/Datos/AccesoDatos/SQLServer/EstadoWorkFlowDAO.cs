﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TGENESIS.FHC.EntidadesNegocio;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace TGENESIS.FHC.AccesoDatos
{
    public class EstadoWorkFlowDAO
    {


        #region [Variables Publicas]
        List<string> listaEsquema = new List<string>();
        #endregion

        #region Constructores
        public EstadoWorkFlowDAO()
        {
        }
        #endregion

        #region Metodos Privados

        private List<string> LlenarEsquema(IDataReader dr)
        {
            DataTable schemaTable;
            schemaTable = dr.GetSchemaTable();
            listaEsquema = new List<string>();
            const int columnaNombre = 0;
            //Para cada campo de la tabla...
            foreach (DataRow myField in schemaTable.Rows)
            {
                //Agrega nombre del campo a la lista generica.
                listaEsquema.Add(myField[columnaNombre].ToString());
            }
            return listaEsquema;

        }

        private EstadoWorkFlow Crear(IDataReader dr)
        {
            EstadoWorkFlow vObj = new EstadoWorkFlow();
            try
            {
                listaEsquema.Sort();
                if (listaEsquema.BinarySearch("EstadoWorkFlowId", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowId"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowId = (int)dr["EstadoWorkFlowId"];
                    }
                }

                if (listaEsquema.BinarySearch("EstadoWorkFlowDescripcion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowDescripcion"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowDescripcion = (string)dr["EstadoWorkFlowDescripcion"];
                    }
                }

                if (listaEsquema.BinarySearch("EstadoWorkFlowEstado", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowEstado"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowEstado = (bool)dr["EstadoWorkFlowEstado"];
                    }
                }


                if (listaEsquema.BinarySearch("EstadoWorkFlowFechaIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowFechaIngreso"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowFechaIngreso = (DateTime)dr["EstadoWorkFlowFechaIngreso"];
                    }
                }


                if (listaEsquema.BinarySearch("EstadoWorkFlowUsuarioIngreso", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowUsuarioIngreso"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowUsuarioIngreso = (string)dr["EstadoWorkFlowUsuarioIngreso"];
                    }
                }

                if (listaEsquema.BinarySearch("EstadoWorkFlowFechaModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowFechaModificacion"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowFechaModificacion = (DateTime)dr["EstadoWorkFlowFechaModificacion"];
                    }
                }


                if (listaEsquema.BinarySearch("EstadoWorkFlowUsuarioModificacion", StringComparer.OrdinalIgnoreCase) > -1)
                {
                    if (!(dr["EstadoWorkFlowUsuarioModificacion"] is System.DBNull))
                    {
                        vObj.EstadoWorkFlowUsuarioModificacion = (string)dr["EstadoWorkFlowUsuarioModificacion"];
                    }
                }



                return vObj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<EstadoWorkFlow> CrearDTOs(IDataReader dr)
        {
            List<EstadoWorkFlow> listaDTO = new List<EstadoWorkFlow>();
            if (dr != null)
            {
                listaEsquema = LlenarEsquema(dr);// Validadores.LlenarEsquema(dr);

                while (dr.Read())
                {
                    listaDTO.Add(Crear(dr));
                }
                dr.Close();
            }
            return listaDTO;
        }

        #endregion

        #region Metodos Publicos

        public List<EstadoWorkFlow> Listar()
        {
            List<EstadoWorkFlow> lista = new List<EstadoWorkFlow>();
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_ListarEstadoWorkFlow");
                using (IDataReader objDataReader = objDataBase.ExecuteReader(objDbCommand))
                {
                    lista = CrearDTOs(objDataReader);
                }
                return lista;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Insertar(EstadoWorkFlow dto)
        {
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_InsertaEstadoWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowDescripcion", DbType.AnsiString, dto.EstadoWorkFlowDescripcion);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowEstado", DbType.Boolean, dto.EstadoWorkFlowEstado );
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowFechaIngreso", DbType.DateTime, DateTime.Now);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowUsuarioIngreso", DbType.AnsiString, dto.EstadoWorkFlowUsuarioIngreso);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowFechaModificacion", DbType.DateTime, DateTime.Now);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowUsuarioModificacion", DbType.AnsiString, dto.EstadoWorkFlowUsuarioModificacion);

                objDataBase.ExecuteNonQuery(objDbCommand);
                return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int Eliminar(EstadoWorkFlow dto)
        {
             int RetornaValor;
            try
            {
                Database objDataBase = DatabaseFactory.CreateDatabase("BDAdminis");
                DbCommand objDbCommand = objDataBase.GetStoredProcCommand("dbo.Usp_EliminarEstadoWorkFlow");
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowUsuarioModificacion", DbType.AnsiString, dto.EstadoWorkFlowUsuarioModificacion);
                objDataBase.AddInParameter(objDbCommand, "EstadoWorkFlowId", DbType.Int32, dto.EstadoWorkFlowId);

                using (DataSet ds = new DataSet())
                {
                    objDataBase.LoadDataSet(objDbCommand, ds, "dtEliminar");
                    if (ds.Tables[0].Rows.Count > 0)
                        RetornaValor = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                    else
                        RetornaValor = 0;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return RetornaValor;
        }        
        

        
        #endregion


    }
}
