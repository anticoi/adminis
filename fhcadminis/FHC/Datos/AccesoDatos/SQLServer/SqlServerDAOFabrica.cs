﻿using System;
using System.Collections.Generic;
using System.Text;
using TGENESIS.FHC.SQLServer;
using TGENESIS.FHC.AccesoDatos;
 
 

namespace TGENESIS.FHC.AccesoDatos
{
    public class SqlServerDAOFabrica : DAOFabrica
    {
        #region Patron Singleton

        private static SqlServerDAOFabrica instancia = null;

        public static SqlServerDAOFabrica GetInstancia()
        {
            if (instancia == null)
            {
                instancia = new SqlServerDAOFabrica();
            }
            return instancia;
        }

        public override ConexionDAO CrearConexionDAO()
        {
            return new ConexionDAO();
        }


        public override MenuDAO CrearMenuDAO()
        {
            return new MenuDAO();
        }


        public override TipoWorkFlowDAO CrearTipoWorkFlowDAO()
        {
            return new TipoWorkFlowDAO();
        }


        public override EstadoWorkFlowDAO CrearEstadoWorkFlowDAO()
        {
            return new EstadoWorkFlowDAO();
        }

        
        public override WorkFlowDAO CrearWorkFlowDAO()
        {
            return new WorkFlowDAO();
        }

        public override WorkFlowAdjDAO CrearWorkFlowAdjDAO()
        {
            return new WorkFlowAdjDAO();
        }


        public override ParametrosDAO CrearParametrosDAO()
        {
            return new ParametrosDAO();
        }

        public override ContratoDAO CrearContratoDAO()
        {
            return new ContratoDAO();
        }

        public override ContingenciaDAO CrearContingenciaDAO()
        {
            return new ContingenciaDAO();
        }


        #endregion
    }
}
